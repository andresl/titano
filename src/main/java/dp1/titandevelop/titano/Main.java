package dp1.titandevelop.titano;
import dp1.titandevelop.titano.persistent.*;
//import dp1.titandevelop.titano.persistent.
import dp1.titandevelop.titano.view.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;

public class Main {
	public static void main(String[] args) {
            try {
               // UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            } catch(Exception e) {
                System.out.println("Error setting Motif LAF: " + e);
            }
            
            LoginT login = new LoginT();
            login.setVisible(true);
            

	}
}
