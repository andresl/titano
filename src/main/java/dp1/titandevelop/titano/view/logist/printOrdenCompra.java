/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.logist;
import dp1.titandevelop.titano.view.ventas.*;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dp1.titandevelop.titano.persistent.DetalleCotizacion;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.ParametroService;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heli
 */
public class printOrdenCompra {
    private static Font fontBold = new Font(Font.FontFamily.COURIER, 12, Font.BOLD);
    private static Font fontNormal = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL);
    private String nomArch;
    private Float subtotal;
     public printOrdenCompra(List<DetalleCotizacion> dDv,String Proveedor,String fechaEntrega,String fechaRegistro, String nombreArchivo) throws Exception{
		
                
                this.nomArch = nombreArchivo;
		Document document = new Document();
               this.subtotal=(float)0;
		PdfWriter.getInstance(document, 
			new FileOutputStream(nombreArchivo));
		document.open();
		
		PdfPTable table = new PdfPTable(4); // Code 1
                
                document.add(getHeader("ORDEN DE COMPRA     "));
                
                document.add(getInformation(" "));
                
                Image image1 = Image.getInstance(getClass().getResource("/img/GOOD_COOKIE.jpg"));
                 

                PdfPTable table2 = new PdfPTable(3); // Code 1
                
                PdfPTable table3 = new PdfPTable(1); // Code 1
                PdfPTable table4 = new PdfPTable(1); // Code 1
                PdfPTable table5 = new PdfPTable(1); // Code 1
                PdfPCell cell001y = 
			new PdfPCell(image1);
                        cell001y.setBorder(0);
                table4.addCell(cell001y);
                PdfPCell cell001yy = 
			new PdfPCell();
                        cell001y.setBorder(1);
                table5.addCell(cell001yy);
                
                
                
                Chunk ch=new Chunk();
                ch.setBackground(BaseColor.GRAY);
               ch.getFont().setColor(BaseColor.WHITE);
               
                table3.getChunks().add(ch);
             
                
                
                
                
                Chunk c1 = cabeceraTable("Proveedor");
                Chunk c2 = cabeceraTable("Fecha de Registro");
                Chunk c3 = cabeceraTable("Fecha de Entrega");
                Chunk c4 = cabeceraTable("Cantidad");
                Chunk c5 = cabeceraTable("Producto");
                Chunk c6 = cabeceraTable("Precio Unitario");
                Chunk c7 = cabeceraTable("Precio Total");
                Chunk c11 = cabeceraTable("Total");
                
                
                Chunk c12;
                 
                 c12 = cabeceraTable("ORDEN DE COMPRA");
        
                
                 Paragraph p1 = new Paragraph(c12);
               p1.setAlignment(Element.ALIGN_CENTER);
                
                
                PdfPCell cell001x = 
			new PdfPCell(p1);
                        cell001x.setBorder(0);
                       
                table3.addCell(cell001x);
                
                 
                PdfPCell cell001 = 
			new PdfPCell(new Paragraph(c1));
                        cell001.setBorder(0);
                table2.addCell(cell001);
                
                PdfPCell cell002 = 
			new PdfPCell(new Paragraph(dataTable(""+Proveedor)));
                        cell002.setBorder(0);
                table2.addCell(cell002);  
                 
                
               table5.addCell(table3); 
                PdfPCell cell001xy = 
			new PdfPCell(table5);
                        cell001xy.setBorder(0);
                        
                        cell001xy.setRowspan(2);
                        
                table2.addCell(cell001xy);
                
                
                
                PdfPCell cell005 = 
			new PdfPCell(new Paragraph(c3));
                        cell005.setBorder(0);
                table2.addCell(cell005);
                
                PdfPCell cell006 = 
			new PdfPCell(new Paragraph(dataTable(""+fechaEntrega)));
                        cell006.setBorder(0);
                table2.addCell(cell006); 
                

                
                
                
                
		// Code 2
                PdfPCell cell007 = 
			new PdfPCell(new Paragraph(c4));
		table.addCell(cell007);
                
                PdfPCell cell008 = 
			new PdfPCell(new Paragraph(c5));
		table.addCell(cell008);
                
                PdfPCell cell009 = 
			new PdfPCell(new Paragraph(c6));
		table.addCell(cell009);
                
                PdfPCell cell010 = 
			new PdfPCell(new Paragraph(c7));
		table.addCell(cell010);
		
              
		
		for(int i=0;i<dDv.size();i++){
                table.addCell(celda(String.valueOf(dDv.get(i).getCantidad())));
		table.addCell(celda(dDv.get(i).getToRequerimientoXProducto().getToProducto().getDescripcion()));
		
		table.addCell(celda(String.valueOf(dDv.get(i).getToRequerimientoXProducto().getToProducto().getCosto())));
                table.addCell(celda("S/."+String.valueOf(dDv.get(i).getToRequerimientoXProducto().getToProducto().getCosto()*dDv.get(i).getCantidad())));
                subtotal+=dDv.get(i).getToRequerimientoXProducto().getToProducto().getCosto()*dDv.get(i).getCantidad();
                }
                
                
                PdfPCell cell4 = 
			new PdfPCell(new Paragraph(""));
		cell4.setColspan(2);
                cell4.setBorder(0);
		table.addCell(cell4);
                
                PdfPCell cell04 = 
			new PdfPCell(new Paragraph(c11));
		cell04.setColspan(1);
                cell04.setBorder(0);
                table.addCell(cell04);
                
                
                
                cell4.setBorder(1);
                table.addCell(celda("S/."+String.valueOf(subtotal)));
                
                document.add(table4);
                document.add(table2);
                document.add(getInformation(" "));
		document.add(table);		
                
		document.close();
                //imprimirFactura();
	}
     
     private Chunk cabeceraTable(String cadena) {
        Chunk c = new Chunk(cadena);
        c.setFont(fontBold);
        c.getFont().setSize(8);
        c.getFont().setFamily("Arial");
        return c;
    }
     
     private Chunk cabeceraTable15(String cadena) {
        Chunk c = new Chunk(cadena);
        c.setFont(fontBold);
        c.getFont().setSize(15);
        c.getFont().setFamily("Arial");
        return c;
    }
     private Chunk cabeceraTable13(String cadena) {
        Chunk c = new Chunk(cadena);
        c.setFont(fontBold);
        c.getFont().setSize(13);
        c.getFont().setFamily("Arial");
        return c;
    }
     
     private Chunk dataTable(String cadena) {
        Chunk c = new Chunk(cadena);
        c.getFont().setSize(8);
        c.getFont().setFamily("Arial");
        return c;
    }

    private PdfPCell celda(String cadena) {

        PdfPCell cell = new PdfPCell(new Paragraph(dataTable(cadena)));
        return cell;
    }
     
     private Paragraph getHeader(String header) {
    	Paragraph paragraph = new Paragraph();
  		Chunk chunk = new Chunk();
                
		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append( header + getCurrentDateTime() + "\n");
  		chunk.setFont(fontBold);
  		paragraph.add(chunk);
  		return paragraph;
     }
     private Paragraph getInformation(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append(informacion);
  		chunk.setFont(fontNormal);
                
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformation2(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_LEFT);
  		chunk.append(informacion);
                chunk.setFont(fontBold);
  		//chunk.setFont(fontNormal);
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformationFooter(String informacion) {
     	Paragraph paragraph = new Paragraph();
     	Chunk chunk = new Chunk();
   		paragraph.setAlignment(Element.ALIGN_CENTER);
   		chunk.append(informacion);
   		chunk.setFont(new Font(Font.FontFamily.COURIER, 8, Font.NORMAL));
   		paragraph.add(chunk);
    		return paragraph;
       }
      private float getConvertCmsToPoints(float cm) {
     	return cm * 28.4527559067f;
     }
     
   public void imprimirFactura(){
     
     Desktop d=Desktop.getDesktop();
     try {
      if(Desktop.isDesktopSupported()){
          //d.open(new File("/tmp/factura2.pdf"));
          d.open(new File(this.nomArch));
      // d.print(new File("factura2.pdf"));
      }
     } catch (IOException e) {
     // TODO Auto-generated catch block
    e.printStackTrace();
        }
   }
      
      
     private String getCurrentDateTime() {
     	Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yy '-' hh:mm");
     	return ft.format(dNow);
    }
	public static void main(String[] args,List<DetalleCotizacion> dDv,String proveedor,String fechaEntrega,String fechaRegistro, String nombreArchivo) {	
		try{
			printOrdenCompra pdfTable = new printOrdenCompra(dDv,proveedor,fechaEntrega,fechaRegistro,nombreArchivo);
		}catch(Exception e){
			System.out.println(e);
		}
	}
        
}
