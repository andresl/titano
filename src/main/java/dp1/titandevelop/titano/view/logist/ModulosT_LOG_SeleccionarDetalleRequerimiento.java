/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.logist;

import dp1.titandevelop.titano.persistent.Proveedor;
import dp1.titandevelop.titano.persistent.Receta;
import dp1.titandevelop.titano.persistent.Requerimiento;
import dp1.titandevelop.titano.persistent.RequerimientoXProducto;
import dp1.titandevelop.titano.service.ProductoProveedorService;
import dp1.titandevelop.titano.service.ProveedorService;
import dp1.titandevelop.titano.service.RequerimientoService;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author andres
 */
public class ModulosT_LOG_SeleccionarDetalleRequerimiento extends javax.swing.JInternalFrame {

    /**
     * Creates new form ModulosT_LOG_SeleccionarDetalleRequerimiento
     */
    private ArrayList<RequerimientoXProducto> listaDetalleRequerimiento;
    private ModulosT_LOG_NuevaCotizacion caller;
    private int proveedor;
    private AbstractTableModel model = new AbstractTableModel() {

        public int getRowCount() {
            return listaDetalleRequerimiento.size();

        }

        public int getColumnCount() {
            return 4;
        }

        public Object getValueAt(int fi, int co) {
            switch ( co ) {
                case 0: return listaDetalleRequerimiento.get(fi).getToProducto().getDescripcion();
                case 1: return listaDetalleRequerimiento.get(fi).getCantidad();
                case 2: return listaDetalleRequerimiento.get(fi).getToProducto().getUm();
                case 3: return listaDetalleRequerimiento.get(fi).isSeleccionado();
            }
            return null;
        }
        
        @Override
        public void setValueAt(Object val, int fi, int co) {
            switch ( co ) {
                case 3: listaDetalleRequerimiento.get(fi).setSeleccionado( (Boolean) val ); 
            }
            model.fireTableDataChanged();
         
        }
        
        @Override
        public String getColumnName(int co) {
            String[] t = {"Producto", "Cantidad", "Unidades","Seleccionar"};
            return t[co];
        }
        
        @Override 
        public Class<?> getColumnClass(int co) {
            Class<?>[] t = {String.class, String.class ,String.class, Boolean.class};
            return t[co];
        }
        
        @Override
        public boolean isCellEditable(int fi, int co) {
            switch ( co ) {
                case 3:
                    return true;
            }
            return false;
        }
    };
    public ModulosT_LOG_SeleccionarDetalleRequerimiento(ModulosT_LOG_NuevaCotizacion caller, int proveedor) {
        initComponents();
        this.listaDetalleRequerimiento = new ArrayList<RequerimientoXProducto>();
        this.caller = caller;
        this.jTable1.setModel(model);
        this.proveedor=proveedor;
        this.btnOk.setEnabled(false);
        
        RequerimientoService rs = new RequerimientoService();

        Requerimiento r = rs.buscarActivo();
        this.txtReq.setText(r.getIdrequerimiento()+"");
        ArrayList<RequerimientoXProducto> listadr = new ArrayList<RequerimientoXProducto>( );
        listadr.addAll(r.getRequerimientoXProductoArray());
        this.listaDetalleRequerimiento = new ArrayList<RequerimientoXProducto>( );
        // validar que el producto lo venda ese proveedor
        ProductoProveedorService pps = new ProductoProveedorService();
        ProveedorService ps = new ProveedorService();

        for (int i=0; i<listadr.size(); i++) {

            if ( pps.esProvistoPorProveedor(ps.buscarPorId(null, this.proveedor), listadr.get(i).getToProducto()) ){
                this.listaDetalleRequerimiento.add(listadr.get(i));;
            }

        }
        this.model.fireTableDataChanged();
    }

    public void setIdReq(int id) {
            this.txtReq.setText(""+id);
            RequerimientoService rs = new RequerimientoService();
            Requerimiento r = rs.buscarPorId(null,id);
            ArrayList<RequerimientoXProducto> listadr = new ArrayList<RequerimientoXProducto>( );
            listadr.addAll(r.getRequerimientoXProductoArray());
            this.listaDetalleRequerimiento = new ArrayList<RequerimientoXProducto>( );
            // validar que el producto lo venda ese proveedor
            ProductoProveedorService pps = new ProductoProveedorService();
            ProveedorService ps = new ProveedorService();

            for (int i=0; i<listadr.size(); i++) {

                if ( pps.esProvistoPorProveedor(ps.buscarPorId(null, this.proveedor), listadr.get(i).getToProducto()) ){
                    this.listaDetalleRequerimiento.add(listadr.get(i));;
                }

            }
            //this.listaDetalleRequerimiento.addAll( r.getRequerimientoXProductoArray() );
            this.model.fireTableDataChanged();
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtReq = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnOk = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        setTitle("Seleccion de Requerimiento");

        jLabel1.setText("Requerimiento:");

        txtReq.setEditable(false);
        txtReq.setEnabled(false);
        txtReq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReqActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        btnOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/asignar.png"))); // NOI18N
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(265, 265, 265)
                            .addComponent(btnOk)
                            .addGap(1, 1, 1)
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(8, 8, 8)
                            .addComponent(jLabel1)
                            .addGap(4, 4, 4)
                            .addComponent(txtReq, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtReq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk)
                    .addComponent(jButton4))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtReqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReqActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReqActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        // TODO add your handling code here:
        for (int i=0; i<this.listaDetalleRequerimiento.size(); i++)
            if ( this.listaDetalleRequerimiento.get(i).isSeleccionado() )
                this.caller.agregarALista( this.listaDetalleRequerimiento.get(i) );
        this.caller.refrescarTabla();
        this.dispose();
    }//GEN-LAST:event_btnOkActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        if(evt.getClickCount()==1){
            this.btnOk.setEnabled(true);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOk;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtReq;
    // End of variables declaration//GEN-END:variables
}
