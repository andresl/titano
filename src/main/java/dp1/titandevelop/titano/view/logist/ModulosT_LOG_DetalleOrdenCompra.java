/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.logist;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.Cotizacion;
import dp1.titandevelop.titano.persistent.DetalleCotizacion;
import dp1.titandevelop.titano.persistent.OrdenCompra;
import dp1.titandevelop.titano.service.OrdenCompraService;
import dp1.titandevelop.titano.service.ValidacionService;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chrestean
 */
public class ModulosT_LOG_DetalleOrdenCompra extends javax.swing.JInternalFrame {

    /**
     * Creates new form TitanoInternalSegADMUserNew
     */
    private AbstractTableModel model = new AbstractTableModel() {
        @Override
        public int getRowCount() {
            return detalleOrdenCompra.size();

        }

        @Override
        public int getColumnCount() {
            return 5;
        }

        @Override
        public boolean isCellEditable(int fi, int co) {
            switch (co) {
                case 2:
                    return true;
            }
            return false;
        }

        @Override
        public Object getValueAt(int fi, int co) {
            switch (co) {
                case 0:
                    return detalleOrdenCompra.get(fi).getToRequerimientoXProducto().getToProducto().getDescripcion();
                case 1:
                    return new DecimalFormat("#.##").format(detalleOrdenCompra.get(fi).getToRequerimientoXProducto().getCantidad());
                case 2:

                    return detalleOrdenCompra.get(fi).getToRequerimientoXProducto().getToProducto().getUm();
                   
                case 3:

                    return "S./"+new DecimalFormat("#.##").format(detalleOrdenCompra.get(fi).getToRequerimientoXProducto().getToProducto().getCosto());
                case 4:
       
                    return "S./"+new DecimalFormat("#.##").format(detalleOrdenCompra.get(fi).getToRequerimientoXProducto().getCantidad() * detalleOrdenCompra.get(fi).getToRequerimientoXProducto().getToProducto().getCosto());


            }
            return null;
        }

        @Override
        public void setValueAt(Object value, int fi, int co) {
            switch (co) {
                case 3:
                    detalleOrdenCompra.get(fi).getToRequerimientoXProducto().getToProducto().setCosto(Float.parseFloat((String) value));
                    float suma = 0;
                    for (int i = 0; i < detalleOrdenCompra.size(); i++) {
                        suma += detalleOrdenCompra.get(i).getCantidad() * detalleOrdenCompra.get(i).getToRequerimientoXProducto().getToProducto().getCosto();
                    }

                    txtMontoTot.setText("" + new DecimalFormat("#.##").format(suma).toString());
                    this.fireTableDataChanged();

            }
        }

        @Override
        public String getColumnName(int col) {

            String[] titles = {"Producto", "Cantidad","Unidad", "Precio Unitario", "Precio"};

            return titles[col];
        }
    };
    private ModulosT_LOG_AdministrarOrdenCompra caller = null;

    ModulosT_LOG_DetalleOrdenCompra(ModulosT_LOG_AdministrarOrdenCompra caller, OrdenCompra c) {
        this.initComponents();
        this.caller = caller;
        OrdenCompraService service=new OrdenCompraService();
        
        this.detalleOrdenCompra = service.detalleOrdenCompra(c);
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
                String fecha2=formatoDeFecha.format(c.getFechaentrega());
                String fecha3=formatoDeFecha.format(c.getFecharegistro());
        this.txtFechaEntrega.setText("" + fecha3);
        this.txtFechaRecepcion.setText(""+fecha2);
        this.txtProveedor.setText(""+c.getCotizacionArray().get(0).getToProveedor().getRazonsocial());
        //this.datePkFechaRecepcion.setDate(this.detalleOrdenCompra.getFecharecepcion());
        this.model.fireTableDataChanged();
        this.jTable1.setModel(model);

        float suma = 0;
        for (int i = 0; i < detalleOrdenCompra.size(); i++) {
            suma += detalleOrdenCompra.get(i).getCantidad() * detalleOrdenCompra.get(i).getToRequerimientoXProducto().getToProducto().getCosto();
        }

        txtMontoTot.setText("" + new DecimalFormat("#.##").format(suma).toString());
        this.model.fireTableDataChanged();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtFechaEntrega = new javax.swing.JTextField();
        txtFechaRecepcion = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtProveedor = new javax.swing.JTextField();
        btnSalir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtMontoTot = new javax.swing.JTextField();
        btnImprimir = new javax.swing.JButton();

        setTitle("Detalle Orden de Compra");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos De Proveedor"));
        jPanel1.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        jLabel3.setText("Fecha de Entrega");

        jLabel5.setText("Fecha de Recepción");

        txtFechaEntrega.setEnabled(false);

        txtFechaRecepcion.setEnabled(false);
        txtFechaRecepcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFechaRecepcionActionPerformed(evt);
            }
        });

        jLabel4.setText("Proveedor");

        txtProveedor.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtFechaEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(txtFechaRecepcion, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtFechaEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFechaRecepcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addContainerGap())
        );

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cotización de Productos", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID Producto", "Producto", "Cantidad", "Monto"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("Monto Total");

        txtMontoTot.setEnabled(false);
        txtMontoTot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMontoTotActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMontoTot, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 554, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtMontoTot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/imprimir.png"))); // NOI18N
        btnImprimir.setToolTipText("Imprimir");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnImprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSalir)
                    .addComponent(btnImprimir))
                .addContainerGap())
        );

        jPanel1.getAccessibleContext().setAccessibleName("Datos de la Orden de Compra");

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private List<DetalleCotizacion> detalleOrdenCompra = null;

    private void txtMontoTotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMontoTotActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMontoTotActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        //this.caller.show();
        ModulosT_LOG_AdministrarOrdenCompra ventana= new ModulosT_LOG_AdministrarOrdenCompra();
        this.getParent().add(ventana);
        
         Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ventana.getSize();
        ventana.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height)/4 );
        
        ventana.show();
        
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtFechaRecepcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFechaRecepcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaRecepcionActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        try {
            ValidacionService validacionService = new ValidacionService();
            if(!detalleOrdenCompra.isEmpty() && !validacionService.esVacio(txtProveedor) && !validacionService.esVacio(txtFechaEntrega)
                    && !validacionService.esVacio(txtFechaEntrega)){
            String nombreArchivo = "/tmp/orden-compra.pdf";
            FileDialog fd = new FileDialog(new JFrame(), "Guardar", FileDialog.SAVE);
            fd.setDirectory(".");
            fd.setFile("*.pdf");
            fd.setVisible(true);
            nombreArchivo = fd.getDirectory() + fd.getFile();
            if(fd.getFile()!=null){
                this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                printOrdenCompra print=new printOrdenCompra(detalleOrdenCompra,this.txtProveedor.getText(),this.txtFechaEntrega.getText(),this.txtFechaRecepcion.getText(),nombreArchivo);
                this.setCursor(Cursor.getDefaultCursor());
                JOptionPane.showInternalMessageDialog(this.getRootPane(),"El Reporte a sido generado satisfactoriamente","Mensaje",JOptionPane.INFORMATION_MESSAGE);
            }
            
            }else{
            JOptionPane.showInternalMessageDialog(this.getRootPane(), "Faltan datos para generar el reporte","Aviso",JOptionPane.WARNING_MESSAGE);
        }
            
        } catch (Exception ex) {
            Logger.getLogger(ModulosT_LOG_DetalleOrdenCompra.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btnImprimirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtFechaEntrega;
    private javax.swing.JTextField txtFechaRecepcion;
    private javax.swing.JTextField txtMontoTot;
    private javax.swing.JTextField txtProveedor;
    // End of variables declaration//GEN-END:variables
}
