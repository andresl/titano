/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.logist;

import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.ProductoProveedor;
import dp1.titandevelop.titano.persistent.Proveedor;
import dp1.titandevelop.titano.service.ProductoProveedorService;
import dp1.titandevelop.titano.service.ProductoService;
import dp1.titandevelop.titano.service.ProveedorService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import dp1.titandevelop.titano.view.MensajeError;
import dp1.titandevelop.titano.view.admin.ModulosT_ADM;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author chrestean
 */
public class ModulosT_LOG_APEdit extends javax.swing.JInternalFrame {

    private  List<Producto> productos ;
    private  List<Integer> idproductos;
    private  List<Float> precios ;
    private  boolean editable = false;
    private int idProv;
    
    public void addProducto(int e){
        ProductoService prod = new ProductoService();
        Producto p = prod.buscarPorId(e);
        this.productos.add(p);
        this.idproductos.add(e);
    }
    public void addPrecios(float e){
        this.precios.add(e);
    }
    public void RefreshTable() {
        AbstractTableModel aux = (AbstractTableModel)tblprod.getModel();
        aux.fireTableDataChanged();
    }
    public void habilitaBotones(){
        this.btnEliminar.setEnabled(true);
    }
    private void inhabilitaBotones(){
        this.btnEliminar.setEnabled(false);
    }
    
    private void rellenaProd_Cap(List<ProductoProveedor> lista){
        int i = lista.size();
        for(int j=0;j<i;j++){
            Producto p = lista.get(j).getToProducto();
            this.addProducto(p.getIdproducto());
            this.addPrecios(lista.get(j).getPrecio());
        }
    }
    
    
   //Modelo de tabla de proveedor
   private AbstractTableModel modeloTabla = new AbstractTableModel()  {  
        
        public int getRowCount() {
            return precios.size(); // cuantos registros (filas)
        }      
        
        public int getColumnCount() {
            return 2; // cuantas columnas
        }  
        
        String [] titles = {"Productos", "Precio"};
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }        
         
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res=null;
            Producto prod = productos.get(rowIndex);
            Float prec = precios.get(rowIndex);
            switch(columnIndex){
                case 0: res = prod.getDescripcion(); break;
                case 1: res = prec; break;
            }
            return res;
        }  
        
        @Override
        public void setValueAt(Object value, int row, int col) {
            if ( col == 1 ) {           
                precios.set(row, Float.parseFloat(""+value));
            }
            this.fireTableDataChanged();
        };
        
    };
    
    public ModulosT_LOG_APEdit() {
        initComponents();  
    }
    public ModulosT_LOG_APEdit(Proveedor prov) {
        initComponents();
        idProv = prov.getIdproveedor();
        this.productos=new ArrayList<Producto>();
        this.idproductos = new ArrayList<Integer>();
        this.precios = new ArrayList<Float>();
       
        ProductoProveedorService servA = new ProductoProveedorService();
        List<ProductoProveedor> listaProds = servA.buscarPorIdProdProv(prov.getIdproveedor());
       
        
        this.txtCContacto.setText(prov.getEmailcontacto());
//        this.txtCalificacion.setText(prov.getCalificaciontiempoentrega()+"");
        this.txtCelContacto.setText(prov.getCelularcontacto());
        this.txtContacto.setText(prov.getContacto());
        this.txtDNIContacto.setText(prov.getDnicontacto());
        this.txtDireccion.setText(prov.getDireccionempresa());
        this.txtMarca.setText(prov.getMarca());
        this.txtRazonSocial.setText(prov.getRazonsocial());
        this.txtRUC.setText(prov.getRuc());
        this.txtmail.setText(prov.getEmailempresa());
        this.txttelefono.setText(prov.getTelefonocontacto());
        
        rellenaProd_Cap(listaProds);
        //Setear la tabla
        this.tblprod.setModel(modeloTabla);
        this.inhabilitaBotones(); 
        ValidacionService val = new ValidacionService();
        val.SAlfanumerico(txtRazonSocial);
        val.SNumeros(txtRUC);
        val.Longitud(txtRUC,10);
        val.SAlfanumerico(txtDireccion);
        val.SSoloLetra(txtMarca);
        val.SAlfanumerico(txtContacto);
        val.SNumeros(txtDNIContacto);
        val.Longitud(txtDNIContacto, 8);
        val.SNumeros(txtCelContacto);
        val.Longitud(txtCelContacto, 9);
        val.SNumeros(txttelefono);
        val.Longitud(txttelefono, 7);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        nombre = new javax.swing.JLabel();
        apPaterno = new javax.swing.JLabel();
        apMaterno = new javax.swing.JLabel();
        fechaNac = new javax.swing.JLabel();
        telefono = new javax.swing.JLabel();
        correo = new javax.swing.JLabel();
        txtRazonSocial = new javax.swing.JTextField();
        txtRUC = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        txtContacto = new javax.swing.JTextField();
        txtCContacto = new javax.swing.JTextField();
        txtDNIContacto = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtmail = new javax.swing.JTextField();
        txtMarca = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtCelContacto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        spcalf = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblprod = new javax.swing.JTable();
        btnEliminar = new javax.swing.JButton();
        btnAsignar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setTitle("Editar Proveedor");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos De Proveedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11))); // NOI18N
        jPanel1.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        nombre.setText("Razon Social");

        apPaterno.setText("RUC");

        apMaterno.setText("Direccion");

        fechaNac.setText("Contacto");

        telefono.setText("Correo Contacto");

        correo.setText("DNI Contacto");

        jLabel1.setText("Correo Empresa");

        txtmail.setText(" ");

        txtMarca.setText(" ");

        jLabel2.setText("Marca");

        jLabel3.setText("Celular Contacto");

        txtCelContacto.setText(" ");

        jLabel6.setText("Telefono Cont.");

        txttelefono.setText(" ");

        spcalf.setModel(new javax.swing.SpinnerNumberModel(0, 0, 5, 1));

        jLabel7.setText("Calificación");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(nombre)
                        .addGap(18, 18, 18)
                        .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(apMaterno)
                                .addComponent(apPaterno))
                            .addGap(35, 35, 35)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtRUC, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel6)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(82, 82, 82)
                            .addComponent(spcalf, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel7)
                            .addGap(71, 71, 71))))
                .addGap(45, 45, 45)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(correo)
                            .addComponent(jLabel1)
                            .addComponent(telefono)
                            .addComponent(fechaNac))
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCelContacto, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtContacto, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDNIContacto, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCContacto, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtmail))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nombre))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtRUC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(apPaterno))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(apMaterno)
                            .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(fechaNac)
                            .addComponent(txtContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(telefono))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(correo)
                            .addComponent(txtDNIContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtCelContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(spcalf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/grabar.png"))); // NOI18N
        btnGuardar.setToolTipText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Insumos a Asignar", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        tblprod.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Producto", "Unidades", "Precio"
            }
        ));
        tblprod.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblprodMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblprod);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminar.setToolTipText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnAsignar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/agregar.png"))); // NOI18N
        btnAsignar.setToolTipText("Asignar Producto");
        btnAsignar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsignarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAsignar)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnAsignar)
                        .addGap(18, 18, 18)
                        .addComponent(btnEliminar))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalir)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardar)
                    .addComponent(btnSalir))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        ModulosT_LOG.menuProv.RefreshTable();
        this.dispose();
        
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
            //ID 52 Editar Proveedor
        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (52 == (Integer) LoginT.idspermisosxperfil.get(i)) {
               ValidacionService val = new ValidacionService();
                Mensajes mensajes = new Mensajes();
                ProveedorService pp = new ProveedorService();
                     if ((val.esVacio(this.txtCContacto)) || (val.esVacio(this.txtCelContacto)) || (val.esVacio(this.txtContacto)) || (val.esVacio(this.txtmail))|| (val.esVacio(this.txttelefono))
                            || (val.esVacio(this.txtDNIContacto))|| (val.esVacio(this.txtDireccion)) || (val.esVacio(this.txtMarca)) || (val.esVacio(this.txtRUC)) || (val.esVacio(this.txtRazonSocial))) {
                        JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getCompletarCampos(), "Aviso", JOptionPane.WARNING_MESSAGE);

                    } else {
                       if (val.validaCorreo(this.txtmail) || val.validaCorreo(this.txtCContacto)){
                         if (val.LongitudExacta(txtDNIContacto.getText(), 8) && val.LongitudExacta(txtCelContacto.getText(), 9)&& val.LongitudExacta(txttelefono.getText(), 7)&& val.LongitudExacta(txtRUC.getText(), 10)){
                                try {

                                
                                String razon, ruc, direcc, marca,  contacto,  dni,  celcont, mailemp, mailcont, telf;
                                int calif;
                                razon= this.txtRazonSocial.getText();
                                ruc=this.txtRUC.getText();
                                direcc = this.txtDireccion.getText();
                                marca = this.txtMarca.getText();
                                contacto = this.txtContacto.getText();
                                mailcont = this.txtCContacto.getText();
                                dni= this.txtDNIContacto.getText();
                                celcont= this.txtCelContacto.getText();
                                mailemp = this.txtmail.getText();
                                telf = this.txttelefono.getText();
                                calif = (Integer)this.spcalf.getValue();
                                pp.editar(idProv, razon,ruc, direcc, calif, marca, contacto, dni, celcont, mailemp, mailcont, telf, idproductos, precios);


                                JOptionPane.showInternalMessageDialog(this.getRootPane(),mensajes.getEditado(),"Mensaje",JOptionPane.INFORMATION_MESSAGE);
                                ModulosT_LOG.menuProv.RefreshTable();
                                this.dispose();


                               } catch(Exception e) {
                                      JOptionPane.showInternalMessageDialog(this.getRootPane(),mensajes.getError(),"Error",JOptionPane.ERROR_MESSAGE);
                                      
                               }
                                }else {
                                    if (val.LongitudExacta(this.txtCelContacto.getText(), 9)){
                                     }else{
                                        JOptionPane.showInternalMessageDialog(this.getRootPane(), "El celular debe tener 9 digitos ", "Aviso", JOptionPane.WARNING_MESSAGE);
                                    }
                                    if (val.LongitudExacta(this.txttelefono.getText(), 7)){
                                    }else{
                                        JOptionPane.showInternalMessageDialog(this.getRootPane(), "El telefono debe tener 7 digitos ", "Aviso", JOptionPane.WARNING_MESSAGE);
                                    }
                                    if (val.LongitudExacta(this.txtDNIContacto.getText(), 8)){
                                    }else{
                                        JOptionPane.showInternalMessageDialog(this.getRootPane(), "El DNI debe tener 8 digitos ", "Aviso", JOptionPane.WARNING_MESSAGE);
                                    }
                                    if (val.LongitudExacta(this.txtRUC.getText(), 10)){
                                    }else{
                                        JOptionPane.showInternalMessageDialog(this.getRootPane(), "El RUC debe tener 10 digitos ", "Aviso", JOptionPane.WARNING_MESSAGE);
                                    }
                            }
                      
                     }else{
                            JOptionPane.showInternalMessageDialog(this.getRootPane(), "Correo con formato erroneo ", "Aviso", JOptionPane.WARNING_MESSAGE);
                       }   
                     }   
                     break;
                 }
             }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
      
        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnAsignarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsignarActionPerformed
        Frame f = JOptionPane.getFrameForComponent(this);
        ModulosT_LOG_APDetalle dialog = new ModulosT_LOG_APDetalle(f,true,this.idproductos);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnAsignarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        this.productos.remove(this.tblprod.getSelectedRow());
        this.precios.remove(this.tblprod.getSelectedRow());
        this.idproductos.remove(this.tblprod.getSelectedRow());
        this.RefreshTable();
        this.inhabilitaBotones();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void tblprodMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblprodMouseClicked
        if(evt.getClickCount()==1){
            this.btnEliminar.setEnabled(true);
        }
    }//GEN-LAST:event_tblprodMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel apMaterno;
    private javax.swing.JLabel apPaterno;
    private javax.swing.JButton btnAsignar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel correo;
    private javax.swing.JLabel fechaNac;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nombre;
    private javax.swing.JSpinner spcalf;
    private javax.swing.JTable tblprod;
    private javax.swing.JLabel telefono;
    private javax.swing.JTextField txtCContacto;
    private javax.swing.JTextField txtCelContacto;
    private javax.swing.JTextField txtContacto;
    private javax.swing.JTextField txtDNIContacto;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtRUC;
    private javax.swing.JTextField txtRazonSocial;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txttelefono;
    // End of variables declaration//GEN-END:variables
}
