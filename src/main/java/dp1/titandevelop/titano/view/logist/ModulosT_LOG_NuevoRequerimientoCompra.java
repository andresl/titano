/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.logist;

import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.OrdenProduccion;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.Receta;
import dp1.titandevelop.titano.persistent.Requerimiento;
import dp1.titandevelop.titano.persistent.RequerimientoXProducto;
import dp1.titandevelop.titano.service.AlmacenService;
import dp1.titandevelop.titano.service.OrdenProduccionService;
import dp1.titandevelop.titano.service.ProductoService;
import dp1.titandevelop.titano.service.RecetaService;
import dp1.titandevelop.titano.service.RequerimientoService;
import dp1.titandevelop.titano.service.ValidacionService;
import java.awt.Cursor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;

/**
 *
 * @author chrestean
 */
public class ModulosT_LOG_NuevoRequerimientoCompra extends javax.swing.JInternalFrame {

    /**
     * Creates new form TitanoInternalSegADMUserNew
     */
    private ArrayList<RequerimientoXProducto> listaDetalleRequerimiento;
    private Boolean editable;
    private int idRequerimiento;
    private AdministrarRequerimientoCompra caller;
    private AbstractTableModel model = new AbstractTableModel() {
        public int getRowCount() {
            return listaDetalleRequerimiento.size();
        }

        public int getColumnCount() {
            return 6;
        }

        public Object getValueAt(int fi, int co) {
            AlmacenService as = new AlmacenService();

            switch (co) {
                case 0:
                    return listaDetalleRequerimiento.get(fi).getToProducto().getIdproducto();
                case 1:
                    return listaDetalleRequerimiento.get(fi).getToProducto().getDescripcion();
                case 2:
                    return listaDetalleRequerimiento.get(fi).getCantidad();
                case 3:
                    return as.capacidad_StockTotalProducto(listaDetalleRequerimiento.get(fi).getToProducto().getIdproducto()).get(1);

                case 4:
                    Float cantDisp = as.capacidad_StockTotalProducto(listaDetalleRequerimiento.get(fi).getToProducto().getIdproducto()).get(1) + 0f;
                    if ( cantDisp >= listaDetalleRequerimiento.get(fi).getCantidad().floatValue()) {
                        return 0;
                    } else {
                        return listaDetalleRequerimiento.get(fi).getCantidad() - cantDisp;
                    }
                case 5: // id producto
                    ProductoService ps = new ProductoService();
                    return ps.buscarPorId(listaDetalleRequerimiento.get(fi).getToProducto().getIdproducto()).getUm();

            }
            return null;
        }

        @Override
        public String getColumnName(int co) {
            String[] titulos = {"Id Producto", "Producto", "Cantidad ", "Unidades"};
            return titulos[co];
        }
    };

    public ModulosT_LOG_NuevoRequerimientoCompra(AdministrarRequerimientoCompra caller) {
        // nuevo

        initComponents();
        ValidacionService vs = new ValidacionService();
        vs.SNumeros(txtOrdenProduccion);
        //vs.SNumeros(txtEstado);
        this.caller = caller;
        listaDetalleRequerimiento = new ArrayList<RequerimientoXProducto>();
        this.jTable1.setModel(this.model);

        editable = false;
        OrdenProduccionService ops = new OrdenProduccionService();
        OrdenProduccion op = ops.buscarActivo();
//        if ( op == null ) {
//            JOptionPane.showMessageDialog(this, "En este momento no existe Orden de Producción alguna que se encuentre activa.", "Orden de Producción", JOptionPane.ERROR_MESSAGE);
//            this.btnSalirActionPerformed(null);
//            this.dispose();
//        } 
//        RequerimientoService rs  = new RequerimientoService();
//        if ( rs.buscarPorOrdenProduccion(op) != null ) {
//            JOptionPane.showMessageDialog(this, "La orden de produccion activa ya tiene un requerimiento asignado.", "Orden de Producción", JOptionPane.ERROR_MESSAGE);
//            this.btnSalirActionPerformed(null);
//            this.dispose();
//        }
        this.txtOrdenProduccion.setText("" + op.getIdordenproduccion());
        //this.datePkFechaRegistro.setDate(op.getFecha());
        //this.datePkFechaRegistro.setEnabled(false);
        this.txtFechaRegistroOrdenProduccion.setText("" + new SimpleDateFormat("dd-MM-yyyy").format(op.getFecha()));
        this.txtFechaRegistroOrdenProduccion.setEnabled(false);
        this.btnEscoger.setVisible(false);
        this.btnBuscarProducto.setVisible(false);
        this.btnBuscarProductoActionPerformed(null);
    }

    public ModulosT_LOG_NuevoRequerimientoCompra(AdministrarRequerimientoCompra caller, int req) {
        // para la edicion
        initComponents();
        ValidacionService vs = new ValidacionService();
        vs.SNumeros(txtOrdenProduccion);
        //vs.SNumeros(txtEstado);
        listaDetalleRequerimiento = new ArrayList<RequerimientoXProducto>();
        this.jTable1.setModel(this.model);
        this.caller = caller;
        this.idRequerimiento = req;
        editable = true;
        this.setTitle("Editar requerimiento");
        RequerimientoService rs = new RequerimientoService();
        Requerimiento r = rs.buscarPorId(null, idRequerimiento);

        //this.txtEstado.setText("" + r.getEstado());
        this.datePkFecha.setDate(r.getFecha());
        this.txtOrdenProduccion.setText("" + r.getToOrdenProduccion().getIdordenproduccion());
        this.txtFechaRegistroOrdenProduccion.setText("" + new SimpleDateFormat("dd-MM-yyyy").format(r.getToOrdenProduccion().getFecha()));
        //this.jPanel2.setVisible(false);
        //this.btnBuscarProducto.setVisible(false);
        this.btnEscoger.setVisible(false);
        this.txtOrdenProduccion.setEnabled(false);
        this.btnBuscarProductoActionPerformed(null);
        this.btnBuscarProducto.setVisible(false);


    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblOProduccion = new javax.swing.JLabel();
        lblFechaRegistro = new javax.swing.JLabel();
        txtOrdenProduccion = new javax.swing.JTextField();
        btnBuscarProducto = new javax.swing.JButton();
        btnEscoger = new javax.swing.JButton();
        datePkFecha = new org.jdesktop.swingx.JXDatePicker();
        txtFechaRegistroOrdenProduccion = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setTitle("Nuevo Requerimiento de Compra");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11))); // NOI18N
        jPanel1.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        lblOProduccion.setText("ID Orden Produccion");

        lblFechaRegistro.setText("Fecha");

        txtOrdenProduccion.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N
        txtOrdenProduccion.setEnabled(false);
        txtOrdenProduccion.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                txtOrdenProduccionPropertyChange(evt);
            }
        });

        btnBuscarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        btnBuscarProducto.setToolTipText("Buscar Producto");
        btnBuscarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProductoActionPerformed(evt);
            }
        });

        btnEscoger.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar2.png"))); // NOI18N
        btnEscoger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEscogerActionPerformed(evt);
            }
        });

        txtFechaRegistroOrdenProduccion.setText("jTextField1");
        txtFechaRegistroOrdenProduccion.setEnabled(false);

        jLabel1.setText("Registrada: ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblOProduccion)
                        .addGap(21, 21, 21)
                        .addComponent(txtOrdenProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblFechaRegistro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(datePkFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtFechaRegistroOrdenProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(btnEscoger, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBuscarProducto)
                .addGap(8, 8, 8))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(btnBuscarProducto)
                .addGap(21, 21, 21))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEscoger, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblOProduccion)
                        .addComponent(txtOrdenProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtFechaRegistroOrdenProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(datePkFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFechaRegistro))
                .addContainerGap())
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/grabar.png"))); // NOI18N
        btnGuardar.setToolTipText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Búsqueda de Productos"));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID Producto", "Producto", "Cantidad"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 557, Short.MAX_VALUE)
                        .addComponent(btnGuardar)
                        .addGap(1, 1, 1)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardar)
                    .addComponent(btnSalir))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

//        ModulosT_LOG_AdministrarRequerimientoCompra admReqCompras = new ModulosT_LOG_AdministrarRequerimientoCompra();
//        this.getParent().add(admReqCompras);
//
//        Dimension desktopSize = this.getParent().getSize();
//        Dimension jInternalFrameSize = admReqCompras.getSize();
//        admReqCompras.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
//                (desktopSize.height - jInternalFrameSize.height) / 4);

        this.caller.show();
        this.dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void insertarIngrediente(int idIngrediente, float cantidad, String unidad) {
        // sobre this.listaDetalleRequerimiento
        for (int i = 0; i < this.listaDetalleRequerimiento.size(); i++) {
            if (this.listaDetalleRequerimiento.get(i).getToProducto().getIdproducto() == idIngrediente) {
                this.listaDetalleRequerimiento.get(i).setCantidad(
                        cantidad + this.listaDetalleRequerimiento.get(i).getCantidad());
                return;
            }
        }

        RequerimientoXProducto detalleRequerimiento = new RequerimientoXProducto();

        ProductoService ps = new ProductoService();

        Producto p = ps.buscarPorId(idIngrediente);
        detalleRequerimiento.setToProducto(p);
        // la relacion con el objeto requerimiento se hace despues
        detalleRequerimiento.setCantidad(cantidad);
        detalleRequerimiento.setEstado(1);

        this.listaDetalleRequerimiento.add(detalleRequerimiento);
        // ok... que hago con la unidad... hay forma de setearlo??

    }
    private void btnBuscarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProductoActionPerformed
        // TODO add your handling code here:
        ObjectContext context = DataContext.createDataContext();
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        if (this.txtOrdenProduccion.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Especifique primero una orden de produccion.", "Campos Faltantes", JOptionPane.ERROR_MESSAGE);
            return;
        }
        this.busco = true;
        this.listaDetalleRequerimiento = new ArrayList<RequerimientoXProducto>();

        OrdenProduccionService ops = new OrdenProduccionService();
        OrdenProduccion o = ops.buscarPorId(Integer.parseInt(this.txtOrdenProduccion.getText()));
        //this.contenidosOrdenProd=new ArrayList<DetalleOrdenProduccion>();
        //this.contenidosOrdenProd.addAll(o.getDetalleOrdenProduccionArray());
        // iteramos para obtener los productos 
        for (int i = 0; i < o.getDetalleOrdenProduccionArray().size(); i++) {
            // para este producto buscamos sus componentes
            System.out.println(">>>>>" + o.getDetalleOrdenProduccionArray().get(i).getToProducto().getDescripcion());
            RecetaService receta_srv = new RecetaService();
            Expression exp = ExpressionFactory.greaterExp(Receta.ESTADO_PROPERTY, 0);
            ArrayList<Receta> listaIngredientes = new ArrayList<Receta>();

            listaIngredientes.addAll(exp.filterObjects(o.getDetalleOrdenProduccionArray().get(i).getToProducto().getRecetaArray()));
            //listaIngredientes.addAll(receta_srv.BuscarPorProducto(context, o.getDetalleOrdenProduccionArray().get(i).getToProducto()));
            // receta_srv.devuelveIngredientes(o.getDetalleOrdenProduccionArray().get(i).getToProducto().getIdproducto());

            for (int j = 0; j < listaIngredientes.size(); j++) {

                if (listaIngredientes.get(j).getToProducto1().getToTipoProducto().getIdtipoproducto() == 1) {
                    //System.out.println(">>>>>> I:" + listaIngredientes.get(j).getToProducto().getDescripcion());

                    // es insumo
                    // digamos que estamos en los ingredientes de las galletas rellenas 
                    // y esto son, dos tapas de galleta y crema
                    // en el caso de la crema entraria aqui...

                    AlmacenService aps = new AlmacenService();
                    float merma = 0, existencias = 0;
                    /*
                     if (aps.capacidad_StockTotalProducto(listaIngredientes.get(j).getToProducto().getIdproducto()) != null && !aps.capacidad_StockTotalProducto(listaIngredientes.get(j).getToProducto().getIdproducto()).isEmpty()) {
                     int ex = aps.capacidad_StockTotalProducto(listaIngredientes.get(j).getToProducto().getIdproducto()).get(1);
                     existencias = ex; //al.getStock();

                     if (existencias - o.getDetalleOrdenProduccionArray().get(i).getCantidad() > 0) {
                     // al.setStock(Math.round(existencias - o.getDetalleOrdenProduccionArray().get(i).getCantidad()));
                     // aps.
                     }

                     //al.getObjectContext().commitChanges();
                     }

                     // fue la vida...
                     */


                    if (o.getDetalleOrdenProduccionArray().get(i).getCantidad() + merma - existencias > 0) {
                        this.insertarIngrediente(
                                listaIngredientes.get(j).getToProducto1().getIdproducto(),
                                (o.getDetalleOrdenProduccionArray().get(i).getCantidad() + merma - existencias) * listaIngredientes.get(j).getCantidad(),
                                listaIngredientes.get(j).getUm());

                    }
                } else {
                    // ...en el caso de la tapa de galleta entraria aqui
                    // es producto intermedio
                    //System.out.println(">>>>>> D:" + listaIngredientes.get(j).getToProducto().getDescripcion());

                    listaIngredientes.addAll(exp.filterObjects(listaIngredientes.get(j).getToProducto1().getRecetaArray()));
                    //listaIngredientes.addAll(receta_srv.BuscarPorProducto(context, listaIngredientes.get(j).getToProducto()));

                    // saca sus ingredientes y los coloca para que sean separados en ingredientes basicos
                }
            }
        }


        this.datePkFecha.setDate(o.getFecha());
        //this.txtEstado.setText("" + o.getEstado());
        this.model.fireTableChanged(null);
        this.setCursor(Cursor.getDefaultCursor());


    }//GEN-LAST:event_btnBuscarProductoActionPerformed

    private void btnEscogerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEscogerActionPerformed
        // TODO add your handling code here:
        ModulosT_LOG_BuscarOrdenProduccion boc = new ModulosT_LOG_BuscarOrdenProduccion(this);
        this.getParent().add(boc);
        boc.show();
    }//GEN-LAST:event_btnEscogerActionPerformed

    private void txtOrdenProduccionPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_txtOrdenProduccionPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOrdenProduccionPropertyChange
    private Boolean busco = false;
    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // La accion GUARDAR toma todo lo que hay en la lista de arriba y lo guarda
        Mensajes m = new Mensajes();
        if (!busco) {
            return;
        }
       try{     
        if (!this.editable) {
            RequerimientoService rs = new RequerimientoService();
            rs.insertarRequerimiento(Integer.parseInt(this.txtOrdenProduccion.getText()), 1, listaDetalleRequerimiento, this.datePkFecha.getDate());
            JOptionPane.showInternalMessageDialog(this.getRootPane(), m.getRegistrado(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);

        } else {
            // nuevo 
            RequerimientoService rs = new RequerimientoService();
            Requerimiento r = rs.buscarPorId(null, idRequerimiento);
            //r.setEstado(Integer.parseInt(this.txtEstado.getText()));
            r.setEstado(1);
            r.setFecha(this.datePkFecha.getDate());
            r.setFecharegistro(new Date());
            r.getObjectContext().commitChanges();
            
        }
        this.model.fireTableChanged(null);
        this.caller.show();
        this.dispose();
        } catch(Exception e) {
              JOptionPane.showInternalMessageDialog(this.getRootPane(),m.getError(),"Error",JOptionPane.ERROR_MESSAGE);

       }


    }//GEN-LAST:event_btnGuardarActionPerformed

    public void setIdOrdenProduccion(String t) {
        this.txtOrdenProduccion.setText(t);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarProducto;
    private javax.swing.JButton btnEscoger;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSalir;
    private org.jdesktop.swingx.JXDatePicker datePkFecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lblFechaRegistro;
    private javax.swing.JLabel lblOProduccion;
    private javax.swing.JTextField txtFechaRegistroOrdenProduccion;
    private javax.swing.JTextField txtOrdenProduccion;
    // End of variables declaration//GEN-END:variables
}
