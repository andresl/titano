/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroDemanda;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.service.DemandaService;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author heli
 */
public class ModulosT_VEN_PProduccion extends javax.swing.JInternalFrame {

    public ModulosT_VEN_PProduccionNuevo nuevo = null;
    public ModulosT_VEN_PProduccionNuevo editar = null;
    public ModulosT_VEN_PProduccionNuevo detalle = null;
    
    public int estadoNuevo = 0;    
    //Lista usada de tipos de movimiento
    public List<Demanda> demandas = null;
    //metodo para refrescar la tabla
    
    public FiltroDemanda busqueda=null;
    
    public void habilitaBotones(){
        this.btnEditarA.setEnabled(true);
        this.btnEliminarA.setEnabled(true);
        this.btnDetalle.setEnabled(true);
    }
    private void inhabilitaBotones(){
        this.btnEditarA.setEnabled(false);
        this.btnEliminarA.setEnabled(false);
        this.btnDetalle.setEnabled(false);
    }
    
    
    public FiltroDemanda filtro(){
        FiltroDemanda f=new FiltroDemanda();
        f.setFechaIni(this.txtFechaInicio.getDate());
        f.setFechaFin(this.txtFechaFin.getDate());
        f.setDescripcion(this.txtDescripcion.getText());
        f.setEstado(this.cboxEstado.getSelectedIndex());
        return f;
    }
    
    public void llenarFiltro(FiltroDemanda f){
        this.txtFechaInicio.setDate(f.getFechaIni());
        this.txtFechaFin.setDate(f.getFechaFin());
        this.txtDescripcion.setText(f.getDescripcion());
        this.cboxEstado.setSelectedIndex(f.getEstado());
        
    }
    public void  buscar(FiltroDemanda f){
        DemandaService serv = new DemandaService();
        demandas = serv.buscarFiltrado(f);
        
        this.actualizarDemandaList();
    }
    
    Thread actualizardemanda = null;
    private void actualizarDemandaList(){
        if( actualizardemanda!=null && actualizardemanda.isAlive() ){
            actualizardemanda.interrupt();
        }
        actualizardemanda = new Thread( new Runnable() {
            @Override
            public void run() {
                RefreshTable();
                ModeloTabla modelo= new ModeloTabla();
                tableResultados.setModel(modelo);
                
            }
        });
        actualizardemanda.start();
    }

    
    
    
    public void RefreshTable() {
        
        AbstractTableModel aux = (AbstractTableModel)tableResultados.getModel();
        aux.fireTableDataChanged();
    }
    
    class ModeloTabla extends AbstractTableModel {  
        //Atributos de los datos de la tabla
        private String [] titles = {"Fecha", "Descripción", "Estado"};
        //private List<DataTabla> datos;
        //private Object [][]datos;
        
        public int getRowCount() {
            return demandas.size();//tipos.size(); // cuantos registros (filas)
        }      
        
        public int getColumnCount() {
            return 3; // cuantas columnas
        }          
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }                
       
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res=null;
            Demanda p = demandas.get(rowIndex);
            //boolean b = datos.get(rowIndex).seleccionado;//tipos.get(rowIndex);
            switch(columnIndex){
                case 0: res = p.getFecha(); break;
                case 1: res = p.getDescripcion(); break;
                case 2: if(p.getEstado()==1) {res="Activo"; }else {res="Inactivo";} break;
                //case 2: res = b;
            }
            return res;
        } 
        //Ultima celda editable para selección
        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res=false;
            switch (col) {
                case 0:break;
                case 1:break;
               // case 2:res=true;
            }
            return res;
        }
        //para obtener las clases de cada celda y asi mostrar estas correctamente
        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
//        @Override
//        public void setValueAt(Object value, int row, int col) {
//            datos.get(row).seleccionado = (Boolean)value;
//        }
    }

    public ModulosT_VEN_PProduccion() {
        initComponents(); 
        ValidacionService validacionService = new ValidacionService();
        validacionService.NoEscribe(txtFechaInicio);
        validacionService.NoEscribe(txtFechaFin);
        validacionService.SAlfanumerico(txtDescripcion);
        Estado state = new Estado();
        this.inhabilitaBotones();
        this.cboxEstado.addItem(state.getEstadosActivoInactivo().get(0));
        this.cboxEstado.addItem(state.getEstadosActivoInactivo().get(1)); 
        
        this.cboxEstado.setSelectedIndex(1);
    }
    
    public ModulosT_VEN_PProduccion(FiltroDemanda f) {
        initComponents(); 
        Estado state = new Estado();
        this.cboxEstado.addItem(state.getEstadosActivoInactivo().get(0));
        this.cboxEstado.addItem(state.getEstadosActivoInactivo().get(1)); 
     this.cboxEstado.setSelectedIndex(1);
        this.inhabilitaBotones();
        if(f!=null){
            this.busqueda=f;
        this.llenarFiltro(f);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtDescripcion = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtFechaInicio = new org.jdesktop.swingx.JXDatePicker();
        txtFechaFin = new org.jdesktop.swingx.JXDatePicker();
        cboxEstado = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResultados = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnDetalle = new javax.swing.JButton();
        btnEditarA = new javax.swing.JButton();
        btnEliminarA = new javax.swing.JButton();

        setResizable(true);
        setTitle("Modulo Ventas - Administrar Demanda");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtro de Búsqueda"));

        jLabel5.setText("Fecha Inicio");

        jLabel6.setText("Estado");

        jLabel7.setText("Descripción");

        jLabel8.setText("Fecha Fin");

        cboxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));
        cboxEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxEstadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6)))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(txtFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jLabel8)
                        .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados de Búsqueda"));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Descripcion", "Estado"
            }
        ));
        tableResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableResultados);
        tableResultados.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        btnLimpiar.setToolTipText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/detalle.png"))); // NOI18N
        btnDetalle.setToolTipText("Ver Detalle");
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        btnEditarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/editar.png"))); // NOI18N
        btnEditarA.setToolTipText("Editar");
        btnEditarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarAActionPerformed(evt);
            }
        });

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(btnNuevo)
                                .addGap(0, 0, 0)
                                .addComponent(btnLimpiar)
                                .addGap(0, 0, 0)
                                .addComponent(btnSalir))
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDetalle)
                        .addGap(0, 0, 0)
                        .addComponent(btnEditarA)
                        .addGap(0, 0, 0)
                        .addComponent(btnEliminarA)))
                .addGap(8, 8, 8))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscar)
                    .addComponent(btnNuevo)
                    .addComponent(btnLimpiar)
                    .addComponent(btnSalir))
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnEditarA)
                        .addComponent(btnEliminarA))
                    .addComponent(btnDetalle, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
          //ID 36 Plan de produccion (Demanda)

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (36 == (Integer) LoginT.idspermisosxperfil.get(i)) {
            if (estadoNuevo==0){
            estadoNuevo=1;
          
            DocumentoVentaService service= new DocumentoVentaService();
            if(service.listProduct().size()==0){
            JOptionPane.showMessageDialog(this, "No existen productos!!! Por favor agregue productos antes de crear una demanda",
                    "Mensaje",
                    JOptionPane.WARNING_MESSAGE);
            }else{
            
            nuevo = new ModulosT_VEN_PProduccionNuevo();            
            this.getParent().add(nuevo);
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = nuevo.getSize();
            nuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                    (desktopSize.height - jInternalFrameSize.height)/4 );
            nuevo.show();
            this.dispose();
            break;
            }
            
        }
            }
            }
         if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
        
        
        
        
        
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        
        FiltroDemanda filtro=null; 
        filtro=this.filtro();
        this.buscar(filtro);
        
        
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        txtFechaInicio.setDate(null);
        txtFechaFin.setDate(null);
        txtDescripcion.setText("");
        cboxEstado.setSelectedIndex(0);
        this.demandas.clear();
        this.actualizarDemandaList();
        
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void tableResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultadosMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tableResultadosMouseClicked

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed
        Demanda dem;
        dem = demandas.get(this.tableResultados.getSelectedRow());
        //Instancio la ventana
        FiltroDemanda busqueda=null;
        busqueda=this.filtro();
        detalle = new ModulosT_VEN_PProduccionNuevo(dem,busqueda,2);
        this.getParent().add(detalle);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize =detalle.getSize();
        detalle.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        detalle.show();

        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
        this.dispose();
    }//GEN-LAST:event_btnDetalleActionPerformed

    private void btnEditarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarAActionPerformed
            //ID 37 Editar Plan de produccion (Demanda)

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (37 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                Demanda dem;
        dem = demandas.get(this.tableResultados.getSelectedRow());
        FiltroDemanda busqueda=null;
        busqueda=this.filtro();
        //Instancio la ventana
        editar = new ModulosT_VEN_PProduccionNuevo(dem,busqueda,1);
        this.getParent().add(editar);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = editar.getSize();
        editar.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        editar.show();

        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
        this.dispose();
        break;
            }
            }
        
         if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
        
    }//GEN-LAST:event_btnEditarAActionPerformed

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed
       
          //ID 37 Editar Plan de produccion (Demanda)

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (37 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(),"¿Está seguro que desea eliminar el registro?","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

        if (JOptionPane.OK_OPTION == confirmar)
        {//Obtengo el objeto seleccionado
            DemandaService serv = new DemandaService();
            Demanda d = demandas.get(this.tableResultados.getSelectedRow());
            this.demandas.remove(this.tableResultados.getSelectedRow());

            serv.eliminar(d.getIddemanda());
        }

        this.RefreshTable();
        this.inhabilitaBotones();
        break;
            }
        }
        
         if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void cboxEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxEstadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboxEstadoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton btnEditarA;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboxEstado;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable tableResultados;
    private javax.swing.JTextField txtDescripcion;
    private org.jdesktop.swingx.JXDatePicker txtFechaFin;
    private org.jdesktop.swingx.JXDatePicker txtFechaInicio;
    // End of variables declaration//GEN-END:variables
}
