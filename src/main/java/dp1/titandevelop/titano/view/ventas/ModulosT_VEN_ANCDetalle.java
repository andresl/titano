/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroNC;
import dp1.titandevelop.titano.bean.FiltroSV;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.DetalleNotaCredito;
import dp1.titandevelop.titano.persistent.DetalleSolicitudVenta;
import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.NotaCredito;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.SolicitudVenta;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.NotaCreditoService;
import dp1.titandevelop.titano.service.SolicitudVentaService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.BusquedaProductoCantidad;
import dp1.titandevelop.titano.view.MensajeError;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;

/**
 *
 * @author heli
 */
public class ModulosT_VEN_ANCDetalle extends javax.swing.JInternalFrame {
    
    public ModulosT_VEN_ASPDetalleNuevo nuevoDSV = null;
    public ModulosT_VEN_ASPDetalleEditar editarDSV = null;
    public int estadoNuevo = 0;    
    public int idDistribuidor;
    public int tipo;
    public int idNC;
    public int idDoc;
    private Mensajes sms = new Mensajes();
    public List<DetalleNotaCredito> productos = new ArrayList<DetalleNotaCredito>();
    
    //Lista usada de tipos de movimiento
    
    public FiltroNC busqueda=null; 
    //metodo para refrescar la tabla
        
    private void inhabilitarTodo(){
          
          this.btnGuardar.setVisible(false);
          this.btnNuevo.setVisible(false);
          this.btnEliminarA.setVisible(false);
          this.txtFecha.setEditable(false);
          this.txtDescripcion.setEditable(false);
      
    }
    public void habilitaBotones(){
        //this.btnEliminarA.setEnabled(true);
        
    }
    private void inhabilitaBotones(){
        this.btnEliminarA.setEnabled(false);
        this.btnNuevo.setVisible(false);
        
    }
     Thread actualizarproductos = null;
    private void actualizarProductosList(){
        if( actualizarproductos!=null && actualizarproductos.isAlive() ){
            actualizarproductos.interrupt();
        }
        actualizarproductos = new Thread( new Runnable() {
            @Override
            public void run() {
                RefreshTable();
                ModeloTabla modelo= new ModeloTabla();
                tableResultados.setModel(modelo);
                
            }
        });
        actualizarproductos.start();
    }
    
    public void RefreshTable() {
        
        AbstractTableModel aux = (AbstractTableModel)tableResultados.getModel();
        aux.fireTableDataChanged();
    }
    
    class ModeloTabla extends AbstractTableModel {  
        //Atributos de los datos de la tabla
        
        private String [] titles = {"Producto", "Cantidad (Unidades)"};
        //private List<DataTabla> datos;
        //private Object [][]datos;
        
        public int getRowCount() {
            return productos.size();//tipos.size(); // cuantos registros (filas)
        }      
        
        public int getColumnCount() {
            return 2; // cuantas columnas
        }          
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }                
       
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res=null;
            DetalleNotaCredito dsv = productos.get(rowIndex);
            //boolean b = datos.get(rowIndex).seleccionado;//tipos.get(rowIndex);
            switch(columnIndex){
                case 0: res = dsv.getToProducto().getDescripcion(); break;
                case 1: res = dsv.getCantidad(); break;
                //case 2: res = dsv.getCantidad()*dsv.getToProducto().getCosto(); break;
                //case 2: res = b;
            }
            return res;
        } 
        //Ultima celda editable para selección
        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res=false;
            switch (col) {
                case 0:break;
                case 1:break;
                case 2:break;
               // case 2:res=true;
            }
            return res;
        }
        //para obtener las clases de cada celda y asi mostrar estas correctamente
        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
//        @Override
//        public void setValueAt(Object value, int row, int col) {
//            datos.get(row).seleccionado = (Boolean)value;
//        }
    }
    
    public ModulosT_VEN_ANCDetalle(Integer id, String contacto, Float monto) {
        initComponents();
        ValidacionService validacionService = new ValidacionService();
        validacionService.SLetras(this.txtDescripcion);
        validacionService.NoEscribe(this.txtFecha);
        validacionService.SNumeros(this.txtMonto);
        this.tipo=0;
        this.idDoc=id;
        System.out.println("este es el id"+id);
        this.txtDistribuidor.setText(contacto);
        this.txtMonto.setText(String.valueOf(monto));
        NotaCreditoService service=new NotaCreditoService();
        productos=service.buscarDetalleNotaCredito(idDoc);
        System.out.println(productos.size());
        this.actualizarProductosList();
    }
    
    public ModulosT_VEN_ANCDetalle(NotaCredito nc, FiltroNC fnc,int tipo) {
        initComponents();
        ValidacionService validacionService = new ValidacionService();
        validacionService.SLetras(this.txtDescripcion);
        validacionService.NoEscribe(this.txtFecha);
        
        this.txtFecha.setDate(nc.getFecha());
        this.txtDistribuidor.setText(nc.getToDocumentoVenta().getContacto());
        this.txtDescripcion.setText(nc.getDescripcion());
        this.txtMonto.setText(String.valueOf(nc.getMonto()));
        this.cboxMotivo.setSelectedIndex(nc.getTipo());
        this.idDoc=nc.getIdnotacredito();
        this.busqueda=fnc;
        this.tipo=tipo;
        NotaCreditoService service=new NotaCreditoService();
        productos=service.buscarDetalleNotaCredito(idDoc);
        if(this.tipo==2){
        this.inhabilitarTodo();
        this.txtMonto.setEditable(false);
        this.cboxMotivo.setEnabled(false);
        }
        this.actualizarProductosList();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtFecha = new org.jdesktop.swingx.JXDatePicker();
        txtDescripcion = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtMonto = new javax.swing.JTextField();
        cboxMotivo = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        txtDistribuidor = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableResultados = new javax.swing.JTable();
        btnEliminarA = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();

        setBorder(null);
        setResizable(true);
        setTitle("Detalle Nota de Crédito");
        setMaximumSize(new java.awt.Dimension(500, 550));
        setMinimumSize(new java.awt.Dimension(500, 550));
        setPreferredSize(new java.awt.Dimension(500, 550));

        jPanel1.setPreferredSize(new java.awt.Dimension(476, 508));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle de Nota de Crédito"));

        jLabel5.setText("Fecha");

        jLabel7.setText("Descripción");

        txtDescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripcionActionPerformed(evt);
            }
        });

        jLabel8.setText("Monto Total (S/.)");

        txtMonto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMontoActionPerformed(evt);
            }
        });

        cboxMotivo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Anulación Factura", "Devolución" }));
        cboxMotivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxMotivoActionPerformed(evt);
            }
        });

        jLabel9.setText("Cliente/Distribuidor");

        txtDistribuidor.setEditable(false);

        jLabel6.setText("Motivo Emisión");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel6))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboxMotivo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboxMotivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/grabar.png"))); // NOI18N
        btnGuardar.setToolTipText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        btnCancelar.setToolTipText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle de Productos Devueltos"));

        jScrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Cantidad (Unidades)"
            }
        ));
        tableResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultadosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableResultados);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/agregar.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/imprimir.png"))); // NOI18N
        btnImprimir.setToolTipText("Imprimir");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnNuevo)
                        .addGap(1, 1, 1)
                        .addComponent(btnEliminarA)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnImprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(btnCancelar))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar)
                    .addComponent(btnEliminarA)
                    .addComponent(btnNuevo)
                    .addComponent(btnImprimir)))
        );

        jPanel4.getAccessibleContext().setAccessibleName("Detalle de Productos Devueltos");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        Mensajes mensajes = new Mensajes();
        ValidacionService validacionService = new ValidacionService();
        if ((validacionService.esVacio(this.txtDescripcion)) || (validacionService.esVacio(this.txtMonto)) || (validacionService.esFechaVacia(this.txtFecha))) {
            JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getCompletarCampos(), "Aviso", JOptionPane.WARNING_MESSAGE);

        } else {
        try {
            //Usamos el atributo creado para modificar el registro seleccionado
           
            ObjectContext context = DataContext.createDataContext();
            NotaCredito aux=(NotaCredito)context.newObject(NotaCredito.class);
            DocumentoVenta d=(DocumentoVenta)context.newObject(DocumentoVenta.class);
            
            d.setIddocumento(this.idDoc);
            aux.setToDocumentoVenta(d);
            aux.setDescripcion(this.txtDescripcion.getText());
            aux.setTipo(this.cboxMotivo.getSelectedIndex());
            aux.setMonto(Float.valueOf(this.txtMonto.getText()));

            aux.setFecha(this.txtFecha.getDate());
            aux.setEstado(1);
            NotaCreditoService service=new NotaCreditoService();
            if(this.tipo==0){
                //service.insertar(aux, productos);
                JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getRegistrado(),"Mensaje",JOptionPane.INFORMATION_MESSAGE);
            }
            if(this.tipo==1){
                service.editar(aux);
                JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getEditado(),"Mensaje",JOptionPane.INFORMATION_MESSAGE);
            }
            //System.out.println("pase esto");

        } catch(Exception e) {
            //e.printStackTrace();
            //System.out.println("Noooooooooooooooooooooooo");
            MensajeError dialog = new MensajeError(new javax.swing.JFrame(), true);
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        }
        
        this.dispose();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        ModulosT_VEN.menuItemNC= new ModulosT_VEN_ANC();
        this.getParent().add(ModulosT_VEN.menuItemNC);
        
         Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ModulosT_VEN.menuItemNC.getSize();
        ModulosT_VEN.menuItemNC.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height)/4 );
        
        ModulosT_VEN.menuItemNC.show();
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtMontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMontoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMontoActionPerformed

    private void cboxMotivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxMotivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboxMotivoActionPerformed

    private void tableResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultadosMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tableResultadosMouseClicked

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed
        int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(),"¿Está seguro que desea eliminar el registro?","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

        if (JOptionPane.OK_OPTION == confirmar)
        {//Obtengo el objeto seleccionado
          //  SolicitudVentaService serv = new SolicitudVentaService();
            //DetalleDocumentoVenta dist = productos.get(this.tableResultados.getSelectedRow());
            //this.productos.remove(this.tableResultados.getSelectedRow());
            //if(this.tipo==1)
            //serv.eliminarItemDetalle(dist.getIddetalledocumentoventa());
        }

        this.RefreshTable();
        this.inhabilitaBotones();
    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        /*final int i=0;
         DetalleDocumentoVenta dist = productos.get(this.tableResultados.getSelectedRow());
        final busquedaProductoCantidadEdit seleccionarProductoForm =new busquedaProductoCantidadEdit(null, true,dist.getIddetalledocumentoventa());
        seleccionarProductoForm.setLocationRelativeTo(this);
        seleccionarProductoForm.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                Estado state=new Estado();
                if(seleccionarProductoForm.idProducto!=null){
                    if(seleccionarProductoForm.idProducto>0){
                        int aumentar=1;
                        DetalleNotaCredito d=new DetalleNotaCredito();
                        d.setCantidad(seleccionarProductoForm.cantidad);
                        ObjectContext context = DataContext.createDataContext();
                        Producto p=(Producto)context.newObject(Producto.class);
                        p.setIdproducto(seleccionarProductoForm.idProducto);
                        p.setDescripcion(seleccionarProductoForm.producto);
                        p.setCosto(seleccionarProductoForm.costo);
                        d.setToProducto(p);
                        seleccionarProductoForm.idProducto=0;


                        //d.getToProducto().setDescripcion();

                        for(int i=0;i<productos.size();i++){
                            if(productos.get(i).getToProducto().getDescripcion().equals(p.getDescripcion())){
                                productos.get(i).setCantidad(d.getCantidad());
                                aumentar=0;
                            }
                        }

                        

                        actualizarProductosList();
                        //System.out.println(seleccionarProductoForm.producto);
                    }
                }
                //this;
            }
        });
        seleccionarProductoForm.setVisible(true);*/
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
         SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
                String fecha2=formatoDeFecha.format(this.txtFecha.getDate());
        
            String nombreArchivo = "/tmp/nota-credito.pdf";
            FileDialog fd = new FileDialog(new JFrame(), "Guardar", FileDialog.SAVE);
            fd.setDirectory(".");
            fd.setFile("*.pdf");
            fd.setVisible(true);
            nombreArchivo = fd.getDirectory() + fd.getFile();
        try {     
            if(fd.getFile()!=null){
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            printNotaCredito print=new printNotaCredito(this.productos,this.txtDistribuidor.getText(), fecha2, this.txtMonto.getText(),nombreArchivo);
            this.setCursor(Cursor.getDefaultCursor());
            JOptionPane.showInternalMessageDialog(this.getRootPane(),"La Nota de Credito a sido generado satisfactoriamente","Mensaje",JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex) {
            Logger.getLogger(ModulosT_VEN_ANCNuevo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnImprimirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox cboxMotivo;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    public javax.swing.JTable tableResultados;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtDistribuidor;
    private org.jdesktop.swingx.JXDatePicker txtFecha;
    private javax.swing.JTextField txtMonto;
    // End of variables declaration//GEN-END:variables
}
