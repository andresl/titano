/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.service.DistribuidorService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import java.awt.Dimension;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chrestean
 */
public class ModulosT_VEN_AD extends javax.swing.JInternalFrame {

    public ModulosT_VEN_ADNuevo nuevo = null;
    public ModulosT_VEN_ADEdit editar = null;
    public ModulosT_VEN_ADDetalle detalle = null;
    private Mensajes sms = new Mensajes();
    /**
     * Creates new form TitanoInteralSegADMUser
     */
    public ModulosT_VEN_AD() {
        initComponents();
        ValidacionService validacionService = new ValidacionService();

        validacionService.SNumeros(txtDNI);
        validacionService.SNumeros(txtRUC);
        validacionService.SAlfanumerico(txtRazonSocial);
        
        validacionService.Longitud(this.txtRazonSocial,30);
        validacionService.Longitud(this.txtRUC,11);
        validacionService.Longitud(this.txtDNI,8);
        
        
        this.cboxEstado.setSelectedIndex(1);
        this.inhabilitaBotones();
    }
    public List<Distribuidor> distribuidores = null;
    //metodo para refrescar la tabla

    public void habilitaBotones() {
        this.btnEditarA.setEnabled(true);
        this.btnEliminarA.setEnabled(true);
        this.btnDetalle.setEnabled(true);
    }

    private void inhabilitaBotones() {
        this.btnEditarA.setEnabled(false);
        this.btnEliminarA.setEnabled(false);
        this.btnDetalle.setEnabled(false);
    }

    public Distribuidor filtro() {
        Distribuidor df = new Distribuidor();
        df.setRazonsocial(this.txtRazonSocial.getText());
        df.setRuc(this.txtRUC.getText());
        df.setDni(this.txtDNI.getText());
        df.setEstado(this.cboxEstado.getSelectedIndex());
        return df;
    }

    public void llenarFiltro(Distribuidor df) {
        this.txtRazonSocial.setText(df.getRazonsocial());
        this.txtRUC.setText(df.getRuc());
        this.txtDNI.setText(df.getDni());
        this.cboxEstado.setSelectedIndex(df.getEstado());

    }

    public void buscar(Distribuidor dF) {
        DistribuidorService serv = new DistribuidorService();
        distribuidores = serv.buscarFiltrado(dF);

        this.actualizarDistribuidorList();
    }
    Thread actualizardistribuidor = null;

    private void actualizarDistribuidorList() {
        if (actualizardistribuidor != null && actualizardistribuidor.isAlive()) {
            actualizardistribuidor.interrupt();
        }
        actualizardistribuidor = new Thread(new Runnable() {
            @Override
            public void run() {
                RefreshTable();
                ModeloTabla modelo = new ModeloTabla();
                tableResultados.setModel(modelo);

            }
        });
        actualizardistribuidor.start();
    }

    public void RefreshTable() {

        AbstractTableModel aux = (AbstractTableModel) tableResultados.getModel();
        aux.fireTableDataChanged();
    }

    class ModeloTabla extends AbstractTableModel {
        //Atributos de los datos de la tabla

        private String[] titles = {"Razón Social", "RUC", "Dirección", "Contacto", "DNI", "Estado"};
        //private List<DataTabla> datos;
        //private Object [][]datos;

        public int getRowCount() {
            return distribuidores.size();
        }

        public int getColumnCount() {
            return 6; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res = null;
            Distribuidor p = distribuidores.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    res = p.getRazonsocial();
                    break;
                case 1:
                    res = p.getRuc();
                    break;
                case 2:
                    res = p.getDireccion();
                    break;
                case 3:
                    res = p.getContacto();
                    break;
                case 4:
                    res = p.getDni();
                    break;
                case 5:
                    res = Estado.devuelveCadena(p.getEstado());
                    break;

            }
            return res;
        }
        //Ultima celda editable para selección

        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res = false;
            switch (col) {
                case 0:
                    break;
                case 1:
                    break;
                // case 2:res=true;
            }
            return res;
        }
        //para obtener las clases de cada celda y asi mostrar estas correctamente

        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
//        @Override
//        public void setValueAt(Object value, int row, int col) {
//            datos.get(row).seleccionado = (Boolean)value;
//        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtRazonSocial = new javax.swing.JTextField();
        txtRUC = new javax.swing.JTextField();
        txtDNI = new javax.swing.JTextField();
        cboxEstado = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResultados = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnDetalle = new javax.swing.JButton();
        btnEditarA = new javax.swing.JButton();
        btnEliminarA = new javax.swing.JButton();
        btnCargaMasiva = new javax.swing.JButton();

        setResizable(true);
        setTitle("Administrar Distribuidor");
        setPreferredSize(new java.awt.Dimension(650, 550));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros de Búsqueda"));
        jPanel1.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        jLabel9.setText("Razon Social");

        jLabel10.setText("RUC");

        jLabel12.setText("DNI");

        cboxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Inactivo", "Activo" }));
        cboxEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxEstadoActionPerformed(evt);
            }
        });

        jLabel13.setText("Estado");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtRUC, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(txtRazonSocial))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel12))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel12)
                    .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRUC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultado de Búsqueda"));
        jPanel2.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        tableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Razon Social", "RUC", "Direccion", "Contacto", "DNI", "Estado"
            }
        ));
        tableResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableResultados);
        tableResultados.getColumnModel().getColumn(2).setResizable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        btnLimpiar.setToolTipText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/detalle.png"))); // NOI18N
        btnDetalle.setToolTipText("Ver Detalle");
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        btnEditarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/editar.png"))); // NOI18N
        btnEditarA.setToolTipText("Editar");
        btnEditarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarAActionPerformed(evt);
            }
        });

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        btnCargaMasiva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/texto.png"))); // NOI18N
        btnCargaMasiva.setActionCommand("masivo");
        btnCargaMasiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargaMasivaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDetalle)
                .addGap(0, 0, 0)
                .addComponent(btnEditarA)
                .addGap(0, 0, 0)
                .addComponent(btnEliminarA)
                .addGap(27, 27, 27))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBuscar)
                .addGap(0, 0, 0)
                .addComponent(btnNuevo)
                .addGap(0, 0, 0)
                .addComponent(btnLimpiar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCargaMasiva, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalir)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCargaMasiva, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnBuscar)
                        .addComponent(btnNuevo)
                        .addComponent(btnLimpiar)
                        .addComponent(btnSalir)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnEditarA)
                        .addComponent(btnEliminarA))
                    .addComponent(btnDetalle, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed

        //ID 32 Nuevo  Distribuidor 

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (32 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                try{
                nuevo = new ModulosT_VEN_ADNuevo();
                this.getParent().add(nuevo);
                Dimension desktopSize = this.getParent().getSize();
                Dimension jInternalFrameSize = nuevo.getSize();
                nuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                        (desktopSize.height - jInternalFrameSize.height) / 4);
                
                    nuevo.show();
                }catch(Exception e){
                    JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
                }
                this.dispose();
                break;
            }
        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed
        //Obtengo el objeto seleccionado
        Distribuidor dist, busqueda;
        busqueda = this.filtro();
        dist = distribuidores.get(this.tableResultados.getSelectedRow());
        //Instancio la ventana
        try{
        detalle = new ModulosT_VEN_ADDetalle(dist, busqueda);
        this.getParent().add(detalle);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = detalle.getSize();
        detalle.setLocation((desktopSize.width - jInternalFrameSize.width) / 2, (desktopSize.height - jInternalFrameSize.height) / 4);
       
            detalle.show();
        }catch(Exception e){
            JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
        }
        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
        this.dispose();
    }//GEN-LAST:event_btnDetalleActionPerformed

    private void btnEditarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarAActionPerformed
        //ID 33 Guardar/Eliminar Distribuidor 

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (33 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                try{
                Distribuidor dist, busqueda;
                busqueda = this.filtro();
                dist = distribuidores.get(this.tableResultados.getSelectedRow());
                //Instancio la ventana
                editar = new ModulosT_VEN_ADEdit(dist, busqueda);
                this.getParent().add(editar);

                Dimension desktopSize = this.getParent().getSize();
                Dimension jInternalFrameSize = editar.getSize();
                editar.setLocation((desktopSize.width - jInternalFrameSize.width) / 2, (desktopSize.height - jInternalFrameSize.height) / 4);
                
                    editar.show();
                }catch(Exception e){
                    JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
                }
                //Inhabilito los botones una vez realizada la acción
                this.inhabilitaBotones();
                this.dispose();
                break;
            }
        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnEditarAActionPerformed

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed

        //ID 33 Guardar/Eliminar Distribuidor 

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (33 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(), "¿Está seguro que desea eliminar el registro?", "Aviso!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (JOptionPane.OK_OPTION == confirmar) {//Obtengo el objeto seleccionado
                    DistribuidorService serv = new DistribuidorService();
                    Distribuidor dist = distribuidores.get(this.tableResultados.getSelectedRow());
                    this.distribuidores.remove(this.tableResultados.getSelectedRow());
                        try{
                        serv.eliminar(dist.getIddistribuidor());
                        }catch(Exception e){
                        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
                        }
                }
                this.RefreshTable();
                this.inhabilitaBotones();
                break;
            }

        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }




    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        try{
        Distribuidor dF = new Distribuidor();
        dF = this.filtro();
        this.buscar(dF);
        }catch(Exception e){
        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void cboxEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxEstadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboxEstadoActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        try{
        txtRazonSocial.setText("");
        cboxEstado.setSelectedIndex(0);
        txtRUC.setText("");
        txtDNI.setText("");
        distribuidores.clear();
        this.RefreshTable();
        this.inhabilitaBotones();
        }catch(Exception e){
        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void tableResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultadosMouseClicked
        if (evt.getClickCount() == 1) {
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tableResultadosMouseClicked

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnCargaMasivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargaMasivaActionPerformed
        try{
            Carga_Masiva_Dist cargaMasiva = new Carga_Masiva_Dist();
        
        this.getParent().add(cargaMasiva);
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = cargaMasiva.getSize();
        cargaMasiva.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
            (desktopSize.height - jInternalFrameSize.height) / 4);
        cargaMasiva.show();
        }catch(Exception e){
        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCargaMasivaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCargaMasiva;
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton btnEditarA;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboxEstado;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableResultados;
    private javax.swing.JTextField txtDNI;
    private javax.swing.JTextField txtRUC;
    private javax.swing.JTextField txtRazonSocial;
    // End of variables declaration//GEN-END:variables

    private void setLocationRelativeTo(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Object getparent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
