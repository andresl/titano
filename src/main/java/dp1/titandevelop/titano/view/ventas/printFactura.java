/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.ParametroService;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heli
 */
public class printFactura {
    private static Font fontBold = new Font(Font.FontFamily.COURIER, 12, Font.BOLD);
    private static Font fontNormal = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL);
    private String nomArch;
     public printFactura(List<DetalleDocumentoVenta> dDv,Integer numfact,String Cliente, String fecha, String RUC,Integer tipoD,Float subtotal,Float igv,Float notaCredito,Float total,String nombreArchivo, String direccion,String numFactura) throws Exception{
		
                
                this.nomArch = nombreArchivo;
		Document document = new Document();
               DocumentoVentaService serv=new DocumentoVentaService();
               Parametro parametros=serv.parametros();
		PdfWriter.getInstance(document, 
			new FileOutputStream(nombreArchivo));
		document.open();
		
		PdfPTable table = new PdfPTable(5); // Code 1
                if(tipoD==1)
                document.add(getHeader("FACTURA DE VENTA     "));
                else 
                    document.add(getHeader("BOLETA DE VENTA     "));
  		
                document.add(getInformation(" "));
                
                Image image1 = Image.getInstance(getClass().getResource("/img/GOOD_COOKIE.jpg"));
                 

                PdfPTable table2 = new PdfPTable(3); // Code 1
                
                PdfPTable table3 = new PdfPTable(1); // Code 1
                PdfPTable table4 = new PdfPTable(1); // Code 1
                PdfPTable table5 = new PdfPTable(1); // Code 1
                PdfPCell cell001y = 
			new PdfPCell(image1);
                        cell001y.setBorder(0);
                table4.addCell(cell001y);
                PdfPCell cell001yy = 
			new PdfPCell();
                        cell001y.setBorder(1);
                table5.addCell(cell001yy);
                
                
                
                Chunk ch=new Chunk();
                ch.setBackground(BaseColor.GRAY);
               ch.getFont().setColor(BaseColor.WHITE);
               
                table3.getChunks().add(ch);
             
                
                
                
                
                Chunk c1 = cabeceraTable("Cliente");
                Chunk c2 = cabeceraTable("RUC");
                Chunk c3 = cabeceraTable("Fecha");
                Chunk c4 = cabeceraTable("Cantidad");
                Chunk c41 = cabeceraTable("Unidad");
                Chunk c5 = cabeceraTable("Producto");
                Chunk c6 = cabeceraTable("Precio Unitario");
                Chunk c7 = cabeceraTable("Importe");
                Chunk c8 = cabeceraTable("Sub-Total");
                Chunk c9 = cabeceraTable("IGV");
                Chunk c10 = cabeceraTable("Nota de Crédito (Descuento)");
                Chunk c11 = cabeceraTable("Total");
                Chunk c16 = cabeceraTable("Dirección");
                
                
                Chunk c12;
                 if(tipoD==1){
                 c12 = cabeceraTable("FACTURA DE VENTA");
                 
                 }else{
                 c12 = cabeceraTable("BOLETA DE VENTA");
                 }
                Chunk c13 = cabeceraTable("RUC:"+String.valueOf(parametros.getRuc()));
                Chunk c14 = cabeceraTable(""+String.valueOf(parametros.getNombresoftware()));
                Chunk c15 = cabeceraTable(""+String.valueOf("N° "+numFactura));
                
                
                
                
                Paragraph p1 = new Paragraph(c12);
               p1.setAlignment(Element.ALIGN_CENTER);
                Paragraph p2 = new Paragraph(c13);
                p2.setAlignment(Element.ALIGN_CENTER);
                Paragraph p3 = new Paragraph(c14);
                p3.setAlignment(Element.ALIGN_CENTER);
                Paragraph p4 = new Paragraph(c15);
                p4.setAlignment(Element.ALIGN_CENTER);
                
                
                PdfPCell cell001x = 
			new PdfPCell(p1);
                        cell001x.setBorder(0);
                       
                table3.addCell(cell001x);
                
                PdfPCell cell002x = 
			new PdfPCell(p2);
                        cell002x.setBorder(0);
                table3.addCell(cell002x);
                
                PdfPCell cell003x = 
			new PdfPCell(p3);
                        cell003x.setBorder(0);
                table3.addCell(cell003x);
                
                PdfPCell cell004x = 
			new PdfPCell(p4);
                        cell004x.setBorder(0);
                table3.addCell(cell004x);
                
                 
                PdfPCell cell001 = 
			new PdfPCell(new Paragraph(c1));
                        cell001.setBorder(0);
                table2.addCell(cell001);
                
                PdfPCell cell002 = 
			new PdfPCell(new Paragraph(dataTable(""+Cliente)));
                        cell002.setBorder(0);
                table2.addCell(cell002);  
                 
                
               table5.addCell(table3); 
                PdfPCell cell001xy = 
			new PdfPCell(table5);
                        cell001xy.setBorder(0);
                        if(tipoD==1){
                        cell001xy.setRowspan(4);
                        }else{
                        cell001xy.setRowspan(3);
                        }
                table2.addCell(cell001xy);
                
                
                
                 if(tipoD==1){
                PdfPCell cell003 = 
			new PdfPCell(new Paragraph(c2));
                        cell003.setBorder(0);
                table2.addCell(cell003);
                 
               
                PdfPCell cell004 = 
			new PdfPCell(new Paragraph(dataTable(""+RUC)));
                        cell004.setBorder(0);
                table2.addCell(cell004); 
                
                
                
                 }
                PdfPCell cell005 = 
			new PdfPCell(new Paragraph(c3));
                        cell005.setBorder(0);
                table2.addCell(cell005);
                
                PdfPCell cell006 = 
			new PdfPCell(new Paragraph(dataTable(""+fecha)));
                        cell006.setBorder(0);
                table2.addCell(cell006); 
                
                
                PdfPCell cell005x = 
			new PdfPCell(new Paragraph(c16));
                        cell005x.setBorder(0);
                table2.addCell(cell005x);
                
                PdfPCell cell006x = 
			new PdfPCell(new Paragraph(dataTable(""+direccion)));
                        cell006x.setBorder(0);
                table2.addCell(cell006x); 
                
                
                
                
		// Code 2
                PdfPCell cell007 = 
			new PdfPCell(new Paragraph(c4));
		table.addCell(cell007);
                
                PdfPCell cell0071 = 
			new PdfPCell(new Paragraph(c41));
		table.addCell(cell0071);
                
                PdfPCell cell008 = 
			new PdfPCell(new Paragraph(c5));
		table.addCell(cell008);
                
                PdfPCell cell009 = 
			new PdfPCell(new Paragraph(c6));
		table.addCell(cell009);
                
                PdfPCell cell010 = 
			new PdfPCell(new Paragraph(c7));
		table.addCell(cell010);
		
              
		
		for(int i=0;i<dDv.size();i++){
                table.addCell(celda(String.valueOf(dDv.get(i).getCantidad())));
                table.addCell(celda(String.valueOf(dDv.get(i).getToProducto().getUm())));
		table.addCell(celda(dDv.get(i).getToProducto().getDescripcion()));
		
		table.addCell(celda(""+(dDv.get(i).getToProducto().getCosto()+dDv.get(i).getToProducto().getBeneficio())));
                table.addCell(celda("S/."+(dDv.get(i).getMonto())));
                System.out.println("Monto: "+dDv.get(i).getMonto()+" Cantidad: "+dDv.get(i).getCantidad());
                //subtotal+=dDv.get(i).getMonto();
                }
                //subtotal
		PdfPCell cell = 
			new PdfPCell(new Paragraph(""));
		cell.setColspan(3);
                cell.setBorder(0);
		table.addCell(cell);
                PdfPCell cell0 = 
			new PdfPCell(new Paragraph(c8));
		cell0.setColspan(1);
                cell0.setBorder(0);
                table.addCell(cell0);
                
                cell.setBorder(1);
                table.addCell(celda("S/."+String.valueOf(subtotal)));
                //fin subtotal
                
                //IGV
                if(tipoD==1){
                PdfPCell cell2 = 
			new PdfPCell(new Paragraph(""));
		cell2.setColspan(3);
                cell2.setBorder(0);
		table.addCell(cell2);
                
                PdfPCell cell02 = 
			new PdfPCell(new Paragraph(c9));
		cell02.setColspan(1);
                cell02.setBorder(0);
                table.addCell(cell02);
                
                cell2.setBorder(1);
                table.addCell(celda("S/."+String.valueOf(igv)));
                //Fin IGV
                
                //Nota Credito
                
                PdfPCell cell3 = 
			new PdfPCell(new Paragraph(""));
		cell3.setColspan(3);
                cell3.setBorder(0);
		table.addCell(cell3);
                PdfPCell cell03 = 
			new PdfPCell(new Paragraph(c10));
		cell03.setColspan(1);
                cell03.setBorder(0);
                table.addCell(cell03);
                
                cell3.setBorder(1);
                table.addCell(celda("S/."+String.valueOf(notaCredito)));
                
                
                
                }
                
                PdfPCell cell4 = 
			new PdfPCell(new Paragraph(""));
		cell4.setColspan(3);
                cell4.setBorder(0);
		table.addCell(cell4);
                
                PdfPCell cell04 = 
			new PdfPCell(new Paragraph(c11));
		cell04.setColspan(1);
                cell04.setBorder(0);
                table.addCell(cell04);
                
                
                
                cell4.setBorder(1);
                table.addCell(celda("S/."+String.valueOf(total)));
                
                document.add(table4);
                document.add(table2);
                document.add(getInformation(" "));
		document.add(table);		
                
		document.close();
                //imprimirFactura();
	}
     
     private Chunk cabeceraTable(String cadena) {
        Chunk c = new Chunk(cadena);
        c.setFont(fontBold);
        c.getFont().setSize(8);
        c.getFont().setFamily("Arial");
        return c;
    }
     
     private Chunk cabeceraTable15(String cadena) {
        Chunk c = new Chunk(cadena);
        c.setFont(fontBold);
        c.getFont().setSize(15);
        c.getFont().setFamily("Arial");
        return c;
    }
     private Chunk cabeceraTable13(String cadena) {
        Chunk c = new Chunk(cadena);
        c.setFont(fontBold);
        c.getFont().setSize(13);
        c.getFont().setFamily("Arial");
        return c;
    }
     
     private Chunk dataTable(String cadena) {
        Chunk c = new Chunk(cadena);
        c.getFont().setSize(8);
        c.getFont().setFamily("Arial");
        return c;
    }

    private PdfPCell celda(String cadena) {

        PdfPCell cell = new PdfPCell(new Paragraph(dataTable(cadena)));
        return cell;
    }
     
     private Paragraph getHeader(String header) {
    	Paragraph paragraph = new Paragraph();
  		Chunk chunk = new Chunk();
                
		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append( header + getCurrentDateTime() + "\n");
  		chunk.setFont(fontBold);
  		paragraph.add(chunk);
  		return paragraph;
     }
     private Paragraph getInformation(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append(informacion);
  		chunk.setFont(fontNormal);
                
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformation2(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_LEFT);
  		chunk.append(informacion);
                chunk.setFont(fontBold);
  		//chunk.setFont(fontNormal);
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformationFooter(String informacion) {
     	Paragraph paragraph = new Paragraph();
     	Chunk chunk = new Chunk();
   		paragraph.setAlignment(Element.ALIGN_CENTER);
   		chunk.append(informacion);
   		chunk.setFont(new Font(Font.FontFamily.COURIER, 8, Font.NORMAL));
   		paragraph.add(chunk);
    		return paragraph;
       }
      private float getConvertCmsToPoints(float cm) {
     	return cm * 28.4527559067f;
     }
     
   public void imprimirFactura(){
     
     Desktop d=Desktop.getDesktop();
     try {
      if(Desktop.isDesktopSupported()){
          //d.open(new File("/tmp/factura2.pdf"));
          d.open(new File(this.nomArch));
      // d.print(new File("factura2.pdf"));
      }
     } catch (IOException e) {
     // TODO Auto-generated catch block
    e.printStackTrace();
        }
   }
      
      
     private String getCurrentDateTime() {
     	Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yy '-' hh:mm");
     	return ft.format(dNow);
    }
	public static void main(String[] args,List<DetalleDocumentoVenta> dDv,Integer numfact,String Cliente, String fecha, String RUC,Integer tipoD,Float subtotal,Float igv,Float notaCredito,Float total, String nombreArchivo, String direccion,String numFactura) {	
		try{
			printFactura pdfTable = new printFactura(dDv,numfact,Cliente,fecha,RUC,tipoD,subtotal,igv,notaCredito,total,nombreArchivo,direccion,numFactura);
		}catch(Exception e){
			System.out.println(e);
		}
	}
        
}
