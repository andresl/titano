/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroDV;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.TipoDocumento;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Heli
 */
public class ModulosT_VEN_AV extends javax.swing.JInternalFrame {

    private Mensajes sms = new Mensajes();
    public ModulosT_VEN_AVDocNuevo ventanaNuevo = null;
    public ModulosT_VEN_AVDocDetalle editar = null;
    public ModulosT_VEN_ANCNuevo nota = null;
    public int estadoNuevo = 0;
    public List<TipoDocumento> tipoD;
    public int tipo;
    //Lista usada de tipos de movimiento
    public FiltroDV busqueda = null;
    //Lista usada de tipos de movimiento
    public List<DocumentoVenta> documentos = new ArrayList<DocumentoVenta>();

     private void addList (List<TipoDocumento> lista){
        
        for(int i=0;i<lista.size();i++){
            this.cboxTipo.addItem(lista.get(i).getTipodocumento());
        }
    }
    
    private void seleccionarTipo (Integer id){
     Integer j;   
        for(int i=0;i<this.tipoD.size();i++){
            if(id==tipoD.get(i).getIdtipodocumento()){
                this.cboxTipo.setSelectedIndex(i);
                this.tipo=tipoD.get(i).getIdtipodocumento();
            }
        }
        
    }
    
    public   float roundTwoDecimals(float d) { 
System.out.println("d*100="+d*100);
        Integer v=Math.round(d*100);
System.out.println("v="+v);
float v2=(float)((float)v/(float)100);
System.out.println("v2="+(float)v2);
return v2;
}
    
    public void habilitaBotones() {

        this.btnEliminarA.setEnabled(true);
        this.btnDetalle.setEnabled(true);
    }

    private void inhabilitaBotones() {

        this.btnEliminarA.setEnabled(false);
        this.btnDetalle.setEnabled(false);
    }

    public FiltroDV filtro() {
        FiltroDV f = new FiltroDV();
        f.setFechaIni(this.txtFechaInicio.getDate());
        f.setFechaFin(this.txtFechaFin.getDate());
        f.setDistribuidor(this.txtDistribuidor.getText());
        f.setProducto(this.txtProducto.getText());
        f.setIdTipo(Integer.valueOf(this.cboxTipo.getSelectedIndex()+1));
        return f;
    }

    public void llenarFiltro(FiltroDV f) {
        this.txtFechaInicio.setDate(f.getFechaIni());
        this.txtFechaFin.setDate(f.getFechaFin());
        this.txtDistribuidor.setText(f.getDistribuidor());
        this.txtProducto.setText(f.getProducto());

    }

    public void buscar(FiltroDV f) {
        DocumentoVentaService serv = new DocumentoVentaService();
        documentos = serv.buscarFiltrado(f);

        this.actualizarDocumentosList();
    }
    //metodo para refrescar la tabla
    Thread actualizardocumento = null;

    private void actualizarDocumentosList() {
        if (actualizardocumento != null && actualizardocumento.isAlive()) {
            actualizardocumento.interrupt();
        }
        actualizardocumento = new Thread(new Runnable() {
            @Override
            public void run() {
                RefreshTable();
                ModeloTabla modelo = new ModeloTabla();
                tableResultados.setModel(modelo);

            }
        });
        actualizardocumento.start();
    }

    public void RefreshTable() {

        AbstractTableModel aux = (AbstractTableModel) tableResultados.getModel();
        aux.fireTableDataChanged();
    }

    class ModeloTabla extends AbstractTableModel {
        //Atributos de los datos de la tabla

        private String[] titles = {"Fecha", "Cliente/Distribuidor","Tipo Documento", "Total (S/.)"};
        //private List<DataTabla> datos;
        //private Object [][]datos;

        public int getRowCount() {
            return documentos.size();//tipos.size(); // cuantos registros (filas)
        }

        public int getColumnCount() {
            return 4; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res = null;
            DocumentoVenta p = documentos.get(rowIndex);
            //boolean b = datos.get(rowIndex).seleccionado;//tipos.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    res = p.getFechafacturacion();
                    break;
                case 1:
                    res = p.getContacto();
                    break;    
                case 2:
                    if(p.getToTipoDocumento().getIdtipodocumento()==1) res= "Factura"; else res="Boleta";
                    break;                
                case 3:
                    res = String.valueOf(roundTwoDecimals(p.getTotal()));
                    break;
                //case 2: res = b;
            }
            return res;
        }
        //Ultima celda editable para selección

        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res = false;
            switch (col) {
                case 0:
                    break;
                case 1:
                    break;
                // case 2:res=true;
            }
            return res;
        }
        //para obtener las clases de cada celda y asi mostrar estas correctamente

        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
//        @Override
//        public void setValueAt(Object value, int row, int col) {
//            datos.get(row).seleccionado = (Boolean)value;
//        }
    }

    public ModulosT_VEN_AV() {
        initComponents();
        ValidacionService validacionService = new ValidacionService();
        validacionService.NoEscribe(txtFechaInicio);
        validacionService.NoEscribe(txtFechaFin);
        validacionService.SLetras(txtDistribuidor);
        validacionService.SLetras(txtProducto);
        inhabilitaBotones();
        
        
        Estado state = new Estado();
        this.inhabilitaBotones();
        this.cboxTipo.addItem("Factura");
        this.cboxTipo.addItem("Boleta");
        

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtProducto = new javax.swing.JTextField();
        txtDistribuidor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtFechaInicio = new org.jdesktop.swingx.JXDatePicker();
        jLabel9 = new javax.swing.JLabel();
        txtFechaFin = new org.jdesktop.swingx.JXDatePicker();
        jLabel13 = new javax.swing.JLabel();
        cboxTipo = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResultados = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnDetalle = new javax.swing.JButton();
        btnEliminarA = new javax.swing.JButton();
        btnCargaMasiva = new javax.swing.JButton();

        setResizable(true);
        setTitle("Administración de Ventas");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtro de Búsqueda"));

        jLabel5.setText("Fecha Inicio");

        jLabel7.setText("Producto");

        jLabel8.setText("Distribuidor");

        jLabel9.setText("Fecha Fin");

        jLabel13.setText("Tipo de Doc.");

        cboxTipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxTipoItemStateChanged(evt);
            }
        });
        cboxTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxTipoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel8))
                            .addComponent(jLabel7))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel9)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(12, 12, 12)
                        .addComponent(cboxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cboxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados de Búsqueda"));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Cliente/Distribuidor", "Tipo de Documento", "Total (S/.)"
            }
        ));
        tableResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableResultados);
        tableResultados.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        btnLimpiar.setToolTipText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/detalle.png"))); // NOI18N
        btnDetalle.setToolTipText("Ver Detalle");
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        btnCargaMasiva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/texto.png"))); // NOI18N
        btnCargaMasiva.setActionCommand("masivo");
        btnCargaMasiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargaMasivaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addComponent(btnNuevo)
                                .addGap(1, 1, 1)
                                .addComponent(btnLimpiar)
                                .addGap(0, 0, 0)
                                .addComponent(btnCargaMasiva, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(btnSalir))
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDetalle)
                        .addGap(1, 1, 1)
                        .addComponent(btnEliminarA)))
                .addGap(8, 8, 8))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscar)
                            .addComponent(btnNuevo, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addComponent(btnLimpiar, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(btnSalir)
                    .addComponent(btnCargaMasiva, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDetalle, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnEliminarA, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed

        //ID 34 Nueva Venta

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (34 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                if (estadoNuevo == 0) {
                    estadoNuevo = 1;
                   try{
                    ventanaNuevo = new ModulosT_VEN_AVDocNuevo();
                    this.getParent().add(ventanaNuevo);
                    Dimension desktopSize = this.getParent().getSize();
                    Dimension jInternalFrameSize = ventanaNuevo.getSize();
                    ventanaNuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                            (desktopSize.height - jInternalFrameSize.height) / 4);
                    ventanaNuevo.show();
                    
                    this.dispose();
                  }catch(Exception e){
                    JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
                   }
                }
                  break;
            }
        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        try{
            DocumentoVenta dv = new DocumentoVenta();
            FiltroDV f = null;
            f = this.filtro();
            this.buscar(f);
        }catch(Exception e){
            JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
         }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        txtProducto.setText("");
        txtDistribuidor.setText("");
        txtFechaInicio.setDate(null);
        txtFechaFin.setDate(null);
        this.cboxTipo.setSelectedIndex(0);
        this.documentos.clear();
        this.actualizarDocumentosList();
        //txtDuracion.setText("");

    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void tableResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultadosMouseClicked
        if (evt.getClickCount() == 1) {
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tableResultadosMouseClicked

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed
        try{
        DocumentoVenta sol;
        FiltroDV busqueda = null;
        busqueda = this.filtro();
        sol = documentos.get(this.tableResultados.getSelectedRow());
        //Instancio la ventana
        editar = new ModulosT_VEN_AVDocDetalle(sol, busqueda, 2);
        this.getParent().add(editar);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = editar.getSize();
        editar.setLocation((desktopSize.width - jInternalFrameSize.width) / 2, (desktopSize.height - jInternalFrameSize.height) / 4);
        editar.show();

        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
        this.dispose();
        }catch(Exception e){
            JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
            }
    }//GEN-LAST:event_btnDetalleActionPerformed

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed


        //ID 35 Eliminar Venta

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (35 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(), "¿Está seguro que desea eliminar el registro?", "Aviso!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

                if (JOptionPane.OK_OPTION == confirmar) {//Obtengo el objeto seleccionado
                    
                    int confirmar2 = JOptionPane.showInternalConfirmDialog(this.getRootPane(), "¿Desea generar una Nota de Crédito?", "Aviso!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                        if (JOptionPane.OK_OPTION == confirmar2) {
                             nota = new ModulosT_VEN_ANCNuevo(documentos.get(this.tableResultados.getSelectedRow()).getIddocumento(),documentos.get(this.tableResultados.getSelectedRow()).getContacto(),documentos.get(this.tableResultados.getSelectedRow()).getMontototal());
                             this.getParent().add(nota);
                             Dimension desktopSize = this.getParent().getSize();
                             Dimension jInternalFrameSize = nota.getSize();
                             nota.setLocation((desktopSize.width - jInternalFrameSize.width) / 2, (desktopSize.height - jInternalFrameSize.height) / 4);
                             nota.show();  
                        }
                    try{
                    DocumentoVentaService serv = new DocumentoVentaService();
                    DocumentoVenta sol = documentos.get(this.tableResultados.getSelectedRow());
                    this.documentos.remove(this.tableResultados.getSelectedRow());
                    serv.eliminar(sol.getIddocumento());
                    }catch(Exception e){
                        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
                    }
                    
                }

                this.RefreshTable();
                this.inhabilitaBotones();
                break;
            }
        }

        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void cboxTipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxTipoItemStateChanged
      
    }//GEN-LAST:event_cboxTipoItemStateChanged

    private void cboxTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxTipoActionPerformed
       
    }//GEN-LAST:event_cboxTipoActionPerformed

    private void btnCargaMasivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargaMasivaActionPerformed
        try{
            Carga_Masiva_Sol_Ventas cargaMasiva = new Carga_Masiva_Sol_Ventas();
            this.getParent().add(cargaMasiva);
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = cargaMasiva.getSize();
            cargaMasiva.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 4);
            cargaMasiva.show();
        }catch(Exception e){
            JOptionPane.showInternalMessageDialog(this.getRootPane(),"Ha ocurrido un problema de conexión, vuelvalo a intentar","Mensaje",JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnCargaMasivaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCargaMasiva;
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboxTipo;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable tableResultados;
    private javax.swing.JTextField txtDistribuidor;
    private org.jdesktop.swingx.JXDatePicker txtFechaFin;
    private org.jdesktop.swingx.JXDatePicker txtFechaInicio;
    private javax.swing.JTextField txtProducto;
    // End of variables declaration//GEN-END:variables
}
