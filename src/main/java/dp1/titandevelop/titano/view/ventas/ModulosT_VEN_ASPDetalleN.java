/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroSV;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.DetalleSolicitudVenta;
import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.SolicitudVenta;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.service.AlmacenService;
import dp1.titandevelop.titano.service.SolicitudVentaService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.BusquedaProductoCantidad;
import dp1.titandevelop.titano.view.MensajeCantidadProducto;
import dp1.titandevelop.titano.view.MensajeError;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;

/**
 *
 * @author Yulian
 */
public class ModulosT_VEN_ASPDetalleN extends javax.swing.JInternalFrame {

    public ModulosT_VEN_ASPDetalleNuevo nuevoDSV = null;
    public ModulosT_VEN_ASPDetalleEditar editarDSV = null;
    public int estadoNuevo = 0;    
    public int idDistribuidor;
    public int tipo;
    public int idSV;
    private Mensajes sms = new Mensajes();
    //Lista usada de tipos de movimiento
    public List<DetalleSolicitudVenta> solicitudes = new ArrayList<DetalleSolicitudVenta>();
    public FiltroSV busqueda=null; 
    //metodo para refrescar la tabla
    
    private void actualizarD(){
        solicitudes.clear();
    SolicitudVentaService service=new SolicitudVentaService();
    this.solicitudes=service.buscarFiltrado2(this.idSV);
    
    }
    
    public void habilitaBotones(){
        this.btnEliminarA.setEnabled(true);
        
    }
    private void inhabilitaBotones(){
        this.btnEliminarA.setEnabled(false);
        
    }
    
    private void inhabilitarTodo(){
          this.btnEliminarA.setVisible(false);
          this.btnNuevo.setVisible(false);
          this.btnBuscarDistribuidor.setVisible(false);
          this.btnGuardar.setVisible(false);
          this.txtFecha.setEditable(false);
          this.txtDistribuidor.setEditable(false);
          this.cboxEstado.setEnabled(false);
      
    }
    
    private float sumaItem(){
        Estado state=new Estado();
    float valor=0,sbt=0;
    for(int i=0;i<solicitudes.size();i++){
    sbt=solicitudes.get(i).getCantidad()*solicitudes.get(i).getToProducto().getCosto();
    valor+=sbt;
    solicitudes.get(i).setSubtotal(sbt);
    solicitudes.get(i).setEstado(state.getPendiente());
    }
    return valor;
    
    }

    Thread actualizarsolicitudes = null;
    private void actualizarSolicitudesList(){
        if( actualizarsolicitudes!=null && actualizarsolicitudes.isAlive() ){
            actualizarsolicitudes.interrupt();
        }
        actualizarsolicitudes = new Thread( new Runnable() {
            @Override
            public void run() {
                RefreshTable();
                ModeloTabla modelo= new ModeloTabla();
                tableResultados.setModel(modelo);
                
            }
        });
        actualizarsolicitudes.start();
    }
    
    public void RefreshTable() {
        
        AbstractTableModel aux = (AbstractTableModel)tableResultados.getModel();
        aux.fireTableDataChanged();
    }
    
    class ModeloTabla extends AbstractTableModel {  
        //Atributos de los datos de la tabla
        
        private String [] titles = {"Producto", "Cantidad","Unidad","Sub-Total (S/.)"};
        //private List<DataTabla> datos;
        //private Object [][]datos;
        
        public int getRowCount() {
            return solicitudes.size();//tipos.size(); // cuantos registros (filas)
        }      
        
        public int getColumnCount() {
            return 4; // cuantas columnas
        }          
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }                
       
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res=null;
            DetalleSolicitudVenta dsv = solicitudes.get(rowIndex);
            //boolean b = datos.get(rowIndex).seleccionado;//tipos.get(rowIndex);
            switch(columnIndex){
                case 0: res = dsv.getToProducto().getDescripcion(); break;
                case 1: res = dsv.getCantidad(); break;
                    case 2: res = dsv.getToProducto().getUm(); break;
                case 3: res = dsv.getCantidad()*(dsv.getToProducto().getCosto()+dsv.getToProducto().getBeneficio()); break;
                //case 2: res = b;
            }
            return res;
        } 
        //Ultima celda editable para selección
        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res=false;
            switch (col) {
                case 0:break;
                case 1:break;
                case 2:break;
               // case 2:res=true;
            }
            return res;
        }
        //para obtener las clases de cada celda y asi mostrar estas correctamente
        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
//        @Override
//        public void setValueAt(Object value, int row, int col) {
//            datos.get(row).seleccionado = (Boolean)value;
//        }
    }

    public ModulosT_VEN_ASPDetalleN() {
        initComponents();
        ValidacionService validacionService = new ValidacionService();
        validacionService.SLetras(this.txtDistribuidor);
        validacionService.NoEscribe(this.txtFecha);
        this.tipo=0;
        inhabilitaBotones();
        
        this.cboxEstado.setVisible(false);
        this.labelEstado.setVisible(false);
    }
    
    public ModulosT_VEN_ASPDetalleN(SolicitudVenta s, FiltroSV sv,int tipo) {
        initComponents();
        this.setTitle("Ventas - Editar Solicitud de Pedido");
        ValidacionService validacionService = new ValidacionService();
        validacionService.SLetras(this.txtDistribuidor);
        validacionService.NoEscribe(this.txtFecha);
        SolicitudVentaService service=new SolicitudVentaService();
        this.txtFecha.setDate(s.getFecha());
        this.txtDistribuidor.setText(s.getToDistribuidor().getRazonsocial());
        this.solicitudes=service.buscarFiltrado2(s.getIdsolicitudventa());
        this.busqueda=sv;
        this.idDistribuidor=s.getToDistribuidor().getIddistribuidor();
        
        Estado state = new Estado();
        this.cboxEstado.addItem(state.devuelveCadena(state.getPendiente()));
        this.cboxEstado.addItem(state.devuelveCadena(state.getCancelado()));
        if(tipo==2)
        this.cboxEstado.addItem(state.devuelveCadena(state.getAceptado()));
        
        this.cboxEstado.setSelectedItem(state.devuelveCadena(s.getEstado()));
        
        this.actualizarSolicitudesList();
        this.tipo=tipo;
        this.idSV=s.getIdsolicitudventa();
        inhabilitaBotones();
        
        if(this.tipo==2){
            this.setTitle("Ventas - Ver Solicitud de Pedido");
        this.inhabilitarTodo();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtFecha = new org.jdesktop.swingx.JXDatePicker();
        txtDistribuidor = new javax.swing.JTextField();
        btnBuscarDistribuidor = new javax.swing.JButton();
        cboxEstado = new javax.swing.JComboBox();
        labelEstado = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResultados = new javax.swing.JTable();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnEliminarA = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();

        setBorder(null);
        setResizable(true);
        setTitle("Nueva Solicitud de Pedido");
        setToolTipText("");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de Solicitud"));

        jLabel5.setText("Fecha");

        jLabel7.setText("Cliente/Distribuidor");

        txtDistribuidor.setEditable(false);
        txtDistribuidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistribuidorActionPerformed(evt);
            }
        });

        btnBuscarDistribuidor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar2.png"))); // NOI18N
        btnBuscarDistribuidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarDistribuidorActionPerformed(evt);
            }
        });

        cboxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));

        labelEstado.setText("Estado");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel7))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(labelEstado))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel5)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarDistribuidor)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarDistribuidor))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelEstado)))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle Solicitud de Pedido"));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Cantidad", "Unidad", "Sub-Total (S/.)"
            }
        ));
        tableResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableResultados);
        tableResultados.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/grabar.png"))); // NOI18N
        btnGuardar.setToolTipText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        btnCancelar.setToolTipText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/agregar.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(btnNuevo)
                        .addGap(1, 1, 1)
                        .addComponent(btnEliminarA)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(btnCancelar))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(8, 8, 8))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnGuardar)
                        .addComponent(btnCancelar))
                    .addComponent(btnNuevo)
                    .addComponent(btnEliminarA))
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        final int i=0;
        final busquedaProductoCantidad seleccionarProductoForm =new busquedaProductoCantidad(null, true);
        seleccionarProductoForm.setLocationRelativeTo(this);
        seleccionarProductoForm.addWindowListener(new WindowAdapter() {
                        @Override
			public void windowClosed(WindowEvent e) {
                            int cantx=0,iddetallesolicitudx=0,posx=-1;
                            Estado state=new Estado();
                            if(seleccionarProductoForm.idProducto!=null){
                                if(seleccionarProductoForm.idProducto>0){
                                    int aumentar=1;
                                DetalleSolicitudVenta d=new DetalleSolicitudVenta();
                                d.setCantidad(seleccionarProductoForm.cantidad);
                                d.setSubtotal(seleccionarProductoForm.cantidad*seleccionarProductoForm.costo);
                                d.setEstado(state.getPendiente());
                                ObjectContext context = DataContext.createDataContext();
                                Producto p=(Producto)context.newObject(Producto.class);
                                p.setIdproducto(seleccionarProductoForm.idProducto);
                                p.setDescripcion(seleccionarProductoForm.producto);
                                p.setCosto(seleccionarProductoForm.costo);
                                p.setBeneficio(seleccionarProductoForm.beneficio);
                                p.setUm(seleccionarProductoForm.unidad);
                                d.setToProducto(p);
                                seleccionarProductoForm.idProducto=0;
                                
                                
                                for(int i=0;i<solicitudes.size();i++){
                                    if(solicitudes.get(i).getToProducto().getDescripcion().equals(p.getDescripcion()) || solicitudes.get(i).getToProducto().getIdproducto().equals(p.getIdproducto()) ){
                                        
                                        if(tipo==1)
                                            posx=i;
                                        else
                                        solicitudes.get(i).setCantidad(d.getCantidad());
                                       
                                        aumentar=0;
                                        if(solicitudes.get(i).getIddetallesolicitudventa()!=null){
                                        cantx=d.getCantidad();
                                        iddetallesolicitudx=solicitudes.get(i).getIddetallesolicitudventa();
                                        }
                                    }
                                }
                                
                                
                                if(tipo==1){
                                SolicitudVentaService service= new SolicitudVentaService();
                                SolicitudVenta dd=(SolicitudVenta)context.newObject(SolicitudVenta.class);
                                System.out.println(idSV);
                                dd.setIdsolicitudventa(idSV);
                                d.setToSolicitudVenta(dd);
                            
                                if(aumentar==1){
                                    int confirmar = JOptionPane.showInternalConfirmDialog(getRootPane(),"¿Está seguro que desea aumentar el producto seleccionado a su demanda? esto se registrará de manera permanente","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

                                    if (JOptionPane.OK_OPTION == confirmar){
                                        service.agregarItem(d);
                                    }else{
                                        aumentar=0;
                                    }
                                }else{
                                    if(cantx!=0 && iddetallesolicitudx!=0){
                                        int confirmar = JOptionPane.showInternalConfirmDialog(getRootPane(),"¿Está seguro que desea modificar la cantidad del producto seleccionado?, su cambio sera permantente","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

                                       if (JOptionPane.OK_OPTION == confirmar){
                                        System.out.println("Voy a editar el idsolitudventa="+iddetallesolicitudx);
                                        d.setIddetallesolicitudventa(iddetallesolicitudx);
                                       service.editarItem(d);
                                       if(posx>-1){
                                       solicitudes.get(posx).setCantidad(d.getCantidad());
                                       }
                                       }else{
                                       aumentar=0;
                                       }
                                    }
                                }
                              }
                                
                                
                                if(aumentar==1) solicitudes.add(d);
                                
                           if(tipo==1) actualizarD();     
                           actualizarSolicitudesList();
                                
                                
                                
                                
                                
                                
                                
                                /*
                              if(tipo==1){
                                SolicitudVentaService service= new SolicitudVentaService();
                                
                                SolicitudVenta ss=(SolicitudVenta)context.newObject(SolicitudVenta.class);
                                ss.setIdsolicitudventa(idSV);
                                d.setToSolicitudVenta(ss);
                            
                                service.agregarItem(d);
                              }
                                
                                
                                //d.getToProducto().setDescripcion();
                              
                              for(int i=0;i<solicitudes.size();i++){
                                    if(solicitudes.get(i).getToProducto().getDescripcion().equals(p.getDescripcion())){
                                        solicitudes.get(i).setCantidad(d.getCantidad());
                                        aumentar=0;
                                    }
                                }
                                
                                if(aumentar==1) solicitudes.add(d);
                            
                           actualizarSolicitudesList();*/
                           //System.out.println(seleccionarProductoForm.producto);
                                }
                            }
                            //this;
			}
		});
        seleccionarProductoForm.setVisible(true);   
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void tableResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultadosMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tableResultadosMouseClicked

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        Mensajes mensajes = new Mensajes();
        Integer estado=1;
        ValidacionService validacionService = new ValidacionService();
        if ((validacionService.esVacio(this.txtDistribuidor)) || (validacionService.esFechaVacia(this.txtFecha))) {
            JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getCompletarCampos(), "Aviso", JOptionPane.WARNING_MESSAGE);

        } else {
            if(solicitudes.size()==0) 
                JOptionPane.showInternalMessageDialog(this.getRootPane(), "No ha seleccionado productos para su solicitud", "Aviso", JOptionPane.WARNING_MESSAGE);
            else{
                AlmacenService serviceA =new AlmacenService();
            
            //aca debo pasar el id del producto, devuelve una lista: o capacidad total del almcen y el stock es el 1
            for(int i=0;i<solicitudes.size();i++){
            List<Integer> capacidad_StockTotalProducto = serviceA.capacidad_StockTotalProducto(solicitudes.get(i).getToProducto().getIdproducto());
            Integer cantMax=capacidad_StockTotalProducto.get(1);
                if(cantMax<this.solicitudes.get(i).getCantidad()){
                    System.out.println("Acabo de salir");
                    estado=0;
                    JOptionPane.showMessageDialog(this,"No hay suficiente stock. Stock actual:"+cantMax, "Mensaje", JOptionPane.WARNING_MESSAGE);
                    break;
                }
            }
            if(estado==1){    
        try {
            //Usamos el atributo creado para modificar el registro seleccionado
           final Estado state=new Estado();
            ObjectContext context = DataContext.createDataContext();
            SolicitudVenta aux=(SolicitudVenta)context.newObject(SolicitudVenta.class);
            Distribuidor d=(Distribuidor)context.newObject(Distribuidor.class);
            
            
            
            d.setIddistribuidor(idDistribuidor);

            aux.setFecha(this.txtFecha.getDate());
            aux.setTotal(this.sumaItem());
            aux.setEstado(state.getPendiente());
            aux.setToDistribuidor(d);
            aux.setIdsolicitudventa(this.idSV);
            SolicitudVentaService service=new SolicitudVentaService();
            if(this.tipo==0){
                service.insertar(aux, solicitudes);
                JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getRegistrado(),"Mensaje",JOptionPane.INFORMATION_MESSAGE);
            }
            if(this.tipo==1){
             aux.setEstado(state.getEstadoId(this.cboxEstado.getSelectedItem().toString()));
             System.out.println("llegue hasta aqui"+aux.getEstado());
                service.editar(aux,solicitudes);
                JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getEditado(),"Mensaje",JOptionPane.INFORMATION_MESSAGE);
            }
            
            
            System.out.println("pase esto");

        } catch(Exception e) {
            //e.printStackTrace();
            System.out.println("Noooooooooooooooooooooooo");
            MensajeError dialog = new MensajeError(new javax.swing.JFrame(), true);
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        }
        
        ModulosT_VEN.menuItemSPed = new ModulosT_VEN_ASP(this.busqueda);
        this.getParent().add( ModulosT_VEN.menuItemSPed);
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize =  ModulosT_VEN.menuItemSPed.getSize();
        ModulosT_VEN.menuItemSPed.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height)/4 );
        ModulosT_VEN.menuItemSPed.show();
        
        this.dispose();
        }
        }
      }     
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        ModulosT_VEN.menuItemSPed = new ModulosT_VEN_ASP(this.busqueda);
        this.getParent().add( ModulosT_VEN.menuItemSPed);
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize =  ModulosT_VEN.menuItemSPed.getSize();
        ModulosT_VEN.menuItemSPed.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height)/4 );
        ModulosT_VEN.menuItemSPed.show();
        
        
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed
        int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(),"¿Está seguro que desea eliminar el registro?","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

        if (JOptionPane.OK_OPTION == confirmar)
        {//Obtengo el objeto seleccionado
            SolicitudVentaService serv = new SolicitudVentaService();
            DetalleSolicitudVenta dist = solicitudes.get(this.tableResultados.getSelectedRow());
            this.solicitudes.remove(this.tableResultados.getSelectedRow());
            if(this.tipo==1)
            serv.eliminarItemDetalle(dist.getIddetallesolicitudventa());
        }

        this.RefreshTable();
        this.inhabilitaBotones();
    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void txtDistribuidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistribuidorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDistribuidorActionPerformed

    private void btnBuscarDistribuidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarDistribuidorActionPerformed
        final BusquedaDistribuidor seleccionarProductoForm =new BusquedaDistribuidor(null, true);
        seleccionarProductoForm.setLocationRelativeTo(this);
        seleccionarProductoForm.addWindowListener(new WindowAdapter() {
                        @Override
			public void windowClosed(WindowEvent e) {
                            if(seleccionarProductoForm.idDistribuidor!=null){
                                if(seleccionarProductoForm.idDistribuidor>0){
                                idDistribuidor=seleccionarProductoForm.idDistribuidor;
                                txtDistribuidor.setText(seleccionarProductoForm.distribuidor);
                           //System.out.println(seleccionarProductoForm.producto);
                                    }
                                }
                            //this;
			}
		});
        seleccionarProductoForm.setVisible(true);   
        
    }//GEN-LAST:event_btnBuscarDistribuidorActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarDistribuidor;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox cboxEstado;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    private javax.swing.JLabel labelEstado;
    public javax.swing.JTable tableResultados;
    private javax.swing.JTextField txtDistribuidor;
    private org.jdesktop.swingx.JXDatePicker txtFecha;
    // End of variables declaration//GEN-END:variables
}
