/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroDV;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.DetalleDamanda;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.DetalleSolicitudVenta;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.SolicitudVenta;
import dp1.titandevelop.titano.persistent.TipoDocumento;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.service.AlmacenService;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.SolicitudVentaService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.BusquedaProductoCantidad;
import dp1.titandevelop.titano.view.MensajeCantidadProducto;
import dp1.titandevelop.titano.view.MensajeError;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;

/**
 *
 * @author Heli
 */
public class ModulosT_VEN_AVDocNuevo extends javax.swing.JInternalFrame {
    
    public ModulosT_VEN_AV ventanaNuevo = null;
    public List<TipoDocumento> tipoD;
    public Parametro parametro;
    private Mensajes sms = new Mensajes();
    
    public String cadenaMensaje=null;
    
    List <DetalleDocumentoVenta> dDv=new ArrayList<DetalleDocumentoVenta>();
    public int estadoNuevo = 0;    
    public int idDistribuidor;
    public int idNotaCredito=0;
    public int tipo;
    public int idSV;
    public int idSD;
    public float impuestos;
    public FiltroDV busqueda;
    public float subtotalMasIgv;
    public int tipoDocumento;
    public int idDocumento;
    public String ruc;
    
    
    private void actualizarSuma(){
        Float valor;
       
        
        if(!txtDescuento.getText().equalsIgnoreCase(""))
        valor=sumaItem()+Float.valueOf(txtIgv.getText())-impuestos-(Float.valueOf(txtDescuento.getText()));
        else
            valor=sumaItem()+Float.valueOf(txtIgv.getText())-impuestos;
        
        if(valor<0){
        JOptionPane.showMessageDialog(this,"El monto Total no puede ser negativo. No podra usted guardar esta factura si su monto Total es negativo", "Mensaje", JOptionPane.WARNING_MESSAGE);
        impuestos=0;
        txtDescuento.setText("0");
        }else{
        txtTotal.setText(String.valueOf(roundTwoDecimals(valor)));
        }
        
    }
    
    private void addList (List<TipoDocumento> lista){
        
        for(int i=0;i<lista.size();i++){
            this.cboxTipo.addItem(lista.get(i).getTipodocumento());
        }
    }
    
    private void seleccionarTipo (Integer id){
     Integer j;   
        for(int i=0;i<this.tipoD.size();i++){
            if(id==tipoD.get(i).getIdtipodocumento()){
                this.cboxTipo.setSelectedIndex(i);
                this.tipo=tipoD.get(i).getIdtipodocumento();
            }
        }
        
    }
    
    
    //public DocumentoVenta doc=
    
    public void habilitaBotones(){
        this.btnBuscarDistribuidor.setEnabled(true);
        this.txtDistribuidor.setEditable(true);
        
        
    }
    
    public void habilitaBotonesList(){
        //this.btnNuevo.setEnabled(false);
        if(this.idSV==0)
        this.btnEliminarA.setEnabled(true);
        else 
        this.btnEliminarA.setEnabled(false);
        
    }
    
    public void inhabilitaBotonesList(){
       // this.btnNuevo.setEnabled(true);
        this.btnEliminarA.setEnabled(false);
        
        
    }
    private void inhabilitaBotonesBoleta(){
        this.tipoDocumento=2;
        this.btnBuscarDistribuidor.setEnabled(false);
        this.txtDistribuidor.setEnabled(false);
        this.idDistribuidor=0;
        this.idSV=0;
        this.txtIgv.setText("0");
        this.txtDistribuidor.setText("");
        this.txtContacto.setText("");
        this.txtPedido.setText("");
        this.txtIgv.setEnabled(false);
        this.txtContacto.setEnabled(true);
        this.txtDireccion.setEnabled(true);
        this.txtDNI.setEnabled(true);
        this.txtPedido.setEnabled(false);
        this.btnBuscarSolicitud.setEnabled(false);
        DocumentoVentaService servicex=new DocumentoVentaService();
        this.txtNumDoc.setText(servicex.numeroBoleta());
        this.btnBuscarNotaCredito.setEnabled(false);
        this.btnNuevo.setEnabled(true);
        this.txtDescuento.setEnabled(false);
        this.labeContacto.setText("Contacto");
        this.labelDNI.setVisible(true);
        this.txtDNI.setVisible(true);
        this.txtDireccion.setText("");
        this.txtRuc.setText("");
        this.txtRUC.setText("");
        
    }
    private void inhabilitaBotonesFactura(){
        this.tipoDocumento=1;
        this.btnBuscarDistribuidor.setEnabled(true);
        this.txtDistribuidor.setEditable(true);
        
        this.btnBuscarSolicitud.setEnabled(true);
        this.txtPedido.setEnabled(true);
        this.txtPedido.setText("");
        
        this.idDistribuidor=0;
        this.txtIgv.setText("0");
        
        this.txtIgv.setEnabled(true);
        this.txtDistribuidor.setText("");
        this.txtContacto.setText("");
        this.txtDistribuidor.setEnabled(true);
        this.txtContacto.setEnabled(false);
        this.txtDireccion.setEnabled(false);
        this.txtDNI.setEnabled(false);
        DocumentoVentaService servicex=new DocumentoVentaService();
        this.txtNumDoc.setText(servicex.numeroFactura());
        this.btnBuscarNotaCredito.setEnabled(true);
        this.txtDescuento.setEnabled(true);
        this.labeContacto.setText("Razón Social");
        this.txtRUC.setText("RUC:");
        this.txtDNI.setVisible(false);
        this.labelDNI.setVisible(false);
        
    }
    
    private void resetearTodo(){
            idSV=0;
        this.txtPedido.setText("");
        this.txtFecha.setDate(null);
        this.txtDistribuidor.setText("");
        this.txtContacto.setText("");
            
        
    }
    
    
    private void inhabilitaBotonesSolicitud(){
        this.btnBuscarDistribuidor.setEnabled(false);
        this.txtDistribuidor.setEditable(false);
        
    }
    
    private void inhabilitarInicio(){
          this.txtDistribuidor.setEditable(false);
          this.txtPedido.setEditable(false);
          this.txtTotal.setEditable(false);
          this.txtDescuento.setEditable(false);
          //this.tableResultados.setEnabled(false);
      
    }
    
    
    
    private void inhabilitarTodo(){
          this.btnNuevo.setVisible(false);
          this.btnEliminarA.setVisible(false);
          this.btnBuscarDistribuidor.setVisible(false);
          this.btnBuscarSolicitud.setVisible(false);
          this.btnGuardar.setVisible(false);
          this.txtFecha.setEditable(false);
          this.txtDistribuidor.setEditable(false);
          this.txtPedido.setEditable(false);
          this.txtFecha.setEditable(false);
          this.txtDescuento.setEditable(false);
          //this.tableResultados.setEnabled(false);
      
    }
    
    private float sumaItem(){
    float valor=0,sbt=0;
    for(int i=0;i<dDv.size();i++){
    sbt=dDv.get(i).getCantidad()*(dDv.get(i).getToProducto().getCosto()+dDv.get(i).getToProducto().getBeneficio());
    valor+=sbt;
    dDv.get(i).setMonto(sbt);
    dDv.get(i).setEstado(1);
    }
    return valor;
    
    }
    
public   float roundTwoDecimals(float d) { 
    float d0=d*100;
    int v=Math.round(d0);
    float v2=(float)v/(float)100;
return (float)v2;
}  
    
    public void incrustarDSVaDDV(List<DetalleSolicitudVenta>dsv){
        this.dDv.clear();
        ObjectContext context = DataContext.createDataContext();
        for(int i=0;i<dsv.size();i++){
            DetalleDocumentoVenta ddv=(DetalleDocumentoVenta)context.newObject(DetalleDocumentoVenta.class);
            Producto p=(Producto)context.newObject(Producto.class);
             System.out.println(i+" producto: "+p.getDescripcion());
            p.setIdproducto(dsv.get(i).getToProducto().getIdproducto());
            p.setDescripcion(dsv.get(i).getToProducto().getDescripcion());
            p.setCosto(dsv.get(i).getToProducto().getCosto());
            p.setBeneficio(dsv.get(i).getToProducto().getBeneficio());
            p.setUm(dsv.get(i).getToProducto().getUm());
            ddv.setCantidad(dsv.get(i).getCantidad());
            ddv.setToProducto(p);
            ddv.setMonto((dsv.get(i).getToProducto().getCosto()+dsv.get(i).getToProducto().getBeneficio())*dsv.get(i).getCantidad());
            dDv.add(ddv);
            
        }
     this.actualizarProductosList();
    }
    
    public ModulosT_VEN_AVDocNuevo() {
        initComponents(); 
        inhabilitaBotonesList();
        Date fechaActual= new Date();
        ValidacionService validacionService = new ValidacionService();
        validacionService.SLetras(this.txtContacto);
        validacionService.SNumeros(this.txtDNI);
        validacionService.SAlfanumerico(this.txtDireccion);
        validacionService.NoEscribe(this.txtFecha);
        validacionService.Longitud(this.txtDireccion,50);
        validacionService.Longitud(this.txtContacto,50);
        validacionService.Longitud(this.txtDNI,8);
        
        
        //validacionService.SNumeros(this.txtDescuento);
        //validacionService.esFechaVacia(txtFecha);
        
        this.btnImprimir.setVisible(false);
        this.txtDescuento.setText("0");
        this.txtFecha.setDate(fechaActual);
        this.labeContacto.setText("Razón Social");
        
        DocumentoVentaService service=new DocumentoVentaService();
        this.tipoD=service.buscarTipoDocumento();
        this.parametro=service.parametros();
        addList(tipoD);
        this.actualizarAllList();
        this.inhabilitarInicio();
    }
    
    public ModulosT_VEN_AVDocNuevo(DocumentoVenta d, FiltroDV dv,int tipo) {
        initComponents(); 
        inhabilitaBotonesList();
        this.txtDescuento.setText("0");
        inhabilitaBotonesBoleta();
        DocumentoVentaService service=new DocumentoVentaService();
        this.tipoD=service.buscarTipoDocumento();
        this.parametro=service.parametros();
        addList(tipoD);
        this.idDocumento=d.getIddocumento();
        this.ruc=d.getToSolicitudVenta().getToDistribuidor().getRuc();
    
        
        this.txtFecha.setDate(d.getFechafacturacion());
        this.txtDistribuidor.setText(d.getToSolicitudVenta().getToDistribuidor().getRazonsocial());
        this.txtRuc.setText(d.getToSolicitudVenta().getToDistribuidor().getRuc());
        this.txtContacto.setText(d.getContacto());
        this.txtDNI.setText(d.getDni());
        this.txtDireccion.setText(d.getDireccion());
        this.txtSubTotal.setText(String.valueOf(roundTwoDecimals(d.getMontototal())));
        this.txtDescuento.setText(String.valueOf(roundTwoDecimals(d.getDescuento())));
        this.txtIgv.setText(String.valueOf(roundTwoDecimals(d.getIgv())));
        this.txtTotal.setText(String.valueOf(roundTwoDecimals(d.getTotal())));
        this.tipoDocumento=d.getToTipoDocumento().getIdtipodocumento();
        this.cboxTipo.setSelectedIndex(this.tipoDocumento-1);
        
        this.dDv=service.buscarFiltrado2(d.getIddocumento());
        this.busqueda=dv;
        this.idDistribuidor=d.getToSolicitudVenta().getToDistribuidor().getIddistribuidor();
        this.actualizarProductosList();
        
        this.tipo=tipo;
        this.idSD=d.getIddocumento();
        //this.actualizarSuma();
        if(this.tipo==2){
        this.inhabilitarTodo();
        this.btnImprimir.setVisible(true);
        this.actualizarAllList();
        }
    }
    
    
    
    
    
    
    
    
    
    
Thread actualizarproducto = null;
    private void actualizarProductosList(){
        if( actualizarproducto!=null && actualizarproducto.isAlive() ){
            actualizarproducto.interrupt();
        }
        actualizarproducto = new Thread( new Runnable() {
            @Override
            public void run() {
                RefreshTable();
                ModeloTabla modelo= new ModeloTabla();
                tableResultados.setModel(modelo);
                txtSubTotal.setText(String.valueOf(roundTwoDecimals(sumaItem())));
                float valor=sumaItem()+Float.valueOf(txtIgv.getText())-impuestos;
                txtTotal.setText(String.valueOf(roundTwoDecimals(valor)));
            }
        });
        actualizarproducto.start();
    }
    
Thread actualizarAll = null;
    private void actualizarAllList(){
        if( actualizarAll!=null && actualizarAll.isAlive() ){
            actualizarAll.interrupt();
        }
        actualizarAll = new Thread( new Runnable() {
            @Override
            public void run() {
                while(true){
                
                Float d0=roundTwoDecimals(sumaItem());
                
                txtSubTotal.setText(String.valueOf(d0));
                if(tipoDocumento==1){
                  
                    txtIgv.setText(String.valueOf(roundTwoDecimals(Float.valueOf(txtSubTotal.getText())*parametro.getIgv())));
               
                }else
                    txtIgv.setText("0");
                subtotalMasIgv=roundTwoDecimals(Float.valueOf(txtSubTotal.getText())+Float.valueOf(txtIgv.getText()));
                actualizarSuma();
               try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ModulosT_VEN_AVDocNuevo.class.getName()).log(Level.SEVERE, null, ex);
                }
                }
            }
        });
        actualizarAll.start();
    }
    
    
    //metodo para refrescar la tabla
    public void RefreshTable() {
        
        AbstractTableModel aux = (AbstractTableModel)tableResultados.getModel();
        aux.fireTableDataChanged();
    }
    
    class ModeloTabla extends AbstractTableModel {  
        //Atributos de los datos de la tabla
        private String [] titles = {"Producto", "Cantidad","Unidad","Precio Unitario (S/.)","Importe (S/.)"};
        //private List<DataTabla> datos;
        //private Object [][]datos;
        
        public int getRowCount() {
            return dDv.size();//tipos.size(); // cuantos registros (filas)
        }      
        
        public int getColumnCount() {
            return 5; // cuantas columnas
        }          
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }                
       
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res=null;
            DetalleDocumentoVenta d = dDv.get(rowIndex);
            //boolean b = datos.get(rowIndex).seleccionado;//tipos.get(rowIndex);
            switch(columnIndex){
                case 0: res = d.getToProducto().getDescripcion(); break;
                case 1: res = d.getCantidad(); break;
                case 2: res = d.getToProducto().getUm(); break;
                case 3: res = d.getToProducto().getCosto()+d.getToProducto().getBeneficio(); break;
                case 4: res = String.valueOf(roundTwoDecimals(d.getMonto())); break;
                //case 2: res = b;
            }
            return res;
        } 
        //Ultima celda editable para selección
        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res=false;
            switch (col) {
                case 0:break;
                case 1:break;
               // case 2:res=true;
            }
            return res;
        }
        //para obtener las clases de cada celda y asi mostrar estas correctamente
        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
//        @Override
//        public void setValueAt(Object value, int row, int col) {
//            datos.get(row).seleccionado = (Boolean)value;
//        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtDistribuidor = new javax.swing.JTextField();
        btnBuscarDistribuidor = new javax.swing.JButton();
        txtDNI = new javax.swing.JTextField();
        labelDNI = new javax.swing.JLabel();
        labeContacto = new javax.swing.JLabel();
        txtContacto = new javax.swing.JTextField();
        labelDNI2 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        txtRuc = new java.awt.Label();
        txtRUC = new java.awt.Label();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResultados = new javax.swing.JTable();
        btnNuevo = new javax.swing.JButton();
        btnEliminarA = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        txtSubTotal = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtDescuento = new javax.swing.JTextField();
        txtIgv = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        btnBuscarNotaCredito = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        txtPedido = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        btnBuscarSolicitud = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        cboxTipo = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txtFecha = new org.jdesktop.swingx.JXDatePicker();
        txtNumDoc = new javax.swing.JLabel();
        btnImprimir = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();

        setResizable(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Documento de Venta"));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del Cliente"));

        jLabel7.setText("Cliente/Distribuidor");

        btnBuscarDistribuidor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar2.png"))); // NOI18N
        btnBuscarDistribuidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarDistribuidorActionPerformed(evt);
            }
        });

        labelDNI.setText("DNI");

        labeContacto.setText("Contacto");

        labelDNI2.setText("Dirección");

        txtRUC.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtRUC.setText("RUC:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel7))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(labeContacto))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(labelDNI2)))
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtContacto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                    .addComponent(txtDistribuidor, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtDireccion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(labelDNI)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnBuscarDistribuidor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtRUC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(txtRuc, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBuscarDistribuidor))
                    .addComponent(txtRuc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRUC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labeContacto)
                    .addComponent(labelDNI)
                    .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDNI2))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle de Documento de Venta"));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Cantidad", "Unidad", "Precio Unitario (S/.)", "Importe (S/.)"
            }
        ));
        tableResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableResultados);
        tableResultados.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/agregar.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNuevo)
                    .addComponent(btnEliminarA))
                .addGap(8, 8, 8))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminarA)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/grabar.png"))); // NOI18N
        btnGuardar.setToolTipText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        btnCancelar.setToolTipText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        txtSubTotal.setEditable(false);

        jLabel12.setText("Nota de Crédito (S/.)");

        txtDescuento.setEditable(false);

        txtIgv.setEditable(false);

        jLabel13.setText("Sub-total (S/.)");

        jLabel14.setText("IGV (S/.)");

        jLabel15.setText("Total (S/.)");

        btnBuscarNotaCredito.setText("Nota de Crédito");
        btnBuscarNotaCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarNotaCreditoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(btnBuscarNotaCredito)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                            .addComponent(jLabel15)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel14)
                                .addComponent(jLabel13))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtIgv, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13))
                                .addGap(29, 29, 29))
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel14)
                                .addComponent(txtIgv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btnBuscarNotaCredito)
                        .addGap(2, 2, 2)))
                .addGap(8, 8, 8)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));

        jLabel8.setText("Pedido");

        btnBuscarSolicitud.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar2.png"))); // NOI18N
        btnBuscarSolicitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarSolicitudActionPerformed(evt);
            }
        });

        jLabel6.setText("T. de Documento");

        cboxTipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboxTipoItemStateChanged(evt);
            }
        });
        cboxTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxTipoActionPerformed(evt);
            }
        });

        jLabel5.setText("Fecha");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(cboxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(txtPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarSolicitud))
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNumDoc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cboxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)))
                .addGap(8, 8, 8)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtPedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarSolicitud))
                .addGap(8, 8, 8)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/imprimir.png"))); // NOI18N
        btnImprimir.setToolTipText("Imprimir");
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        btnLimpiar.setToolTipText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnLimpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(688, 688, 688)
                        .addComponent(btnImprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(8, 8, 8))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnCancelar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnGuardar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnImprimir, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimpiar))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        final int i=0;
        final busquedaProductoCantidad seleccionarProductoForm0 =new busquedaProductoCantidad(null, true);
        seleccionarProductoForm0.setLocationRelativeTo(this);
        seleccionarProductoForm0.addWindowListener(new WindowAdapter() {
                        @Override
			public void windowClosed(WindowEvent e) {
                            if(seleccionarProductoForm0.idProducto!=null && seleccionarProductoForm0.estoy=="productoCantidad"){
                            if(seleccionarProductoForm0.idProducto>0){
                                int aumentar=1;
                                DetalleDocumentoVenta d=new DetalleDocumentoVenta();
                                d.setCantidad(seleccionarProductoForm0.cantidad);
                                ObjectContext context = DataContext.createDataContext();
                                Producto p=(Producto)context.newObject(Producto.class);
                                p.setIdproducto(seleccionarProductoForm0.idProducto);
                                p.setDescripcion(seleccionarProductoForm0.producto);
                                p.setCosto(seleccionarProductoForm0.costo);
                                p.setBeneficio(seleccionarProductoForm0.beneficio);
                                p.setUm(seleccionarProductoForm0.unidad);
                                d.setToProducto(p);
                                seleccionarProductoForm0.idProducto=0;
                                
                                
                                //d.getToProducto().setDescripcion();
                            
                                for(int i=0;i<dDv.size();i++){
                                    if(dDv.get(i).getToProducto().getDescripcion().equals(p.getDescripcion())){
                                        dDv.get(i).setCantidad(d.getCantidad());
                                        aumentar=0;
                                    }
                                }
                                
                                if(aumentar==1) dDv.add(d);
                           actualizarProductosList();
                           //System.out.println(seleccionarProductoForm.producto);
                            }
                            }
                            //this;
			}
		});
        seleccionarProductoForm0.setVisible(true);   
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void tableResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultadosMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotonesList();
        }
        
    }//GEN-LAST:event_tableResultadosMouseClicked

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        try {
        Mensajes mensajes = new Mensajes();
        ValidacionService validacionService = new ValidacionService();
        cadenaMensaje="No hay suficiente stock en los siguientes productos:\n";
        int paso=1;
         System.out.println("Estoy aqui");
                if(tipoDocumento==2){
                   
                    System.out.println("entre aqui porque soy tipodocumento 2");
                    if((validacionService.esVacio(txtContacto)) || (validacionService.esVacio(txtDNI)) || (validacionService.esVacio(txtDireccion)) ){
                        paso=0;
                        JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getCompletarCampos(), "Aviso", JOptionPane.WARNING_MESSAGE);
                    }
                }
         System.out.println("estoy aqui 2");
        if(paso==1){
        if ((validacionService.esFechaVacia(txtFecha)) || (validacionService.esVacio(txtSubTotal)) || (validacionService.esVacio(txtTotal))  || (Float.valueOf(txtTotal.getText())<0) || dDv.size()==0) {
            if((Float.valueOf(txtTotal.getText())<0) )
            JOptionPane.showInternalMessageDialog(this.getRootPane(),"El monto total de la factura no puede ser negativo", "Aviso", JOptionPane.WARNING_MESSAGE);
            else
            JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getCompletarCampos(), "Aviso", JOptionPane.WARNING_MESSAGE);    
           
            
         } else {
         System.out.println("entre aqui");
            AlmacenService serviceA =new AlmacenService();
            Integer estado=1;
            //aca debo pasar el id del producto, devuelve una lista: o capacidad total del almcen y el stock es el 1
            for(int i=0;i<dDv.size();i++){
            List<Integer> capacidad_StockTotalProducto = serviceA.capacidad_StockTotalProducto(dDv.get(i).getToProducto().getIdproducto());
            Integer cantMax=capacidad_StockTotalProducto.get(1);
                if(cantMax<this.dDv.get(i).getCantidad()){
                    estado=0;
                    System.out.println("Acabo de salir");
                    cadenaMensaje+=dDv.get(i).getToProducto().getDescripcion()+" Stock Actual: "+cantMax+"\n";
                    
                }
            }
            
            if(estado==0){
            JOptionPane.showMessageDialog(this,cadenaMensaje, "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
            
            
      if(estado==1){        
       
            //Usamos el atributo creado para modificar el registro seleccionado
            
            Estado state= new Estado();
            Date fechaActual=new Date();
            ObjectContext context = DataContext.createDataContext();
            DocumentoVenta dv=(DocumentoVenta)context.newObject(DocumentoVenta.class);
            SolicitudVenta sv=(SolicitudVenta)context.newObject(SolicitudVenta.class);
            TipoDocumento td=(TipoDocumento)context.newObject(TipoDocumento.class);
            sv.setIdsolicitudventa(idSV);
            this.tipo=tipoD.get(this.cboxTipo.getSelectedIndex()).getIdtipodocumento();
            td.setIdtipodocumento(Integer.valueOf(this.tipo));
            
            dv.setToSolicitudVenta(sv);
            dv.setToTipoDocumento(td);
            
            if(!this.txtContacto.getText().isEmpty())            
                dv.setContacto(this.txtContacto.getText());
            else
                dv.setContacto("");
            
            if(!this.txtDNI.getText().isEmpty())            
                dv.setDni(this.txtDNI.getText());
            else
                dv.setDni("");
            
            if(!this.txtDireccion.getText().isEmpty())            
                dv.setDireccion(this.txtDireccion.getText());
            else
                dv.setDireccion("");
            
            dv.setFechaemision(fechaActual);
            dv.setMontototal(Float.valueOf(this.txtSubTotal.getText()));
            dv.setFechafacturacion(this.txtFecha.getDate());
            
            
            if(!this.txtIgv.getText().isEmpty())            
                dv.setIgv(Float.valueOf(this.txtIgv.getText()));
            else
                dv.setIgv(Float.valueOf("0"));
            //impuestos
            
            dv.setRetencion(Float.valueOf("0"));
            dv.setPercepcion(Float.valueOf("0"));
            dv.setDetraccion(Float.valueOf("0"));
                        
            if(!this.txtDescuento.getText().isEmpty())            
                dv.setDescuento(Float.valueOf(this.txtDescuento.getText()));
            else
                dv.setDescuento(Float.valueOf("0"));
            
            if(!this.txtTotal.getText().isEmpty())            
                dv.setTotal(Float.valueOf(this.txtTotal.getText()));
            else
                dv.setTotal(Float.valueOf("0"));
            
            
            dv.setEstado(state.getPendiente());
            
            System.out.println("Llegue hasta aki");
            DocumentoVentaService service= new DocumentoVentaService(); 
            service.insertar(dv,dDv,this.idNotaCredito);
           
           JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getRegistrado(),"Mensaje",JOptionPane.INFORMATION_MESSAGE);
            
           
          /* String nombreArchivo = "/tmp/factura.pdf";
            FileDialog fd = new FileDialog(new JFrame(), "Guardar", FileDialog.SAVE);
            fd.setDirectory(".");
            fd.setFile("*.pdf");
            fd.setVisible(true);
            nombreArchivo = fd.getDirectory() + fd.getFile();
           */
           
          // printFactura print=new printFactura(this.dDv,this.idDocumento,this.txtContacto.getText(), this.txtFecha.getDate().toString(), this.txtRuc.getText(),this.tipoDocumento,Float.valueOf(this.txtSubTotal.getText()),
           //Float.valueOf(this.txtIgv.getText()),Float.valueOf(this.txtDescuento.getText()),Float.valueOf(this.txtTotal.getText()),nombreArchivo,this.txtDireccion.getText(),this.txtNumDoc.getText());
        
     
        ventanaNuevo = new ModulosT_VEN_AV();            
            this.getParent().add(ventanaNuevo);
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = ventanaNuevo.getSize();
            ventanaNuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                    (desktopSize.height - jInternalFrameSize.height)/4 );
            ventanaNuevo.show();
        this.dispose();
        }
        }
        
       // System.out.println("No se como llegue aqui");
      }
        
       } catch(Exception e) {
            //e.printStackTrace();
           JOptionPane.showInternalMessageDialog(this.getRootPane(),"Ha ocurrido un problema de conexión, vuelvalo a intentar","Mensaje",JOptionPane.INFORMATION_MESSAGE);
        }
        
        
        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        ventanaNuevo = new ModulosT_VEN_AV();            
            this.getParent().add(ventanaNuevo);
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = ventanaNuevo.getSize();
            ventanaNuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                    (desktopSize.height - jInternalFrameSize.height)/4 );
            ventanaNuevo.show();
        this.dispose();
        
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnBuscarDistribuidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarDistribuidorActionPerformed
        final BusquedaDistribuidor seleccionarProductoForm =new BusquedaDistribuidor(null, true);
        seleccionarProductoForm.setLocationRelativeTo(this);
        seleccionarProductoForm.addWindowListener(new WindowAdapter() {
                        @Override
			public void windowClosed(WindowEvent e) {
                            if(seleccionarProductoForm.idDistribuidor!=null && seleccionarProductoForm.estoy=="distribuidor"){
                                if(seleccionarProductoForm.idDistribuidor>0){
                                idDistribuidor=seleccionarProductoForm.idDistribuidor;
                                txtDistribuidor.setText(seleccionarProductoForm.distribuidor);
                                txtContacto.setText(seleccionarProductoForm.distribuidor);
                                txtRuc.setText(seleccionarProductoForm.ruc);
                               //con esto paro el evento
                                seleccionarProductoForm.idDistribuidor=0;
                              }
                            }
                            //this;
			}
		});
        seleccionarProductoForm.setVisible(true);
    }//GEN-LAST:event_btnBuscarDistribuidorActionPerformed

    private void btnBuscarSolicitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarSolicitudActionPerformed
       final BusquedaSolicitudPedido seleccionarProductoForm1 =new BusquedaSolicitudPedido(null, false);
        seleccionarProductoForm1.setLocationRelativeTo(this);
        seleccionarProductoForm1.addWindowListener(new WindowAdapter() {
                        @Override
			public void windowClosed(WindowEvent e) {
                            if(seleccionarProductoForm1.idSolicitudVenta!=null && seleccionarProductoForm1.estoy=="solicitudVenta"){
                                if(seleccionarProductoForm1.idSolicitudVenta>0){
                                idDistribuidor=seleccionarProductoForm1.idDistribuidor;
                                idSV=seleccionarProductoForm1.idSolicitudVenta;
                                txtDistribuidor.setText(seleccionarProductoForm1.distribuidor);
                                txtContacto.setText(seleccionarProductoForm1.distribuidor);
                                txtRuc.setText(seleccionarProductoForm1.ruc);
                                labeContacto.setText("Razón Social:");
                                labelDNI.setVisible(false);
                                txtDNI.setVisible(false);
                                txtDireccion.setText(seleccionarProductoForm1.direccion);
                                txtPedido.setText("Solicitud #"+seleccionarProductoForm1.idSolicitudVenta);
                                 SolicitudVentaService service = new SolicitudVentaService();
                               List<DetalleSolicitudVenta> dsv= new ArrayList<DetalleSolicitudVenta>();
                               System.out.println("Sorprendentement ingrese aqui");
                               dsv=service.buscarFiltrado2(idSV);
                               dDv.clear();         
                               btnNuevo.setEnabled(false);
                               btnEliminarA.setEnabled(false);
                               incrustarDSVaDDV(dsv);
                               dsv.clear();
                               btnBuscarDistribuidor.setVisible(false);
                               
                               //con esto paro el evento
                                seleccionarProductoForm1.idSolicitudVenta=0;
                                    }
                                }
                            //this;
			}
		});
        seleccionarProductoForm1.setVisible(true);
    }//GEN-LAST:event_btnBuscarSolicitudActionPerformed

    private void cboxTipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboxTipoItemStateChanged
        if(this.cboxTipo.getSelectedIndex()==1){
        inhabilitaBotonesBoleta();
        }else{
        if(this.cboxTipo.getSelectedIndex()==2)
            inhabilitaBotonesFactura();
        else 
            inhabilitaBotonesFactura();
        }
        
        
        
    }//GEN-LAST:event_cboxTipoItemStateChanged

    private void cboxTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxTipoActionPerformed
        if(this.cboxTipo.getSelectedIndex()==1){
        inhabilitaBotonesBoleta();
        }else{
        if(this.cboxTipo.getSelectedIndex()==2)
            inhabilitaBotonesFactura();
        else 
            inhabilitaBotonesFactura();
        }
    }//GEN-LAST:event_cboxTipoActionPerformed

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed
        int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(),"¿Está seguro que desea eliminar el registro?","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

        if (JOptionPane.OK_OPTION == confirmar)
        {//Obtengo el objeto seleccionado
            DocumentoVentaService serv = new DocumentoVentaService();
            DetalleDocumentoVenta dist = dDv.get(this.tableResultados.getSelectedRow());
            this.dDv.remove(this.tableResultados.getSelectedRow());
            
        }

        this.RefreshTable();
        this.inhabilitaBotonesList();
    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void btnBuscarNotaCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarNotaCreditoActionPerformed
        final BusquedaNotaCredito seleccionarNotaCreditoFormx =new BusquedaNotaCredito(null, false, this.txtContacto.getText());
        seleccionarNotaCreditoFormx.setLocationRelativeTo(this);
        seleccionarNotaCreditoFormx.addWindowListener(new WindowAdapter() {
                        @Override
			public void windowClosed(WindowEvent e) {
                            if(seleccionarNotaCreditoFormx.idNotaCredito!=null && seleccionarNotaCreditoFormx.estoy=="notacredito"){
                                if(seleccionarNotaCreditoFormx.idNotaCredito>0){
                                idNotaCredito=seleccionarNotaCreditoFormx.idNotaCredito;
                                txtDescuento.setText(String.valueOf(seleccionarNotaCreditoFormx.notacredito));
                                
                               
                               //con esto paro el evento
                                seleccionarNotaCreditoFormx.idNotaCredito=0;
                                    }
                                }
                            //this;
			}
		});
        seleccionarNotaCreditoFormx.setVisible(true);
    }//GEN-LAST:event_btnBuscarNotaCreditoActionPerformed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        try {
            
            SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
                String fecha2=formatoDeFecha.format(this.txtFecha.getDate());
            String nombreArchivo = "/tmp/factura.pdf";
            FileDialog fd = new FileDialog(new JFrame(), "Guardar", FileDialog.SAVE);
            fd.setDirectory(".");
            fd.setFile("*.pdf");
            fd.setVisible(true);
            nombreArchivo = fd.getDirectory() + fd.getFile();
            if(!fd.getFile().equals("*.pdf")){
                
            printFactura print=new printFactura(this.dDv,this.idDocumento,this.txtContacto.getText(), fecha2, this.ruc,this.tipoDocumento,Float.valueOf(this.txtSubTotal.getText()),
           Float.valueOf(this.txtIgv.getText()),Float.valueOf(this.txtDescuento.getText()),Float.valueOf(this.txtTotal.getText()),nombreArchivo,this.txtDireccion.getText(),this.txtNumDoc.getText());
            }
            } catch (Exception ex) {
            Logger.getLogger(ModulosT_VEN_AVDocNuevo.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }//GEN-LAST:event_btnImprimirActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        inhabilitaBotonesList();
        this.txtDescuento.setText("");
        inhabilitaBotonesBoleta();
        DocumentoVentaService service=new DocumentoVentaService();
        //this.tipoD=service.buscarTipoDocumento();
        this.parametro=service.parametros();
        //addList(tipoD);
        this.idDocumento=0;
        this.ruc="0";
    
        Date fechaActual=new Date();
        this.txtFecha.setDate(fechaActual);
        this.txtDistribuidor.setText("");
        this.txtContacto.setText("");
        this.txtDNI.setText("");
        this.txtDireccion.setText("");
        this.txtSubTotal.setText("");
        this.txtDescuento.setText("");
        this.txtIgv.setText("");
        this.txtRuc.setText("");
        this.txtTotal.setText("");
        this.tipoDocumento=1;
        this.labelDNI.setVisible(true);
        this.txtDNI.setVisible(true);
        this.txtContacto.setText("Razón Social:");
        this.cboxTipo.setSelectedIndex(0);
        this.btnNuevo.setEnabled(true);
        this.btnBuscarDistribuidor.setVisible(true);
        dDv.clear();
        this.idDistribuidor=0;
        
        this.actualizarProductosList();
        
        this.idSD=0;
        
        if(this.tipo==2){
        this.inhabilitarTodo();
    this.btnImprimir.setVisible(true);
        this.actualizarAllList();
        }
    }//GEN-LAST:event_btnLimpiarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarDistribuidor;
    private javax.swing.JButton btnBuscarNotaCredito;
    private javax.swing.JButton btnBuscarSolicitud;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox cboxTipo;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labeContacto;
    private javax.swing.JLabel labelDNI;
    private javax.swing.JLabel labelDNI2;
    public javax.swing.JTable tableResultados;
    private javax.swing.JTextField txtContacto;
    private javax.swing.JTextField txtDNI;
    private javax.swing.JTextField txtDescuento;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtDistribuidor;
    private org.jdesktop.swingx.JXDatePicker txtFecha;
    private javax.swing.JTextField txtIgv;
    private javax.swing.JLabel txtNumDoc;
    private javax.swing.JTextField txtPedido;
    private java.awt.Label txtRUC;
    private java.awt.Label txtRuc;
    private javax.swing.JTextField txtSubTotal;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
}
