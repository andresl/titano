/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.ventas;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroSV;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.SolicitudVenta;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.service.DistribuidorService;
import dp1.titandevelop.titano.service.SolicitudVentaService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Heli
 */
public class ModulosT_VEN_ASP extends javax.swing.JInternalFrame {

    private Mensajes sms = new Mensajes();
    public ModulosT_VEN_ASPDetalleN nuevo=null;
    public ModulosT_VEN_ASPDetalleN editar=null;
    //public ModulosT_VEN_ASPDetalleD detalle=null;
    public int estadoNuevo = 0;    
    public FiltroSV busqueda=null;
    //Lista usada de tipos de movimiento
    public List<SolicitudVenta> solicitudes =new  ArrayList<SolicitudVenta>();
    //metodo para refrescar la tabla
    public void RefreshTable() {
        
        AbstractTableModel aux = (AbstractTableModel)tableResultados.getModel();
        aux.fireTableDataChanged();
    }
    
    public void habilitaBotones(){
        this.btnEditarA.setEnabled(true);
        this.btnEliminarA.setEnabled(true);
        this.btnDetalle.setEnabled(true);
    }
    private void inhabilitaBotones(){
        this.btnEditarA.setEnabled(false);
        this.btnEliminarA.setEnabled(false);
        this.btnDetalle.setEnabled(false);
    }
    
    public FiltroSV filtro(){
        FiltroSV f=new FiltroSV();
        f.setFechaIni(this.txtFechaInicio.getDate());
        f.setFechaFin(this.txtFechaFin.getDate());
        f.setDistribuidor(this.txtDistribuidor.getText());
        
        Estado state=new Estado();
        f.setEstado(state.getEstadoId(this.cboxEstado.getSelectedItem().toString()));
        return f;
    }
    
    public void llenarFiltro(FiltroSV f){
        this.txtFechaInicio.setDate(f.getFechaIni());
        this.txtFechaFin.setDate(f.getFechaFin());
        this.txtDistribuidor.setText(f.getDistribuidor());
        this.cboxEstado.setSelectedIndex(f.getEstado());
        
    }
    
    public void  buscar(FiltroSV f){
        SolicitudVentaService serv = new SolicitudVentaService();
        solicitudes = serv.buscarFiltrado(f);
        if(solicitudes!=null)
        this.actualizarDistribuidorList();
    }
    
    Thread actualizardistribuidor = null;
    private void actualizarDistribuidorList(){
        if( actualizardistribuidor!=null && actualizardistribuidor.isAlive() ){
            actualizardistribuidor.interrupt();
        }
        actualizardistribuidor = new Thread( new Runnable() {
            @Override
            public void run() {
                RefreshTable();
                ModeloTabla modelo= new ModeloTabla();
                tableResultados.setModel(modelo);
                
            }
        });
        actualizardistribuidor.start();
    }
    
    class ModeloTabla extends AbstractTableModel {  
        //Atributos de los datos de la tabla
        private String [] titles = {"Fecha","Distribuidor", "Estado"};
        //private List<DataTabla> datos;
        //private Object [][]datos;
        
        public int getRowCount() {
            return solicitudes.size();//tipos.size(); // cuantos registros (filas)
        }      
        
        public int getColumnCount() {
            return 3; // cuantas columnas
        }          
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }                
       
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res=null;
            Estado state=new Estado();
            SolicitudVenta s = solicitudes.get(rowIndex);
            //boolean b = datos.get(rowIndex).seleccionado;//tipos.get(rowIndex);
            switch(columnIndex){
                case 0: res = s.getFecha(); break;
                case 1: res = s.getToDistribuidor().getRazonsocial(); break;
                case 2: res= state.devuelveCadena(s.getEstado()); break;
                //case 2: res = b;
            }
            return res;
        } 
        //Ultima celda editable para selección
        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res=false;
            switch (col) {
                case 0:break;
                case 1:break;
               // case 2:res=true;
            }
            return res;
        }
        //para obtener las clases de cada celda y asi mostrar estas correctamente
        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
//        }
    }

    public ModulosT_VEN_ASP() {
        initComponents(); 
        ValidacionService validacionService = new ValidacionService();
        validacionService.NoEscribe(this.txtFechaInicio);
        validacionService.NoEscribe(this.txtFechaFin);
        validacionService.SLetras(this.txtDistribuidor);
        
        
        this.inhabilitaBotones();
        Estado state = new Estado();
        this.cboxEstado.addItem(state.devuelveCadena(state.getPendiente()));
        this.cboxEstado.addItem(state.devuelveCadena(state.getCancelado()));
        this.cboxEstado.addItem(state.devuelveCadena(state.getAceptado()));
        
        this.cboxEstado.setSelectedItem(state.devuelveCadena(state.getPendiente()));
    }
    
    public ModulosT_VEN_ASP(FiltroSV sv) {
        initComponents(); 
        ValidacionService validacionService = new ValidacionService();
        validacionService.NoEscribe(this.txtFechaInicio);
        validacionService.NoEscribe(this.txtFechaFin);
        validacionService.SLetras(this.txtDistribuidor);
        
        Estado state = new Estado();
        this.cboxEstado.addItem(state.devuelveCadena(state.getPendiente()));
        this.cboxEstado.addItem(state.devuelveCadena(state.getCancelado()));
        this.cboxEstado.addItem(state.devuelveCadena(state.getAceptado()));
        this.cboxEstado.setSelectedItem(state.devuelveCadena(state.getPendiente()));
        
//        this.cboxEstado.setSelectedItem(sv.getEstado());
        
        this.inhabilitaBotones();
        if(sv!=null)
            this.busqueda=sv;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtDistribuidor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtFechaInicio = new org.jdesktop.swingx.JXDatePicker();
        txtFechaFin = new org.jdesktop.swingx.JXDatePicker();
        jLabel7 = new javax.swing.JLabel();
        cboxEstado = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResultados = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnDetalle = new javax.swing.JButton();
        btnEditarA = new javax.swing.JButton();
        btnEliminarA = new javax.swing.JButton();

        setResizable(true);
        setTitle("Administración de Solicitud de Pedido");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtro de Búsqueda"));

        jLabel5.setText("Fecha Inicio");

        jLabel6.setText("Estado");

        jLabel8.setText("Distribuidor");

        jLabel7.setText("Fecha Fin");

        cboxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));
        cboxEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboxEstadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6)))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtDistribuidor, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(93, 93, 93))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDistribuidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cboxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados de Búsqueda"));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Distribuidor", "Estado"
            }
        ));
        tableResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableResultados);
        tableResultados.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 444, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        btnLimpiar.setToolTipText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/detalle.png"))); // NOI18N
        btnDetalle.setToolTipText("Ver Detalle");
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        btnEditarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/editar.png"))); // NOI18N
        btnEditarA.setToolTipText("Editar");
        btnEditarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarAActionPerformed(evt);
            }
        });

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addComponent(btnNuevo)
                                .addGap(1, 1, 1)
                                .addComponent(btnLimpiar)
                                .addGap(0, 0, 0)
                                .addComponent(btnSalir))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDetalle)
                        .addGap(0, 0, 0)
                        .addComponent(btnEditarA)
                        .addGap(0, 0, 0)
                        .addComponent(btnEliminarA)))
                .addGap(8, 8, 8))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSalir)
                    .addComponent(btnNuevo)
                    .addComponent(btnBuscar)
                    .addComponent(btnLimpiar))
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnEditarA)
                        .addComponent(btnEliminarA))
                    .addComponent(btnDetalle, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed

         //ID 30 Nuevo Solicitud Pedido
        DistribuidorService service=new DistribuidorService();
        int cant=service.listDistribuidor().size();
        if(cant>0){
        
        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (30 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                if (estadoNuevo==0){
            estadoNuevo=1;
            try{
            nuevo = new ModulosT_VEN_ASPDetalleN();            
            this.getParent().add(nuevo);
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = nuevo.getSize();
            nuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                    (desktopSize.height - jInternalFrameSize.height)/4 );
            
                nuevo.show();
            }catch(Exception e){
                JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
            }
            
            this.dispose();
            break;
        }
            }
            
            }
         if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
       }else{
        JOptionPane.showMessageDialog(this, "No puedes proceder, no tienes distribuidores.",
                    "Aviso",
                    JOptionPane.WARNING_MESSAGE);
        
        }
        
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        try{
        SolicitudVenta sv = new SolicitudVenta();  
        FiltroSV f=null;
        f=this.filtro();
        this.buscar(f);
        this.inhabilitaBotones();
        }catch(Exception e){
        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        this.txtFechaInicio.setDate(null);
        this.txtFechaFin.setDate(null);
        this.txtDistribuidor.setText("");
        cboxEstado.setSelectedIndex(0);
        this.solicitudes.clear();
        this.actualizarDistribuidorList();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void tableResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultadosMouseClicked
        if(evt.getClickCount()==1){
            this.inhabilitaBotones();
            if(solicitudes.get(this.tableResultados.getSelectedRow()).getEstado()==7)
            this.btnDetalle.setEnabled(true);
            else
                this.habilitaBotones();
        }
    }//GEN-LAST:event_tableResultadosMouseClicked

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed
        try{
        SolicitudVenta sol;
        FiltroSV busqueda=null;
        busqueda=this.filtro();
        sol = solicitudes.get(this.tableResultados.getSelectedRow());
        //Instancio la ventana
        editar = new ModulosT_VEN_ASPDetalleN(sol,busqueda,2);
        this.getParent().add(editar);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = editar.getSize();
        editar.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        editar.show();

        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
        }catch(Exception e){
        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
        }
        this.dispose();
    }//GEN-LAST:event_btnDetalleActionPerformed

    private void btnEditarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarAActionPerformed
           //ID 31 Nuevo Solicitud Pedido

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (31 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                 SolicitudVenta sol;
        FiltroSV busqueda=null;
        busqueda=this.filtro();
        sol = solicitudes.get(this.tableResultados.getSelectedRow());
        //Instancio la ventana
        try{
        editar = new ModulosT_VEN_ASPDetalleN(sol,busqueda,1);
        this.getParent().add(editar);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = editar.getSize();
        editar.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        editar.show();
        }catch(Exception e){
        JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
        }
        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
        this.dispose();
        break;
            }
        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnEditarAActionPerformed

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed
        
               //ID 31 Guardar/Eliminar/Editar Pedido

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (31 == (Integer) LoginT.idspermisosxperfil.get(i)) {
            int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(),"¿Está seguro que desea eliminar el registro?","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

        if (JOptionPane.OK_OPTION == confirmar)
        {//Obtengo el objeto seleccionado
            try{
            SolicitudVentaService serv = new SolicitudVentaService();
            SolicitudVenta sol = solicitudes.get(this.tableResultados.getSelectedRow());
            this.solicitudes.remove(this.tableResultados.getSelectedRow());
            
            serv.eliminar(sol.getIdsolicitudventa());
            }catch(Exception e){
            JOptionPane.showInternalMessageDialog(this.getRootPane(),sms.getError(),"Error",JOptionPane.ERROR_MESSAGE);
            }
        }

        this.RefreshTable();
        this.inhabilitaBotones();
        break;
            }
            }
        
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
        
    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void cboxEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboxEstadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboxEstadoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton btnEditarA;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboxEstado;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    public javax.swing.JTable tableResultados;
    private javax.swing.JTextField txtDistribuidor;
    private org.jdesktop.swingx.JXDatePicker txtFechaFin;
    private org.jdesktop.swingx.JXDatePicker txtFechaInicio;
    // End of variables declaration//GEN-END:variables
}
