/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.sec;
import dp1.titandevelop.titano.view.ventas.*;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.service.DistribuidorService;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.ParametroService;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author heli
 */
public class reporte_auditoria {
    
    private static Font fontBold = new Font(Font.FontFamily.COURIER, 12, Font.BOLD);
    private static Font fontNormal = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL);
    private String nomArch;
     public reporte_auditoria(List<String> fechas,List<String> username, List<String> perfiles,List<String> tablas,List<String> acciones,String nombreArchivo) throws Exception{
		
                this.nomArch = nombreArchivo;   
		Document document = new Document();
		PdfWriter pdfWriter =PdfWriter.getInstance(document, 
			new FileOutputStream(nombreArchivo));
		document.open();
		
                      document.add(getHeader("Reporte de Auditoria  "));
                      
                      document.add(getInformation(" "));
                    
                      Image image1 = Image.getInstance(getClass().getResource("/img/GOOD_COOKIE.jpg"));
                      PdfPTable table4 = new PdfPTable(1); // Code 1
                PdfPCell cell001y = 
			new PdfPCell(image1);
                        cell001y.setBorder(0);
                table4.addCell(cell001y);
                
                document.add(table4);
                document.add(getInformation(" "));
                      
                PdfPTable table = new PdfPTable(5);
                
                        PdfPCell cell00 = new PdfPCell(new Paragraph(cabeceraTable("Fecha"))); 
                        table.addCell(cell00);
                
                        PdfPCell cell01 = new PdfPCell(new Paragraph(cabeceraTable("Username"))); 
                        table.addCell(cell01);
                        
                        PdfPCell cell02 = new PdfPCell(new Paragraph(cabeceraTable("Perfil"))); 
                        table.addCell(cell02);
                        
                        PdfPCell cell03 = new PdfPCell(new Paragraph(cabeceraTable("Tabla"))); 
                        table.addCell(cell03);
                        
                        PdfPCell cell04 = new PdfPCell(new Paragraph(cabeceraTable("Accion"))); 
                        table.addCell(cell04);
                        for(int i=0;i<fechas.size();i++){
                            
                        table.addCell(fechas.get(i));
                        table.addCell(username.get(i));
                        table.addCell(perfiles.get(i));
                        table.addCell(tablas.get(i));
                        table.addCell(acciones.get(i));
                        }
                 document.add(table);
                    document.close();
               // imprimir();
	}
     
     private Chunk cabeceraTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.setFont(fontBold);c.getFont().setSize(10);
         c.getFont().setFamily("Arial");
      return c;
     }
     
     private Chunk dataTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.getFont().setSize(8);
         c.getFont().setFamily("Arial");
      return c;
     }
     private PdfPCell celda(String cadena){
     
         PdfPCell cell = new PdfPCell(new Paragraph(dataTable(cadena))); 
         return cell;
     }
     
     private Paragraph getHeader(String header) {
    	Paragraph paragraph = new Paragraph();
  		Chunk chunk = new Chunk();
                
		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append( header + getCurrentDateTime() + "\n");
  		chunk.setFont(fontBold);
                chunk.getFont().setFamily("Arial");
  		paragraph.add(chunk);
  		return paragraph;
     }
     private Paragraph getInformation(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append(informacion);
  		chunk.setFont(fontNormal);
                
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformation2(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_LEFT);
  		chunk.append(informacion);
                chunk.setFont(fontBold);
  		//chunk.setFont(fontNormal);
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformationFooter(String informacion) {
     	Paragraph paragraph = new Paragraph();
     	Chunk chunk = new Chunk();
   		paragraph.setAlignment(Element.ALIGN_CENTER);
   		chunk.append(informacion);
   		chunk.setFont(new Font(Font.FontFamily.COURIER, 8, Font.NORMAL));
   		paragraph.add(chunk);
    		return paragraph;
       }
      private float getConvertCmsToPoints(float cm) {
     	return cm * 28.4527559067f;
     }
     
   public void imprimir(){
     
     Desktop d=Desktop.getDesktop();
     try {
      if(Desktop.isDesktopSupported()){
          //d.open(new File("reporte-auditoria.pdf"));
          d.open(new File(this.nomArch));
      // d.print(new File("factura2.pdf"));
      }
     } catch (IOException e) {
     // TODO Auto-generated catch block
    e.printStackTrace();
        }
   }
      
      
     private String getCurrentDateTime() {
     	Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yy '-' hh:mm");
     	return ft.format(dNow);
    }
	public static void main(String[] args,List<String> fechas,List<String> username, List<String> perfiles,List<String> tablas,List<String> acciones,String nombreArchivo) {	
		try{
			reporte_auditoria  pdfTable = new reporte_auditoria(fechas,username,perfiles,tablas,acciones,nombreArchivo);
		}catch(Exception e){
			System.out.println(e);
		}
	}
        
}
