/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.sec;

import dp1.titandevelop.titano.persistent.Perfil;
import dp1.titandevelop.titano.persistent.PermisoXPerfil;
import dp1.titandevelop.titano.persistent.Usuario;
import dp1.titandevelop.titano.service.PerfilService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chrestean
 */
public class ModulosT_SEG_APerfil extends javax.swing.JInternalFrame {

    public List<Perfil> listaPerfiles;
    public ModulosT_SEG_APerfilEdit ventanaEdita;

    public void RefreshTable() {

        AbstractTableModel aux = (AbstractTableModel) tablaPerfiles.getModel();
        aux.fireTableDataChanged();
    }

    public void habilitaBotones() {
        this.btnEditar.setEnabled(true);
        this.btnEliminar.setEnabled(true);
    }

    private void inhabilitaBotones() {
        this.btnEditar.setEnabled(false);
        this.btnEliminar.setEnabled(false);
    }
    //Modelo de la tabla 
    private AbstractTableModel ModeloTabla = new AbstractTableModel() {
        //Atributos de los datos de la tabla
        private String[] titles = {"Descripcion"};

        public int getRowCount() {
            return listaPerfiles.size();//tipos.size(); // cuantos registros (filas)
        }

        public int getColumnCount() {
            return 1; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Object resultado = null;
            Perfil perf = listaPerfiles.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    resultado = perf.getDescripcion();
                    break;

            }
            return resultado;
        }
    };

    // Fin del modelo de la tabla Permisos
    public ModulosT_SEG_APerfil() {
        initComponents();
        Integer longitudMaximaPermitida = 30;
        
        this.listaPerfiles = new ArrayList<Perfil>();
        this.tablaPerfiles.setModel(ModeloTabla);
        ValidacionService validacionService = new ValidacionService();
        validacionService.SLetras(this.txtPerfil);
        validacionService.Longitud(this.txtPerfil, longitudMaximaPermitida);
        inhabilitaBotones();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaPerfiles = new javax.swing.JTable();
        btnSalir = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblPerfil = new javax.swing.JLabel();
        txtPerfil = new javax.swing.JTextField();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();

        setResizable(true);
        setTitle("Administrar Perfiles");
        setMaximumSize(new java.awt.Dimension(550, 450));
        setMinimumSize(new java.awt.Dimension(550, 450));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(550, 450));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultado de Búsqueda"));
        jPanel2.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        tablaPerfiles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Descripcion Perfil"
            }
        ));
        tablaPerfiles.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaPerfilesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaPerfiles);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        btnLimpiar.setToolTipText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros de Búsqueda"));
        jPanel1.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        lblPerfil.setText("Descripcion");

        txtPerfil.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPerfil)
                .addGap(18, 18, 18)
                .addComponent(txtPerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPerfil)
                    .addComponent(txtPerfil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/editar.png"))); // NOI18N
        btnEditar.setToolTipText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminar.setToolTipText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnEditar)
                        .addGap(1, 1, 1)
                        .addComponent(btnEliminar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(2, 2, 2)
                            .addComponent(btnBuscar)
                            .addGap(1, 1, 1)
                            .addComponent(btnNuevo)
                            .addGap(0, 0, 0)
                            .addComponent(btnLimpiar)
                            .addGap(0, 0, 0)
                            .addComponent(btnSalir))
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnEliminar)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscar)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnNuevo)
                                .addComponent(btnLimpiar)
                                .addComponent(btnSalir)))
                        .addGap(8, 8, 8)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed

        //ID 11 Nuevo Perfil

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (11 == (Integer) LoginT.idspermisosxperfil.get(i)) {

                ModulosT_SEG_APerfilNuevo admPermisoNew = new ModulosT_SEG_APerfilNuevo();
                this.getParent().add(admPermisoNew);
                Dimension desktopSize = this.getParent().getSize();
                Dimension jInternalFrameSize = admPermisoNew.getSize();
                admPermisoNew.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                        (desktopSize.height - jInternalFrameSize.height) / 4);
                admPermisoNew.show();
                this.dispose();


                break;
            }

        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }






    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

        PerfilService perfil = new PerfilService();
        this.listaPerfiles = perfil.buscarPerfiles(this.txtPerfil.getText());
        this.RefreshTable();

    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        Perfil perfilSeleccionado;
        perfilSeleccionado = listaPerfiles.get(this.tablaPerfiles.getSelectedRow());

        ventanaEdita = new ModulosT_SEG_APerfilEdit(perfilSeleccionado);
        ModulosT_SEG_APerfilEdit.txtIDPerfil.setText(String.valueOf(perfilSeleccionado.getIdperfil()));
        ModulosT_SEG_APerfilEdit.txtPerfil.setText(perfilSeleccionado.getDescripcion());

        this.getParent().add(ventanaEdita);
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ventanaEdita.getSize();
        ventanaEdita.setLocation((desktopSize.width - jInternalFrameSize.width) / 2, (desktopSize.height - jInternalFrameSize.height) / 4);
        ventanaEdita.show();
        this.dispose();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
     //ID 12 = Eliminar/Guardar
        
        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (12 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(), "¿Está seguro que desea eliminar el registro?", "Aviso!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

                if (JOptionPane.OK_OPTION == confirmar) {
                    PerfilService perfilservice = new PerfilService();
                    Integer idperfil = this.listaPerfiles.get(this.tablaPerfiles.getSelectedRow()).getIdperfil();
                    if (perfilservice.buscarPerfilUsado(idperfil)) {
                        perfilservice.eliminaPerfil(idperfil);
                        this.RefreshTable();
                        this.inhabilitaBotones();
                    } else {
                        JOptionPane.showMessageDialog(this, "No puedes proceder, existen usuarios asociados al perfil seleccionado.",
                                "Error Eliminar",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                 break;
            }
           
        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }



    }//GEN-LAST:event_btnEliminarActionPerformed

    private void tablaPerfilesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaPerfilesMouseClicked
        if (evt.getClickCount() == 1) {
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tablaPerfilesMouseClicked

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
       this.txtPerfil.setText("");
       this.listaPerfiles = new ArrayList<Perfil>();
       this.RefreshTable();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblPerfil;
    private javax.swing.JTable tablaPerfiles;
    private javax.swing.JTextField txtPerfil;
    // End of variables declaration//GEN-END:variables
}
