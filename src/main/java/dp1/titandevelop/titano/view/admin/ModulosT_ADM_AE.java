/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.admin;

import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.PermisoXPerfil;
import dp1.titandevelop.titano.persistent.TipoEmpleado;
import dp1.titandevelop.titano.persistent.Usuario;
import dp1.titandevelop.titano.service.EmpleadoService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import dp1.titandevelop.titano.view.MensajeError;
import dp1.titandevelop.titano.view.logist.ModulosT_LOG;
import dp1.titandevelop.titano.view.logist.ModulosT_LOG_APNuevo;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chrestean
 */
public class ModulosT_ADM_AE extends javax.swing.JInternalFrame {

    public List<Empleado> empleados;
    //Para el manejo del cbox
    public List<TipoEmpleado> tipos;
    
    private void addList (List<TipoEmpleado> lista){
        
        for(int i=0;i<lista.size();i++){
            this.cboxtipo.addItem(lista.get(i).getDescripcion());
        }
    }
    public void RefreshTable() {
        AbstractTableModel aux = (AbstractTableModel)tblResultados.getModel();
        aux.fireTableDataChanged();
    }
    public void habilitaBotones(){
        this.btnEditar.setEnabled(true);
        this.btnEliminar.setEnabled(true);
    }
    private void inhabilitaBotones(){
        this.btnEditar.setEnabled(false);
        this.btnEliminar.setEnabled(false);
    }
    
    
    private AbstractTableModel modeloTabla = new AbstractTableModel() {  
        
        public int getRowCount() {
            return empleados.size(); // cuantos registros (filas)
        }      
        
        public int getColumnCount() {
            return 4; // cuantas columnas
        }  
        
        String [] titles = {"DNI", "Nombre", "Ap. Paterno", "Tipo Emp."};
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }        
          
        public Object getValueAt(int rowIndex, int columnIndex) {
            String res=null;
            Empleado p = empleados.get(rowIndex);
            switch(columnIndex){
                case 0: res = p.getDni(); break;
                case 1: res = p.getNombres();break;
                case 2: res = p.getApellidop(); break;
                case 3: res = p.getToTipoEmpleado().getDescripcion(); break;
            }
            return res;
        }  
    };
    public ModulosT_ADM_AE() {
        initComponents();
        ValidacionService validacionService = new ValidacionService();
        validacionService.SSoloLetra(this.textApPaterno);
        validacionService.SSoloLetra(this.textNombre);
        validacionService.SNumeros(this.textDNI);
        validacionService.Longitud(textDNI, 8);
        this.empleados=new ArrayList<Empleado>();
        this.tblResultados.setModel(modeloTabla);
        inhabilitaBotones();
        
       EmpleadoService serv = new EmpleadoService();
       tipos = serv.buscarTEmpSinOperario();
       addList(tipos);
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        botonBuscar = new javax.swing.JButton();
        botonLimpiar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblResultados = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        textNombre = new javax.swing.JTextField();
        textApPaterno = new javax.swing.JTextField();
        textDNI = new javax.swing.JTextField();
        cboxsexo = new javax.swing.JComboBox();
        cboxtipo = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        botonSalir = new javax.swing.JButton();
        botonNuevo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setResizable(true);
        setTitle("Administrar Empleado");
        setMaximumSize(new java.awt.Dimension(650, 440));
        setMinimumSize(new java.awt.Dimension(650, 440));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(650, 440));

        botonBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        botonBuscar.setToolTipText("Buscar");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        botonLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        botonLimpiar.setToolTipText("Limpiar");
        botonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiarActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultado de Búsqueda"));
        jPanel2.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        tblResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "DNI", "Nombre", "Ap. Paterno", "Tipo Empleado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblResultados);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros de Búsqueda"));
        jPanel1.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        jLabel9.setText("Nombre");

        jLabel10.setText("Ap. Paterno");

        jLabel12.setText("DNI");

        jLabel13.setText("Sexo");

        textNombre.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        textApPaterno.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        textDNI.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        cboxsexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ambos", "Masculino", "Femenino" }));

        cboxtipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));

        jLabel14.setText("Tipo Emp.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(31, 31, 31)
                        .addComponent(textNombre))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textApPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(textDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboxtipo, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cboxsexo, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                        .addComponent(jLabel9)
                        .addComponent(jLabel12)
                        .addComponent(textNombre)
                        .addComponent(textDNI))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cboxtipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14)))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textApPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboxsexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel13))
                .addGap(8, 8, 8))
        );

        botonSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        botonSalir.setToolTipText("Salir");
        botonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSalirActionPerformed(evt);
            }
        });

        botonNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo.png"))); // NOI18N
        botonNuevo.setToolTipText("Nuevo");
        botonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevoActionPerformed(evt);
            }
        });

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/editar.png"))); // NOI18N
        btnEditar.setToolTipText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminar.setToolTipText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/texto.png"))); // NOI18N
        jButton1.setActionCommand("masivo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnEditar)
                .addGap(1, 1, 1)
                .addComponent(btnEliminar)
                .addGap(8, 8, 8))
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botonBuscar)
                        .addGap(0, 0, 0)
                        .addComponent(botonNuevo)
                        .addGap(0, 0, 0)
                        .addComponent(botonLimpiar)
                        .addGap(1, 1, 1)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(botonSalir)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(8, 8, 8))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(botonBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonLimpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonSalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEditar)
                    .addComponent(btnEliminar))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevoActionPerformed
        
        
        //ID 62 Nueva Empleado

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (62 == (Integer) LoginT.idspermisosxperfil.get(i)) {
              ModulosT_ADM.ventanaNuevo = new ModulosT_ADM_AENuevo();
        this.getParent().add(ModulosT_ADM.ventanaNuevo);
        
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ModulosT_ADM.ventanaNuevo.getSize();
        ModulosT_ADM.ventanaNuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height)/4 );
        ModulosT_ADM.ventanaNuevo.show();
        
        this.dispose();
        break;
            }
            }
          if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
      
       
        
    }//GEN-LAST:event_botonNuevoActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        //Obtengo el objeto seleccionado
        Empleado emplea;
        emplea = empleados.get(this.tblResultados.getSelectedRow());
        
        //Instancio la ventana
        ModulosT_ADM.ventanaEdita = new ModulosT_ADM_AEEdit(emplea);
        this.getParent().add(ModulosT_ADM.ventanaEdita);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ModulosT_ADM.ventanaEdita.getSize();
        ModulosT_ADM.ventanaEdita.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        ModulosT_ADM.ventanaEdita.show();

        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
  //ID 63 Editar/GuardR/Eliminar Empleado

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (63 == (Integer) LoginT.idspermisosxperfil.get(i)) {
                  int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(),"¿Está seguro que desea eliminar el registro?","Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
              try{
                if (JOptionPane.OK_OPTION == confirmar)
                {//Obtengo el objeto seleccionado
                    EmpleadoService serv = new EmpleadoService();
                    int idEmp =this.empleados.get(this.tblResultados.getSelectedRow()).getIdempleado();
                    this.empleados.remove(this.tblResultados.getSelectedRow());            
                    serv.eliminarEmpleado(idEmp);
                }


                } catch(Exception e) {
                    //e.printStackTrace();
                    MensajeError dialog = new MensajeError(new javax.swing.JFrame(), true);
                    dialog.setLocationRelativeTo(null);
                    dialog.setVisible(true);
                }
      this.RefreshTable();
        this.inhabilitaBotones(); 
        break;
            }
            }
          if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
      
        
        
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        EmpleadoService serv = new EmpleadoService();
        ValidacionService val = new ValidacionService();

        if (this.cboxtipo.getSelectedIndex() - 1 == -1){
            if (this.cboxsexo.getSelectedIndex() ==0){
                empleados = serv.buscarEmpleado(this.textDNI.getText(),this.textNombre.getText(),this.textApPaterno.getText());
            }else{
                empleados = serv.buscarEmpleado(this.textDNI.getText(),this.textNombre.getText(),this.textApPaterno.getText(),(String)this.cboxsexo.getSelectedItem());
            }
            
        }
        else {
            if (this.cboxsexo.getSelectedIndex() ==0){
                empleados = serv.buscarEmpleadoT(this.textDNI.getText(),this.textNombre.getText(),this.textApPaterno.getText(),tipos.get(this.cboxtipo.getSelectedIndex()-1).getIdtipoempleado());
            }else{
                empleados = serv.buscarEmpleadoT(this.textDNI.getText(),this.textNombre.getText(),this.textApPaterno.getText(),(String)this.cboxsexo.getSelectedItem(),tipos.get(this.cboxtipo.getSelectedIndex()-1).getIdtipoempleado());
            }
            
        }
            
        this.RefreshTable();
    }//GEN-LAST:event_botonBuscarActionPerformed

    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_botonSalirActionPerformed

    private void botonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLimpiarActionPerformed
       this.textDNI.setText("");
       this.cboxsexo.setSelectedIndex(0);
       this.textNombre.setText("");
       this.textApPaterno.setText("");
       
       this.cboxtipo.setSelectedIndex(0);
       empleados.clear();
       this.RefreshTable();
       this.inhabilitaBotones();
    }//GEN-LAST:event_botonLimpiarActionPerformed

    private void tblResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tblResultadosMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
             Carga_Masiva_Emp cargaMasiva = new Carga_Masiva_Emp();
                this.getParent().add(cargaMasiva);
                  Dimension desktopSize = this.getParent().getSize();
                Dimension jInternalFrameSize = cargaMasiva.getSize();
                cargaMasiva.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                        (desktopSize.height - jInternalFrameSize.height) / 4);
                cargaMasiva.show();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonLimpiar;
    private javax.swing.JButton botonNuevo;
    private javax.swing.JButton botonSalir;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JComboBox cboxsexo;
    private javax.swing.JComboBox cboxtipo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblResultados;
    private javax.swing.JTextField textApPaterno;
    private javax.swing.JTextField textDNI;
    private javax.swing.JTextField textNombre;
    // End of variables declaration//GEN-END:variables

    private void setLocationRelativeTo(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Object getparent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
