/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.almac;

import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.Almacen;
import dp1.titandevelop.titano.persistent.Kardex;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.TipoMovimiento;
import dp1.titandevelop.titano.service.AlmacenService;
import dp1.titandevelop.titano.service.KardexService;
import dp1.titandevelop.titano.service.ProductoService;
import dp1.titandevelop.titano.service.TipoMovimientoService;
import dp1.titandevelop.titano.service.ValidacionService;
import dp1.titandevelop.titano.view.LoginT;
import java.awt.Cursor;
import java.awt.Dimension;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Yulian
 */
public class ModulosT_ALM_AdmMovMat extends javax.swing.JInternalFrame {
   
    //Listas para la tabla
    private List<Kardex> listaMov;
    //Listas para los cbox
    private List<Almacen> listaAlm;
    private List<Producto> listaProd;
    private List<TipoMovimiento> listaTMov;
    
    private Mensajes sms = new Mensajes();
    
    private void inhabilitaBotones(){
        //this.btnEditarA.setEnabled(false);
        this.btnEliminarA.setEnabled(false);
    }
    public void habilitaBotones(){
        //this.btnEditarA.setEnabled(true);
        this.btnEliminarA.setEnabled(true);
    }
    //Metodo para refrescar la tabla
    public void RefreshTable() {
        AbstractTableModel aux = (AbstractTableModel)tblResultados.getModel();
        aux.fireTableDataChanged();
    }
    private void addList (){
        
        for(int i=0;i<listaAlm.size();i++){
            this.cboxAlmacen.addItem(listaAlm.get(i).getDescripcion());
        }
        for(int i=0;i<listaProd.size();i++){
            this.cboxProducto.addItem(listaProd.get(i).getDescripcion());
        }
        for(int i=0;i<listaTMov.size();i++){
            this.cboxTMov.addItem(listaTMov.get(i).getDescripcion());
        }        
    }
    private AbstractTableModel modeloTabla = new AbstractTableModel()  {  
        
        @Override
        public int getRowCount() {
            return listaMov.size(); // cuantos registros (filas)
        }      
        
        @Override
        public int getColumnCount() {
            return 8; // cuantas columnas
        }  
        
        String [] titles = {"Fecha Mov","Área","Almacén","Producto","UM","Tipo Mov","Cantidad","Stock Ant","Stock Fin"};
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }        
          
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object res=null;
            DateFormat fecha = DateFormat.getDateInstance(DateFormat.MEDIUM);
            int cantIn ,cantPrev;
            Kardex movs = listaMov.get(rowIndex);
            cantIn = movs.getCantidadmovimiento();
            cantPrev = movs.getStockanterior();
            int tipo;
            if(movs.getToTipoMovimiento().getIdtipomovimiento()==2)//Entrada Externa
                tipo= 1;
            else if(movs.getToTipoMovimiento().getIdtipomovimiento()==4)//Salida Externa
                tipo=  2;
            else if(movs.getToTipoMovimiento().getIdtipomovimiento()==3)//Salida Interna
                tipo= 3;
            else if(movs.getToTipoMovimiento().getIdtipomovimiento()==1)//Entrada Interna
                tipo = 4;
            else 
                tipo = 5;//Entrada por Devolución
            TipoMovimientoService tm= new TipoMovimientoService();
            
            switch(columnIndex){
                case 0: res = fecha.format(movs.getToLote().getFechaingreso()); break;
                case 1: if(tipo == 1) res = "Compras"; else if (tipo==2||tipo==5) res = "Ventas"; else if (tipo==3||tipo==4) res = "Producción"; break;
                case 2: res = movs.getToAlmacenXProducto().getToAlmacen().getDescripcion();break;
                case 3: res = movs.getToAlmacenXProducto().getToProducto().getDescripcion();break;
                case 4: res = movs.getToAlmacenXProducto().getToProducto().getUm();break;
                case 5: res = movs.getToTipoMovimiento().getDescripcion();break;
                case 6: res = cantIn;break;
                case 7: res = cantPrev;break;
                case 8: if(tm.verificaTipoMov(movs.getToTipoMovimiento().getDescripcion(),"Entrada")) 
                            res=cantPrev+cantIn;
                        else{
                            res=cantPrev-cantIn;
                        }
            }
            return res;
        }  
    };
    public ModulosT_ALM_AdmMovMat() {
        initComponents();
        
        this.listaMov = new ArrayList<Kardex>();
        
        AlmacenService servAlm = new AlmacenService();
        ProductoService servProd = new ProductoService();
        TipoMovimientoService servTMov = new TipoMovimientoService();        
        ValidacionService val = new ValidacionService();  
        
        val.NoEscribe(dpFechaIn);
        val.NoEscribe(dpFechaFin);
        val.SNumeros(txtCantidadMin);
        val.Longitud(txtCantidadMin,9);
        val.Longitud(txtCantidadMax,9);
        val.SNumeros(txtCantidadMax);
        
        tblResultados.setModel(modeloTabla);
        
        this.listaAlm = servAlm.buscar();
        this.listaProd = servProd.buscar();
        this.listaTMov = servTMov.buscar();
        
        addList();
        this.inhabilitaBotones();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblResultados = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        btnDetalle = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cboxAlmacen = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        cboxTMov = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cboxProducto = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtCantidadMin = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCantidadMax = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        dpFechaIn = new org.jdesktop.swingx.JXDatePicker();
        dpFechaFin = new org.jdesktop.swingx.JXDatePicker();
        btnBuscar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnEliminarA = new javax.swing.JButton();
        btnCargaMasiva = new javax.swing.JButton();

        setResizable(true);
        setTitle("Administración de Movimientos de Material (KARDEX)");

        jPanel1.setPreferredSize(new java.awt.Dimension(925, 530));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Registro de Movimientos "));

        tblResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha Mov", "Almacén", "Producto", "Lote", "TipoMovimiento", "Cantidad", "Stock Anterior", "Stock Actual"
            }
        ));
        tblResultados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblResultadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblResultados);
        tblResultados.getColumnModel().getColumn(0).setPreferredWidth(8);
        tblResultados.getColumnModel().getColumn(1).setPreferredWidth(35);
        tblResultados.getColumnModel().getColumn(2).setPreferredWidth(15);
        tblResultados.getColumnModel().getColumn(3).setPreferredWidth(8);
        tblResultados.getColumnModel().getColumn(4).setPreferredWidth(35);
        tblResultados.getColumnModel().getColumn(5).setPreferredWidth(10);
        tblResultados.getColumnModel().getColumn(6).setPreferredWidth(10);
        tblResultados.getColumnModel().getColumn(7).setPreferredWidth(10);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/pediente.png"))); // NOI18N
        jButton1.setToolTipText("Ver Pendientes");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/almacen_1.png"))); // NOI18N
        btnDetalle.setToolTipText("Detalle Stock");
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 865, Short.MAX_VALUE)
                .addGap(8, 8, 8))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDetalle)
                .addGap(0, 0, 0)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addComponent(btnDetalle))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros de Búsqueda"));

        jLabel1.setText("Almacén");

        cboxAlmacen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));

        jLabel2.setText("Tipo de Movimiento");

        cboxTMov.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));

        jLabel3.setText("Producto");

        cboxProducto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Por Rangos"));

        jLabel4.setText("Cantidad Mín:");

        jLabel5.setText("Cantidad Máx:");

        jLabel6.setText("Fecha Mín:");

        jLabel7.setText("Fecha Máx:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addGap(15, 15, 15)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCantidadMin)
                    .addComponent(dpFechaIn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(25, 25, 25)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addGap(15, 15, 15)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCantidadMax)
                    .addComponent(dpFechaFin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(97, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCantidadMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtCantidadMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(jLabel7)
                        .addComponent(dpFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dpFechaIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboxAlmacen, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboxProducto, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboxTMov, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(cboxAlmacen, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboxProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboxTMov, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)))
                .addGap(9, 9, 9))
        );

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo.png"))); // NOI18N
        btnNuevo.setToolTipText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/limpiar.png"))); // NOI18N
        btnLimpiar.setToolTipText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnEliminarA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminarA.setToolTipText("Eliminar");
        btnEliminarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAActionPerformed(evt);
            }
        });

        btnCargaMasiva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/texto.png"))); // NOI18N
        btnCargaMasiva.setToolTipText("Carga Masiva");
        btnCargaMasiva.setActionCommand("masivo");
        btnCargaMasiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargaMasivaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addComponent(btnNuevo)
                                .addGap(1, 1, 1)
                                .addComponent(btnLimpiar)
                                .addGap(0, 0, 0)
                                .addComponent(btnCargaMasiva)
                                .addGap(0, 0, 0)
                                .addComponent(btnSalir)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnEliminarA)
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscar)
                            .addComponent(btnNuevo, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addComponent(btnLimpiar, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(btnSalir)
                    .addComponent(btnCargaMasiva, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminarA)
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 913, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        KardexService serv = new KardexService();
        String test =this.txtCantidadMin.getText();
        String test2=this.txtCantidadMax.getText();
        String min = null; if(test.compareTo("")!=0){min=test;}
        String max = null; if(test2.compareTo("")!=0){max=test2;}
        
        Date in = this.dpFechaIn.getDate();
        Date fin = this.dpFechaFin.getDate();
        
        int indexAlm = this.cboxAlmacen.getSelectedIndex()-1,idAlm = 0;
        int indexProd = this.cboxProducto.getSelectedIndex()-1,idProd = 0;
        int indexTMov = this.cboxTMov.getSelectedIndex()-1,idTMov = 0;
        
        
        if ((indexAlm == -1)&&(indexProd == -1)&&(indexTMov == -1)&&(in==null)&&(fin==null)&&(min==null)&&(max==null)){
            //Filtrado vacio
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            listaMov = serv.buscar();                    
            this.setCursor(Cursor.getDefaultCursor());
        }
        else {//Filatrado con por lo menos un campo
            //Obtendo los ids de los cbox seleccionados, los que no se mantendran con "0"
            if (indexAlm != -1)
                idAlm = listaAlm.get(indexAlm).getIdalmacen();
            if (indexProd != -1)
                idProd = listaProd.get(indexProd).getIdproducto();
            if (indexTMov != -1)
                idTMov = listaTMov.get(indexTMov).getIdtipomovimiento();
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            listaMov = serv.buscarFiltrado(idAlm,idProd,idTMov,min,max,in,fin);                    
        this.setCursor(Cursor.getDefaultCursor());
        }        
        this.setCursor(Cursor.getDefaultCursor());
        
        Collections.reverse(listaMov);
        this.RefreshTable();
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
    //ID 21 Nuevo Movimiento de material

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (21 == (Integer) LoginT.idspermisosxperfil.get(i)) {
            ModulosT_ALM.ventana2Nuevo = new ModulosT_ALM_MovMatNuevo();
            this.getParent().add(ModulosT_ALM.ventana2Nuevo);
            
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = ModulosT_ALM.ventana2Nuevo.getSize();
            ModulosT_ALM.ventana2Nuevo.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height)/4 );
            ModulosT_ALM.ventana2Nuevo.show();
            
            this.dispose();
            break;
            }
        }
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        this.txtCantidadMax.setText("");
        this.txtCantidadMin.setText("");
        this.dpFechaIn.setDate(null);
        this.dpFechaFin.setDate(null);
        this.cboxAlmacen.setSelectedIndex(0);
        this.cboxProducto.setSelectedIndex(0);
        this.cboxTMov.setSelectedIndex(0);
        
        listaMov.clear();
        this.RefreshTable();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnEliminarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAActionPerformed
    //ID 22 eliminar Movimiento de material

        Integer i = 0;
        for (; i < LoginT.permisosxperfil.size(); i++) {
            if (22 == (Integer) LoginT.idspermisosxperfil.get(i)) {
    
        int confirmar = JOptionPane.showInternalConfirmDialog(this.getRootPane(),sms.getPregEliminar(),"Aviso!",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);

        if (JOptionPane.OK_OPTION == confirmar)
        {//Obtengo el objeto seleccionado
            KardexService serv = new KardexService();
            int idMov =this.listaMov.get(this.tblResultados.getSelectedRow()).getIdkardex();
            if(serv.eliminar(idMov)!=1){
                JOptionPane.showInternalConfirmDialog(this.getRootPane(),"No puede eliminarse un registro de movimiento previo al día de hoy","Aviso!",JOptionPane.CLOSED_OPTION,JOptionPane.WARNING_MESSAGE);
            }
            else{
                this.listaMov.remove(this.tblResultados.getSelectedRow());
            }
            
        }

        this.RefreshTable();
        this.inhabilitaBotones();
        break;
            }
        }
        
        if (i >= LoginT.permisosxperfil.size()) {
            JOptionPane.showMessageDialog(this, "No puedes proceder, al parecer careces de permisos.",
                    "No posee permisos",
                    JOptionPane.WARNING_MESSAGE);
        }
        
        
    }//GEN-LAST:event_btnEliminarAActionPerformed

    private void tblResultadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tblResultadosMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
            ModulosT_ALM_ActividadesPen vent = new ModulosT_ALM_ActividadesPen();
            
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            
            this.getParent().add(vent);
            
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = vent.getSize();
            vent.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height)/4 );
            vent.show();            
        
            this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnCargaMasivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargaMasivaActionPerformed
        Carga_Masiva_Kardex cargaMasiva = new Carga_Masiva_Kardex();
        this.getParent().add(cargaMasiva);
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = cargaMasiva.getSize();
        cargaMasiva.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
            (desktopSize.height - jInternalFrameSize.height) / 4);
        cargaMasiva.show();
    }//GEN-LAST:event_btnCargaMasivaActionPerformed

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed
    
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        ModulosT_ALM_AdmMovMatDetalle ventanaDetalle = new ModulosT_ALM_AdmMovMatDetalle();
        this.getParent().add(ventanaDetalle);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ventanaDetalle.getSize();
        ventanaDetalle.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        this.setCursor(Cursor.getDefaultCursor());

        ventanaDetalle.show();
    }//GEN-LAST:event_btnDetalleActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCargaMasiva;
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton btnEliminarA;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboxAlmacen;
    private javax.swing.JComboBox cboxProducto;
    private javax.swing.JComboBox cboxTMov;
    private org.jdesktop.swingx.JXDatePicker dpFechaFin;
    private org.jdesktop.swingx.JXDatePicker dpFechaIn;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblResultados;
    private javax.swing.JTextField txtCantidadMax;
    private javax.swing.JTextField txtCantidadMin;
    // End of variables declaration//GEN-END:variables
}
