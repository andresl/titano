/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.almac;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Almacen;
import dp1.titandevelop.titano.service.AlmacenService;
import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Yulian
 */
public class ModulosT_ALM_AdmMovMatDetalle extends javax.swing.JInternalFrame {

    public List<Almacen> almacenes;
    
    public void habilitaBotones(){
        this.btnDetalle.setEnabled(true);
    }
    private void inhabilitaBotones(){
        this.btnDetalle.setEnabled(false);
    }
    
    public void RefreshTable() {
        AbstractTableModel aux = (AbstractTableModel)tblResultadosAlmacen.getModel();
        aux.fireTableDataChanged();
    }
    public ModulosT_ALM_AdmMovMatDetalle() {
        initComponents();
        AlmacenService servA = new AlmacenService();
        this.almacenes= servA.buscarTodos();        
        this.tblResultadosAlmacen.setModel(modeloTabla);
        inhabilitaBotones(); 
    }
    private AbstractTableModel modeloTabla = new AbstractTableModel() {  
        
        @Override
        public int getRowCount() {
            return almacenes.size(); // cuantos registros (filas)
        }      
        
        @Override
        public int getColumnCount() {
            return 3; // cuantas columnas
        }  
        
        String [] titles = {"Almacén", "Dirección", "Estado"};
        
        @Override
        public String getColumnName(int col){
            return titles[col];
        }        
          
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            String res=null;
            Almacen p = almacenes.get(rowIndex);
            switch(columnIndex){
                case 0: res = p.getDescripcion(); break;
                case 1: res = p.getDireccion();break;
                case 2: res = Estado.devuelveCadena(p.getEstado());
            }
            return res;
        }  
    };
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblResultadosAlmacen = new javax.swing.JTable();
        btnSalir = new javax.swing.JButton();
        btnDetalle = new javax.swing.JButton();

        setTitle("Detalle de Almacenes");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados de Búsqueda"));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblResultadosAlmacen.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Almacén", "Dirección", "Estado"
            }
        ));
        tblResultadosAlmacen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblResultadosAlmacenMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblResultadosAlmacen);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 8, Short.MAX_VALUE))
        );

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.setToolTipText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/detalle.png"))); // NOI18N
        btnDetalle.setToolTipText("Ver Detalle");
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDetalle)
                .addGap(0, 0, 0)
                .addComponent(btnSalir)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSalir)
                    .addComponent(btnDetalle))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 12, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblResultadosAlmacenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResultadosAlmacenMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tblResultadosAlmacenMouseClicked

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed
        //Obtengo el objeto seleccionado
        Almacen alm;
        alm = almacenes.get(this.tblResultadosAlmacen.getSelectedRow());
        //Instancio la ventana

        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        ModulosT_ALM_AlmDetalle ventanaDetalle = new ModulosT_ALM_AlmDetalle(alm.getIdalmacen());
        this.getParent().add(ventanaDetalle);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ventanaDetalle.getSize();
        ventanaDetalle.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        this.setCursor(Cursor.getDefaultCursor());

        ventanaDetalle.show();

        //Inhabilito los botones una vez realizada la acción
        this.inhabilitaBotones();
    }//GEN-LAST:event_btnDetalleActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton btnSalir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblResultadosAlmacen;
    // End of variables declaration//GEN-END:variables
}
