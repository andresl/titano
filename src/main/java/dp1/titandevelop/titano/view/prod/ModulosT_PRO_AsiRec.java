/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;

import dp1.titandevelop.titano.bean.EmpleadoEficiencia;
import dp1.titandevelop.titano.bean.EmpleadoSeleccionado;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.helper.HelperProcess;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.DetalleDamanda;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.service.AsignacionService;
import dp1.titandevelop.titano.service.EmpleadoService;
import dp1.titandevelop.titano.service.EmpleadoXProcesoTurnoService;
import dp1.titandevelop.titano.service.ProcesoService;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chrestean
 */
public class ModulosT_PRO_AsiRec extends javax.swing.JInternalFrame {

    private ArrayList<EmpleadoEficiencia> listaEmpleadosBD;
    private ArrayList<EmpleadoEficiencia> listaEmpleadosMorning;
    private ArrayList<EmpleadoEficiencia> listaEmpleadosTarde;
    private ArrayList<EmpleadoEficiencia> listaEmpleadosNoche;
    private ArrayList<EmpleadoXProcesoTurno> listaEmpleadosAlgoritmo;
    private ArrayList<Proceso> listaProcesosBD;
    private List<Integer> cantidadMaquinas;
    private ArrayList<EmpleadoXProcesoTurno> listaEmpleadoTotales; 
    private ArrayList<DetalleDamanda> listaDemanda;
    
    public void habilitaInBotones() {
//        this.btnMorning.setEnabled(true);
//        this.btnTarde.setEnabled(true);
//        this.btnNoche.setEnabled(true);
//        this.btnBuscar.setEnabled(true);
    }
    public void habilitaOutBotones() {
//        this.btnQuitarMorning.setEnabled(true);
//        this.btnQuitarTarde.setEnabled(true);
//        this.btnQuitarNoche.setEnabled(true);
    }

    private void inhabilitaInBotones() {
//        this.btnMorning.setEnabled(false);
//        this.btnTarde.setEnabled(false);
//        this.btnNoche.setEnabled(false);
    }
    private void inhabilitaOutBotones() {
//        this.btnQuitarMorning.setEnabled(false);
//        this.btnQuitarTarde.setEnabled(false);
//        this.btnQuitarNoche.setEnabled(false);
    }
    
    private AbstractTableModel ModeloEmpleadosBD = new AbstractTableModel() {
        //Atributos de los datos de la tabla
        private String[] titles = {"Nombres", "Ap. Paterno", "T.Mañana","T.Tarde","T.Noche"};

        @Override
        public int getRowCount() {
            return listaEmpleadosBD.size();//tipos.size(); // cuantos registros (filas)
        }

        @Override
        public int getColumnCount() {
            return 5; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object resultado = null;
           
            EmpleadoEficiencia emp = listaEmpleadosBD.get(rowIndex);
            switch (columnIndex) {
                case 0: resultado = emp.getE().getNombres();break;
                case 1: resultado = emp.getE().getApellidop();break;
                case 2: resultado = String.format("%.2f",emp.getEficienciaTM()*100 )+"%";break;
                case 3: resultado = String.format("%.2f", emp.getEficienciaTT()*100)+"%";break;
                case 4: resultado = String.format("%.2f", emp.getEficienciaTN()*100)+"%";
            }
            return resultado;
        }
    };
    private AbstractTableModel ModeloEmpleadosMorning = new AbstractTableModel() {
        //Atributos de los datos de la tabla
        private String[] titles = {"Nombres", "Ap. Paterno", "Eficiencia"};

        @Override
        public int getRowCount() {
            return listaEmpleadosMorning.size();//tipos.size(); // cuantos registros (filas)
        }
        @Override
        public int getColumnCount() {
            return 3; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object resultado = null;
            EmpleadoEficiencia emp = listaEmpleadosMorning.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    resultado = emp.getE().getNombres();
                    break;
                case 1:
                    resultado = emp.getE().getApellidop();
                    break;
                case 2:
                    resultado = String.format("%.2f",emp.getEficienciaTM()*100)+"%";
                    break;


            }
            return resultado;
        }
    };
    private AbstractTableModel ModeloEmpleadosTarde = new AbstractTableModel() {
        //Atributos de los datos de la tabla
        private String[] titles = {"Nombres", "Ap. Paterno","Eficiencia"};

        @Override
        public int getRowCount() {
            return listaEmpleadosTarde.size();//tipos.size(); // cuantos registros (filas)
        }

        @Override
        public int getColumnCount() {
            return 3; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object resultado = null;
            EmpleadoEficiencia emp = listaEmpleadosTarde.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    resultado = emp.getE().getNombres();
                    break;
                case 1:
                    resultado = emp.getE().getApellidop();
                    break;
                case 2:
                    resultado = String.format("%.2f",emp.getEficienciaTT()*100)+"%";
                    break;


            }
            return resultado;
        }
    };
    private AbstractTableModel ModeloEmpleadosNoche = new AbstractTableModel() {
        //Atributos de los datos de la tabla
        private String[] titles = {"Nombres", "Ap. Paterno", "Eficiencia"};

        @Override
        public int getRowCount() {
            return listaEmpleadosNoche.size();//tipos.size(); // cuantos registros (filas)
        }

        @Override
        public int getColumnCount() {
            return 3; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object resultado = null;
            EmpleadoEficiencia emp = listaEmpleadosNoche.get(rowIndex);
            
            switch (columnIndex) {
                case 0:
                    resultado = emp.getE().getNombres();
                    break;
                case 1:
                    resultado = emp.getE().getApellidop();
                    break;
                case 2:
                    resultado =String.format("%.2f", emp.getEficienciaTN()*100)+"%";
                    break;


            }
            return resultado;
        }

    };
    
    private AbstractTableModel ModeloDemanda = new AbstractTableModel() {
        //Atributos de los datos de la tabla
        private String[] titles = {"Galleta", "Cantidad"};

        @Override
        public int getRowCount() {
            return listaDemanda.size();//tipos.size(); // cuantos registros (filas)
        }

        @Override
        public int getColumnCount() {
            return 2; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object resultado = null;
            DetalleDamanda emp = listaDemanda.get(rowIndex);
            
            switch (columnIndex) {
                case 0:
                    resultado = emp.getToProducto().getDescripcion();
                    break;
                case 1:
                    resultado = emp.getCantidad();
                    break;
             
            }
            return resultado;
        }

    };
    private AbstractTableModel ModeloProcesosBD = new AbstractTableModel() {
//Atributos de los datos de la tabla
        private String[] titles = {"Proceso", "Cantidad de Maquinas"};

        @Override
        public int getRowCount() {
            return listaProcesosBD.size();//tipos.size(); // cuantos registros (filas)
        }

        @Override
        public int getColumnCount() {
            return 2; // cuantas columnas
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return (column == 1);
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object resultado = null;
            Proceso proc = listaProcesosBD.get(rowIndex);
            Integer cant = cantidadMaquinas.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    resultado = proc.getDescripcion();
                    break;
                case 1:
                    resultado = cant;
                    break;

            }
            return resultado;
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            if (col == 1) {
                if (cantidadMaquinas.get(row)>= Integer.parseInt("" + value)){
                    cantidadMaquinas.set(row, Integer.parseInt("" + value));
                }else {
                    cantidadMaquinas.get(row);
                }
                
            }
            this.fireTableDataChanged();
        }
    ;

    };
   
    public ModulosT_PRO_AsiRec() {

        initComponents();
        ProcesoService procesoservice = new ProcesoService();
        this.listaProcesosBD = (ArrayList) procesoservice.buscarProcAll();
        cantidadMaquinas = procesoservice.llenaCapacidades(listaProcesosBD);
        listaEmpleadosBD = hallarListaEmpleado();
        this.jTextCantidadEmpleado.setText(""+listaEmpleadosBD.size());
        this.jTextCantidadEmpleado.setEnabled(false);
        this.listaEmpleadosMorning = new ArrayList<EmpleadoEficiencia>();
        this.listaEmpleadosTarde = new ArrayList<EmpleadoEficiencia>();
        this.listaEmpleadosNoche = new ArrayList<EmpleadoEficiencia>();
        /*Datos Demanda*/
        
         Date fecha=new Date();
        Demanda demanda=dp1.titandevelop.titano.service.DemandaService.buscarPorFecha(fecha);
        this.listaDemanda=dp1.titandevelop.titano.service.DetalleDeDemandaService.buscarPorIdDemanda(demanda.getIddemanda()); 
        this.tablaEmpleadosBD.setModel(ModeloEmpleadosBD);
        this.tablaEmpleadosMorning.setModel(ModeloEmpleadosMorning);
        this.tablaEmpleadosTarde.setModel(ModeloEmpleadosTarde);
        this.tablaEmpleadosNoche.setModel(ModeloEmpleadosNoche);
        this.tablaProceso.setModel(ModeloProcesosBD);
        this.tablaDemanda.setModel(ModeloDemanda);
        this.inhabilitaInBotones();
        this.inhabilitaOutBotones();
        
        
        
       

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaEmpleadosMorning = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaEmpleadosBD = new javax.swing.JTable();
        btnQuitarMorning = new javax.swing.JButton();
        btnMorning = new javax.swing.JButton();
        btnTarde = new javax.swing.JButton();
        btnNoche = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaEmpleadosNoche = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaEmpleadosTarde = new javax.swing.JTable();
        btnQuitarTarde = new javax.swing.JButton();
        btnQuitarNoche = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaDemanda = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextCantidadEmpleado = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProceso = new javax.swing.JTable();

        setResizable(true);
        setTitle("Asignación de Recursos");
        setMinimumSize(new java.awt.Dimension(0, 0));
        setPreferredSize(new java.awt.Dimension(1070, 540));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Asignacion de Empleados"));
        jPanel2.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        tablaEmpleadosMorning.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        tablaEmpleadosMorning.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaEmpleadosMorningMouseClicked(evt);
            }
        });
        tablaEmpleadosMorning.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaEmpleadosMorningKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tablaEmpleadosMorning);

        tablaEmpleadosBD.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaEmpleadosBD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaEmpleadosBDMouseClicked(evt);
            }
        });
        tablaEmpleadosBD.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaEmpleadosBDKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(tablaEmpleadosBD);

        btnQuitarMorning.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/adelante.png"))); // NOI18N
        btnQuitarMorning.setToolTipText("Quitar");
        btnQuitarMorning.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarMorningActionPerformed(evt);
            }
        });

        btnMorning.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/atras.png"))); // NOI18N
        btnMorning.setText("Turno Mañana");
        btnMorning.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMorningActionPerformed(evt);
            }
        });

        btnTarde.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/atras.png"))); // NOI18N
        btnTarde.setText("Turno Tarde");
        btnTarde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTardeActionPerformed(evt);
            }
        });

        btnNoche.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/atras.png"))); // NOI18N
        btnNoche.setText("Turno Noche");
        btnNoche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNocheActionPerformed(evt);
            }
        });

        tablaEmpleadosNoche.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        tablaEmpleadosNoche.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaEmpleadosNocheMouseClicked(evt);
            }
        });
        tablaEmpleadosNoche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaEmpleadosNocheKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tablaEmpleadosNoche);

        tablaEmpleadosTarde.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        tablaEmpleadosTarde.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaEmpleadosTardeMouseClicked(evt);
            }
        });
        tablaEmpleadosTarde.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaEmpleadosTardeKeyPressed(evt);
            }
        });
        jScrollPane5.setViewportView(tablaEmpleadosTarde);

        btnQuitarTarde.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/adelante.png"))); // NOI18N
        btnQuitarTarde.setToolTipText("Quitar");
        btnQuitarTarde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarTardeActionPerformed(evt);
            }
        });

        btnQuitarNoche.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/adelante.png"))); // NOI18N
        btnQuitarNoche.setToolTipText("Quitar");
        btnQuitarNoche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarNocheActionPerformed(evt);
            }
        });

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/detalle.png"))); // NOI18N
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnTarde, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNoche)
                            .addComponent(btnMorning, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnQuitarMorning)
                            .addComponent(btnQuitarTarde)
                            .addComponent(btnQuitarNoche))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnMorning)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnQuitarMorning)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnTarde)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnQuitarTarde)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnNoche)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnQuitarNoche)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar))
        );

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/asignar.png"))); // NOI18N
        btnBuscar.setText("Asignar");
        btnBuscar.setToolTipText("Asignar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        jButton1.setToolTipText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Generales"));

        tablaDemanda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Galleta", "Cantidad Mensual"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane6.setViewportView(tablaDemanda);

        jLabel1.setText("Cantidad Empleado:");

        jLabel2.setText("Demanda:");

        jTextCantidadEmpleado.setEditable(false);

        tablaProceso.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Campo 1", "Campo 2", "Campo 3"
            }
        ));
        jScrollPane1.setViewportView(tablaProceso);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(jTextCantidadEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextCantidadEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscar)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMorningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMorningActionPerformed
       
         int[] listaseleccionada = this.tablaEmpleadosBD.getSelectedRows();
         for (int j = 0; j < listaseleccionada.length; j++) {           
            this.listaEmpleadosMorning.add(this.listaEmpleadosBD.get(listaseleccionada[j]));
            this.ModeloEmpleadosMorning.fireTableChanged(null);         
        }
     
         for (int j = 0; j < listaseleccionada.length; j++) {          
             int pos=listaseleccionada[j]-j;
             this.listaEmpleadosBD.remove(pos);                   
             this.ModeloEmpleadosBD.fireTableChanged(null);
          
        }
        
//        this.listaEmpleadosMorning.add(this.listaEmpleadosBD.get(this.tablaEmpleadosBD.getSelectedRow()));
//        this.ModeloEmpleadosMorning.fireTableChanged(null);
//        this.listaEmpleadosBD.remove(this.tablaEmpleadosBD.getSelectedRow());
//        this.ModeloEmpleadosBD.fireTableChanged(null);
     
    }//GEN-LAST:event_btnMorningActionPerformed

    private void btnQuitarMorningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarMorningActionPerformed
           int[] listaseleccionada = this.tablaEmpleadosMorning.getSelectedRows();
         for (int j = 0; j < listaseleccionada.length; j++) {           
            this.listaEmpleadosBD.add(this.listaEmpleadosMorning.get(listaseleccionada[j]));
            this.ModeloEmpleadosBD.fireTableChanged(null);         
        }
     
         for (int j = 0; j < listaseleccionada.length; j++) {          
             int pos=listaseleccionada[j]-j;
             this.listaEmpleadosMorning.remove(pos);                   
             this.ModeloEmpleadosMorning.fireTableChanged(null);
          
        }
        
//        
//        this.listaEmpleadosBD.add(this.listaEmpleadosMorning.get(this.tablaEmpleadosMorning.getSelectedRow()));
//        this.ModeloEmpleadosBD.fireTableChanged(null);
//        this.listaEmpleadosMorning.remove(this.tablaEmpleadosMorning.getSelectedRow());
//        this.ModeloEmpleadosMorning.fireTableChanged(null);
        
    }//GEN-LAST:event_btnQuitarMorningActionPerformed

    private void tablaEmpleadosBDKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaEmpleadosBDKeyPressed
//        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            this.btnMorning.doClick();
//        }
    }//GEN-LAST:event_tablaEmpleadosBDKeyPressed

    private void tablaEmpleadosMorningKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaEmpleadosMorningKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
          
                this.btnQuitarMorning.doClick();
    
        }
    }//GEN-LAST:event_tablaEmpleadosMorningKeyPressed

    @SuppressWarnings("empty-statement")
    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
       this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        Mensajes m= new Mensajes();
       ArrayList<EmpleadoXProcesoTurno> turn1,turn2,turn3; 
        try {
              
            listaEmpleadosAlgoritmo=new ArrayList<EmpleadoXProcesoTurno>();
                
            if (this.listaEmpleadosAlgoritmo.isEmpty()){
           
             turn1=HelperProcess.hallarEmpleadoXProceso(listaEmpleadosMorning,1);
             turn2=HelperProcess.hallarEmpleadoXProceso(listaEmpleadosTarde,2);
             turn3=HelperProcess.hallarEmpleadoXProceso(listaEmpleadosNoche,3);
             
             if(turn1!=null)
             listaEmpleadosAlgoritmo.addAll(turn1);
             if(turn2!=null)
             listaEmpleadosAlgoritmo.addAll(turn2);
             if(turn3!=null)
             listaEmpleadosAlgoritmo.addAll(turn3);
             //JOptionPane.showInternalMessageDialog(this.getRootPane(), m.getAsignado(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            }
             
        }
       catch (Exception e){
          JOptionPane.showMessageDialog(this, "Ha ocurrido un error, faltan registrar los operarios",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
       };    

       /*Prepara Procesos*/
       try{
           for(int i=0; i<this.listaProcesosBD.size();i++){
               listaProcesosBD.get(i).setCantidadmaquina(this.cantidadMaquinas.get(i));
           }
       }catch (Exception e){
          JOptionPane.showMessageDialog(this, "Ha ocurrido un error, faltan registrar maquinas",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
       };    

       
       
        ArrayList<EmpleadoSeleccionado> solucion=HelperProcess.ejecutar(listaEmpleadosAlgoritmo,listaProcesosBD,listaDemanda);
        
        /*Halla id de asignacion*/
        AsignacionService service=new AsignacionService();
        int indice = service.ultimoId();
       
        ModulosT_PRO_GuardarAsignacion ventanaG= new ModulosT_PRO_GuardarAsignacion(solucion,indice);
        this.getParent().add(ventanaG);
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ventanaG.getSize();
        ventanaG.setLocation((desktopSize.width - jInternalFrameSize.width) / 2, (desktopSize.height - jInternalFrameSize.height) / 4);
        ventanaG.show();       
        
        this.dispose();
        this.setCursor(Cursor.getDefaultCursor());
        
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnTardeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTardeActionPerformed
           int[] listaseleccionada = this.tablaEmpleadosBD.getSelectedRows();
         for (int j = 0; j < listaseleccionada.length; j++) {           
            this.listaEmpleadosTarde.add(this.listaEmpleadosBD.get(listaseleccionada[j]));
            this.ModeloEmpleadosTarde.fireTableChanged(null);         
        }
     
         for (int j = 0; j < listaseleccionada.length; j++) {          
             int pos=listaseleccionada[j]-j;
             this.listaEmpleadosBD.remove(pos);                   
             this.ModeloEmpleadosBD.fireTableChanged(null);
          
        }
        
    }//GEN-LAST:event_btnTardeActionPerformed

    private void btnNocheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNocheActionPerformed
           int[] listaseleccionada = this.tablaEmpleadosBD.getSelectedRows();
         for (int j = 0; j < listaseleccionada.length; j++) {           
            this.listaEmpleadosNoche.add(this.listaEmpleadosBD.get(listaseleccionada[j]));
            this.ModeloEmpleadosNoche.fireTableChanged(null);         
        }
     
         for (int j = 0; j < listaseleccionada.length; j++) {          
             int pos=listaseleccionada[j]-j;
             this.listaEmpleadosBD.remove(pos);                   
             this.ModeloEmpleadosBD.fireTableChanged(null);
          
        }
        
      
    }//GEN-LAST:event_btnNocheActionPerformed

    private void tablaEmpleadosNocheKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaEmpleadosNocheKeyPressed
      if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
                this.btnQuitarNoche.doClick();
    
        }
    }//GEN-LAST:event_tablaEmpleadosNocheKeyPressed

    private void tablaEmpleadosTardeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaEmpleadosTardeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
          
                this.btnQuitarTarde.doClick();
    
        }
    }//GEN-LAST:event_tablaEmpleadosTardeKeyPressed

    private void btnQuitarTardeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarTardeActionPerformed
       int[] listaseleccionada = this.tablaEmpleadosTarde.getSelectedRows();
         for (int j = 0; j < listaseleccionada.length; j++) {           
            this.listaEmpleadosBD.add(this.listaEmpleadosTarde.get(listaseleccionada[j]));
            this.ModeloEmpleadosBD.fireTableChanged(null);         
        }
     
         for (int j = 0; j < listaseleccionada.length; j++) {          
             int pos=listaseleccionada[j]-j;
             this.listaEmpleadosTarde.remove(pos);                   
             this.ModeloEmpleadosTarde.fireTableChanged(null);
          
        }
        
        
        
    }//GEN-LAST:event_btnQuitarTardeActionPerformed

    private void btnQuitarNocheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarNocheActionPerformed
         int[] listaseleccionada = this.tablaEmpleadosNoche.getSelectedRows();
         for (int j = 0; j < listaseleccionada.length; j++) {           
            this.listaEmpleadosBD.add(this.listaEmpleadosNoche.get(listaseleccionada[j]));
            this.ModeloEmpleadosBD.fireTableChanged(null);         
        }
     
         for (int j = 0; j < listaseleccionada.length; j++) {          
             int pos=listaseleccionada[j]-j;
             this.listaEmpleadosNoche.remove(pos);                   
             this.ModeloEmpleadosNoche.fireTableChanged(null);
          
        }
        
//        
//        this.listaEmpleadosBD.add(this.listaEmpleadosNoche.get(this.tablaEmpleadosNoche.getSelectedRow()));
//        this.ModeloEmpleadosBD.fireTableChanged(null);
//        this.listaEmpleadosNoche.remove(this.tablaEmpleadosNoche.getSelectedRow());
//        this.ModeloEmpleadosNoche.fireTableChanged(null);
       
    }//GEN-LAST:event_btnQuitarNocheActionPerformed

    private void tablaEmpleadosBDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaEmpleadosBDMouseClicked
        this.habilitaInBotones();
    }//GEN-LAST:event_tablaEmpleadosBDMouseClicked

    private void tablaEmpleadosMorningMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaEmpleadosMorningMouseClicked
        this.btnQuitarMorning.setEnabled(true);      // TODO add your handling code here:
    }//GEN-LAST:event_tablaEmpleadosMorningMouseClicked

    private void tablaEmpleadosTardeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaEmpleadosTardeMouseClicked
        this.btnQuitarTarde.setEnabled(true);
    }//GEN-LAST:event_tablaEmpleadosTardeMouseClicked

    private void tablaEmpleadosNocheMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaEmpleadosNocheMouseClicked
        this.btnQuitarNoche.setEnabled(true);
    }//GEN-LAST:event_tablaEmpleadosNocheMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        
        Empleado e=this.listaEmpleadosBD.get(this.tablaEmpleadosBD.getSelectedRow()).getE();
        
        

        DetalleAsigRecTurno ventanaVerDetalle = new DetalleAsigRecTurno(e);

        this.getParent().add(ventanaVerDetalle);
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ventanaVerDetalle.getSize();
        ventanaVerDetalle.setLocation((desktopSize.width - jInternalFrameSize.width) / 2, (desktopSize.height - jInternalFrameSize.height) / 4);
        ventanaVerDetalle.show();

    }//GEN-LAST:event_btnEditarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnMorning;
    private javax.swing.JButton btnNoche;
    private javax.swing.JButton btnQuitarMorning;
    private javax.swing.JButton btnQuitarNoche;
    private javax.swing.JButton btnQuitarTarde;
    private javax.swing.JButton btnTarde;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextField jTextCantidadEmpleado;
    private javax.swing.JTable tablaDemanda;
    private javax.swing.JTable tablaEmpleadosBD;
    private javax.swing.JTable tablaEmpleadosMorning;
    private javax.swing.JTable tablaEmpleadosNoche;
    private javax.swing.JTable tablaEmpleadosTarde;
    private javax.swing.JTable tablaProceso;
    // End of variables declaration//GEN-END:variables

    private ArrayList<EmpleadoEficiencia> hallarListaEmpleado() {
           EmpleadoService empleadoservice = new EmpleadoService();
           ArrayList<Empleado> empleados = (ArrayList) empleadoservice.buscarTOperario();
           EmpleadoXProcesoTurnoService empleadoProcesoService = new EmpleadoXProcesoTurnoService();
           this.listaEmpleadoTotales=empleadoProcesoService.buscarAll();
           ArrayList<EmpleadoEficiencia> aux=new ArrayList();
           for(int i=0;i<empleados.size();i++){
               EmpleadoEficiencia e=new EmpleadoEficiencia();
               e.setE(empleados.get(i));
               e.setEficienciaM(buscarData(empleados.get(i).getIdempleado(),1));
               e.setEficienciaT(buscarData(empleados.get(i).getIdempleado(),2));
               e.setEficienciaN(buscarData(empleados.get(i).getIdempleado(),3));
               float total=e.getEficienciaM()+e.getEficienciaN()+e.getEficienciaT();
               e.setEficienciaTM(e.getEficienciaM()/total);
               e.setEficienciaTT(e.getEficienciaT()/total);
               e.setEficienciaTN(e.getEficienciaN()/total);
               aux.add(e);
           }
           
          return aux;
    }
    
     private float buscarData(int idempleado, int idTurno) {
        float total=0;
        float eficienciaParcial=0;
        float eficiencia=0;
        int cantidad=0;
                
        for(int i=0;i<listaEmpleadoTotales.size();i++){
            if(listaEmpleadoTotales.get(i).getToEmpleado().getIdempleado().equals(idempleado)
                    && listaEmpleadoTotales.get(i).getToTurno().getIdturno().equals(idTurno)){
            total=listaEmpleadoTotales.get(i).getProductividad()+listaEmpleadoTotales.get(i).getRotura();
            eficienciaParcial+=listaEmpleadoTotales.get(i).getProductividad()/total;
            cantidad++;
            }
        }
        
        eficiencia=eficienciaParcial/cantidad;
       
      
        return eficiencia; 
        }
}
