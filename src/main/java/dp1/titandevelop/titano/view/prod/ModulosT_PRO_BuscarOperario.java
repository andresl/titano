/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;
import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.persistent.DetalleOrdenProduccion;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.OrdenProduccion;
import dp1.titandevelop.titano.persistent.Produccion;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.Receta;
import dp1.titandevelop.titano.service.AlmacenService;
import dp1.titandevelop.titano.service.AsignacionService;
import dp1.titandevelop.titano.service.EmpleadoXProcesoTurnoService;
import dp1.titandevelop.titano.service.KardexService;
import dp1.titandevelop.titano.service.OrdenProduccionService;
import dp1.titandevelop.titano.service.ProduccionService;
import dp1.titandevelop.titano.service.ProductoXProcesoService;
import dp1.titandevelop.titano.service.RecetaService;
import dp1.titandevelop.titano.service.TipoMovimientoService;
import dp1.titandevelop.titano.service.ValidacionService;
import java.util.ArrayList;
import javax.swing.JOptionPane;


import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Dorita
 */


public class ModulosT_PRO_BuscarOperario extends javax.swing.JDialog {

    AsignacionService sa= new AsignacionService();
    AlmacenService saa = new AlmacenService();
    OrdenProduccionService so= new OrdenProduccionService();
    RecetaService sr = new RecetaService();
    KardexService sk= new KardexService();
    EmpleadoXProcesoTurnoService sept = new EmpleadoXProcesoTurnoService();
    private ArrayList<EmpleadoXProcesoTurno> ept;
    
    private ValidacionService val = new ValidacionService();
    private Mensajes sms = new Mensajes();    
//    ProduccionService sp= new ProduccionService();
    private ArrayList<Asignacion> listaAsignacion;
    private Asignacion a;
    private EmpleadoXProcesoTurno e;
   
   
    
    //Refrescar la tabla de productos
    public void RefrescarTabla() {
        AbstractTableModel aux = (AbstractTableModel) tblOperarios.getModel();
        this.inhabilitaBotones();
        aux.fireTableDataChanged();
    }
    
    
    //habilitar y deshabilitar botones para actualizar y eliminar
    public void habilitaBotones(){
        this.btnAsignar.setEnabled(true);
      
    }

    public void inhabilitaBotones() {
        this.btnAsignar.setEnabled(false);
       
    }
    
    //Modelo de tabla
    private AbstractTableModel modelo = new AbstractTableModel() {
        public int getRowCount() {
            if (listaAsignacion!=null)
            return listaAsignacion.size();
            else return 0;
        }

        public int getColumnCount() {
        return 6;
        }

        String [] titles = {"Producto","Proceso","Nombre","Apellido Paterno" ,"Produccion Teorica", "Rotura Teorica"};

        @Override
        public String getColumnName(int col){
        return titles[col];
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
        //String res=null;
        Asignacion a=listaAsignacion.get(rowIndex);
        switch (columnIndex){
        case 0: return a.getToProducto().getDescripcion();
        case 1: return a.getToMaquina().getToProceso().getDescripcion();
        case 2: return a.getToEmpleado().getNombres();
        case 3: return a.getToEmpleado().getApellidop();
        case 4: return ept.get(rowIndex).getProductividad();
        case 5: return ept.get(rowIndex).getRotura();
        }
        return null;
        } 
    };
    
    //Constructor
   public ModulosT_PRO_BuscarOperario(java.awt.Frame parent, boolean modal)  
   {
        
        super(parent, modal);
        initComponents();  
        val.SNumeros(txtProduccion);
        val.Longitud(txtProduccion,4);
        val.SNumeros(txtRotura);
        val.Longitud(txtRotura,4);
        listaAsignacion = sa.SacaUltimo();
        Estado e=new Estado();
        a= new Asignacion();
        ept= new ArrayList<EmpleadoXProcesoTurno>();
        listaAsignacion = sa.SacaUltimo();
        //Filtrar con productos disponibles para se producidos
         //Buscar los detalles de ordenes de produccion aprobados y solo los productos con un detalle aprobado se produciran
        //1. Buscar Orden de Produccion Activa.
       OrdenProduccion op = so.buscarActiva();
        //2. Buscar Detalle de orden de Produccion buscar producto de receta y el idde orden de produccion
         for (int i=0;i<listaAsignacion.size();i++)
        {
            ProductoXProcesoService spp= new ProductoXProcesoService();
            Producto prod1= spp.DevueveSalida(listaAsignacion.get(i).getToProducto().getIdproducto(), listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso());
            ArrayList<Receta> rr =sr.BuscarPorProducto(prod1);
            int idDop;
            //busco detalle para producto final
            idDop = so.buscarIdDetalleOperacion(op.getIdordenproduccion(),listaAsignacion.get(i).getToProducto().getIdproducto());
            for (int j=0;j<rr.size();j++)
            {
                   // idDop = so.buscarIdDetalleOperacion(op.getIdordenproduccion(),rr.get(j).getToProducto().getIdproducto());
                    if (idDop!=0) {
                       //quito los operarios de la lista de asignacion, cuyo producto de asignacion no tenga sus insumos aceptados
                        if (!sk.aceptoMovimientoProductoDOP(idDop,rr.get(j).getToProducto1().getIdproducto() )){
                                    listaAsignacion.remove(i);
                                    i--;
                                    break;
                                }
                    }
                    
            }
           
            
        }
        for (int i=0; i<listaAsignacion.size();i++)
        {
            ept.add(this.sept.buscarUnEmpleadoXProceso(listaAsignacion.get(i).getToEmpleado().getIdempleado(),listaAsignacion.get(i).getToTurno().getIdturno(), listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso()));
        }
        this.pnlDatos.setVisible(false);
        this.tblOperarios.setModel(this.modelo);       
        
    }
        
   
    
    
    //Constructor
    public ModulosT_PRO_BuscarOperario(java.awt.Frame parent, boolean modal, ArrayList<Produccion> l) {       
        super(parent, modal);
        initComponents();
        a= new Asignacion();
        val.SNumeros(txtProduccion);
        val.Longitud(txtProduccion,6);
        val.SNumeros(txtRotura);
        ept= new ArrayList<EmpleadoXProcesoTurno>();
        //this.sept= new EmpleadoXProcesoTurnoService();
        val.Longitud(txtRotura,6);
        listaAsignacion = sa.SacaUltimo();
        //Filtrar los operarios ya seleccionados
        for (int j=0;j<listaAsignacion.size();j++)
        {
            int int1= listaAsignacion.get(j).getIdasignacion();
            for(int i=0;i<l.size();i++)
            {
                int int2= l.get(i).getToAsignacion().getIdasignacion();
                if(int1==int2)
                {
                    listaAsignacion.remove(j);
                    j--;
                }
            }
        }
        //Filtrar con productos disponibles para se producidos
         //Buscar los detalles de ordenes de produccion aprobados y solo los productos con un detalle aprobado se produciran
         //1. Buscar Orden de Produccion Activa.
       OrdenProduccion op = so.buscarEnProceso();
        //2. Buscar Detalle de orden de Produccion buscar producto de receta y el idde orden de produccion
         for (int i=0;i<listaAsignacion.size();i++)//para cada producto de la lista asignacion
        {
            ProductoXProcesoService spp= new ProductoXProcesoService();
            TipoMovimientoService st= new TipoMovimientoService();
            Producto prod1= spp.DevueveSalida(listaAsignacion.get(i).getToProducto().getIdproducto(), listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso());
            ArrayList<Receta> rr =sr.BuscarPorProducto(prod1);
            int idDop;
            int tipo= st.buscarFiltrado("Salida Interna").get(0).getIdtipomovimiento();; 
            for (int j=0;j<rr.size();j++)
            {
               
                //Buscamos el detalle de Orden de Produccion para el producto final!    
                idDop = so.buscarIdDetalleOperacion(op.getIdordenproduccion(),listaAsignacion.get(i).getToProducto().getIdproducto());
                if (idDop!=0) {
                    
                        if (!so.movimientosDetalleAceptados(idDop, rr.get(j).getToProducto1().getIdproducto())){
                                    listaAsignacion.remove(i);
                                    i--;
                                    break;
                                }
                    }
                    
            }
            
        }    
        
          for (int i=0; i<listaAsignacion.size();i++)
        {
            ept.add(this.sept.buscarUnEmpleadoXProceso(listaAsignacion.get(i).getToEmpleado().getIdempleado(),listaAsignacion.get(i).getToTurno().getIdturno(), listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso()));
        }
        this.tblOperarios.setModel(this.modelo);     
        txtOperario.setEnabled(false);
        txtProceso.setEnabled(false);
        txtTurno.setEnabled(false);
        
    }    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAsignar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        pnlBusqueda = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOperarios = new javax.swing.JTable();
        pnlDatos = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtOperario = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtTurno = new javax.swing.JTextField();
        txtProceso = new javax.swing.JTextField();
        txtProduccion = new javax.swing.JTextField();
        txtRotura = new javax.swing.JTextField();

        setTitle("Escoger Producto");

        btnAsignar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/asignar.png"))); // NOI18N
        btnAsignar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsignarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        pnlBusqueda.setBorder(javax.swing.BorderFactory.createTitledBorder("Búsqueda de Producto"));

        tblOperarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblOperarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOperariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblOperarios);

        javax.swing.GroupLayout pnlBusquedaLayout = new javax.swing.GroupLayout(pnlBusqueda);
        pnlBusqueda.setLayout(pnlBusquedaLayout);
        pnlBusquedaLayout.setHorizontalGroup(
            pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlBusquedaLayout.setVerticalGroup(
            pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBusquedaLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlDatos.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));

        jLabel1.setText("Operario");

        txtOperario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOperarioActionPerformed(evt);
            }
        });

        jLabel5.setText("Turno");

        jLabel6.setText("Proceso");

        jLabel7.setText("Produccion");

        jLabel8.setText("Rotura");

        txtRotura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRoturaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDatosLayout = new javax.swing.GroupLayout(pnlDatos);
        pnlDatos.setLayout(pnlDatosLayout);
        pnlDatosLayout.setHorizontalGroup(
            pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatosLayout.createSequentialGroup()
                        .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTurno, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                            .addComponent(txtRotura))
                        .addGap(41, 41, 41)
                        .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtProduccion, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                            .addComponent(txtProceso)))
                    .addComponent(txtOperario))
                .addContainerGap())
        );
        pnlDatosLayout.setVerticalGroup(
            pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtOperario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProceso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(txtProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRotura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAsignar, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnlBusqueda, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDatos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(pnlBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(pnlDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAsignar)
                    .addComponent(btnSalir))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
private boolean verificaStock(String proceso, Producto prod){
    ArrayList<Receta> rr = this.sr.BuscarPorProducto(prod);
    
    return true;
}
    private void btnAsignarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsignarActionPerformed
        // TODO add your handling code here:
       
        {
            if (ModulosT_PRO.ventanaProduccionNuevo!=null)
            {
               ProductoXProcesoService spp= new ProductoXProcesoService();
                    //if(verificaStock(this.txtProceso, spp.DevueveSalida(a.getToProducto().getIdproducto(), a.getToMaquina().getToProceso().getIdproceso())));{
                   
                    ModulosT_PRO.ventanaProduccionNuevo.AgregaProduccion(e,a, Float.parseFloat(txtRotura.getText()), Float.parseFloat(txtProduccion.getText()));
                    ModulosT_PRO.ventanaProduccionNuevo.RefrescarTabla();
                    listaAsignacion.remove(this.tblOperarios.getSelectedRow());
                    ept.remove(this.tblOperarios.getSelectedRow());
                    this.RefrescarTabla();
                    txtOperario.setText("");
                    txtTurno.setText("");
                    txtProceso.setText("");
                    txtProduccion.setText("");
                    txtRotura.setText("");
                    
                   
            }
           // else  JOptionPane.showInternalMessageDialog(this.getRootPane(),"Ventana nuevo null", "Aviso", JOptionPane.WARNING_MESSAGE);

        }
    }//GEN-LAST:event_btnAsignarActionPerformed

    private void tblOperariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOperariosMouseClicked
        // TODO add your handling code here:
        if(evt.getClickCount()==1)
        {
            this.habilitaBotones(); 
            a = this.listaAsignacion.get(this.tblOperarios.getSelectedRow());
            e = this.ept.get(this.tblOperarios.getSelectedRow());
            this.txtOperario.setText(a.getToEmpleado().getNombres()+" "+a.getToEmpleado().getApellidop()+ " "+a.getToEmpleado().getApellidom());
            this.txtProceso.setText(a.getToMaquina().getToProceso().getDescripcion());
            this.txtTurno.setText(a.getToTurno().getDescripcion());
            
        } 
    }//GEN-LAST:event_tblOperariosMouseClicked

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
                 this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtOperarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOperarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOperarioActionPerformed

    private void txtRoturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRoturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRoturaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAsignar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlBusqueda;
    private javax.swing.JPanel pnlDatos;
    private javax.swing.JTable tblOperarios;
    private javax.swing.JTextField txtOperario;
    private javax.swing.JTextField txtProceso;
    private javax.swing.JTextField txtProduccion;
    private javax.swing.JTextField txtRotura;
    private javax.swing.JTextField txtTurno;
    // End of variables declaration//GEN-END:variables

  
}
