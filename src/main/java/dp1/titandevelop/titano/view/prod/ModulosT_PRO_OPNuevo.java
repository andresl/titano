/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;

import dp1.titandevelop.titano.view.admin.*;
import dp1.titandevelop.titano.bean.Mensajes;
import dp1.titandevelop.titano.bean.TurnoProceso;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.TipoEmpleado;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.service.EmpleadoService;
import dp1.titandevelop.titano.service.ProcesoService;
import dp1.titandevelop.titano.service.TurnoService;
import dp1.titandevelop.titano.service.ValidacionService;
import java.awt.Dimension;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chrestean
 */
public class ModulosT_PRO_OPNuevo extends javax.swing.JInternalFrame {

   
    private TurnoProceso procesos ;
    //maneja los id de los turnos asignados
    private ArrayList<Integer> idturnos;
    private ArrayList<Integer> idprocesos;
    private ArrayList<Turno> turnos;
    private ArrayList<Proceso> process;
    private ArrayList<Integer> products;
    private ArrayList<Integer> merms;
    private int total;
    private int parcial;
    

    public void addidProcesos(int valor) {
        //int i = this.posicionTurno(turno);
        ProcesoService p =  new ProcesoService();
        Proceso v = p.buscarPorId(valor);
        this.procesos.getProcesos().add(v);
        idprocesos.add(valor);
        process.add(v);
        
        
    }

    public void addProduct(int e) {
        //int i = this.posicionTurno(turno);
        this.procesos.getProductividades().add(e);
        this.products.add(e);
    }

    public void addMerma(int e) {
        //int i = this.posicionTurno(turno);
        this.procesos.getMermas().add(e);
        this.merms.add(e);
    }

    public void addidTurno(int e) {
        TurnoService t= new TurnoService();
        Turno tur = t.buscarPorId(e);
        turnos.add(tur);
        this.procesos.getTurnos().add(tur);
        this.idturnos.add(e);
        this.parcial = parcial+1;
        
    }
    public void habilitaBotones(){
        this.btnEliminar.setEnabled(true);
    }
    private void inhabilitaBotones(){
        this.btnEliminar.setEnabled(false);
    }
    public void RefreshTable() {
        AbstractTableModel aux = (AbstractTableModel)tblprocesos.getModel();
        aux.fireTableDataChanged();
    }
    
    
   //Modelo de tabla de procesos
    private AbstractTableModel modeloTabla = new AbstractTableModel()  {  
        @Override
        public int getRowCount() {
            return idturnos.size(); // cuantos registros (filas)
//            return procesos.getIdturnos().size();
        }      
        @Override
        public int getColumnCount() {
            return 4; // cuantas columnas
        }  
        
        String [] titles = {"Turno", "Proceso", "Productividad", "Merma"};
        
         @Override
        public String getColumnName(int col){
            return titles[col];
        }                
       @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            
            Object res=null;
            //boolean b = datos.get(rowIndex).seleccionado;//tipos.get(rowIndex);
            switch(columnIndex){
                case 0: res = turnos.get(rowIndex).getDescripcion();break;
                case 1: res = process.get(rowIndex).getDescripcion(); break;
                case 2: res = products.get(rowIndex); break;
                case 3: res = merms.get(rowIndex); break;
//                
            }
            return res;
        } 
        
        @Override
        public void setValueAt(Object value, int rowIndex, int columnIndex) {
            switch(columnIndex){
//                case 0: res = p.getDescripcion();break;
                case 2: products.set(rowIndex,((Integer)value).intValue()); break;
                case 3: merms.set(rowIndex, ((Integer)value).intValue()) ; break;
            }
        }
        @Override
        public boolean isCellEditable(int row, int col) {
            boolean res=false;
            switch (col) {
                case 0:break;
                case 1:break;
                case 2: res = true; break;
                case 3: res = true; break;
            }
            return res;
        }
        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
    };
    public ModulosT_PRO_OPNuevo() {
        initComponents();
        TurnoService turserv = new TurnoService();
        ProcesoService pserv =  new ProcesoService();
        
        
        ArrayList<Turno> t = turserv.buscar();
        List<Proceso> p = pserv.buscarProcAll();
        this.total = p.size()*t.size();
        this.parcial= 0;
        ValidacionService val = new ValidacionService();
        
        val.SNumeros(this.txtCel);
        val.SNumeros(this.txtDNI);
        val.SNumeros(this.txtTelefono);
        val.SSoloLetra(this.txtAMaterno);
        val.SSoloLetra(this.txtAPaterno);
        val.SSoloLetra(this.txtNombre);
        val.Longitud(txtCel, 9);
        val.Longitud(txtDNI, 8);
        val.Longitud(txtTelefono, 7);
        
        this.tblprocesos.setModel(modeloTabla);
        this.inhabilitaBotones();
//        idlistaturnos= new ArrayList<Integer>();
        idturnos = new ArrayList<Integer>();
        idprocesos = new ArrayList<Integer>();
        this.products = new ArrayList<Integer>();
        this.merms = new ArrayList<Integer>();
        this.turnos = new ArrayList<Turno>();
        this.process = new ArrayList<Proceso>();

//        nprocesos = new ArrayList<Integer>();
//        this.turnos = t.size();
        procesos = new TurnoProceso();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        nombre = new javax.swing.JLabel();
        apPaterno = new javax.swing.JLabel();
        apMaterno = new javax.swing.JLabel();
        fechaNac = new javax.swing.JLabel();
        DNI = new javax.swing.JLabel();
        telefono = new javax.swing.JLabel();
        celular = new javax.swing.JLabel();
        correo = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtAPaterno = new javax.swing.JTextField();
        txtAMaterno = new javax.swing.JTextField();
        txtDNI = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        txtCel = new javax.swing.JTextField();
        txtCorreo = new javax.swing.JTextField();
        txtdireccion = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        cmbsexo = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txtfechanac = new org.jdesktop.swingx.JXDatePicker();
        btnAsignar = new javax.swing.JButton();
        botonGuardar = new javax.swing.JButton();
        botonAtras = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblprocesos = new javax.swing.JTable();
        btnEliminar = new javax.swing.JButton();

        setClosable(true);
        setResizable(true);
        setTitle("Nuevo Operario");
        setToolTipText("");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Personales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 0, 13))); // NOI18N

        nombre.setText("Nombre");

        apPaterno.setText("Ap. Paterno");

        apMaterno.setText("Ap. Materno");

        fechaNac.setText("Fecha de Nacimiento");

        DNI.setText("DNI");

        telefono.setText("Telefono");

        celular.setText("Celular");

        correo.setText("E-mail");

        txtDNI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDNIActionPerformed(evt);
            }
        });

        txtdireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdireccionActionPerformed(evt);
            }
        });

        jLabel1.setText("Dirección");

        cmbsexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Masculino", "Femenino" }));

        jLabel2.setText("Sexo");

        btnAsignar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/agregar.png"))); // NOI18N
        btnAsignar.setToolTipText("Asignar Producto");
        btnAsignar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsignarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(fechaNac)
                        .addComponent(nombre, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(apPaterno, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(apMaterno, javax.swing.GroupLayout.Alignment.LEADING))
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtAMaterno, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAPaterno, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombre, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtfechanac, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DNI)
                            .addComponent(telefono)
                            .addComponent(celular)
                            .addComponent(correo))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCel, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(20, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbsexo, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(btnAsignar)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombre)
                    .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DNI))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(apPaterno)
                            .addComponent(txtAPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(telefono)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(apMaterno)
                            .addComponent(txtAMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(celular)
                        .addComponent(txtCel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(fechaNac)
                        .addComponent(correo)
                        .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtfechanac, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(cmbsexo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnAsignar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        botonGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/grabar.png"))); // NOI18N
        botonGuardar.setToolTipText("Guardar");
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        botonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancelar.png"))); // NOI18N
        botonAtras.setToolTipText("Salir");
        botonAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAtrasActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Procesos Asignados", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 0, 13))); // NOI18N

        tblprocesos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Unidades", "Precio"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tblprocesos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblprocesosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblprocesos);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar.png"))); // NOI18N
        btnEliminar.setToolTipText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEliminar)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnEliminar)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(botonGuardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonAtras, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonAtras)
                    .addComponent(botonGuardar))
                .addContainerGap(51, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAtrasActionPerformed

        ModulosT_PRO_OP admEmp = new ModulosT_PRO_OP();
        this.getParent().add(admEmp);

        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = admEmp.getSize();
        admEmp.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 4);

        admEmp.show();
        this.dispose();

                   
                        
    }//GEN-LAST:event_botonAtrasActionPerformed

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        ValidacionService val = new ValidacionService();
        Mensajes mensajes = new Mensajes();
        EmpleadoService empleado = new EmpleadoService();
             if ((val.esVacio(this.txtAMaterno)) || (val.esVacio(this.txtAPaterno)) || (val.esVacio(this.txtCel)) || (val.esVacio(this.txtCorreo))
                    || (val.esVacio(this.txtDNI))|| (val.esFechaVacia(this.txtfechanac)) || (val.esVacio(this.txtNombre)) || (val.esVacio(this.txtTelefono)) || (val.esVacio(this.txtdireccion))) {
                JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getCompletarCampos(), "Aviso", JOptionPane.WARNING_MESSAGE);

            } else {
              
              if ( ( parcial== total) && !this.idturnos.isEmpty()){
               if (val.validaCorreo(this.txtCorreo)){
                 List<Empleado> validaremp = empleado.buscarDNI(this.txtDNI.getText());
                    if (!validaremp.isEmpty() ){
                        JOptionPane.showInternalMessageDialog(this.getRootPane(),"DNI ya fue ingresado con anterioridad" , "Aviso", JOptionPane.WARNING_MESSAGE);

                    }else{
                     
                 if (val.LongitudExacta(txtDNI.getText(), 8) && val.LongitudExacta(txtCel.getText(), 9)&& val.LongitudExacta(txtTelefono.getText(), 7)){
                    
                      switch (val.fechaNacimiento(this.txtfechanac,18)){
                        case 1: 
                            JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getFechaposterior(), "Aviso", JOptionPane.WARNING_MESSAGE);
                            break;
                        case 2: 
                            JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getFechamayor(), "Aviso", JOptionPane.WARNING_MESSAGE);
                            break;
                        case 3:   
                        {
                            
                            try {
                                String nombres, apepat, apemat, direcc, dni, sexo, tlf, cel, mail;
                                Date fecha;
                                Integer idtipo;

                                nombres = this.txtNombre.getText(); apepat = this.txtAPaterno.getText(); apemat = this.txtAMaterno.getText();
                                direcc = this.txtdireccion.getText(); dni = this.txtDNI.getText();   tlf = this.txtTelefono.getText();  cel = this.txtCel.getText();  
                                mail = this.txtCorreo.getText();    fecha = this.txtfechanac.getDate();

                                if (this.cmbsexo.getSelectedIndex() == 0) { sexo = "Masculino";} 
                                else { sexo = "Femenino";}

                                    idtipo = empleado.idOperario();
//                                    empleado.insertarOperario(idtipo, nombres, apepat, apemat, direcc, fecha, dni, sexo, tlf, cel, mail, nprocesos,products,merms, idturnos);
                                    empleado.insertarOperario(idtipo, nombres, apepat, apemat, direcc, fecha, dni, sexo, tlf, cel, mail, idprocesos,products,merms, idturnos);
                                     JOptionPane.showInternalMessageDialog(this.getRootPane(), mensajes.getRegistrado(), "Mensaje", JOptionPane.INFORMATION_MESSAGE);

                                    ModulosT_PRO.menuOperario = new ModulosT_PRO_OP();
                                    this.getParent().add(ModulosT_PRO.menuOperario);
                                    Dimension desktopSize = this.getParent().getSize();
                                    Dimension jInternalFrameSize = ModulosT_PRO.menuOperario.getSize();
                                    ModulosT_PRO.menuOperario.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                                            (desktopSize.height - jInternalFrameSize.height) / 4);
                                    ModulosT_PRO.menuOperario.show();

                                    this.dispose();
                                } catch(Exception e) {
                              JOptionPane.showInternalMessageDialog(this.getRootPane(),mensajes.getError(),"Error",JOptionPane.ERROR_MESSAGE);
                      
                       }
                            
                        }
                     }
                    }else {
                            if (val.LongitudExacta(txtCel.getText(), 9)){
                             }else{
                                JOptionPane.showInternalMessageDialog(this.getRootPane(), "El celular debe tener 9 digitos ", "Aviso", JOptionPane.WARNING_MESSAGE);
                            }
                            if (val.LongitudExacta(this.txtTelefono.getText(), 7)){
                            }else{
                                JOptionPane.showInternalMessageDialog(this.getRootPane(), "El telefono debe tener 7 digitos ", "Aviso", JOptionPane.WARNING_MESSAGE);
                            }
                            if (val.LongitudExacta(this.txtDNI.getText(), 8)){
                            }else{
                                JOptionPane.showInternalMessageDialog(this.getRootPane(), "El DNI debe tener 8 digitos ", "Aviso", JOptionPane.WARNING_MESSAGE);
                            }
                    }
              }
             }else{
                    JOptionPane.showInternalMessageDialog(this.getRootPane(), "Correo con formato erroneo ", "Aviso", JOptionPane.WARNING_MESSAGE);
              }  
             }else{
                  if (parcial > total){
                      JOptionPane.showInternalMessageDialog(this.getRootPane(), "Se han asignado turnos de más, corregir", "Aviso", JOptionPane.WARNING_MESSAGE);
                  }else{
                      JOptionPane.showInternalMessageDialog(this.getRootPane(), "Faltan asignar turnos y procesos", "Aviso", JOptionPane.WARNING_MESSAGE);
                  }
                  
              
             }
             }
    }//GEN-LAST:event_botonGuardarActionPerformed

    private void txtDNIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDNIActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDNIActionPerformed

    private void txtdireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdireccionActionPerformed

    private void btnAsignarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsignarActionPerformed
        Frame f = JOptionPane.getFrameForComponent(this);
        ModulosT_PRO_OPDetalle dialog = new ModulosT_PRO_OPDetalle(f,true);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnAsignarActionPerformed

    private void tblprocesosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblprocesosMouseClicked
        if(evt.getClickCount()==1){
            this.habilitaBotones();
        }
    }//GEN-LAST:event_tblprocesosMouseClicked

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        int idp = this.idturnos.get(this.tblprocesos.getSelectedRow());
        int i = 0;
        for ( ; i<idturnos.size();){
            if (idturnos.get(i)==idp){
                idturnos.remove(i);
                idprocesos.remove(i);
                products.remove(i);
                merms.remove(i);
                process.remove(i);
                turnos.remove(i);
                this.parcial = parcial -1;
                i = 0;
            }else {
                i++;
            }
        }
       
        this.RefreshTable();
    }//GEN-LAST:event_btnEliminarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel DNI;
    private javax.swing.JLabel apMaterno;
    private javax.swing.JLabel apPaterno;
    private javax.swing.JButton botonAtras;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton btnAsignar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JLabel celular;
    private javax.swing.JComboBox cmbsexo;
    private javax.swing.JLabel correo;
    private javax.swing.JLabel fechaNac;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nombre;
    private javax.swing.JTable tblprocesos;
    private javax.swing.JLabel telefono;
    private javax.swing.JTextField txtAMaterno;
    private javax.swing.JTextField txtAPaterno;
    private javax.swing.JTextField txtCel;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtDNI;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtTelefono;
    private javax.swing.JTextField txtdireccion;
    private org.jdesktop.swingx.JXDatePicker txtfechanac;
    // End of variables declaration//GEN-END:variables
}
