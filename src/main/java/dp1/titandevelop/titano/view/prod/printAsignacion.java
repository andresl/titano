/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;
import dp1.titandevelop.titano.view.ventas.*;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.EmpleadoXProcesoTurnoService;
import dp1.titandevelop.titano.service.ParametroService;
import dp1.titandevelop.titano.service.ProductoService;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heli
 */
public class printAsignacion {
    private static Font fontBold = new Font(Font.FontFamily.COURIER, 12, Font.BOLD);
    private static Font fontNormal = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL);
    private String nomArch;
    public printAsignacion(List<Asignacion> turno1,List<Asignacion> turno2,List<Asignacion> turno3,String nombreArchivo) throws Exception{
		
       this.nomArch = nombreArchivo;         
       Document document = new Document();
     
        PdfWriter.getInstance(document, 
                new FileOutputStream(nombreArchivo));
        document.open();

        
        
        document.add(getHeader("Detalle de Asignacion "));
        document.add(getInformation(getCurrentDateTime()));
        document.add(getInformation(" "));
        document.add(getInformation(" "));
        
        Image image1 = Image.getInstance(getClass().getResource("/img/GOOD_COOKIE.jpg"));
                      PdfPTable table4 = new PdfPTable(1); // Code 1
                PdfPCell cell001y = 
			new PdfPCell(image1);
                        cell001y.setBorder(0);
                table4.addCell(cell001y);
                
                document.add(table4);
                document.add(getInformation(" "));
                
        
        adicionarTurno(document,"Mañana", turno1);
        adicionarTurno(document,"Tarde",turno2);
        adicionarTurno(document,"Noche",turno3);
       
        
        document.close();
       // imprimirAsignacion();
    }
     
    private void adicionarTurno(Document document,String turno, List<Asignacion> listTurno) throws DocumentException{
        
        
         if(!listTurno.isEmpty()) {
        document.add(getInformation2(turno));
        /*
        Chunk c0 = new Chunk("Producto");
        c0.setFont(fontBold);
        
        Chunk c1 = new Chunk("Empleado");
        c1.setFont(fontBold);
        
        Chunk c2 = new Chunk("Proceso");
        c2.setFont(fontBold);
        
        Chunk c3 = new Chunk("Num Maquina");
        c3.setFont(fontBold);
        
        Chunk c4 = new Chunk("Productividad");
        c4.setFont(fontBold);
        
        Chunk c5 = new Chunk("Rotura");
        c5.setFont(fontBold);
        */
        
        PdfPCell cell006 = new PdfPCell(new Paragraph(""));
        cell006.setBorder(0);
        //table.addCell(cell006);
        
        //for para llenar
        
        DocumentoVentaService service=new DocumentoVentaService();
        EmpleadoXProcesoTurnoService service2= new EmpleadoXProcesoTurnoService();
        int numProd=service.listProduct().size();
        int mayor=0,superior=1000;
        
        for(int j=0;j<numProd;j++){
            
            PdfPTable table = new PdfPTable(6); 
            mayor=0;    
        
            
        for(int i=0;i<listTurno.size();i++){
            if(listTurno.get(i).getToProducto().getIdproducto()>mayor &&
               listTurno.get(i).getToProducto().getIdproducto()<superior ){
            mayor=listTurno.get(i).getToProducto().getIdproducto();
            }
        }
        
        if(mayor>0){
        
            PdfPCell cell000 = new PdfPCell(new Paragraph(cabeceraTable("Producto")));
        table.addCell(cell000);
        
        PdfPCell cell001 = new PdfPCell(new Paragraph(cabeceraTable("Empleado")));
        table.addCell(cell001);
        
        PdfPCell cell002 = new PdfPCell(new Paragraph(cabeceraTable("Proceso")));
        table.addCell(cell002);
        
        PdfPCell cell003 = new PdfPCell(new Paragraph(cabeceraTable("Num Maquina")));
        table.addCell(cell003);
        
        PdfPCell cell004 = new PdfPCell(new Paragraph(cabeceraTable("Productividad")));
        table.addCell(cell004);
        
        PdfPCell cell005 = new PdfPCell(new Paragraph(cabeceraTable("Rotura")));
        table.addCell(cell005);
        
        
        
        }
        
        superior=mayor;
        int ini=0;
        
        for(int i=0;i<listTurno.size();i++){
            if(listTurno.get(i).getToProducto().getIdproducto()==mayor){
              if(ini==0){table.addCell(celda(""+listTurno.get(i).getToProducto().getDescripcion()));ini=1;}
              else
                  table.addCell(cell006);
              table.addCell(celda(""+listTurno.get(i).getToEmpleado().getNombres()+" "+listTurno.get(i).getToEmpleado().getApellidop()));
              table.addCell(celda(""+listTurno.get(i).getToMaquina().getToProceso().getDescripcion()));
              table.addCell(celda(""+listTurno.get(i).getToMaquina().getIdmaquina()));
              EmpleadoXProcesoTurno e=service2.buscarUnEmpleadoXProceso(listTurno.get(i).getToEmpleado().getIdempleado(), listTurno.get(i).getToTurno().getIdturno(),listTurno.get(i).getToMaquina().getToProceso().getIdproceso());
              table.addCell(celda(""+e.getProductividad()));
              table.addCell(celda(""+e.getRotura()));
              
            
            }
        }
        document.add(table);
        document.add(getInformation(" "));
        }
        
        
        
    
    }
    }
    private Chunk cabeceraTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.setFont(fontBold);c.getFont().setSize(9);
         c.getFont().setFamily("Arial");
      return c;
     }
    private Chunk dataTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.getFont().setSize(8);
         c.getFont().setFamily("Arial");
      return c;
     }
     private PdfPCell celda(String cadena){
     
         PdfPCell cell = new PdfPCell(new Paragraph(dataTable(cadena))); 
         return cell;
     }
    
     private Paragraph getHeader(String header) {
    	Paragraph paragraph = new Paragraph();
        Chunk chunk = new Chunk();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        chunk.append( header + "\n");
        chunk.setFont(fontBold);
        paragraph.add(chunk);
        return paragraph;
     }
     private Paragraph getInformation(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        chunk.append(informacion);
        chunk.setFont(fontNormal);

        paragraph.add(chunk);
        return paragraph;
      }
     
     private Paragraph getInformation2(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_LEFT);
  		chunk.append(informacion);
                chunk.setFont(fontBold);
  		//chunk.setFont(fontNormal);
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformationFooter(String informacion) {
     	Paragraph paragraph = new Paragraph();
     	Chunk chunk = new Chunk();
   		paragraph.setAlignment(Element.ALIGN_CENTER);
   		chunk.append(informacion);
   		chunk.setFont(new Font(Font.FontFamily.COURIER, 8, Font.NORMAL));
   		paragraph.add(chunk);
    		return paragraph;
       }
      private float getConvertCmsToPoints(float cm) {
     	return cm * 28.4527559067f;
     }
     
   public void imprimirAsignacion(){
     
     Desktop d=Desktop.getDesktop();
     try {
      if(Desktop.isDesktopSupported()){
          //d.open(new File("/tmp/Asignacion.pdf"));
      d.open(new File(this.nomArch));
      }
     } catch (IOException e) {
            e.printStackTrace();
        }
   }
      
      
   private String getCurrentDateTime() {
     	Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yy '-' hh:mm");
     	return ft.format(dNow);
    }
	
}
