/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;

import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.DetalleDamanda;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.service.DemandaService;
import dp1.titandevelop.titano.service.EmpleadoXProcesoTurnoService;
import dp1.titandevelop.titano.service.ProcesoService;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author cHreS
 */
public class DetalleAsigRecTurno extends javax.swing.JInternalFrame {

    private Empleado empleado = new Empleado();
    
    
    public DetalleAsigRecTurno() {
        initComponents();
    }

    public DetalleAsigRecTurno(Empleado empleado) {
        initComponents();
        
        /*llenar el ComboBox*/
        
        ProcesoService service=new ProcesoService();
        
        ArrayList<Proceso> proceso= (ArrayList<Proceso>)service.buscarProcAll();
        
        for(int i=0;i<proceso.size();i++){
            this.jComboProceso.addItem(proceso.get(i).getDescripcion());
        }
        
        this.empleado=empleado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelNoche = new javax.swing.JPanel();
        jLabelPNoche = new javax.swing.JLabel();
        jLabelRNoche = new javax.swing.JLabel();
        jTextFieldPNoche = new javax.swing.JTextField();
        jTextFieldRNoche = new javax.swing.JTextField();
        jPanelMañana = new javax.swing.JPanel();
        jLabelPMañana = new javax.swing.JLabel();
        jLabelRMañana = new javax.swing.JLabel();
        jTextFieldPMañana = new javax.swing.JTextField();
        jTextFieldRMañana = new javax.swing.JTextField();
        jPanelTarde = new javax.swing.JPanel();
        jLabelPTarde = new javax.swing.JLabel();
        jLabelRTarde = new javax.swing.JLabel();
        jTextFieldPTarde = new javax.swing.JTextField();
        jTextFieldRTarde = new javax.swing.JTextField();
        jComboProceso = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();

        setResizable(true);

        jPanelNoche.setBorder(javax.swing.BorderFactory.createTitledBorder("Turno Noche"));

        jLabelPNoche.setText("Producción :");

        jLabelRNoche.setText("Rotura :");

        jTextFieldPNoche.setEditable(false);
        jTextFieldPNoche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPNocheActionPerformed(evt);
            }
        });

        jTextFieldRNoche.setEditable(false);
        jTextFieldRNoche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldRNocheActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelNocheLayout = new javax.swing.GroupLayout(jPanelNoche);
        jPanelNoche.setLayout(jPanelNocheLayout);
        jPanelNocheLayout.setHorizontalGroup(
            jPanelNocheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNocheLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelNocheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelRNoche)
                    .addComponent(jLabelPNoche, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelNocheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldRNoche, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPNoche, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNocheLayout.setVerticalGroup(
            jPanelNocheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelNocheLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanelNocheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPNoche)
                    .addComponent(jTextFieldPNoche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelNocheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextFieldRNoche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelRNoche, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(13, 13, 13))
        );

        jPanelMañana.setBorder(javax.swing.BorderFactory.createTitledBorder("Turno Mañana"));

        jLabelPMañana.setText("Producción :");

        jLabelRMañana.setText("Rotura :");

        jTextFieldPMañana.setEditable(false);
        jTextFieldPMañana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPMañanaActionPerformed(evt);
            }
        });

        jTextFieldRMañana.setEditable(false);
        jTextFieldRMañana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldRMañanaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelMañanaLayout = new javax.swing.GroupLayout(jPanelMañana);
        jPanelMañana.setLayout(jPanelMañanaLayout);
        jPanelMañanaLayout.setHorizontalGroup(
            jPanelMañanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelMañanaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelMañanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabelPMañana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelRMañana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanelMañanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldRMañana, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPMañana, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanelMañanaLayout.setVerticalGroup(
            jPanelMañanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMañanaLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanelMañanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPMañana)
                    .addComponent(jTextFieldPMañana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelMañanaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldRMañana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelRMañana))
                .addGap(13, 13, 13))
        );

        jPanelTarde.setBorder(javax.swing.BorderFactory.createTitledBorder("Turno Tarde"));

        jLabelPTarde.setText("Producción :");

        jLabelRTarde.setText("Rotura :");

        jTextFieldPTarde.setEditable(false);
        jTextFieldPTarde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPTardeActionPerformed(evt);
            }
        });

        jTextFieldRTarde.setEditable(false);
        jTextFieldRTarde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldRTardeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelTardeLayout = new javax.swing.GroupLayout(jPanelTarde);
        jPanelTarde.setLayout(jPanelTardeLayout);
        jPanelTardeLayout.setHorizontalGroup(
            jPanelTardeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTardeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTardeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabelRTarde, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelPTarde, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanelTardeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldRTarde, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPTarde, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelTardeLayout.setVerticalGroup(
            jPanelTardeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTardeLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanelTardeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPTarde)
                    .addComponent(jTextFieldPTarde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelTardeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldRTarde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelRTarde))
                .addGap(13, 13, 13))
        );

        jComboProceso.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar" }));
        jComboProceso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboProcesoActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        jButton1.setToolTipText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanelTarde, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanelNoche, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(8, 8, 8))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanelMañana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 8, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jComboProceso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboProceso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(jPanelMañana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelTarde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelNoche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jButton1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldPMañanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPMañanaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPMañanaActionPerformed

    private void jTextFieldRMañanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldRMañanaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldRMañanaActionPerformed

    private void jTextFieldPTardeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPTardeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPTardeActionPerformed

    private void jTextFieldRTardeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldRTardeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldRTardeActionPerformed

    private void jTextFieldPNocheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPNocheActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPNocheActionPerformed

    private void jTextFieldRNocheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldRNocheActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldRNocheActionPerformed

    private void jComboProcesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboProcesoActionPerformed
        ProcesoService service=new ProcesoService();
        EmpleadoXProcesoTurnoService serv= new EmpleadoXProcesoTurnoService();
        String descripcionProceso=(String)this.jComboProceso.getSelectedItem();
        Proceso p=service.buscarPorDescripcion(descripcionProceso);
        
        ArrayList<EmpleadoXProcesoTurno> emp=serv.buscarUnEmpleadoXProceso(empleado.getIdempleado(), p.getIdproceso());
        
        if(!emp.isEmpty()){
            for(int i=0;i<emp.size();i++){
                if(emp.get(i).getToTurno().getIdturno()==1){
                this.jTextFieldPMañana.setText(""+emp.get(i).getProductividad());            
                this.jTextFieldRMañana.setText(""+emp.get(i).getRotura());
                }else if(emp.get(i).getToTurno().getIdturno()==2){
                this.jTextFieldPTarde.setText(""+emp.get(i).getProductividad());            
                this.jTextFieldRTarde.setText(""+emp.get(i).getRotura());
                }else if(emp.get(i).getToTurno().getIdturno()==3){
                this.jTextFieldPNoche.setText(""+emp.get(i).getProductividad());            
                this.jTextFieldRNoche.setText(""+emp.get(i).getRotura());
                }
            }
            if(this.jTextFieldPMañana.getText().isEmpty()){
                this.jTextFieldPMañana.setText(""+0);            
            }
            if(this.jTextFieldRMañana.getText().isEmpty()){
            this.jTextFieldRMañana.setText(""+0);
            }
            if(this.jTextFieldPTarde.getText().isEmpty()){
            this.jTextFieldPTarde.setText(""+0);     
            }
            if(this.jTextFieldRTarde.getText().isEmpty()){
            this.jTextFieldRTarde.setText(""+0);
            }
            if(this.jTextFieldPNoche.getText().isEmpty()){
            this.jTextFieldPNoche.setText(""+0);     
            }
            if(this.jTextFieldRNoche.getText().isEmpty()){
            this.jTextFieldRNoche.setText(""+0);
            }
        }else{
            this.jTextFieldPMañana.setText(""+0);            
            this.jTextFieldRMañana.setText(""+0);
            this.jTextFieldPTarde.setText(""+0);            
            this.jTextFieldRTarde.setText(""+0);
            this.jTextFieldPNoche.setText(""+0);            
            this.jTextFieldRNoche.setText(""+0);
        }
    }//GEN-LAST:event_jComboProcesoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboProceso;
    private javax.swing.JLabel jLabelPMañana;
    private javax.swing.JLabel jLabelPNoche;
    private javax.swing.JLabel jLabelPTarde;
    private javax.swing.JLabel jLabelRMañana;
    private javax.swing.JLabel jLabelRNoche;
    private javax.swing.JLabel jLabelRTarde;
    private javax.swing.JPanel jPanelMañana;
    private javax.swing.JPanel jPanelNoche;
    private javax.swing.JPanel jPanelTarde;
    private javax.swing.JTextField jTextFieldPMañana;
    private javax.swing.JTextField jTextFieldPNoche;
    private javax.swing.JTextField jTextFieldPTarde;
    private javax.swing.JTextField jTextFieldRMañana;
    private javax.swing.JTextField jTextFieldRNoche;
    private javax.swing.JTextField jTextFieldRTarde;
    // End of variables declaration//GEN-END:variables
}
