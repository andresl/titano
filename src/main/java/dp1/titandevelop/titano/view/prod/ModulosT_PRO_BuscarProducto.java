/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;



import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.TipoProducto;

import dp1.titandevelop.titano.service.ProductoService;
import java.util.ArrayList;


import javax.swing.table.AbstractTableModel;

/**
 *
 * @author andres
 */
public class ModulosT_PRO_BuscarProducto extends javax.swing.JDialog {

   ProductoService s = new ProductoService();
  
    private ArrayList<Producto> listaProductos;
    private ArrayList<Producto> listaEntrada;
    private ArrayList<TipoProducto> listaTipos;
    private ArrayList<String> listaTiposString;
    int padre;
    Producto prod;
    Producto prodPadre;
   
    
    //Refrescar la tabla de productos
    public void RefrescarTabla() {
        AbstractTableModel aux = (AbstractTableModel) tblProductos.getModel();
        this.inhabilitaBotones();
        aux.fireTableDataChanged();
    }
    
    //Filtra los insumos ya usados
    public ArrayList<Producto> FiltrarLista(ArrayList<Producto> insumos)    
    {
        ArrayList<Producto> l = s.buscarNoTerminados();
        if (insumos!=null && l!=null)
        for (int j=0; j<l.size(); j++)
        {
            for(int i=0; i<insumos.size(); i++)
            {
                if (l.get(j).getIdproducto()==insumos.get(i).getIdproducto())
                {
                    l.remove(j);
                    j=j-1;
                }
                   
            }
        }
        return l;
    } 
    public ArrayList<Producto> FiltrarLista(ArrayList<Producto> insumos, Producto p)    
    {
        ArrayList<Producto> l = s.buscarNoTerminados();
        int lid, pid=0, iid;
       if (p!=null) pid=p.getIdproducto();
        if (insumos!=null && l!=null)
        for (int j=0; j<l.size(); j++)
        {
            lid=l.get(j).getIdproducto();
            if (lid==pid)
                {
                    l.remove(j);
                    j--;
                    break;
                }
            for(int i=0; i<insumos.size(); i++)
            {
                
                iid=insumos.get(i).getIdproducto();
                
                if (lid==iid)
                {
                    l.remove(j);
                    j--;
                    break;
                } 
            }
             
        }
        return l;
    } 
    public ArrayList<Producto> FiltrarListaBTipo(ArrayList<Producto> insumos, Producto p,int tipo)    
    {
        ArrayList<Producto> l = s.buscarPorTipo(tipo);
       int lid, pid=0, iid;
       if (p!=null) pid=p.getIdproducto();
        if (insumos!=null && l!=null)
        for (int j=0; j<l.size(); j++)
        {
            lid=l.get(j).getIdproducto();
            if (lid==pid)
                {
                    l.remove(j);
                    j--;
                    break;
                }
            for(int i=0; i<insumos.size(); i++)
            {
                
                iid=insumos.get(i).getIdproducto();
                
                if (lid==iid)
                {
                    l.remove(j);
                    j--;
                    break;
                } 
            }
             
        }
        return l;
    } 
    
    public ArrayList<Producto> FiltrarListaBusqueda(ArrayList<Producto> insumos)    
    {
        Estado e =new Estado();
        ArrayList<Producto> l = s.buscarMulticriterioNoTer(Estado.devuelveCadena(e.getActivo()),txtProducto.getText(), (String)cmbTipoProductos.getSelectedItem());
        if (insumos!=null)
        for (int j=0; j<l.size(); j++)
        {
            for(int i=0; i<insumos.size(); i++)
            {
                if (l.get(j).getIdproducto()==insumos.get(i).getIdproducto())
                {l.remove(j);
                j=j-1;
                }   
                   
            }
        }
        return l;
    } 
    //habilitar y deshabilitar botones para actualizar y eliminar
    public void habilitaBotones(){
        this.btnAsignar.setEnabled(true);
      
    }

    public void inhabilitaBotones() {
        this.btnAsignar.setEnabled(false);
       
    }
    
    private AbstractTableModel modelo = new AbstractTableModel() {
    public int getRowCount() {
        if(listaProductos!=null)
        return listaProductos.size();
        else return 0;
    }

    public int getColumnCount() {
    return 4;
    }

    String [] titles = {"ID","Descripción","Tipo","UM"};

    @Override
    public String getColumnName(int col){
    return titles[col];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
    //String res=null;
    Producto p=listaProductos.get(rowIndex);
    switch (columnIndex){
    case 0: return p.getIdproducto();
    case 1: return p.getDescripcion();
    case 2: return p.getToTipoProducto().getDescripcion(); 
    case 3: return p.getUm();

    }
    return null;
    } 
    };
    
    
    public ModulosT_PRO_BuscarProducto() {       
        
    }   
    
    public ModulosT_PRO_BuscarProducto(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        
        prod = new Producto();
        prodPadre =new Producto();
        initComponents();
        this.pnlDatos.setVisible(false);
        this.padre =0;
        this.listaProductos = s.buscarNoInsumos();
        this.listaTiposString = new ArrayList<String>();
        this.listaTipos = s.buscarTodoLosTipos();//new ArrayList<TipoProducto>();
        this.tblProductos.setModel(this.modelo);
        
        for (int i=0; i<(this.listaTipos.size()-1); i++)
        {
            listaTiposString.add(listaTipos.get(i).getDescripcion()); 
        }
        
        
        cmbTipoProductos.setModel(new javax.swing.DefaultComboBoxModel(listaTiposString.toArray()));
        cmbTipoProductos.setSelectedItem(null);}
    
       
    public ModulosT_PRO_BuscarProducto(java.awt.Frame parent, boolean modal, ArrayList<Producto> l, int o, Producto prod1) {       
        super(parent, modal);
        initComponents();
        prod = new Producto();
        prodPadre = prod1;
        this.padre = o;
        this.listaEntrada =l;
        if (padre==1)            
            this.listaProductos = FiltrarLista(listaEntrada);
        else
            this.listaProductos = FiltrarLista(listaEntrada, prodPadre);
        this.listaTiposString = new ArrayList<String>();
        this.listaTipos = s.buscarTodoLosTipos();//new ArrayList<TipoProducto>();
        this.tblProductos.setModel(this.modelo);
        
        for (int i=0; i<this.listaTipos.size()-1; i++)
        {
            listaTiposString.add(listaTipos.get(i).getDescripcion()); 
        }
        
        cmbTipoProductos.setModel(new javax.swing.DefaultComboBoxModel(listaTiposString.toArray()));
        cmbTipoProductos.setSelectedItem(null);
        txtProducto.enable(false);
    }    
    
  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAsignar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        pnlBusqueda = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        cmbTipoProductos = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProductos = new javax.swing.JTable();
        pnlDatos = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtProducto = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();

        setTitle("Escoger Producto");

        btnAsignar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/asignar.png"))); // NOI18N
        btnAsignar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsignarActionPerformed(evt);
            }
        });

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        pnlBusqueda.setBorder(javax.swing.BorderFactory.createTitledBorder("Búsqueda de Producto"));

        jLabel3.setText("Tipo ");

        cmbTipoProductos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipoProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoProductosActionPerformed(evt);
            }
        });

        tblProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblProductosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblProductos);

        javax.swing.GroupLayout pnlBusquedaLayout = new javax.swing.GroupLayout(pnlBusqueda);
        pnlBusqueda.setLayout(pnlBusquedaLayout);
        pnlBusquedaLayout.setHorizontalGroup(
            pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
                    .addGroup(pnlBusquedaLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(34, 34, 34)
                        .addComponent(cmbTipoProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlBusquedaLayout.setVerticalGroup(
            pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBusquedaLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipoProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlDatos.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));

        jLabel1.setText("Producto");

        jLabel4.setText("Cantidad");

        txtProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProductoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDatosLayout = new javax.swing.GroupLayout(pnlDatos);
        pnlDatos.setLayout(pnlDatosLayout);
        pnlDatosLayout.setHorizontalGroup(
            pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(31, 31, 31)
                .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlDatosLayout.setVerticalGroup(
            pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(pnlDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(txtProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAsignar, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(btnSalir)))
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(pnlBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAsignar)
                    .addComponent(btnSalir))
                .addGap(8, 8, 8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAsignarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsignarActionPerformed
        // TODO add your handling code here:
       if (ModulosT_PRO.ventanaProductosNuevo!=null && padre ==1 )
            {
                ModulosT_PRO.ventanaProductosNuevo.Llenar_Insumos(prod, Float.parseFloat(txtCantidad.getText()));
                ModulosT_PRO.ventanaProductosNuevo.RefrescarTablaReceta();
                txtCantidad.setText("");
                txtProducto.setText(" ");
                //this.listaEntrada.add(prod);
                listaProductos.remove(this.tblProductos.getSelectedRow());                
                RefrescarTabla();
            }
        else 
            {
                ModulosT_PRO.ventanaPreoductosEdita.Llenar_Insumos(prod, Float.parseFloat(txtCantidad.getText()));
                ModulosT_PRO.ventanaPreoductosEdita.RefrescarTablaReceta();
                txtCantidad.setText("");
                txtProducto.setText(" ");
               // this.listaEntrada.add(prod);
                listaProductos.remove(this.tblProductos.getSelectedRow());
                RefrescarTabla();
            }
        
    }//GEN-LAST:event_btnAsignarActionPerformed

    private void tblProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblProductosMouseClicked
        // TODO add your handling code here:
        if(evt.getClickCount()>=1)
        {
            this.habilitaBotones(); 
            prod = this.listaProductos.get(this.tblProductos.getSelectedRow());
            this.txtProducto.setText(prod.getDescripcion());
        } 
    }//GEN-LAST:event_tblProductosMouseClicked

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
                 this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProductoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProductoActionPerformed

    private void cmbTipoProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoProductosActionPerformed
        // TODO add your handling code here:
        String tipo=(String) cmbTipoProductos.getSelectedItem();
        if(tipo!=null)
         {  if(tipo.equals("Insumo"))
                this.listaProductos=this.FiltrarListaBTipo(this.listaEntrada, prodPadre, 1);
            else if(tipo.equals("Producto Intermedio"))
                this.listaProductos=this.FiltrarListaBTipo(this.listaEntrada, prodPadre, 2);
            this.RefrescarTabla();
        }
    }//GEN-LAST:event_cmbTipoProductosActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAsignar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cmbTipoProductos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlBusqueda;
    private javax.swing.JPanel pnlDatos;
    private javax.swing.JTable tblProductos;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtProducto;
    // End of variables declaration//GEN-END:variables

  
}
