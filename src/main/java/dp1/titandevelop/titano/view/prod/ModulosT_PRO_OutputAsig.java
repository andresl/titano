/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;

import dp1.titandevelop.titano.bean.DetalleAsig;
import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.service.AsignacionService;
import java.awt.Dimension;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author cHreS
 */
public class ModulosT_PRO_OutputAsig extends javax.swing.JInternalFrame {

    public List<DetalleAsig> listaIDAsignacion ;
    public List<Asignacion> listaAsignacion;
    
    public ModulosT_PRO_DetalleAsignacion ventanaDetalle;
    public ModulosT_PRO_AprobarAsignacion ventanaAprobar;
     
    public void RefreshTable() {

       AbstractTableModel aux = (AbstractTableModel) this.tablaCorridaAlgo.getModel();
        aux.fireTableDataChanged();
    }
     
     public void habilitaBotones() {
        this.btnDetalle.setEnabled(true);
        this.btnAprobar.setEnabled(true);
       // this.btnEliminar.setEnabled(true);
    }

    private void inhabilitaBotones() {
        this.btnDetalle.setEnabled(false);
        this.btnAprobar.setEnabled(false);
     //   this.btnEliminar.setEnabled(false);
    }
    
    private AbstractTableModel ModeloTabla = new AbstractTableModel() {
        //Atributos de los datos de la tabla
        private String[] titles = {"Cód.Asignación","Descripción","Fecha","Cant.Empleados","T.Beneficio","T.Costo","Estado"};

        @Override
        public int getRowCount() {
            return listaIDAsignacion.size(); // listaIDAsignacion.size();//tipos.size(); // cuantos registros (filas)
        }

        @Override
        public int getColumnCount() {
            return 7; // cuantas columnas
        }

        @Override
        public String getColumnName(int col) {
            return titles[col];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Estado e= new Estado();
            Object resultado = null;
            
            DateFormat fecha = DateFormat.getDateInstance(DateFormat.MEDIUM);
            AsignacionService serv= new AsignacionService ();
            DetalleAsig detalle = listaIDAsignacion.get(rowIndex);
            //DetalleAsig detalle=serv.ObtenerData(idAsig);
            
            switch (columnIndex) {
                case 0: resultado = detalle.getIdAsig();break;
                case 1: resultado =detalle.getDescripcion();break;
                case 2: resultado =fecha.format(detalle.getFecha());break;
                case 3: resultado =detalle.getCantidadEmpleado();break;
                case 4: resultado =String.format("%.2f",detalle.getBeneficio());break;
                case 5: resultado =String.format("%.2f",detalle.getCosto());break;
                case 6: resultado =dp1.titandevelop.titano.bean.Estado.devuelveCadena(detalle.getEstado());break;
                                    
            }
            return resultado;
        }
    };
    
    public ModulosT_PRO_OutputAsig() {
        initComponents();
        AsignacionService as= new AsignacionService ();
        listaIDAsignacion=as.ObtenerData();        
        this.inhabilitaBotones();
        this.tablaCorridaAlgo.setModel(ModeloTabla);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        PanelCorridaAlgoritmo = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaCorridaAlgo = new javax.swing.JTable();
        btnDetalle = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        btnAprobar = new javax.swing.JButton();

        setBorder(null);
        setResizable(true);
        setTitle("Asignaciones de Distribución de Recursos");
        setToolTipText("");

        PanelCorridaAlgoritmo.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista de Asignaciones"));
        PanelCorridaAlgoritmo.setFont(new java.awt.Font("Tekton Pro", 0, 11)); // NOI18N

        tablaCorridaAlgo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código Asig", "Descripción", "Cant. Empleados", "Cant. Máquinas", "T. Producido", "T.Merma"
            }
        ));
        tablaCorridaAlgo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCorridaAlgoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaCorridaAlgo);

        javax.swing.GroupLayout PanelCorridaAlgoritmoLayout = new javax.swing.GroupLayout(PanelCorridaAlgoritmo);
        PanelCorridaAlgoritmo.setLayout(PanelCorridaAlgoritmoLayout);
        PanelCorridaAlgoritmoLayout.setHorizontalGroup(
            PanelCorridaAlgoritmoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCorridaAlgoritmoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 633, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelCorridaAlgoritmoLayout.setVerticalGroup(
            PanelCorridaAlgoritmoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCorridaAlgoritmoLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/detalle.png"))); // NOI18N
        btnDetalle.setToolTipText("Ver Detalle");
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/retroceder.png"))); // NOI18N
        jButton1.setToolTipText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnAprobar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/asignar.png"))); // NOI18N
        btnAprobar.setToolTipText("Aprobar");
        btnAprobar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAprobarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDetalle)
                        .addGap(0, 0, 0)
                        .addComponent(btnAprobar)
                        .addGap(0, 0, 0)
                        .addComponent(jButton1))
                    .addComponent(PanelCorridaAlgoritmo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PanelCorridaAlgoritmo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton1)
                        .addComponent(btnDetalle))
                    .addComponent(btnAprobar, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaCorridaAlgoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCorridaAlgoMouseClicked
         this.habilitaBotones();
    }//GEN-LAST:event_tablaCorridaAlgoMouseClicked

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed

        AsignacionService asigservice= new AsignacionService ();
        int idObtenido = this.listaIDAsignacion.get(this.tablaCorridaAlgo.getSelectedRow()).getIdAsig();
        this.listaAsignacion=(ArrayList)asigservice.buscarIdAsignacion(idObtenido);
        
        
        ventanaDetalle = new ModulosT_PRO_DetalleAsignacion(listaAsignacion,idObtenido);
        this.getParent().add(ventanaDetalle);
        
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = ventanaDetalle.getSize();
        ventanaDetalle.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
        ventanaDetalle.show();
        
    }//GEN-LAST:event_btnDetalleActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnAprobarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAprobarActionPerformed
        AsignacionService asigservice= new AsignacionService ();
        
        int idObtenido = this.listaIDAsignacion.get(this.tablaCorridaAlgo.getSelectedRow()).getIdAsig();
        
        if(idObtenido!=asigservice.buscarIdAsignado()){
            this.listaAsignacion=(ArrayList)asigservice.buscarIdAsignacion(idObtenido);
            
            ventanaAprobar = new ModulosT_PRO_AprobarAsignacion(listaAsignacion,idObtenido);
            this.getParent().add(ventanaAprobar);

            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = ventanaAprobar.getSize();
            ventanaAprobar.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,(desktopSize.height - jInternalFrameSize.height)/4 );
            ventanaAprobar.show();
            
            this.dispose();
        }else{
            JOptionPane.showInternalMessageDialog(this.getRootPane(),"Esta asignacion ya se encuentra aprobada","Mensaje",JOptionPane.INFORMATION_MESSAGE);
        }
        
        
    }//GEN-LAST:event_btnAprobarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelCorridaAlgoritmo;
    private javax.swing.JButton btnAprobar;
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaCorridaAlgo;
    // End of variables declaration//GEN-END:variables
}
