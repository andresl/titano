/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.persistent.ProductoXProceso;
import dp1.titandevelop.titano.persistent.Receta;
import dp1.titandevelop.titano.service.AlmacenService;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.ParametroService;
import dp1.titandevelop.titano.service.ProcesoService;
import dp1.titandevelop.titano.service.ProductoXProcesoService;
import dp1.titandevelop.titano.service.RecetaService;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heli
 */
public class reporte_receta {
    
    private static Font fontBold = new Font(Font.FontFamily.COURIER, 12, Font.BOLD);
    private static Font fontNormal = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL);
    private String nomArch;
    public reporte_receta(Integer idProd,Float cantidad, String nombreArchivo) throws Exception{
    
                //Float cantidad=(float)1;
                //Integer idProd=17;
                this.nomArch = nombreArchivo;
		Document document = new Document();
		PdfWriter pdfWriter =PdfWriter.getInstance(document, 
			//new FileOutputStream("/tmp/reporte-receta.pdf"));
                        new FileOutputStream(nombreArchivo));
		document.open();
		
                      document.add(getHeader("Reporte de Materiales y Recetas  "));
                      
                      document.add(getInformation(" "));
                      
                      Image image1 = Image.getInstance(getClass().getResource("/img/GOOD_COOKIE.jpg"));
                      PdfPTable table4 = new PdfPTable(1); // Code 1
                PdfPCell cell001y = 
			new PdfPCell(image1);
                        cell001y.setBorder(0);
                table4.addCell(cell001y);
                
                document.add(table4);
                document.add(getInformation(" "));
                      
           
                    PdfPCell cell01 = new PdfPCell(new Paragraph(cabeceraTable("Proceso"))); 
                    PdfPCell cell02 = new PdfPCell(new Paragraph(cabeceraTable("Orden de Proceso")));
                    PdfPCell cell03 = new PdfPCell(new Paragraph(cabeceraTable("Salida")));
                    PdfPCell cell04 = new PdfPCell(new Paragraph(cabeceraTable("Cantidad")));
                    PdfPCell cell05 = new PdfPCell(new Paragraph(cabeceraTable("UM")));
                    
                    PdfPCell cell06 = new PdfPCell(new Paragraph(cabeceraTable("Insumo")));
                    PdfPCell cell06x = new PdfPCell(new Paragraph(cabeceraTable("Cantidad Receta")));
                    PdfPCell cell07 = new PdfPCell(new Paragraph(cabeceraTable("Cantidad Necesaria")));
                    PdfPCell cell08 = new PdfPCell(new Paragraph(cabeceraTable("UM")));
                    PdfPCell cell09 = new PdfPCell(new Paragraph(cabeceraTable("Costo Unitario (S/.)")));
                    PdfPCell cell10 = new PdfPCell(new Paragraph(cabeceraTable("Stock Actual")));
                    PdfPCell cell11 = new PdfPCell(new Paragraph(cabeceraTable("Cantidad a Obtener")));
                    PdfPCell cell12 = new PdfPCell(new Paragraph(cabeceraTable("Costo Total (S/.)")));
                    
                    
                    PdfPCell cell13 = new PdfPCell(new Paragraph(cabeceraTable("Proceso")));
                    PdfPCell cell14 = new PdfPCell(new Paragraph(cabeceraTable("Orden del Proceso")));
                    PdfPCell cell15 = new PdfPCell(new Paragraph(cabeceraTable("Salida")));
                    
                    PdfPCell cell =new PdfPCell(new Paragraph(""));
		    cell.setBorder(0);
                    
                    
                    ProductoXProcesoService service=new ProductoXProcesoService();
                    RecetaService service2=new RecetaService();
                    AlmacenService serviceA =new AlmacenService();
                   List<ProductoXProceso> procesos=service.buscaPorProducto(idProd);
                   List<Integer> insumos=new ArrayList<Integer>();
                   List<PdfPTable> listTable=new ArrayList<PdfPTable>();
                   //primero busco el mayor proceso
                   Integer mayor=0,superior=1000,anterior=0,OrdenSaliente=0,idProductoSaliente=0,idProductoSalienteAnt=0;
                   String ProductoSaliente="",ProcesoSaliente="",UnidadSaliente="";
                   Float cantSig=(float)-1;
                   
                   ProcesoService service3=new ProcesoService();
                   Integer cantProcesos=service3.buscarProcAll().size();
                   
                   
                   PdfPTable table0 = new PdfPTable(3); // Code 1
                   table0.addCell("");
                   
                   
                   for(int i=0;i<cantProcesos;i++){
                    insumos.clear();
                    mayor=0;
                    PdfPTable table = new PdfPTable(9); // Code 1
                    if(superior>1){
                       for(int w=0;w<procesos.size();w++){
                        if(procesos.get(w).getToProceso().getIdproceso()>mayor && procesos.get(w).getToProceso().getIdproceso()<superior){
                            anterior=mayor;
                            mayor=procesos.get(w).getToProceso().getIdproceso();
                            ProcesoSaliente=procesos.get(w).getToProceso().getDescripcion();
                            OrdenSaliente=procesos.get(w).getOrden();
                        }
                       }
                       superior=mayor;
                       
                       System.out.println(superior);
                       //aca debo filtrar los procesos mayor y sacar los que son salida y insumos
                       for(int w=0;w<procesos.size();w++){
                        if(procesos.get(w).getToProceso().getIdproceso()==mayor){
                            
                            if(procesos.get(w).getTipomovimiento()==1){
                            ProductoSaliente=procesos.get(w).getToProducto1().getDescripcion();
                            UnidadSaliente=procesos.get(w).getToProducto1().getUm();
                            idProductoSaliente=procesos.get(w).getToProducto1().getIdproducto();
                            if(cantSig>=0) cantidad=cantSig;
                            }else{
                                insumos.add(procesos.get(w).getToProducto1().getIdproducto());
                            }
                        }else{
                        if(procesos.get(w).getToProceso().getIdproceso()==anterior){
                        if(procesos.get(w).getTipomovimiento()==1){
                            idProductoSalienteAnt=procesos.get(w).getToProducto1().getIdproducto();
                            }
                        }
                        }
                            
                       }    
                       
                       
                    table.addCell(cell01);
                    table.addCell(cell02);
                    table.addCell(cell03);
                    table.addCell(cell04);
                    table.addCell(cell05);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    
                    //llena de datos
                    
                    table.addCell(celda(""+ProcesoSaliente));
                    table.addCell(celda(""+OrdenSaliente));
                    table.addCell(celda(""+ProductoSaliente));
                    table.addCell(celda(""+cantidad));
                    table.addCell(celda(""+UnidadSaliente));
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    table.addCell(cell);
                    
                    // Segunda tabla de datos
                    
                    table.addCell(cell);
                    table.addCell(cell06);
                    table.addCell(cell06x);
                    table.addCell(cell07);
                    table.addCell(cell08);
                    table.addCell(cell09);
                    table.addCell(cell10);
                    table.addCell(cell11);
                    table.addCell(cell12);
                    
                    
                    // Datos de la tabla (bucle)
                    for(int j=0;j<insumos.size();j++){
                           Receta receta = service2.BuscarPorInsumos(idProductoSaliente, insumos.get(j).intValue());
                           List<Integer> capacidad_StockTotalProducto = serviceA.capacidad_StockTotalProducto(insumos.get(j).intValue());
                           Integer cantActual=capacidad_StockTotalProducto.get(1);
                           Float cantObtener=(cantidad*receta.getCantidad())-cantActual;
                           
                           
                           if(cantObtener<0) cantObtener=(float)0;
                           if(receta.getToProducto1().getIdproducto()==idProductoSalienteAnt){
                           cantSig=cantObtener;
                           }
                           Float costoTotal=cantObtener*receta.getToProducto1().getCosto();
                           System.out.println("Cantidad a Obtener: "+cantObtener);
                           System.out.println("Costo: "+receta.getToProducto1().getCosto());
                           System.out.println("Costo Total: "+costoTotal);
                           Float cantTotal=receta.getCantidad()*cantidad;
                        table.addCell(cell);
                        table.addCell(celda(""+receta.getToProducto1().getDescripcion()));
                        table.addCell(celda(""+roundTwoDecimals(receta.getCantidad())));
                        table.addCell(celda(""+roundTwoDecimals(cantTotal)));
                        table.addCell(celda(""+receta.getToProducto1().getUm()));
                        table.addCell(celda(""+receta.getToProducto1().getCosto()));
                        table.addCell(celda(""+cantActual));
                        table.addCell(celda(""+cantObtener));
                        table.addCell(celda(""+roundTwoDecimals(costoTotal)));
                    }
                    //listTable.add(table);
                    document.add(table);
                    document.add(getInformation(" "));
                   }
                   }
                    document.close();
                //imprimir();
	}
     
    public float roundTwoDecimals(float d) { 
        float d0=d*100;
        int v=Math.round(d0);
        float v2=(float)v/100;
        return (float)v2;
    } 
     
     private Chunk cabeceraTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.setFont(fontBold);c.getFont().setSize(8);
         c.getFont().setFamily("Arial");
      return c;
     }
     
     private Chunk dataTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.getFont().setSize(8);
         c.getFont().setFamily("Arial");
      return c;
     }
     private PdfPCell celda(String cadena){
     
         PdfPCell cell = new PdfPCell(new Paragraph(dataTable(cadena))); 
         return cell;
     }
     
     
     private Paragraph getHeader(String header) {
    	Paragraph paragraph = new Paragraph();
  		Chunk chunk = new Chunk();
                
		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append( header + getCurrentDateTime() + "\n");
  		chunk.setFont(fontBold);
                chunk.getFont().setFamily("Arial");
  		paragraph.add(chunk);
  		return paragraph;
     }
     private Paragraph getInformation(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_CENTER);
  		chunk.append(informacion);
  		chunk.setFont(fontNormal);
                
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformation2(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_LEFT);
  		chunk.append(informacion);
                chunk.setFont(fontBold);
  		//chunk.setFont(fontNormal);
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformationFooter(String informacion) {
     	Paragraph paragraph = new Paragraph();
     	Chunk chunk = new Chunk();
   		paragraph.setAlignment(Element.ALIGN_CENTER);
   		chunk.append(informacion);
   		chunk.setFont(new Font(Font.FontFamily.COURIER, 8, Font.NORMAL));
   		paragraph.add(chunk);
    		return paragraph;
       }
      private float getConvertCmsToPoints(float cm) {
     	return cm * 28.4527559067f;
     }
     
   public void imprimir(){
     
     Desktop d=Desktop.getDesktop();
     try {
      if(Desktop.isDesktopSupported()){
          //d.open(new File("/tmp/reporte-receta.pdf"));
          d.open(new File(this.nomArch));
      // d.print(new File("factura2.pdf"));
      }
     } catch (IOException e) {
     // TODO Auto-generated catch block
    e.printStackTrace();
        }
   }
      
      
     private String getCurrentDateTime() {
     	Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yy '-' hh:mm");
     	return ft.format(dNow);
    }
	public static void main(String[] args,Integer idProd,Float cantidad, String fileName) {	
		try{
			reporte_receta pdfTable = new reporte_receta(idProd,cantidad, fileName);
		}catch(Exception e){
			System.out.println(e);
		}
	}
        
}
