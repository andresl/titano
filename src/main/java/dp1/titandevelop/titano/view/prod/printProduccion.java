/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.view.prod;
import dp1.titandevelop.titano.view.ventas.*;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.PadreProduccion;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.persistent.Produccion;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.service.DocumentoVentaService;
import dp1.titandevelop.titano.service.EmpleadoXProcesoTurnoService;
import dp1.titandevelop.titano.service.ParametroService;
import dp1.titandevelop.titano.service.ProduccionService;
import dp1.titandevelop.titano.service.ProductoService;
import dp1.titandevelop.titano.service.ProductoXProcesoService;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heli
 */
public class printProduccion {
    private static Font fontBold = new Font(Font.FontFamily.COURIER, 12, Font.BOLD);
    private static Font fontNormal = new Font(Font.FontFamily.COURIER, 11, Font.NORMAL);
    private String nomArch;
    public printProduccion(Date fechaIni,Date fechaFin,String nombreArchivo) throws Exception{
		
       this.nomArch = nombreArchivo;         
       Document document = new Document();
     
        PdfWriter.getInstance(document, 
                new FileOutputStream(nombreArchivo));
        document.open();

        
        
        document.add(getHeader("Producción Real "));
        document.add(getInformation(getCurrentDateTime()));
        document.add(getInformation(" "));
        document.add(getInformation(" "));
        
        Image image1 = Image.getInstance(getClass().getResource("/img/GOOD_COOKIE.jpg"));
                      PdfPTable table4 = new PdfPTable(1); // Code 1
                PdfPCell cell001y = 
			new PdfPCell(image1);
                        cell001y.setBorder(0);
                table4.addCell(cell001y);
                
                document.add(table4);
                document.add(getInformation(" "));
                
        ProductoXProcesoService spp=new ProductoXProcesoService();
        ProduccionService s= new ProduccionService();
        List <PadreProduccion> pp=s.BuscaPorRangos(fechaIni, fechaFin);
                for(int i=0;i<pp.size();i++){
                SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
                String fecha2=formatoDeFecha.format(pp.get(i).getFecha());
                //colocar fecha
                // colocar descripcion
                // colocar espacio
                //iniciar tabla
                    PdfPTable table2 = new PdfPTable(5); 
                    PdfPCell cell000x = new PdfPCell(new Paragraph(cabeceraTable("Fecha")));
                    cell000x.setBorder(0);
                    table2.addCell(cell000x);
                    
                    PdfPCell cell001x = new PdfPCell(new Paragraph(dataTable(""+fecha2)));
                    cell001x.setBorder(0);
                    table2.addCell(cell001x);
                    
                    PdfPCell cell002x = new PdfPCell(new Paragraph(cabeceraTable("")));
                    cell002x.setBorder(0);
                    cell002x.setColspan(3);
                    table2.addCell(cell002x);
                    
                    PdfPCell cell003x = new PdfPCell(new Paragraph(cabeceraTable("Descripción")));
                    cell003x.setBorder(0);
                    table2.addCell(cell003x);
                    
                    PdfPCell cell004x = new PdfPCell(new Paragraph(dataTable(""+pp.get(i).getDescripcion())));
                    cell004x.setBorder(0);
                    table2.addCell(cell004x);
                    
                    PdfPCell cell005x = new PdfPCell(new Paragraph(cabeceraTable("")));
                    cell005x.setBorder(0);
                    cell005x.setColspan(3);
                    table2.addCell(cell005x);
                    
                document.add(table2);
                document.add(getInformation(" "));    
                    
         List <Produccion> pr=s.SacaDetalle(pp.get(i).getIdpadreproduccion());
         
          PdfPTable table = new PdfPTable(8); 
            //mayor=0;
            PdfPCell cell000 = new PdfPCell(new Paragraph(cabeceraTable("Operario")));
        table.addCell(cell000);
        
        PdfPCell cell001 = new PdfPCell(new Paragraph(cabeceraTable("Producto Producido")));
        table.addCell(cell001);
        
        PdfPCell cell002 = new PdfPCell(new Paragraph(cabeceraTable("Producto Final")));
        table.addCell(cell002);
        
        PdfPCell cell003 = new PdfPCell(new Paragraph(cabeceraTable("Productividad")));
        table.addCell(cell003);
        
        PdfPCell cell0031 = new PdfPCell(new Paragraph(cabeceraTable("Rotura")));
        table.addCell(cell0031);
        
        PdfPCell cell004 = new PdfPCell(new Paragraph(cabeceraTable("UM")));
        table.addCell(cell004);
        
        PdfPCell cell005 = new PdfPCell(new Paragraph(cabeceraTable("Turno")));
        table.addCell(cell005);
        
        PdfPCell cell006 = new PdfPCell(new Paragraph(cabeceraTable("Proceso")));
        table.addCell(cell006);
            for(int j=0;j<pr.size();j++){
                Asignacion a =pr.get(j).getToAsignacion();
            Producto prod1=spp.DevueveSalida(a.getToProducto().getIdproducto(), a.getToMaquina().getToProceso().getIdproceso());

               table.addCell(celda(""+pr.get(j).getToAsignacion().getToEmpleado().getNombres()+" "+pr.get(j).getToAsignacion().getToEmpleado().getApellidop()+" "+pr.get(j).getToAsignacion().getToEmpleado().getApellidom()));
               table.addCell(celda(""+prod1.getDescripcion()));
               table.addCell(celda(""+pr.get(j).getToAsignacion().getToProducto().getDescripcion()));
               table.addCell(celda(""+pr.get(j).getProductividad()));
               table.addCell(celda(""+pr.get(j).getRotura()));
               table.addCell(celda(""+prod1.getUm()));
               table.addCell(celda(""+pr.get(j).getToAsignacion().getToTurno().getDescripcion()));
               table.addCell(celda(""+prod1.getToProceso().getDescripcion()));
            }
                    
            document.add(table);
                document.add(getInformation(" "));
        }
                
                
       
        
        document.close();
       
    }
     
    
    private Chunk cabeceraTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.setFont(fontBold);c.getFont().setSize(9);
         c.getFont().setFamily("Arial");
      return c;
     }
    private Chunk dataTable(String cadena ){
         Chunk c = new Chunk(cadena);
         c.getFont().setSize(8);
         c.getFont().setFamily("Arial");
      return c;
     }
     private PdfPCell celda(String cadena){
     
         PdfPCell cell = new PdfPCell(new Paragraph(dataTable(cadena))); 
         return cell;
     }
    
     private Paragraph getHeader(String header) {
    	Paragraph paragraph = new Paragraph();
        Chunk chunk = new Chunk();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        chunk.append( header + "\n");
        chunk.setFont(fontBold);
        paragraph.add(chunk);
        return paragraph;
     }
     private Paragraph getInformation(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        chunk.append(informacion);
        chunk.setFont(fontNormal);

        paragraph.add(chunk);
        return paragraph;
      }
     
     private Paragraph getInformation2(String informacion) {
    	Paragraph paragraph = new Paragraph();
    	Chunk chunk = new Chunk();
  		paragraph.setAlignment(Element.ALIGN_LEFT);
  		chunk.append(informacion);
                chunk.setFont(fontBold);
  		//chunk.setFont(fontNormal);
  		paragraph.add(chunk);
   		return paragraph;
      }
     
     private Paragraph getInformationFooter(String informacion) {
     	Paragraph paragraph = new Paragraph();
     	Chunk chunk = new Chunk();
   		paragraph.setAlignment(Element.ALIGN_CENTER);
   		chunk.append(informacion);
   		chunk.setFont(new Font(Font.FontFamily.COURIER, 8, Font.NORMAL));
   		paragraph.add(chunk);
    		return paragraph;
       }
      private float getConvertCmsToPoints(float cm) {
     	return cm * 28.4527559067f;
     }
     
   public void imprimirAsignacion(){
     
     Desktop d=Desktop.getDesktop();
     try {
      if(Desktop.isDesktopSupported()){
          //d.open(new File("/tmp/Asignacion.pdf"));
      d.open(new File(this.nomArch));
      }
     } catch (IOException e) {
            e.printStackTrace();
        }
   }
      
      
   private String getCurrentDateTime() {
     	Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yy '-' hh:mm");
     	return ft.format(dNow);
    }
	
}
