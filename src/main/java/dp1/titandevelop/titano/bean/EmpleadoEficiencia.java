/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

import dp1.titandevelop.titano.persistent.Empleado;

/**
 *
 * @author natali
 */
public class EmpleadoEficiencia {
    Empleado e;
    float eficienciaM;
    float eficienciaT;
    float eficienciaN;
    float eficienciaTM;
    float eficienciaTT;
    float eficienciaTN;

    public void setEficienciaTM(float eficienciaTM) {
        this.eficienciaTM = eficienciaTM;
    }

    public void setEficienciaTT(float eficienciaTT) {
        this.eficienciaTT = eficienciaTT;
    }

    public void setEficienciaTN(float eficienciaTN) {
        this.eficienciaTN = eficienciaTN;
    }

    public float getEficienciaTM() {
        return eficienciaTM;
    }

    public float getEficienciaTT() {
        return eficienciaTT;
    }

    public float getEficienciaTN() {
        return eficienciaTN;
    }

    public Empleado getE() {
        return e;
    }

    public float getEficienciaM() {
        return eficienciaM;
    }

    public float getEficienciaT() {
        return eficienciaT;
    }

    public float getEficienciaN() {
        return eficienciaN;
    }

    public void setE(Empleado e) {
        this.e = e;
    }

    public void setEficienciaM(float eficienciaM) {
        this.eficienciaM = eficienciaM;
    }

    public void setEficienciaT(float eficienciaT) {
        this.eficienciaT = eficienciaT;
    }

    public void setEficienciaN(float eficienciaN) {
        this.eficienciaN = eficienciaN;
    }
    
}
