/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

import java.util.Date;

/**
 *
 * @author natali
 */
public class DetalleAsig {
    private int idAsig;

    public void setIdAsig(int idAsig) {
        this.idAsig = idAsig;
    }

    public int getIdAsig() {
        return idAsig;
    }
    private String descripcion;
    private Date fecha;
    private int cantidadEmpleado;
    private float produccion;
    private float rotura;
    private float beneficio;
    private float costo;
    private int estado;

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setCantidadEmpleado(int cantidadEmpleado) {
        this.cantidadEmpleado = cantidadEmpleado;
    }

    public void setProduccion(float produccion) {
        this.produccion = produccion;
    }

    public void setRotura(float rotura) {
        this.rotura = rotura;
    }

    public void setBeneficio(float beneficio) {
        this.beneficio = beneficio;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public int getCantidadEmpleado() {
        return cantidadEmpleado;
    }

    public float getProduccion() {
        return produccion;
    }

    public float getRotura() {
        return rotura;
    }

    public float getBeneficio() {
        return beneficio;
    }

    public float getCosto() {
        return costo;
    }

    public int getEstado() {
        return estado;
    }
    
}
