/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

import dp1.titandevelop.titano.persistent.Parametro;
import java.util.List;

/**
 *
 * @author Usuario1
 */
public class ParametroNombres {
    private String ruta = "Ruta de Auditoria";
    private String nombreSw = "Nombre del Software";
    private String adicional = "Produccion Adicional";
    private String igv = "IGV";
    private String reten = "Retención";
    private String perce = "Persección";
    private String ruc = "RUC";
    private String retra = "Retracción";
    private String est = "Estado";
    private String khw = "Costo de Khw";
    
    
    
     

    /**
     * @return the ruta
     */
    public String getRuta() {
        return ruta;
    }

    /**
     * @param ruta the ruta to set
     */
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    /**
     * @return the nombreSw
     */
    public String getNombreSw() {
        return nombreSw;
    }

    /**
     * @param nombreSw the nombreSw to set
     */
    public void setNombreSw(String nombreSw) {
        this.nombreSw = nombreSw;
    }

    /**
     * @return the adicional
     */
    public String getAdicional() {
        return adicional;
    }

    /**
     * @param adicional the adicional to set
     */
    public void setAdicional(String adicional) {
        this.adicional = adicional;
    }

    /**
     * @return the igv
     */
    public String getIgv() {
        return igv;
    }

    /**
     * @param igv the igv to set
     */
    public void setIgv(String igv) {
        this.igv = igv;
    }

    /**
     * @return the reten
     */
    public String getReten() {
        return reten;
    }

    /**
     * @param reten the reten to set
     */
    public void setReten(String reten) {
        this.reten = reten;
    }

    /**
     * @return the perce
     */
    public String getPerce() {
        return perce;
    }

    /**
     * @param perce the perce to set
     */
    public void setPerce(String perce) {
        this.perce = perce;
    }

    /**
     * @return the ruc
     */
    public String getRuc() {
        return ruc;
    }

    /**
     * @param ruc the ruc to set
     */
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    /**
     * @return the retra
     */
    public String getRetra() {
        return retra;
    }

    /**
     * @param retra the retra to set
     */
    public void setRetra(String retra) {
        this.retra = retra;
    }

    /**
     * @return the est
     */
    public String getEst() {
        return est;
    }

    /**
     * @param est the est to set
     */
    public void setEst(String est) {
        this.est = est;
    }

    /**
     * @return the khw
     */
    public String getKhw() {
        return khw;
    }

    /**
     * @param khw the khw to set
     */
    public void setKhw(String khw) {
        this.khw = khw;
    }
    
    
}
