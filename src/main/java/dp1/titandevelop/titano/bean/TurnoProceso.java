/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.service.ProcesoService;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario1
 */
public class TurnoProceso {
    private ArrayList<Integer> productividades;
    private ArrayList<Integer> mermas;
    private List<Proceso> procesos;
    private ArrayList<Turno> turnos;
    private ArrayList<Integer> idprocesos;
    private ArrayList<Integer> idturnos;
    
    
    public TurnoProceso() {
        ProcesoService ser  = new ProcesoService();
        productividades = new ArrayList<Integer>();
        mermas = new ArrayList<Integer>();
        idturnos = new ArrayList<Integer>();
        idprocesos = new ArrayList<Integer>();
        procesos = new ArrayList<Proceso>();
        turnos = new ArrayList<Turno>();
        
        procesos = ser.buscarProcAll();
        for (int i =  0 ; i< procesos.size();i++){
            mermas.add(0);
            productividades.add(0);
            
        }
    }

    /**
     * @return the productividades
     */
    public ArrayList<Integer> getProductividades() {
        return productividades;
    }

    /**
     * @param productividades the productividades to set
     */
    public void setProductividades(ArrayList<Integer> productividades) {
        this.productividades = productividades;
    }

    /**
     * @return the mermas
     */
    public ArrayList<Integer> getMermas() {
        return mermas;
    }

    /**
     * @param mermas the mermas to set
     */
    public void setMermas(ArrayList<Integer> mermas) {
        this.mermas = mermas;
    }

    /**
     * @return the procesos
     */
    public List<Proceso> getProcesos() {
        return procesos;
    }

    /**
     * @param procesos the procesos to set
     */
    public void setProcesos(ArrayList<Proceso> procesos) {
        this.procesos = procesos;
    }

    /**
     * @return the idprocesos
     */
    public ArrayList<Integer> getIdprocesos() {
        return idprocesos;
    }

    /**
     * @param idprocesos the idprocesos to set
     */
    public void setIdprocesos(ArrayList<Integer> idprocesos) {
        this.idprocesos = idprocesos;
    }

    /**
     * @return the idturnos
     */
    public List<Integer> getIdturnos() {
        return idturnos;
    }

    /**
     * @param idturnos the idturnos to set
     */
    public void setIdturnos(ArrayList<Integer> idturnos) {
        this.idturnos = idturnos;
    }

    /**
     * @return the turnos
     */
    public List<Turno> getTurnos() {
        return turnos;
    }

    /**
     * @param turnos the turnos to set
     */
    public void setTurnos(ArrayList<Turno> turnos) {
        this.turnos = turnos;
    }
    
    
}


