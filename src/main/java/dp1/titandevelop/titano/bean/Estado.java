/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

import java.util.ArrayList;

/**
 *
 * @author natali
 */
public class Estado {
    private int eliminado=0;
    private int activo=1;
    private int inactivo=2;
    private int cancelado=3;
    private int pendiente=4;
    private int cumplido=5;
    private int entregado=6;
    private int aceptado=7;
    private int rechazado=8;
    private int sin_usar=9;
    private int en_uso=10;
    private int en_proceso=11;
    

    
    public static int getEstadoId(String i){
        
        if (i.equals("Eliminado")) return 0;
        if (i.equals("Activo")) return 1;
        if (i.equals("Inactivo")) return 2;
        if (i.equals("Cancelado")) return 3;
        if (i.equals("Pendiente")) return 4;
        if (i.equals("Cumplido")) return 5;
        if (i.equals("Entregado")) return 6;
        if (i.equals("Aceptado")) return 7;
        if (i.equals("Rechazado")) return 8;
        if (i.equals("Sin Usar")) return 9;
        if (i.equals("En Uso")) return 10;
        if (i.equals("En Proceso")) return 11;
        return 0;
    }
    public static ArrayList<String> getEstadosActivoInactivo()
    {
        ArrayList<String> s = new ArrayList<String>();
        s.add("Activo");
        s.add("Inactivo");
        return s;
    }
    
    public ArrayList<String> getSinUsarEnUso(){
        ArrayList<String> s = new ArrayList<String>();
        s.add("Sin Usar");
        s.add("En Uso");
        return s;
    }
    
    public int getEliminado() {
        return eliminado;
    }

    public int getEnProceso(){
        return en_proceso;
    }
    
    public int getInactivo() {
        return inactivo;
    }

    public int getActivo() {
        return activo;
    }

    public int getCancelado() {
        return cancelado;
    }

    public int getPendiente() {
        return pendiente;
    }

    public int getCumplido() {
        return cumplido;
    }

    public int getEntregado() {
        return entregado;
    }

    public int getAceptado() {
        return aceptado;
    }

    public int getRechazado() {
        return rechazado;
    }
    
    public int getEnUso(){
        return en_uso;
    }
    
    public int getSinUsar(){
        return sin_usar;
    }
    
    public static String devuelveCadena(int estado) {
        switch ( estado ){
            case 0: return "Eliminado";
            case 1: return "Activo"; 
            case 2: return "Inactivo";
            case 3: return "Cancelado";
            case 4: return "Pendiente";
            case 5: return "Cumplido";
            case 6: return "Entregado";
            case 7: return "Aceptado";
            case 8: return "Rechazado";
            case 9: return "Sin Usar";
            case 10: return "En Uso";  
            case 11: return "En proceso";  
        }
        return null;
    }

    
}
