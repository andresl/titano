package dp1.titandevelop.titano.bean;

import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;

/**
 *
 * @author natali
 */
public class Movimiento {
    private EmpleadoXProcesoTurno idEmpleado1;
    private EmpleadoXProcesoTurno idEmpleado2;
    private int indiceEmpleado1;
    private int indiceEmpleado2;

    public void setIndiceEmpleado1(int indiceEmpleado1) {
        this.indiceEmpleado1 = indiceEmpleado1;
    }

    public void setIndiceEmpleado2(int indiceEmpleado2) {
        this.indiceEmpleado2 = indiceEmpleado2;
    }

    public int getIndiceEmpleado1() {
        return indiceEmpleado1;
    }

    public int getIndiceEmpleado2() {
        return indiceEmpleado2;
    }
    private double valorMovimiento;
    private int tenencia;

    public EmpleadoXProcesoTurno getIdEmpleado1() {
        return idEmpleado1;
    }

    public EmpleadoXProcesoTurno getIdEmpleado2() {
        return idEmpleado2;
    }

    public void setIdEmpleado1(EmpleadoXProcesoTurno idEmpleado1) {
        this.idEmpleado1 = idEmpleado1;
    }

    public void setIdEmpleado2(EmpleadoXProcesoTurno idEmpleado2) {
        this.idEmpleado2 = idEmpleado2;
    }


    public double getValorMovimiento() {
        return valorMovimiento;
    }

    public void setValorMovimiento(double valorMovimiento) {
        this.valorMovimiento = valorMovimiento;
    }

    public int getTenencia() {
        return tenencia;
    }

    public void setTenencia(int tenencia) {
        this.tenencia = tenencia;
    }
    
}

