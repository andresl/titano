/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

import dp1.titandevelop.titano.persistent.Producto;

/**
 *
 * @author natali
 */
public class galletaAux {
    Producto galleta;
    double beneficio;
    int demanda;

    public Producto getGalleta() {
        return galleta;
    }

    public void setDemanda(int demanda) {
        this.demanda = demanda;
    }

    public int getDemanda() {
        return demanda;
    }

    public double getBeneficio() {
        return beneficio;
    }

    public void setGalleta(Producto galleta) {
        this.galleta = galleta;
    }

    public void setBeneficio(double beneficio) {
        this.beneficio = beneficio;
    }
    
    
}
