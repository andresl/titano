/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

/**
 *
 * @author natali
 */
public class mermaAux {
    private int idProceso;
    private float porcentajeMerma;

    public void setIdProceso(int idProceso) {
        this.idProceso = idProceso;
    }

    public void setPorcentajeMerma(float porcentajeMerma) {
        this.porcentajeMerma = porcentajeMerma;
    }

    public int getIdProceso() {
        return idProceso;
    }

    public float getPorcentajeMerma() {
        return porcentajeMerma;
    }
}
