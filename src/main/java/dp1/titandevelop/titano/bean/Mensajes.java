/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

/**
 *
 * @author Usuario1
 */
public class Mensajes {
    private  String asignado = "La asignación se realizó de forma exitosa!" ;
    private  String registrado = "La operación se realizó de forma exitosa!" ;
    private  String editado =  "La edición se realizó de forma exitosa!";
    private  String pregEliminar = "¿Está seguro que desea eliminar el registro?";
    private  String eliminado = "El registro se eliminó de forma exitosa!";
    private  String noEliminado = "No se puede eliminar un registro en uso";
    private  String noEditado = "No se puede editar un registro en uso";
    private  String agregado = "El registro se agregó de forma exitosa!";;
    private  String completarCampos =  "Uno o más campos no han sido ingresados.";
    private  String numerico =  "El campo debe ser numérico.";
    private String error = "Ha ocurrido un error en el proceso, por favor intentar nuevamente";
    private String fechaposterior = "Debe ingresar una fecha menor a la actual";
    private String fechamayor = "La fecha que intenta ingresar no cumple con el minimo de edad para laborar";
        /**
     * @return the registrado
     */
    public String getNoEliminadoPorUso() {
        return noEliminado;
    }
    public String getNoEditadoPorUso(){
        return noEditado;
    }
    public String getRegistrado() {
        return registrado;
    }

    /**
     * @param registrado the registrado to set
     */
    public void setRegistrado(String registrado) {
        this.registrado = registrado;
    }

    /**
     * @return the editado
     */
    public String getEditado() {
        return editado;
    }

    /**
     * @param editado the editado to set
     */
    public void setEditado(String editado) {
        this.editado = editado;
    }

    /**
     * @return the pregEliminar
     */
    public String getPregEliminar() {
        return pregEliminar;
    }

    /**
     * @param pregEliminar the pregEliminar to set
     */
    public void setPregEliminar(String pregEliminar) {
        this.pregEliminar = pregEliminar;
    }

    /**
     * @return the eliminado
     */
    public String getEliminado() {
        return eliminado;
    }

    /**
     * @param eliminado the eliminado to set
     */
    public void setEliminado(String eliminado) {
        this.eliminado = eliminado;
    }

    /**
     * @return the agregado
     */
    public String getAgregado() {
        return agregado;
    }

    /**
     * @param agregado the agregado to set
     */
    public void setAgregado(String agregado) {
        this.agregado = agregado;
    }

    /**
     * @return the completarCampos
     */
    public String getCompletarCampos() {
        return completarCampos;
    }

    /**
     * @param completarCampos the completarCampos to set
     */
    public void setCompletarCampos(String completarCampos) {
        this.completarCampos = completarCampos;
    }

    /**
     * @return the numerico
     */
    public String getNumerico() {
        return numerico;
    }

    /**
     * @param numerico the numerico to set
     */
    public void setNumerico(String numerico) {
        this.numerico = numerico;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the fechaposterior
     */
    public String getFechaposterior() {
        return fechaposterior;
    }

    /**
     * @param fechaposterior the fechaposterior to set
     */
    public void setFechaposterior(String fechaposterior) {
        this.fechaposterior = fechaposterior;
    }

    /**
     * @return the fechamayor
     */
    public String getFechamayor() {
        return fechamayor;
    }

    /**
     * @param fechamayor the fechamayor to set
     */
    public void setFechamayor(String fechamayor) {
        this.fechamayor = fechamayor;
    }

    /**
     * @return the asignado
     */
    public String getAsignado() {
        return asignado;
    }

    /**
     * @param asignado the asignado to set
     */
    public void setAsignado(String asignado) {
        this.asignado = asignado;
    }

    
    
}
