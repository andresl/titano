/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

/**
 *
 * @author dorita
 */
public class ProduccionAux {
    int idProducto;
    String producto;
    float productividad;
    float merma;
    float beneficio;
    float costo;

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public float getProductividad() {
        return productividad;
    }

    public void setProductividad(float productividad) {
        this.productividad = productividad;
    }

    public float getMerma() {
        return merma;
    }

    public void setMerma(float merma) {
        this.merma = merma;
    }

    public float getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(float beneficio) {
        this.beneficio = beneficio;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
    
   
       
}
