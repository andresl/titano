/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.bean;

import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.Producto;
import java.util.ArrayList;

/**
 *
 * @author dorita
 */
public class RutaAux {
    public Proceso proceso;
    public int orden;
    public ArrayList<Producto> listaEntradas;
    public ArrayList<Producto> listaSalidas;
    
    public RutaAux()
    {
         proceso= new Proceso();
         listaEntradas= new ArrayList<Producto>();
         listaSalidas= new ArrayList<Producto>();
    }
    
}
