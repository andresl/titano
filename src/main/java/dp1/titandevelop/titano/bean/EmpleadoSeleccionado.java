package dp1.titandevelop.titano.bean;

import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import java.util.ArrayList;

/**
 *
 * @author natali
 */
public class EmpleadoSeleccionado {
    public static final int PROCESOFINAL=4;
   private EmpleadoXProcesoTurno empleadoProceso; 
   private int galleta; /*Descripcion: 0=galletaSimple, 1=galletaRellenita, 2=galletaSmile*/
   private double Fo;
   private double beneficio;
   private double costo;

    public double getBeneficio() {
        return beneficio;
    }

    public double getCosto() {
        return costo;
    }

    public void setBeneficio(double beneficio) {
        this.beneficio = beneficio;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }
   

    public void setFo(double Fo) {
        this.Fo = Fo;
    }

    public double getFo() {
        return Fo;
    }

    public EmpleadoSeleccionado(){}
    
    public EmpleadoSeleccionado(EmpleadoXProcesoTurno nuevoElemento, int p) {
     empleadoProceso=nuevoElemento;
     Empleado copiaEmpleado = new Empleado();
     copiaEmpleado.setApellidom(nuevoElemento.getToEmpleado().getApellidom());
     copiaEmpleado.setApellidop(nuevoElemento.getToEmpleado().getApellidop());
     copiaEmpleado.setAsignado(nuevoElemento.getToEmpleado().getAsignado());
     copiaEmpleado.setCelular(nuevoElemento.getToEmpleado().getCelular());
     copiaEmpleado.setDireccion(nuevoElemento.getToEmpleado().getDireccion()); 
     copiaEmpleado.setEstado(nuevoElemento.getToEmpleado().getEstado());
     copiaEmpleado.setFechanacimiento(nuevoElemento.getToEmpleado().getFechanacimiento());
     copiaEmpleado.setIdempleado(nuevoElemento.getToEmpleado().getIdempleado()); 
     copiaEmpleado.setNombres(nuevoElemento.getToEmpleado().getNombres()); 
     copiaEmpleado.setSexo(nuevoElemento.getToEmpleado().getSexo()); 
     empleadoProceso.setToEmpleado(copiaEmpleado);
     galleta=p;
    }

    public void setEmpleadoProceso(EmpleadoXProcesoTurno empleadoProceso) {
        this.empleadoProceso = empleadoProceso;
    }

    public EmpleadoXProcesoTurno getEmpleadoProceso() {
        return empleadoProceso;
    }

    public int getGalleta() {
        return galleta;
    }

    public void setGalleta(int galleta) {
        this.galleta = galleta;
    }

    public float ObtenerProduccionTotal(ArrayList<EmpleadoSeleccionado> lista) {
       float suma=0;
       for(int i=0;i<lista.size();i++){
           if(lista.get(i).getEmpleadoProceso().getToProceso().getIdproceso()==PROCESOFINAL){
            suma+=lista.get(i).getEmpleadoProceso().getProductividad();
           }
       } 
       return suma;
    }

    public float ObtenerRoturaTotal(ArrayList<EmpleadoSeleccionado> lista) {
       float suma=0;
       for(int i=0;i<lista.size();i++){
           if(lista.get(i).getEmpleadoProceso().getToProceso().getIdproceso()==PROCESOFINAL){
            suma+=lista.get(i).getEmpleadoProceso().getRotura();
           }
       } 
       return suma;
    }

}
