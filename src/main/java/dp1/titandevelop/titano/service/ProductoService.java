/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Almacen;
import dp1.titandevelop.titano.persistent.AlmacenXProducto;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.ProductoProveedor;
import dp1.titandevelop.titano.persistent.ProductoXProceso;
import dp1.titandevelop.titano.persistent.Proveedor;
import dp1.titandevelop.titano.persistent.Receta;
import dp1.titandevelop.titano.persistent.TipoProducto;
import dp1.titandevelop.titano.persistent.auto._Proceso;
import dp1.titandevelop.titano.persistent.auto._Proveedor;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Dorita
 */


public class ProductoService {
     Estado e = new Estado();    
     
     
    public int insertar(String tipoProducto, String descripcion, int stockMin, int stockMax, int loteMin, int loteMax, int diasVenc, float beneficio, float costo, String UM, int idProc, float peso ) 
    {
        ObjectContext context = DataContext.createDataContext();
       
        // buscando el tipo
        Expression qualifier = ExpressionFactory.matchExp(TipoProducto.DESCRIPCION_PROPERTY, tipoProducto);
        SelectQuery select = new SelectQuery(TipoProducto.class, qualifier);
        TipoProducto t = (TipoProducto) DataObjectUtils.objectForQuery(context, select);
        // busco el proceso 
        Expression qualifier1 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PK_COLUMN, idProc);
        SelectQuery select1 = new SelectQuery(Proceso.class, qualifier1);
        Proceso p1 = (Proceso) DataObjectUtils.objectForQuery(context, select1);
        
       //Grabo atrinutos     
        Producto p = context.newObject(Producto.class);
        p.setToTipoProducto(t);
        p.setToProceso(p1);
        p.setPeso(peso);
        p.setDescripcion(descripcion);
        p.setStockmin(stockMin);
        p.setStockmax(stockMax);
        p.setLotemaxcompra(loteMax);
        p.setLotemincompra(loteMin);
        p.setDiasvencimiento(diasVenc);
        p.setBeneficio(beneficio);
        p.setCosto(costo);
        p.setUm(UM);
        p.setEstado(e.getActivo());
        
        context.commitChanges();   
        ArrayList<Producto> pp= this.buscarPorDescripcion(descripcion);
        Producto producto1 = pp.get(pp.size()-1);
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Producto");
        return producto1.getIdproducto();
    }
    
    public int insertarmasivo(int idProducto, String tipoProducto, String descripcion, int stockMin, int stockMax, int loteMin, int loteMax, int diasVenc, float beneficio, float costo, String UM, int idProc, float peso ) 
    {
        ObjectContext context = DataContext.createDataContext();
       
        // buscando el tipo
        Expression qualifier = ExpressionFactory.matchExp(TipoProducto.DESCRIPCION_PROPERTY, tipoProducto);
        SelectQuery select = new SelectQuery(TipoProducto.class, qualifier);
        TipoProducto t = (TipoProducto) DataObjectUtils.objectForQuery(context, select);
        // busco el proceso 
        Expression qualifier1 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PK_COLUMN, idProc);
        SelectQuery select1 = new SelectQuery(Proceso.class, qualifier1);
        Proceso p1 = (Proceso) DataObjectUtils.objectForQuery(context, select1);
        
       //Grabo atrinutos     
        Producto p = context.newObject(Producto.class);
        p.setIdproducto(idProducto);
        p.setToTipoProducto(t);
        p.setToProceso(p1);
        p.setPeso(peso);
        p.setDescripcion(descripcion);
        p.setStockmin(stockMin);
        p.setStockmax(stockMax);
        p.setLotemaxcompra(loteMax);
        p.setLotemincompra(loteMin);
        p.setDiasvencimiento(diasVenc);
        p.setBeneficio(beneficio);
        p.setCosto(costo);
        p.setUm(UM);
        p.setEstado(e.getActivo());
        
        context.commitChanges();   
        ArrayList<Producto> pp= this.buscarPorDescripcion(descripcion);
        Producto producto1 = pp.get(pp.size()-1);
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Producto Masivo");
        return producto1.getIdproducto();
    }
    //Inserta el producto en una lista de almacenes
    public void InsertaEnAlmacenes (int idProd, ArrayList<AlmacenXProducto> l){
        //busca producto
        ObjectContext context = DataContext.createDataContext();
        Producto p=this.buscarPorId(context, idProd);
        //Inserta en todos los almacenes
        for (int i=0;i<l.size();i++)
        {
            AlmacenXProducto ap= context.newObject(AlmacenXProducto.class);
            Expression q = ExpressionFactory.matchExp(Almacen.IDALMACEN_PK_COLUMN, l.get(i).getToAlmacen().getIdalmacen());
            Expression q1 = ExpressionFactory.greaterExp(Almacen.ESTADO_PROPERTY, 0);
            Expression q2 = q.andExp(q1);
            SelectQuery s = new SelectQuery(Almacen.class, q2);
            Almacen a=(Almacen) DataObjectUtils.objectForQuery(context, s);
            ap.setCapacidad(l.get(i).getCapacidad());
            ap.setStock(l.get(i).getStock());
            ap.setToAlmacen(a);
            ap.setToProducto(p);
            ap.setEstado(e.getActivo());
            context.commitChanges();
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "AlmacenXProducto");
        }        
    }
    
    //Seleccionar Productos por nombre.
    public ArrayList<Producto> buscarPorDescripcion ( String descripcion ) {
        ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.likeIgnoreCaseExp(Producto.DESCRIPCION_PROPERTY, "%"+descripcion+"%");
        Expression q1 = ExpressionFactory.greaterExp(Producto.ESTADO_PROPERTY, 0);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Producto.class, q2);
        ArrayList<Producto> lp = (ArrayList<Producto>) c.performQuery(s);
        return lp;
    }
    
    //Seleccionar Productos No terminadod.
    public ArrayList<Producto> buscarNoTerminados ( ) {
        ObjectContext c = DataContext.createDataContext();
       ArrayList<Producto> lp = this.buscarTodo();
       for (int i=0;i<lp.size();i++)
       {
           if (lp.get(i).getToTipoProducto().getIdtipoproducto()==3)
           {  lp.remove(i);i--;}
           
       }
        return lp;
    }
    //Seleccionar producto activos
    public List<Producto> buscar(){
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PRODUCTO WHERE ESTADO > "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);        
        List<Producto> productos = context.performQuery(query);       
        return productos;
    }
    public List<Producto> buscarInsumo(){
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PRODUCTO WHERE ESTADO = "+est.getActivo()+" AND IDTIPOPRODUCTO = 1";
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);        
        List<Producto> productos = context.performQuery(query);       
        return productos;
    }

    //Seleccionar Productos por ID
     public Producto buscarPorIdX( int id ) {
         ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, id);
        SelectQuery s = new SelectQuery(Producto.class, q);
        return (Producto) DataObjectUtils.objectForQuery(c, s);
    }
    public Producto buscarPorId ( int id ) {
        ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, id);
        Expression q1 = ExpressionFactory.greaterExp(Producto.ESTADO_PROPERTY, 0);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Producto.class, q2);
        return (Producto) DataObjectUtils.objectForQuery(c, s);
    }
    public boolean isInReceta ( int id ) {
        Estado estado=new Estado();
         ObjectContext context = DataContext.createDataContext();
         Producto prod=this.buscarPorId(id, context);       
        Expression q = ExpressionFactory.matchExp(Receta.TO_PRODUCTO1_PROPERTY, prod);
        Expression q1 = ExpressionFactory.greaterExp(Receta.ESTADO_PROPERTY, 0);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Receta.class, q2);
        ArrayList<Receta> lp =(ArrayList<Receta>) context.performQuery(s);
        if (lp.isEmpty()){
            return false;
        }else{
            return true;
        }
    }
    
     public Producto buscarPorId ( int id, ObjectContext c ) {
       // ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, id);
        Expression q1 = ExpressionFactory.greaterExp(Producto.ESTADO_PROPERTY, 0);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Producto.class, q2);
        return (Producto) DataObjectUtils.objectForQuery(c, s);
    }
    
    public Producto buscarPorId ( ObjectContext c,  int id ) {
        //ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, id);
        SelectQuery s = new SelectQuery(Producto.class, q);
        return (Producto) DataObjectUtils.objectForQuery(c, s);
    }
   
    //Seleccionar Todos los Productos
    public ArrayList<Producto> buscarTodo () {
        ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.greaterExp(Producto.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Producto.class,q);
        ArrayList<Producto> lp = (ArrayList<Producto>) c.performQuery(s);
        return lp;
    }
    
     //Seleccionar Todos los Productos
    public ArrayList<Producto> buscarNoInsumos () {
        ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.greaterExp(Producto.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Producto.class,q);
        ArrayList<Producto> lp = (ArrayList<Producto>) c.performQuery(s);
        for (int i=0;i<lp.size();i++)
        {
            if(lp.get(i).getToTipoProducto().getIdtipoproducto()==1)
                lp.remove(i);
        }
        return lp;
    }
    
    //Seleccionar Multicriterio Tipo y Descripcion
    public ArrayList<Producto> buscarMulticriterio(String descripcion, String tipo){
           
            ArrayList<Producto> lp = buscarPorDescripcion(descripcion);
            ArrayList<Producto> l = new ArrayList<Producto>();
            if (tipo!=null)
            {
                for (int i=0; i<lp.size(); i++)
                {
                    if (tipo.equals(lp.get(i).getToTipoProducto().getDescripcion()))
                        l.add(lp.get(i));
                }
                return l;
            }
            else return lp;
    }
    
    //Seleccionar Multicriterio Tipo y Descripcion
    public ArrayList<Producto> buscarMulticriterioNoInsumo(String descripcion, String tipo){
           
            ArrayList<Producto> lp = buscarPorDescripcion(descripcion);
            ArrayList<Producto> l = new ArrayList<Producto>();
            if (tipo!=null)
            {
                for (int i=0; i<lp.size(); i++)
                {
                    if (tipo.equals(lp.get(i).getToTipoProducto().getDescripcion())&&lp.get(i).getToTipoProducto().getIdtipoproducto()!=1)
                        l.add(lp.get(i));
                }
                return l;
            }
            else return lp;
    }
    
    
    
    public ArrayList<Producto> buscarMulticriterioNoTer(String estado, String descripcion, String tipo){
           
            ArrayList<Producto> lp = this.buscarMulticriterio(descripcion, tipo);
            
            ArrayList<Producto> l = new ArrayList<Producto>();
            if (tipo!=null)
            {
                for (int i=0; i<lp.size(); i++)
                {
                    if ((tipo.equals(lp.get(i).getToTipoProducto().getDescripcion())) && (lp.get(i).getEstado().equals(Estado.getEstadoId(estado)))&&(lp.get(i).getToTipoProducto().getIdtipoproducto()!=3))
                        l.add(lp.get(i));
                }
                return l;
            }
            else return lp;
    }
    
       
    //Seleccionar Por Tipo
   public ArrayList<Producto> buscarPorTipo(int idtipo){
        ObjectContext c= DataContext.createDataContext();
        TipoProducto t= buscarTipoPorId(idtipo);        
        Expression q = ExpressionFactory.matchExp(Producto.TO_TIPO_PRODUCTO_PROPERTY, t);
        Expression q1 = ExpressionFactory.greaterExp(Producto.ESTADO_PROPERTY, 0);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Producto.class,q2);
        ArrayList<Producto> lp = (ArrayList<Producto>) c.performQuery(s);
        return lp;
            
    }
   
   //Eliminar producto
    public void eliminar(int idProd) {
        Estado e = new Estado();
        RecetaService sr= new RecetaService();
        ProductoXProcesoService spp=new ProductoXProcesoService();
        ObjectContext c= DataContext.createDataContext();
        Producto p = this.buscarPorId(idProd,c);
        p.setEstado(e.getEliminado());
        ArrayList<Receta> lr=sr.BuscarPorProducto(c, p);
        ArrayList<AlmacenXProducto> la= this.buscaAlmacenes(c,p);
        ArrayList<ProductoProveedor> lp=this.buscaProveedores(c, p);
        ArrayList<ProductoXProceso> lpp=spp.buscaPorProducto(c, idProd);
        if (!lr.isEmpty())sr.EliminaReceta(lr);
        if (!la.isEmpty())this.EliminaAlmacenes(la);
        if (!lp.isEmpty())this.EliminaProveedores(lp);
        
            for (int i=0;i<lpp.size();i++)
            {
                spp.Elimina(lpp.get(i).getIdproductoxproceso());
            }
        c.commitChanges();  
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Producto");
    }
  
    
    //Seleccionar todos los tipos de Proceso
    public ArrayList<TipoProducto> buscarTodoLosTipos () {
        ObjectContext c = DataContext.createDataContext();
        SelectQuery s = new SelectQuery(TipoProducto.class);
        ArrayList<TipoProducto> lp = (ArrayList<TipoProducto>) c.performQuery(s);
        return lp;
    }
    
    //Buscar Tipo de Producto por id
    public TipoProducto buscarTipoPorId (int idTipo) {
        ObjectContext c = DataContext.createDataContext();
        Expression q= ExpressionFactory.matchExp(TipoProducto.IDTIPOPRODUCTO_PK_COLUMN, idTipo);
        SelectQuery s = new SelectQuery(TipoProducto.class,q);
        return (TipoProducto) DataObjectUtils.objectForQuery(c, s);
    }
        
    public TipoProducto buscarTipoPorId (int idTipo,ObjectContext c ) {
       
        Expression q= ExpressionFactory.matchExp(TipoProducto.IDTIPOPRODUCTO_PK_COLUMN, idTipo);
        SelectQuery s = new SelectQuery(TipoProducto.class,q);
        return (TipoProducto) DataObjectUtils.objectForQuery(c, s);
    }

    //Buscar galletas
    public static ArrayList<dp1.titandevelop.titano.persistent.Producto> buscarGalleta(){
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PRODUCTO a, TIPO_PRODUCTO b WHERE a.IDTIPOPRODUCTO=b.IDTIPOPRODUCTO"
                +" AND b.DESCRIPCION like '%Terminado%' AND a.ESTADO ="+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);
        
        ArrayList<Producto> producto =(ArrayList<Producto>) context.performQuery(query);  
                
        return producto;
    
    }
    
    //Actualizar productos
    public void actualizar(int idProd, String descripcion, int stockMin, int stockMax, int loteMin, int loteMax, int diasVenc, 
            float beneficio, float costo, String UM, int idProc, float peso )
    {
        ObjectContext c = DataContext.createDataContext();
        Producto p = this.buscarPorId(idProd,c);
        //TipoProducto t=this.buscarTipoPorId(idTipo,c);
        // busco el proceso 
        Expression qualifier1 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PK_COLUMN, idProc);
        SelectQuery select1 = new SelectQuery(Proceso.class, qualifier1);
        Proceso p1 = (Proceso) DataObjectUtils.objectForQuery(c, select1);
              
        //p.setToTipoProducto(t);
        p.setStockmin(stockMin);
        p.setToProceso(p1);
        p.setPeso(peso);
        p.setDescripcion(descripcion);
        p.setStockmax(stockMax);
        p.setLotemaxcompra(loteMax);
        p.setLotemincompra(loteMin);
        p.setDiasvencimiento(diasVenc);
        p.setBeneficio(beneficio);
        p.setCosto(costo);
        p.setUm(UM);
        p.setEstado(e.getActivo());
        c.commitChanges();   
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Producto");
}

    public void InsertaProveedores(int idProd, ArrayList<ProductoProveedor> l) {
        //busca producto
        ObjectContext context = DataContext.createDataContext();
        Producto p=this.buscarPorId(context, idProd);
        //Inserta en todos los almacenes
        for (int i=0;i<l.size();i++)
        {
            ProductoProveedor ap= context.newObject(ProductoProveedor.class);
            Expression q = ExpressionFactory.matchExp(Proveedor.IDPROVEEDOR_PK_COLUMN, l.get(i).getToProveedor().getIdproveedor());
            Expression q1 = ExpressionFactory.greaterExp(Proveedor.ESTADO_PROPERTY, 0);
            Expression q2 = q.andExp(q1);
            SelectQuery s = new SelectQuery(Proveedor.class, q2);
            Proveedor prov=(Proveedor) DataObjectUtils.objectForQuery(context, s);
            ap.setCalificacioncalidad(l.get(i).getCalificacioncalidad());
            ap.setEstado(e.getActivo());
            ap.setPrecio(l.get(i).getPrecio());
            ap.setToProducto(p);
            ap.setToProveedor(prov);
            context.commitChanges();
        }        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "ProductoProveedor");
    }
    
    public void ActualizaAlmacenes(ArrayList<AlmacenXProducto> l) {
        
        ObjectContext context = DataContext.createDataContext();
        
        //Inserta en todos los almacenes
        for (int i=0;i<l.size();i++)
        {
            Expression q = ExpressionFactory.matchExp(AlmacenXProducto.IDALMACENXPRODUCTO_PK_COLUMN, l.get(i).getIdalmacenxproducto());
            SelectQuery s = new SelectQuery(AlmacenXProducto.class, q);
            AlmacenXProducto ap=(AlmacenXProducto) DataObjectUtils.objectForQuery(context, s);
            ap.setCapacidad(l.get(i).getCapacidad());
            ap.setEstado(e.getActivo());
            
            context.commitChanges();
        }        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "AlmacenXProducto");
    }
    
    public void EliminaAlmacenes(ArrayList<AlmacenXProducto> l) {
        
        ObjectContext context = DataContext.createDataContext();
        
        //Inserta en todos los almacenes
        for (int i=0;i<l.size();i++)
        {
            Expression q = ExpressionFactory.matchExp(AlmacenXProducto.IDALMACENXPRODUCTO_PK_COLUMN, l.get(i).getIdalmacenxproducto());
            SelectQuery s = new SelectQuery(AlmacenXProducto.class, q);
            AlmacenXProducto ap=(AlmacenXProducto) DataObjectUtils.objectForQuery(context, s);
           
            ap.setEstado(e.getEliminado());
            
            context.commitChanges();
        }   
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "AlmacenXProducto");
    }
    
     public void ActualizaProveedores(ArrayList<ProductoProveedor> l) {
        
        ObjectContext context = DataContext.createDataContext();
        
        //Inserta en todos los almacenes
        for (int i=0;i<l.size();i++)
        {
            Expression q = ExpressionFactory.matchExp(ProductoProveedor.IDPRODUCTOPROVEEDOR_PK_COLUMN, l.get(i).getIdproductoproveedor());
            SelectQuery s = new SelectQuery(ProductoProveedor.class, q);
            ProductoProveedor ap=(ProductoProveedor) DataObjectUtils.objectForQuery(context, s);
            ap.setCalificacioncalidad(l.get(i).getCalificacioncalidad());
            ap.setEstado(e.getActivo());
            ap.setPrecio(l.get(i).getPrecio());
            context.commitChanges();
        }        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "ProductoProveedor");
    }
    
    public void EliminaProveedores(ArrayList<ProductoProveedor> l) {
        
        ObjectContext context = DataContext.createDataContext();
        
        //Inserta en todos los almacenes
        for (int i=0;i<l.size();i++)
        {
            Expression q = ExpressionFactory.matchExp(ProductoProveedor.IDPRODUCTOPROVEEDOR_PK_COLUMN, l.get(i).getIdproductoproveedor());
            SelectQuery s = new SelectQuery(ProductoProveedor.class, q);
            ProductoProveedor ap=(ProductoProveedor) DataObjectUtils.objectForQuery(context, s);
           
            ap.setEstado(e.getEliminado());
            
            context.commitChanges();
        }        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "ProductoProveedor");
    }
    
    public ArrayList<ProductoProveedor> buscaProveedores(Producto p)
    {
        ObjectContext c = DataContext.createDataContext();
        Expression q= ExpressionFactory.matchExp(ProductoProveedor.TO_PRODUCTO_PROPERTY, p);
        Expression q1= ExpressionFactory.greaterExp(ProductoProveedor.ESTADO_PROPERTY, 0);
        Expression q2= q1.andExp(q);
        SelectQuery s = new SelectQuery(ProductoProveedor.class,q2);
        ArrayList<ProductoProveedor> l= (ArrayList<ProductoProveedor>) c.performQuery(s);
        return l; 
    }  
     public ArrayList<ProductoProveedor> buscaProveedores(ObjectContext c,Producto p)
    {
        
        Expression q= ExpressionFactory.matchExp(ProductoProveedor.TO_PRODUCTO_PROPERTY, p);
        Expression q1= ExpressionFactory.greaterExp(ProductoProveedor.ESTADO_PROPERTY, 0);
        Expression q2= q1.andExp(q);
        SelectQuery s = new SelectQuery(ProductoProveedor.class,q2);
        ArrayList<ProductoProveedor> l= (ArrayList<ProductoProveedor>) c.performQuery(s);
        return l; 
    }  
    public ArrayList<AlmacenXProducto> buscaAlmacenes(Producto p)
    {
        ObjectContext c = DataContext.createDataContext();
        Expression q= ExpressionFactory.matchExp(AlmacenXProducto.TO_PRODUCTO_PROPERTY, p);
        Expression q1= ExpressionFactory.greaterExp(AlmacenXProducto.ESTADO_PROPERTY, 0);
        Expression q2= q1.andExp(q);
        SelectQuery s = new SelectQuery(AlmacenXProducto.class,q2);
        ArrayList<AlmacenXProducto> l= (ArrayList<AlmacenXProducto>) c.performQuery(s);
        return l; 
    }
     public ArrayList<AlmacenXProducto> buscaAlmacenes( ObjectContext c,Producto p)
    {
        
        Expression q= ExpressionFactory.matchExp(AlmacenXProducto.TO_PRODUCTO_PROPERTY, p);
        Expression q1= ExpressionFactory.greaterExp(AlmacenXProducto.ESTADO_PROPERTY, 0);
        Expression q2= q1.andExp(q);
        SelectQuery s = new SelectQuery(AlmacenXProducto.class,q2);
        ArrayList<AlmacenXProducto> l= (ArrayList<AlmacenXProducto>) c.performQuery(s);
        return l; 
    }
     
     public int Stock(int idProd)
     {
         int stock=0;
         Producto p = this.buscarPorId(idProd);
         ArrayList<AlmacenXProducto> l=this.buscaAlmacenes(p);
         for (int i=0;i<l.size();i++)
         {
             stock+=l.get(i).getStock();
         }
         return stock;
     }
}
    
     