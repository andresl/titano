/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Cotizacion;
import dp1.titandevelop.titano.persistent.DetalleCotizacion;
import dp1.titandevelop.titano.persistent.OrdenCompra;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author andres
 */
public class OrdenCompraService {
    public OrdenCompra buscarPorId(ObjectContext c, int id) {
        if (c == null) {
            c = DataContext.createDataContext();
        }
        Expression q = ExpressionFactory.matchExp(OrdenCompra.IDORDENCOMPRA_PK_COLUMN, id);
        SelectQuery s = new SelectQuery(OrdenCompra.class, q);
        return (OrdenCompra) DataObjectUtils.objectForQuery(c, s);
    }

    public OrdenCompra insertarOrdenCompra(ArrayList<Cotizacion> listaCotizaciones, Date fechaentrega, Date date, int estado) {
        ObjectContext c = listaCotizaciones.get(0).getObjectContext();
        OrdenCompra oc = c.newObject(OrdenCompra.class);
        
        //cotizacion.setToOrdenCompra(oc);
        oc.setFecharegistro(new Date());
        oc.setFechaentrega(fechaentrega);
        oc.setEstado(estado);

        // no hay ningun arreglo, se les setea la misma orden de compra a todo en listaCotizciones
        for (int i=0; i<listaCotizaciones.size(); i++ ) {
            listaCotizaciones.get(i).setToOrdenCompra(oc);
        }
        c.commitChanges();
        
        for (int i=0; i<listaCotizaciones.size(); i++) {
            for (int j=0; j<listaCotizaciones.get(i).getDetalleCotizacionArray().size(); j++) {     
                listaCotizaciones.get(i).getDetalleCotizacionArray().get(j).getToRequerimientoXProducto().setEstado(Estado.getEstadoId("Cumplido"));
                RequerimientoService rs = new RequerimientoService(); 
                rs.marcarRequerimiento(listaCotizaciones.get(i).getDetalleCotizacionArray().get(j).getToRequerimientoXProducto().getToRequerimiento());
            }
        }
        c.commitChanges();


        return oc;
    }
    
    public ArrayList<DetalleCotizacion> detalleOrdenCompra(OrdenCompra oc) {
        ArrayList<DetalleCotizacion> l = new ArrayList<DetalleCotizacion>();
        for (int i=0; i<oc.getCotizacionArray().size(); i++) {
            l.addAll(oc.getCotizacionArray().get(i).getDetalleCotizacionArray());
        }
        
        return l;
    }
    
    public ArrayList<OrdenCompra> buscarPor(Date fechaRegistro, Date fechaEntrega, int estado) {
        ObjectContext c = DataContext.createDataContext();

        Expression q = ExpressionFactory.greaterExp(OrdenCompra.ESTADO_PROPERTY, 0);
        q= q.orExp(ExpressionFactory.matchExp(OrdenCompra.FECHAREGISTRO_PROPERTY, fechaRegistro));
        q =q.orExp(ExpressionFactory.matchExp(OrdenCompra.FECHAENTREGA_PROPERTY, fechaEntrega));
        q= q.orExp(ExpressionFactory.matchExp(OrdenCompra.ESTADO_PROPERTY, estado));
        q = q.andExp(ExpressionFactory.greaterExp(OrdenCompra.IDORDENCOMPRA_PK_COLUMN, 0));

        SelectQuery s = new SelectQuery(OrdenCompra.class, q);
        
        return (ArrayList<OrdenCompra>) c.performQuery(s);
    }
    public ArrayList<OrdenCompra> buscarPor(Date fechaRegistro, Date fechaEntrega) {
        ObjectContext c = DataContext.createDataContext();
        Estado est = new Estado();
        Expression q = ExpressionFactory.greaterExp(OrdenCompra.ESTADO_PROPERTY, est.getEliminado());
        q= q.orExp(ExpressionFactory.matchExp(OrdenCompra.FECHAREGISTRO_PROPERTY, fechaRegistro));
        q =q.orExp(ExpressionFactory.matchExp(OrdenCompra.FECHAENTREGA_PROPERTY, fechaEntrega));
        q = q.andExp(ExpressionFactory.greaterExp(OrdenCompra.IDORDENCOMPRA_PK_COLUMN, 0));

        SelectQuery s = new SelectQuery(OrdenCompra.class, q);
        
        return (ArrayList<OrdenCompra>) c.performQuery(s);
    }
    public ArrayList<OrdenCompra> buscarPorEstado( int estado) {
        ObjectContext c = DataContext.createDataContext();

        Expression q = ExpressionFactory.matchExp(OrdenCompra.ESTADO_PROPERTY, estado);
        q = q.andExp(ExpressionFactory.greaterExp(OrdenCompra.IDORDENCOMPRA_PK_COLUMN, 0));

        SelectQuery s = new SelectQuery(OrdenCompra.class, q);
        
        return (ArrayList<OrdenCompra>) c.performQuery(s);
    }

    public ArrayList<OrdenCompra> buscarTodo() {
        ObjectContext c = DataContext.createDataContext();
        Estado est = new Estado();
        Expression q = ExpressionFactory.greaterExp(OrdenCompra.ESTADO_PROPERTY, est.getEliminado());
        q = q.andExp(ExpressionFactory.greaterExp(OrdenCompra.IDORDENCOMPRA_PK_COLUMN, 0));
        SelectQuery s = new SelectQuery(OrdenCompra.class, q);
        
        return (ArrayList<OrdenCompra>) c.performQuery(s);
    }
}
