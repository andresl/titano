/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.persistent.Maquina;
import dp1.titandevelop.titano.persistent.Proceso;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author natali
 */
public class MaquinaService {
    
     public  void Asignar(int idMaquina){
                      
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "UPDATE MAQUINA SET  ASIGNADO = true  WHERE IDMAQUINA = "+idMaquina+"";
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
     }
     
     public  void desasignar(){
                      
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "UPDATE MAQUINA SET  ASIGNADO = false";
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
     }
    
     public ArrayList<Maquina> buscarIdProceso(int idProceso){
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM MAQUINA WHERE IDPROCESO= "+ idProceso
                + " AND ESTADO = " + estado.getActivo() 
                + " AND ASIGNADO="+false+"";
        
        
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);
        ArrayList<Maquina> lp =(ArrayList<Maquina>) context.performQuery(query);
        return lp;
    
    }
     
     public static ArrayList<Maquina> buscar(){
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM MAQUINA WHERE"
                + " ESTADO = " + estado.getActivo() 
                + " AND ASIGNADO="+false+"";
        
        
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);
        ArrayList<Maquina> lp =(ArrayList<Maquina>) context.performQuery(query);
        return lp;
    
    }
     public  List<Maquina> buscarAll(){
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM MAQUINA WHERE"
                + " ESTADO = " + estado.getActivo() 
                + " AND ASIGNADO="+false+"";
        
        
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);
        ArrayList<Maquina> lp =(ArrayList<Maquina>) context.performQuery(query);
        return lp;
    
    }
     
     public List<Maquina> buscarMaquina(String desc, String kw,  String desczona) {        
        Estado est = new Estado();
        int idestado ;
        String cadena = "" ;
        ObjectContext context = DataContext.createDataContext();
        idestado = est.getEstadoId(desczona);
        if (kw.equals("")){
            cadena = "SELECT * FROM MAQUINA WHERE DESCRIPCION LIKE '%"+desc+"%' AND ESTADO = " + idestado + "";
        }else{
            cadena = "SELECT * FROM MAQUINA WHERE DESCRIPCION LIKE '%"+desc+"%' AND kwh = "+ Float.parseFloat(kw) +" AND ESTADO = " +idestado+ "";
        }
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);         
        List<Maquina> tipos = context.performQuery(query);
        
        return tipos;
    }
     public List<Maquina> buscarMaquina(int idprocs,String desc, String kw,  String desczona) {        
        Estado est = new Estado();
        int idestado ;
        String cadena = "" ;
        ObjectContext context = DataContext.createDataContext();
        idestado = est.getEstadoId(desczona);
        if (kw.equals("")){
            cadena = "SELECT * FROM MAQUINA WHERE DESCRIPCION LIKE '%"+desc+"%' AND ESTADO = " + idestado +" AND IDPROCESO = " + idprocs + "";
        }else{
            cadena = "SELECT * FROM MAQUINA WHERE DESCRIPCION LIKE '%"+desc+"%' AND kwh = "+ Float.parseFloat(kw) +" AND ESTADO = " +idestado +" AND IDPROCESO = " + idprocs + "";
        }
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);         
        List<Maquina> tipos = context.performQuery(query);
        
        return tipos;
    }
      public void eliminarMaquina(int idTipo){
        Estado est = new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(Maquina.IDMAQUINA_PK_COLUMN,idTipo);
        SelectQuery s = new SelectQuery(Maquina.class,q);
        Maquina m = (Maquina)DataObjectUtils.objectForQuery(context, s);
        
        Expression q2 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PK_COLUMN,m.getToProceso().getIdproceso());
        SelectQuery s2 = new SelectQuery(Proceso.class,q2);
        Proceso p = (Proceso)DataObjectUtils.objectForQuery(context, s2);
        
        int cantidad = p.getCantidadmaquina();
        p.setCantidadmaquina(cantidad-1);
        m.setEstado(est.getEliminado());
       
        context.commitChanges(); 
    }
      public void insertarMaquina(int idproc,String descripcion, String kw) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        
        Expression qualifier = ExpressionFactory.matchExp(Proceso.IDPROCESO_PROPERTY,idproc);
        SelectQuery select = new SelectQuery(Proceso.class, qualifier);
        Proceso proctemp = (Proceso) DataObjectUtils.objectForQuery(context, select);
        
        Maquina tm = context.newObject(Maquina.class);
        tm.setDescripcion(descripcion);
        tm.setKwh(Float.parseFloat(kw));
        tm.setAsignado(Boolean.FALSE);
        tm.setEstado(est.getActivo());
        tm.setToProceso(proctemp);
        int cantidad = proctemp.getCantidadmaquina();
        proctemp.setCantidadmaquina(cantidad+1);
        context.commitChanges();        
    }
      
      public void editarMaquina(int idmaquina, int idproc, String descest, String desc, String kw) {
        
         ProcesoService proc = new ProcesoService();
         List<Proceso> procesos = proc.buscarProcAll();
        
         Estado est = new Estado();
        int idestado = est.getEstadoId(descest);
        ObjectContext context = DataContext.createDataContext();
        Expression qualifier = ExpressionFactory.matchExp(Proceso.IDPROCESO_PROPERTY,idproc);
        SelectQuery select = new SelectQuery(Proceso.class, qualifier);
        Proceso procesostemp = (Proceso) DataObjectUtils.objectForQuery(context, select);
              
        String cadena = "UPDATE MAQUINA SET  IDPROCESO = "+procesostemp.getIdproceso() + ", DESCRIPCION = '"+desc+"', "+" ESTADO = "+idestado+  ", "+
                        "KWH = "+Float.parseFloat(kw)+" WHERE IDMAQUINA = "+idmaquina+"";
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();     
    }
      
        Maquina BuscaPorId( int id) {
        ObjectContext context = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(Maquina.IDMAQUINA_PK_COLUMN, id);       
        SelectQuery s = new SelectQuery(Maquina.class, q);
        return (Maquina) DataObjectUtils.objectForQuery(context, s);
    }
}
