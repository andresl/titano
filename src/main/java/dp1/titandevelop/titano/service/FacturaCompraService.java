/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Cotizacion;
import dp1.titandevelop.titano.persistent.DetalleFacturaCompra;
import dp1.titandevelop.titano.persistent.FacturaCompra;
import dp1.titandevelop.titano.persistent.OrdenCompra;
import java.util.ArrayList;
import java.util.Date;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author andres
 */
public class FacturaCompraService {
    public ArrayList<FacturaCompra> buscarTodos () {
        ObjectContext context = DataContext.createDataContext();

        Expression q = ExpressionFactory.greaterExp(FacturaCompra.ESTADO_PROPERTY, 0);
      
        SelectQuery s = new SelectQuery(FacturaCompra.class, q);
        
        return (ArrayList<FacturaCompra>) context.performQuery(s);
        
    }
    
    public void insertarFactura(OrdenCompra oc, ArrayList<Cotizacion> listaCotizaciones, float mt) {
        ObjectContext context = listaCotizaciones.get(0).getObjectContext();
        FacturaCompra fc = context.newObject(FacturaCompra.class);
        fc.setEstado(1);
        fc.setFecha(new Date());
        fc.setMontototal(mt);
        fc.setToOrdenCompra(oc);
        context.commitChanges();
        
        AlmacenService as = new AlmacenService();
        for (int i = 0; i < listaCotizaciones.size(); i++) {
            for (int j = 0; j < listaCotizaciones.get(i).getDetalleCotizacionArray().size(); j++) {
                DetalleFacturaCompra df = context.newObject(DetalleFacturaCompra.class);
                df.setToProducto(listaCotizaciones.get(i).getDetalleCotizacionArray().get(j).getToRequerimientoXProducto().getToProducto());
                df.setCantidad(listaCotizaciones.get(i).getDetalleCotizacionArray().get(j).getCantidad());
                df.setToFacturaCompra(fc);
                df.setMonto(mt);
                df.setEstado(1);
                fc.addToDetalleFacturaCompraArray(df);
                as.movimientoAlmacen(fc.getIdfacturacompra(), 
                        listaCotizaciones.get(i).getDetalleCotizacionArray().get(j).getToRequerimientoXProducto().getToProducto().getIdproducto(),
                        2, 
                        listaCotizaciones.get(i).getDetalleCotizacionArray().get(j).getCantidad()); //entrada externa... cambiar
                
            }
        }
        
        context.commitChanges();
        
        
        
        
    }
}
