/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.Receta;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author dorita
 */
public class RecetaService {
   
    Estado e=  new Estado();
    
    //Constructor
    public RecetaService()
    {
         
         
    }
    
    //Metodos de Insersion
    public void Insertar(int ipProd, List<Receta> lista){
         ProductoService sp = new ProductoService();
         ObjectContext c = DataContext.createDataContext();
        Producto p = sp.buscarPorId(c,ipProd);
        
        for(int i=0;i<lista.size();i++){
            Receta r = c.newObject(Receta.class);
            r.setCantidad(lista.get(i).getCantidad());
            r.setEstado(e.getActivo());
            r.setUm(lista.get(i).getUm());
            r.setToProducto(p);
            Producto insumo = sp.buscarPorId(c,lista.get(i).getToProducto1().getIdproducto());
            r.setToProducto1(insumo);
            c.commitChanges();        
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Receta");
        }      
    }
    
     public void Insertar(int ipProd, List<Receta> lista, ObjectContext c ){
       
         ProductoService sp = new ProductoService();
         Producto p = sp.buscarPorId(c,ipProd);
        
        for(int i=0;i<lista.size();i++){
            Receta r = c.newObject(Receta.class);
            r.setCantidad(lista.get(i).getCantidad());
            r.setEstado(e.getActivo());
            r.setUm(lista.get(i).getUm());
            r.setToProducto(p);
            Producto insumo = sp.buscarPorId(c,lista.get(i).getToProducto1().getIdproducto());
            r.setToProducto1(insumo);
            c.commitChanges();    
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Receta");
        }      
    }
    
   //Metodos de búsqueda:  
   
    // Busca el conjunto de insumos de un producto, siempre que estos esten activos (Estado>0)
    public ArrayList<Receta> BuscarPorProducto (Producto p)
    {
        ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.greaterExp(Receta.ESTADO_PROPERTY, 0);
        Expression q1 = ExpressionFactory.matchExp(Receta.TO_PRODUCTO_PROPERTY, p);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Receta.class,q2);
        ArrayList<Receta> lp = (ArrayList<Receta>) c.performQuery(s);
        return lp;
    }
    
     public ArrayList<Receta> BuscarPorProducto (ObjectContext c, Producto p)
    {
       // ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.greaterExp(Receta.ESTADO_PROPERTY, 0);
        Expression q1 = ExpressionFactory.matchExp(Receta.TO_PRODUCTO_PROPERTY, p);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Receta.class,q2);
        ArrayList<Receta> lp = (ArrayList<Receta>) c.performQuery(s);
        return lp;
    }
     public List<Receta> BuscarPorIdProducto(int idProducto){
         
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from receta where estado > 0 and idproducto ="+idProducto+"";
        SQLTemplate query = new SQLTemplate(Receta.class,cadena);         
        List<Receta> materiales = context.performQuery(query);
        return materiales;
     }
    
    // Busca una receta en particular activas e inactivas
     public Receta BuscarPorId (int id)
    {
        ObjectContext c = DataContext.createDataContext();
       // Expression q = ExpressionFactory.greaterExp(Receta.ESTADO_PROPERTY, 0);
        Expression q1 = ExpressionFactory.matchExp(Receta.IDRECETA_PK_COLUMN, id);
       // Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(Receta.class,q1);
        Receta r = (Receta) DataObjectUtils.objectForQuery(c, s);
        return r;
    }
     
     // Busca una receta en particular activas e inactivas por producto e insumo
     public Receta BuscarPorInsumos (int idProducto, int idInsumo)
    {
        ObjectContext c = DataContext.createDataContext();
        return BuscarPorInsumos (idProducto,idInsumo,c);
    }
     
     public Receta BuscarPorInsumos (int idProducto, int idInsumo,ObjectContext c)
    {
         ProductoService sp = new ProductoService();
         Producto producto=sp.buscarPorId(c, idProducto);
         Producto insumo=sp.buscarPorId(c, idInsumo);// Expression q = ExpressionFactory.greaterExp(Receta.ESTADO_PROPERTY, 0);
        Expression q1 = ExpressionFactory.matchExp(Receta.TO_PRODUCTO_PROPERTY, producto);
        Expression q2 = ExpressionFactory.matchExp(Receta.TO_PRODUCTO1_PROPERTY, insumo);
        Expression q3 = q1.andExp(q2);
        SelectQuery s = new SelectQuery(Receta.class,q3);
        Receta r = (Receta) DataObjectUtils.objectForQuery(c, s);
        return r;
    }
     
      public Receta BuscarPorId (ObjectContext c,int id)
    {
              
        Expression q1 = ExpressionFactory.matchExp(Receta.IDRECETA_PK_COLUMN, id);
        //Expression q2= ExpressionFactory.greaterExp(Receta.ESTADO_PROPERTY,0);
        //Expression q0=q1.andExp(q2);
        SelectQuery s = new SelectQuery(Receta.class, q1);
        return (Receta) DataObjectUtils.objectForQuery(c, s);
        
    }
     
       
      // Busca todos los productos activos con recetas activas
    public ArrayList<Producto> BuscarProductos ()
    {
         ProductoService sp = new ProductoService();
         ObjectContext c = DataContext.createDataContext();
        ArrayList<Producto> lp = sp.buscarTodo();
        if (lp!=null)
        for (int i=0; i<lp.size();i++)
        {
            if (sacaTamano(lp.get(i))==0) 
            {
                lp.remove(i);
                i--;
            }
        }
        
        return lp;
    }
    
    //Busca productor por descripcion con receta activa
    public ArrayList<Producto> BuscarProductos (String desc )
    {
            
         ProductoService sp = new ProductoService();
         ArrayList<Producto> lp = sp.buscarPorDescripcion(desc);
        if (lp!=null)
        for (int i=0; i<lp.size();i++)
        {
            if (sacaTamano(lp.get(i))==0) 
            {
                lp.remove(i);
                i--;
            }
        }
        
        return lp;
    }
    
    //Busca todas las recetas activas
    public ArrayList<Receta> BuscarTodo()
    {
         ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.greaterExp(Producto.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Receta.class,q);
        ArrayList<Receta> lp = (ArrayList<Receta>) c.performQuery(s);
        return lp;
    }
    
   //Devuelve la cantidad de insumos por receta
    public int sacaTamano(Producto p) {
        ArrayList<Receta> r = new ArrayList<Receta>();
        r = this.BuscarPorProducto(p);
        return r.size();//To change body of generated methods, choose Tools | Templates.
    }

    //Elimina Receta    
    public void EliminaReceta(List<Receta> lista) {
    
        ObjectContext c = DataContext.createDataContext();
       
        for(int i=0;i<lista.size();i++){
            Receta r = this.BuscarPorId(c, lista.get(i).getIdreceta());
            //r.setCantidad(lista.get(i).getCantidad());
            r.setEstado(e.getEliminado());
//            r.setUm(lista.get(i).getUm());
//            Producto producto = sp.buscarPorId(c,lista.get(i).getToProducto1().getIdproducto());
//            r.setToProducto(producto);
//            Producto insumo = sp.buscarPorId(c,lista.get(i).getToProducto().getIdproducto());
//            r.setToProducto(insumo);
            c.commitChanges();      
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Receta");
        }      
        c.commitChanges();
       
    }

    //Actualiza Receta
    public void ActualizaReceta(Receta r, Receta r1) {
       ObjectContext c = r.getObjectContext();
      
            
            r.setCantidad(r1.getCantidad());
            r.setEstado(e.getActivo());
           
            c.commitChanges();          
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Receta");
        
    }
    
        //Busca receta de productos terminados
    public static ArrayList<Receta> BuscarTodosProductosTerminados(){
        Estado estado=new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from receta t1, producto t2 where t1.estado= "+estado.getActivo()+
                " and t1.idproducto=t2.idproducto and t2.idtipoproducto=3";
        SQLTemplate query = new SQLTemplate(Receta.class,cadena);         
        ArrayList<Receta> materiales =(ArrayList<Receta>) context.performQuery(query);
        return materiales;
     }

}
