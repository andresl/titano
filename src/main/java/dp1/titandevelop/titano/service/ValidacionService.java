/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import org.apache.poi.ss.usermodel.Cell;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author cHreS
 */
//Falta correo y alfanumericos
public class ValidacionService {

    public void SLetras(JTextField jtext) {
        jtext.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (Character.isDigit(c)) {
                    e.consume();
                } else {
                }
            }
        });
    }
    public void SAlfanumerico(JTextField jtext) {
        jtext.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isLetterOrDigit(c)) {
                    if(!Character.isSpaceChar(c))
                    e.consume();
                } else {
                }
            }
        });
    }
    public void SSoloLetra(JTextField jtext) {
        jtext.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!Character.isLetter(c)) {
                    if(!Character.isSpaceChar(c))
                    e.consume();
                } else {
                }
            }
        });
    }
    public void NoEscribe(JXDatePicker jxdatepicker) {
        jxdatepicker.getEditor().setEnabled(false);
    }
    public void SNumeros(JTextField jtext) {
        jtext.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (Character.isDigit(c)) {
                } else {
                    e.consume();
                }
            }
        });
    }
    public boolean esVacio(JTextField jtext) {
        return (jtext.getText().isEmpty());
    }
    public boolean esCBoxVacio(JComboBox cbox) {
        int index = cbox.getSelectedIndex() - 1;
        if (index == -1) {
            return true;
        } else {
            return false;
        }
    }
    public boolean esFechaVacia(JXDatePicker jxdatepicker) {
        return (jxdatepicker.getDate() == null);
    }
    public boolean validaCorreo(final JTextField jtext) {
         Pattern pattern;
         Matcher matcher;
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";        
            pattern = Pattern.compile(EMAIL_PATTERN);
       
            matcher = pattern.matcher(jtext.getText());
            return matcher.matches();

        }
    public void Longitud(final JTextField jtext, final Integer longitud) {
            jtext.addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent e) {
                    char c = e.getKeyChar();
                    String Caracteres = jtext.getText();
                    if (Caracteres.length() >= longitud) {
                        e.consume();
                    }
                }
            });
        }
    public boolean LongitudExacta(String caracteres, int longitud) {
                if ( caracteres.length() < longitud) {
                    return false;
                }else{
                    return true;
                }
        }
    public int fechaNacimiento(JXDatePicker jxdatepicker, int edadminima) {
            //la fecha debe ser menor al dia actual
            String formato="yyyy";
            SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
            Date fecha=new Date(); 
            int anioact = Integer.parseInt(dateFormat.format(fecha));
            int aniomenor = Integer.parseInt(dateFormat.format(jxdatepicker.getDate()));
            int aniorf = anioact - edadminima;
            System.out.println(aniorf);
            System.out.println(aniorf);
            System.out.println(aniomenor);
            System.out.println(aniomenor);
            if (jxdatepicker.getDate().before(fecha)){
                if (aniorf < aniomenor){
                    return 2; //significa que es menor que la edad minima que deberia tener
                }else{
                    return 3;
                }
            }else{
                return 1;
                //significa que quieres poner un dia mayor al actual
            }
        }
    public String validaPassword(String password) {
        String cadena="Por favor verifica que su password contenga estos caracteres\n";
        
        boolean alerta=false;
        //  3 validaciones.
        //  1. Mínimo 6 caracteres.
        //  2. Mínimo 1 mayúscula
        //  3. Mínimo 1 número
        for (int i=1;i<4;i++) {
            switch (i) {

                        case 1:
                        {
                           if (password.length()<6){
                               cadena+="- Mínimo 6 caracteres\n";
                               alerta=true;
                           }
                            break; 
                        }
                        case 2:
                        {
                            int contadorMayus=0;
                            for (int j = 0; j < password.length(); j++) {
                                if (Character.isUpperCase(password.charAt(j)))  {
                                    contadorMayus++;
                                }
                            }
                            if (contadorMayus==0) {
                                cadena+="- Mínimo una letra mayúscula\n";
                                alerta=true;
                            }
                            break; 
                        }
                        case 3:
                        {
                           int contadorNumeros=0;
                            for (int j = 0; j < password.length(); j++) {
                                if (Character.isDigit(password.charAt(j)))  {
                                    contadorNumeros++;
                                }
                            }
                            if (contadorNumeros==0) {
                                cadena+="- Mínimo un número\n";
                                alerta=true;
                            }
                            break; 
                        }
                       
            }
        
       
    }
         if (alerta)
            return cadena;// Hay productos para alertar
        else 
            return null;
    }
}
