/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroDV;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.DetalleDamanda;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.DetalleSolicitudVenta;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.NotaCredito;
import dp1.titandevelop.titano.persistent.Parametro;

import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.SolicitudVenta;
import dp1.titandevelop.titano.persistent.TipoDocumento;
import dp1.titandevelop.titano.persistent.TipoProducto;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author natali
 */
public class DocumentoVentaService {
    
     //para tipo de empleados
    public List<DocumentoVenta> buscarDocumentoVenta(Date fechaIni, Date fechaFin,int tipo ,String text, int selectedIndex) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DOCUMENTO_VENTA A, DISTRIBUIDOR B";
        String where="WHERE ";
        String conector=" AND ";
        if(!text.isEmpty()){cadena+=where+"B.RAZONSOCIAL LIKE '%"+text+"%'"+conector; where="";}
        if(fechaFin.toString()==null){ cadena+=where+"A.FECHA>="+fechaIni+""+conector;}
        if(fechaIni.toString()==null){ cadena+=where+"A.FECHA<="+fechaFin+""+conector;}
        if(tipo>=0){ cadena+=where+"A.IDTIPODOCUMENTO="+tipo+"";}
        if(selectedIndex>=0){ cadena+=where+"A.ESTADO="+selectedIndex+"";}
        cadena+=" order by A.IDDOCUMENTO ASC";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        
        List<DocumentoVenta> documentos = context.performQuery(query);       
        return documentos;
    }
    
    public Parametro parametros() {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM PARAMETRO WHERE ESTADO=1";
        SQLTemplate query = new SQLTemplate(Parametro.class,cadena);
        
        List<Parametro> parametros = context.performQuery(query);       
        return parametros.get(0);
    }
    public DocumentoVenta buscarPorId(ObjectContext c, int id) {
        
        Expression q1 = ExpressionFactory.matchExp(DocumentoVenta.IDDOCUMENTO_PK_COLUMN, id);
        Expression q2= ExpressionFactory.greaterExp(DocumentoVenta.ESTADO_PROPERTY,0);
        Expression q0=q1.andExp(q2);
        SelectQuery s = new SelectQuery(DocumentoVenta.class, q0);
        return (DocumentoVenta) DataObjectUtils.objectForQuery(c, s);
    }
    public DetalleDocumentoVenta buscarPorIdDetalle(ObjectContext c, int id) {
        
        Expression q1 = ExpressionFactory.matchExp("toDocumentoVenta.idDocumento",id);
        Expression q2= ExpressionFactory.greaterExp(DetalleDocumentoVenta.ESTADO_PROPERTY,0);
        Expression q0=q1.andExp(q2);
        SelectQuery s = new SelectQuery(DetalleDocumentoVenta.class, q0);
        return (DetalleDocumentoVenta) DataObjectUtils.objectForQuery(c, s);
    }
    public void cambioEstadoVenta(int idDocumento,int estado){
   
        ObjectContext context = DataContext.createDataContext();     
        DocumentoVenta dv = this.buscarPorId(context,idDocumento);
        dv.setEstado(estado);
        
        DetalleDocumentoVenta ddv = this.buscarPorIdDetalle(context, idDocumento);
        ddv.setEstado(estado);
        
        context.commitChanges();   
    
    }
    public List<DocumentoVenta> buscarFiltrado(FiltroDV f) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT distinct  A.* FROM DOCUMENTO_VENTA A INNER JOIN DETALLE_DOCUMENTO_VENTA B ON  (B.IDDOCUMENTO=A.IDDOCUMENTO) INNER JOIN  PRODUCTO C ON  (B.IDPRODUCTO=C.IDPRODUCTO) ";
        String where=" WHERE ";
        String conector=" AND ";
        if(f.getFechaIni()!=null){cadena+=where+"A.FECHAFACTURACION>='"+f.getFechaIni()+"'"; where=" AND ";}
        if(f.getFechaFin()!=null){cadena+=where+"A.FECHAFACTURACION<='"+f.getFechaFin()+"'"; where=" AND ";}
        if(f.getDistribuidor()!=null){cadena+=where+"A.CONTACTO like '%"+f.getDistribuidor()+"%'"; where=" AND ";}
        if(f.getProducto()!=null){cadena+=where+"C.DESCRIPCION like '%"+f.getProducto()+"%'"; where=" AND ";}
        if(f.getIdTipo()!=null){cadena+=where+"A.IDTIPODOCUMENTO="+f.getIdTipo(); where=" AND ";}
        cadena+=where+"A.ESTADO=7";
        cadena+=" order by A.IDDOCUMENTO ASC";
        //if(f.getEstado()!=null){cadena+=where+"A.ESTADO="+f.getEstado()+" AND B.ESTADO="+f.getEstado()+" AND C.ESTADO=1"; where=" AND ";}
        
System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        
        List<DocumentoVenta> documentos = context.performQuery(query);       
        return documentos;
    }
    public List<DetalleDocumentoVenta> buscarFiltrado2(Integer idDocumentoVenta) {
        Estado state=new Estado();
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DETALLE_DOCUMENTO_VENTA WHERE IDDOCUMENTO="+idDocumentoVenta+" AND ESTADO<>"+state.getEliminado();
        //cadena+=" order by IDDETALLEDOCUMENTO ASC";
System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(DetalleDocumentoVenta.class,cadena);
        
        List<DetalleDocumentoVenta> documentos = context.performQuery(query);       
        return documentos;
    }
    public List<TipoDocumento> buscarTipoDocumento() {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM TIPO_DOCUMENTO WHERE ESTADO=1";
        SQLTemplate query = new SQLTemplate(TipoDocumento.class,cadena);
        
        List<TipoDocumento> tipodocumentos = context.performQuery(query);       
        return tipodocumentos;
    }
    public void insertarDocumentoVentaMasiva(int idDocumentoVenta,int idTipoDocumentoVenta,String contacto,
            String direccion,String dni, Date fechaEmision,float montoTotal,Date fechaFacturacion, float igv,
            float retencion, float percepcion, float detraccion, float descuento, float total, int estado, int idSolicitudVenta){
        try{
            //todo lo meto al objeto
            DocumentoVenta d= new DocumentoVenta();
            d.setIddocumento(idDocumentoVenta);
            
            
            //d.getToTipoDocumento().setIdtipodocumento(idTipoDocumentoVenta);
            d.setContacto(contacto);
            d.setDireccion(direccion);
            d.setDni(dni);
            d.setFechaemision(fechaEmision);
            d.setMontototal(montoTotal);
            d.setFechafacturacion(fechaFacturacion);
            d.setIgv(igv);
            d.setRetencion(retencion);
            d.setPercepcion(percepcion);
            d.setDetraccion(detraccion);
            d.setDescuento(descuento);
            d.setTotal(total);
            d.setEstado(estado);
            
            
            //d.getToSolicitudVenta().setIdsolicitudventa(idSolicitudVenta);
            
            
            Date fechaActual = new Date();
           ObjectContext context = DataContext.createDataContext(); 

            Expression qualifier = ExpressionFactory.matchExp(SolicitudVenta.IDSOLICITUDVENTA_PK_COLUMN, idSolicitudVenta);
            SelectQuery select = new SelectQuery(SolicitudVenta.class, qualifier);
            SolicitudVenta sv = (SolicitudVenta) DataObjectUtils.objectForQuery(context, select);

            Expression qualifier2 = ExpressionFactory.matchExp(TipoDocumento.IDTIPODOCUMENTO_PK_COLUMN,idTipoDocumentoVenta);
            SelectQuery select2 = new SelectQuery(TipoDocumento.class, qualifier2);
            TipoDocumento td = (TipoDocumento) DataObjectUtils.objectForQuery(context, select2);

            DocumentoVenta dv = context.newObject(DocumentoVenta.class); 
            dv.setIddocumento(idDocumentoVenta);
            dv.setToSolicitudVenta(sv);
            dv.setToTipoDocumento(td);
            dv.setContacto(d.getContacto());
            dv.setDireccion(d.getDireccion());
            dv.setDni(d.getDni());
            dv.setFechaemision(fechaActual);
            dv.setMontototal(d.getMontototal());
            dv.setFechafacturacion(d.getFechafacturacion());
            dv.setIgv(d.getIgv());
            dv.setRetencion(d.getRetencion());
            dv.setPercepcion(d.getPercepcion());
            dv.setDetraccion(d.getDetraccion());
            dv.setDescuento(d.getDescuento());
            dv.setTotal(d.getTotal());
            dv.setEstado(d.getEstado());

            context.commitChanges();
        }catch(Exception e){
        e.printStackTrace();
        }
        
        
    }
    
    public void insertarDetalleDocumentoVentaMasiva(int idDetalleDocumentoVenta,int idProducto, int idDocumento,
            int cantidad,float monto, int estado){
        try{
            //DetalleDocumentoVenta dDv= new DetalleDocumentoVenta();
            //dDv.setIddetalledocumentoventa(idDetalleDocumentoVenta);
//            dDv.getToProducto().setIdproducto(idProducto);
//            dDv.setIddetalledocumentoventa(idDetalleDocumentoVenta);
//            //dDv.getToDocumentoVenta().setIddocumento(idDocumento);
//            dDv.setCantidad(cantidad);
//            dDv.setMonto(monto);
//            dDv.setEstado(estado);
            
            ObjectContext context4 = DataContext.createDataContext();    
            Expression qualifier4 = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, idProducto);
            SelectQuery select4 = new SelectQuery(Producto.class, qualifier4);
            Producto p = (Producto) DataObjectUtils.objectForQuery(context4, select4);
            
            Expression qualifier5 = ExpressionFactory.matchExp(DocumentoVenta.IDDOCUMENTO_PK_COLUMN,idDocumento);
            SelectQuery select5 = new SelectQuery(DocumentoVenta.class, qualifier5);
            DocumentoVenta d0 = (DocumentoVenta) DataObjectUtils.objectForQuery(context4, select5);
            
            DetalleDocumentoVenta ddv = context4.newObject(DetalleDocumentoVenta.class);
            ddv.setIddetalledocumentoventa(idDetalleDocumentoVenta);
            ddv.setCantidad(cantidad);
            ddv.setMonto(monto);//se guarda el precio unitario de venta
            ddv.setEstado(estado);            
            ddv.setToProducto(p);
            ddv.setToDocumentoVenta(d0);
           
            
            //AlmacenService servA=new AlmacenService();
            //servA.movimientoAlmacen(d0.getIddocumento(),p.getIdproducto(),4,ddv.getCantidad());
            
            context4.commitChanges();
        }catch(Exception e){
            e.printStackTrace();
        }
    
    }
    
    public void  insertar (DocumentoVenta d,List<DetalleDocumentoVenta> dDv,Integer idNota){
        try{
        Estado state=new Estado();
        Date fechaActual = new Date();
        ObjectContext context = DataContext.createDataContext(); 
        
        Expression qualifier = ExpressionFactory.matchExp(SolicitudVenta.IDSOLICITUDVENTA_PK_COLUMN, d.getToSolicitudVenta().getIdsolicitudventa());
        SelectQuery select = new SelectQuery(SolicitudVenta.class, qualifier);
        SolicitudVenta sv = (SolicitudVenta) DataObjectUtils.objectForQuery(context, select);
        
        Expression qualifier2 = ExpressionFactory.matchExp(TipoDocumento.IDTIPODOCUMENTO_PK_COLUMN, d.getToTipoDocumento().getIdtipodocumento());
        SelectQuery select2 = new SelectQuery(TipoDocumento.class, qualifier2);
        TipoDocumento td = (TipoDocumento) DataObjectUtils.objectForQuery(context, select2);
        
        DocumentoVenta dv = context.newObject(DocumentoVenta.class);    
        dv.setToSolicitudVenta(sv);
        dv.setToTipoDocumento(td);
        dv.setContacto(d.getContacto());
        dv.setDireccion(d.getDireccion());
        dv.setDni(d.getDni());
        dv.setFechaemision(fechaActual);
        dv.setMontototal(d.getMontototal());
        dv.setFechafacturacion(d.getFechafacturacion());
        dv.setIgv(d.getIgv());
        dv.setRetencion(d.getRetencion());
        dv.setPercepcion(d.getPercepcion());
        dv.setDetraccion(d.getDetraccion());
        dv.setDescuento(d.getDescuento());
        dv.setTotal(d.getTotal());
        dv.setEstado(state.getAceptado());
        
        context.commitChanges();
        
        ObjectContext context2 = DataContext.createDataContext();       
        String cadena = "UPDATE SOLICITUD_VENTA SET ESTADO = "+state.getAceptado()
                +" WHERE IDSOLICITUDVENTA="+d.getToSolicitudVenta().getIdsolicitudventa();
        SQLTemplate query = new SQLTemplate(SolicitudVenta.class,cadena);
        context2.performQuery(query);
        context2.commitChanges();
        
        ObjectContext context3 = DataContext.createDataContext();       
        String cadena2 = "UPDATE DETALLE_SOLICITUD_VENTA SET ESTADO = "+state.getAceptado()
                +" WHERE IDSOLICITUDVENTA="+d.getToSolicitudVenta().getIdsolicitudventa();
        SQLTemplate query2 = new SQLTemplate(DetalleSolicitudVenta.class,cadena2);
        context3.performQuery(query2);
        context3.commitChanges();
        
        if(idNota>0){
        ObjectContext context5 = DataContext.createDataContext();       
        String cadena5 = "UPDATE NOTA_CREDITO SET ESTADO = "+state.getEnUso()
                +" WHERE IDNOTACREDITO="+idNota;
        SQLTemplate query5 = new SQLTemplate(NotaCredito.class,cadena5);
        context5.performQuery(query5);
        context5.commitChanges();
        }
        
        for(int i=0;i<dDv.size();i++){
            System.out.println("Voy a registrar el detalle");
            ObjectContext context4 = DataContext.createDataContext();    
            Expression qualifier4 = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, dDv.get(i).getToProducto().getIdproducto());
            SelectQuery select4 = new SelectQuery(Producto.class, qualifier4);
            Producto p = (Producto) DataObjectUtils.objectForQuery(context4, select4);
            
            Expression qualifier5 = ExpressionFactory.matchExp(DocumentoVenta.IDDOCUMENTO_PK_COLUMN, dv.getIddocumento());
            SelectQuery select5 = new SelectQuery(DocumentoVenta.class, qualifier5);
            DocumentoVenta d0 = (DocumentoVenta) DataObjectUtils.objectForQuery(context4, select5);
            
            DetalleDocumentoVenta ddv = context4.newObject(DetalleDocumentoVenta.class);
            ddv.setCantidad(dDv.get(i).getCantidad());
            ddv.setMonto(dDv.get(i).getMonto());//se guarda el precio unitario de venta
            ddv.setEstado(state.getAceptado());
            
            ddv.setToProducto(p);
            ddv.setToDocumentoVenta(d0);
            
            AlmacenService servA=new AlmacenService();
            servA.movimientoAlmacen(d0.getIddocumento(),p.getIdproducto(),4,ddv.getCantidad());
            
            context4.commitChanges();
            
        }
        }catch(Exception e){
        System.out.println("Ha ocurrido un error al guardar");
        }
        
    }
    
    public List<Producto> listProduct(){
     ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM PRODUCTO WHERE IDTIPOPRODUCTO=3";
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);
        
        List<Producto> productos = context.performQuery(query);       
        return productos;
        
    }
    
    public List<DocumentoVenta> listVentaDistribuidor(Integer idDistribuidor, Date fechaIni,Date fechaFin){
     ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT A.* FROM DOCUMENTO_VENTA A, SOLICITUD_VENTA B, DISTRIBUIDOR C WHERE"
                +" A.FECHAFACTURACION>='"+fechaIni+"' AND A.FECHAFACTURACION<'"+fechaFin+"' AND "
                + " A.IDSOLICITUDVENTA=B.IDSOLICITUDVENTA AND B.IDDISTRIBUIDOR=C.IDDISTRIBUIDOR AND "
                + "C.IDDISTRIBUIDOR="+idDistribuidor+" AND C.ESTADO>0 AND A.ESTADO=7";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        System.out.println(cadena);
        List<DocumentoVenta> ventas = (ArrayList<DocumentoVenta>)context.performQuery(query);       
        return ventas;
        
    }
    
    public List<DocumentoVenta> listVentaBoleta(Date fechaIni,Date fechaFin){
     ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT A.* FROM DOCUMENTO_VENTA A WHERE"
                +" A.FECHAFACTURACION>='"+fechaIni+"' AND A.FECHAFACTURACION<'"+fechaFin+"' AND A.ESTADO=7 AND A.IDTIPODOCUMENTO=2";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        System.out.println(cadena);
        List<DocumentoVenta> ventas = (ArrayList<DocumentoVenta>)context.performQuery(query);       
        return ventas;
        
    }
    
    public String numeroFactura(){
    ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DOCUMENTO_VENTA WHERE IDTIPODOCUMENTO=1";
        cadena+=" order by IDDOCUMENTO ASC";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        
        List<DocumentoVenta> documentos = context.performQuery(query);       
        String cadena2;
        cadena2="0";
        //cantidad de 0s= 7
                if(documentos.size()>=0 && documentos.size()<10) cadena2="000000"+(documentos.size()+1);
                if(documentos.size()>=10 && documentos.size()<100) cadena2="00000"+(documentos.size()+1);
                if(documentos.size()>=100 && documentos.size()<1000) cadena2="0000"+(documentos.size()+1);
                if(documentos.size()>=1000 && documentos.size()<10000) cadena2="000"+(documentos.size()+1);
                if(documentos.size()>=10000 && documentos.size()<100000) cadena2="00"+(documentos.size()+1);
                if(documentos.size()>=100000 && documentos.size()<1000000) cadena2="0"+(documentos.size()+1);
                if(documentos.size()>=1000000 && documentos.size()<10000000) cadena2=""+(documentos.size()+1);
                
        return cadena2;
    }
    
    public String numeroBoleta(){
    ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DOCUMENTO_VENTA WHERE IDTIPODOCUMENTO=2";
        cadena+=" order by IDDOCUMENTO ASC";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        
        List<DocumentoVenta> documentos= context.performQuery(query);       
        String cadena2;
        cadena2="0";
        //cantidad de 0s= 7
                if(documentos.size()>=0 && documentos.size()<10) cadena2="000000"+(documentos.size()+1);
                if(documentos.size()>=10 && documentos.size()<100) cadena2="00000"+(documentos.size()+1);
                if(documentos.size()>=100 && documentos.size()<1000) cadena2="0000"+(documentos.size()+1);
                if(documentos.size()>=1000 && documentos.size()<10000) cadena2="000"+(documentos.size()+1);
                if(documentos.size()>=10000 && documentos.size()<100000) cadena2="00"+(documentos.size()+1);
                if(documentos.size()>=100000 && documentos.size()<1000000) cadena2="0"+(documentos.size()+1);
                if(documentos.size()>=1000000 && documentos.size()<10000000) cadena2=""+(documentos.size()+1);
                
        return cadena2;
    }
    
    public String numeroBoletaView(Integer id){
        if(id==0) return "";
    ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DOCUMENTO_VENTA WHERE IDTIPODOCUMENTO=2 AND IDDOCUMENTO<="+id+" order by IDDOCUMENTO ASC";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        
        List<DocumentoVenta> documentos= context.performQuery(query);       
        String cadena2;
        cadena2="0";
        //cantidad de 0s= 7
                if(documentos.size()>=0 && documentos.size()<10) cadena2="000000"+documentos.size();
                if(documentos.size()>=10 && documentos.size()<100) cadena2="00000"+documentos.size();
                if(documentos.size()>=100 && documentos.size()<1000) cadena2="0000"+documentos.size();
                if(documentos.size()>=1000 && documentos.size()<10000) cadena2="000"+documentos.size();
                if(documentos.size()>=10000 && documentos.size()<100000) cadena2="00"+documentos.size();
                if(documentos.size()>=100000 && documentos.size()<1000000) cadena2="0"+documentos.size();
                if(documentos.size()>=1000000 && documentos.size()<10000000) cadena2=""+documentos.size();
                
        return cadena2;
    }
    
    public String numeroFacturaView(Integer id){
        if(id==0) return "";
    ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DOCUMENTO_VENTA WHERE IDTIPODOCUMENTO=1 AND IDDOCUMENTO<="+id+" order by IDDOCUMENTO ASC";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        
        List<DocumentoVenta> documentos= context.performQuery(query);
        
        String cadena2;
        cadena2="0";
        //cantidad de 0s= 7
                if(documentos.size()>=0 && documentos.size()<10) cadena2="000000"+documentos.size();
                if(documentos.size()>=10 && documentos.size()<100) cadena2="00000"+documentos.size();
                if(documentos.size()>=100 && documentos.size()<1000) cadena2="0000"+documentos.size();
                if(documentos.size()>=1000 && documentos.size()<10000) cadena2="000"+documentos.size();
                if(documentos.size()>=10000 && documentos.size()<100000) cadena2="00"+documentos.size();
                if(documentos.size()>=100000 && documentos.size()<1000000) cadena2="0"+documentos.size();
                if(documentos.size()>=1000000 && documentos.size()<10000000) cadena2=""+documentos.size();
                
        return cadena2;
    }
    
    
    public void  editar (String estado, int selectedIndex, int idemp){
        //no se edita una factura o boleta, es una transaccion
    }
    
    public void eliminar(int idDocumento){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE DOCUMENTO_VENTA SET ESTADO=0"+ 
                        " WHERE IDDOCUMENTO="+idDocumento+"";
        SQLTemplate query = new SQLTemplate(DocumentoVenta.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        
        ObjectContext context2 = DataContext.createDataContext();
        String cadena2 = "UPDATE DETALLE_DOCUMENTO_VENTA SET ESTADO=0"+ 
                        " WHERE IDDOCUMENTO="+idDocumento+"";
        SQLTemplate query2 = new SQLTemplate(DetalleDocumentoVenta.class,cadena2);
        
        context2.performQuery(query2);
        context2.commitChanges();
        
    }
    
    public void eliminarItemDetalle(int idDetalleDocumento){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE DETALLE_DOCUMENTO_VENTA SET ESTADO=0"+ 
                        " WHERE IDDOCUMENTO="+idDetalleDocumento+"";
        SQLTemplate query = new SQLTemplate(DetalleDocumentoVenta.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        
        
    }
    
}
