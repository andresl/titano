/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.TipoMovimiento;
import dp1.titandevelop.titano.view.LoginT;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Yulian
 */
public class TipoMovimientoService {
    
    public TipoMovimientoService(){
        
    }
    public void insertar(String descripcion) {
        ObjectContext context = DataContext.createDataContext();
        
        TipoMovimiento tm = context.newObject(TipoMovimiento.class);
        tm.setDescripcion(descripcion);
        tm.setEstado(1);
        context.commitChanges();        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Tipo Movimiento");
    }
    
    public void editar(String descripcion,int idTipo){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE TIPO_MOVIMIENTO SET DESCRIPCION = '"+descripcion+"' "+
                        "WHERE IDTIPOMOVIMIENTO="+idTipo+"";
        SQLTemplate query = new SQLTemplate(TipoMovimiento.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Tipo Movimiento");
    }
    public int eliminar(int idTipo){
        ObjectContext context = DataContext.createDataContext();
        KardexService serv = new KardexService();
        if(!serv.tmUtizado(context, idTipo)){
            String cadena = "UPDATE TIPO_MOVIMIENTO SET ESTADO =0 "+ 
                            "WHERE IDTIPOMOVIMIENTO="+idTipo+"";
            SQLTemplate query = new SQLTemplate(TipoMovimiento.class,cadena);

            context.performQuery(query);
            context.commitChanges();
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Tipo Movimiento");
            return 1;
        }
        else
            return 0;
    }
    
    public TipoMovimiento buscarPorId (ObjectContext c, int id ) {
        Expression q = ExpressionFactory.matchExp(TipoMovimiento.IDTIPOMOVIMIENTO_PK_COLUMN, id);
        Expression q1 = ExpressionFactory.greaterExp(TipoMovimiento.ESTADO_PROPERTY, 0);
        Expression q2 = q.andExp(q1);
        SelectQuery s = new SelectQuery(TipoMovimiento.class, q2);
        return (TipoMovimiento) DataObjectUtils.objectForQuery(c, s);
    }
    public List<TipoMovimiento> buscar() {
        ObjectContext context = DataContext.createDataContext();
        TipoMovimiento tm = context.newObject(TipoMovimiento.class);
        SelectQuery consulta = new SelectQuery(TipoMovimiento.class);
        
        List<TipoMovimiento> tipos = context.performQuery(consulta);
        
        return tipos;
    }
    
    public List<TipoMovimiento> buscarFiltrado(String text) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TIPO_MOVIMIENTO WHERE DESCRIPCION LIKE '%"+text+"%' AND ESTADO = 1";
        SQLTemplate query = new SQLTemplate(TipoMovimiento.class,cadena);
        
        List<TipoMovimiento> tipos = context.performQuery(query);       
        return tipos;
    }
    
    public boolean verificaTipoMov(String tipoMov,String cad){
        boolean val=false;
        if(cad.compareTo("Entrada")==0){
            if(tipoMov.compareTo("Entrada Interna")==0||tipoMov.compareTo("Entrada Externa")==0||tipoMov.compareTo("Entrada por Devolucion")==0)
                val=true;
        }
        else{
            if(tipoMov.compareTo("Salida Interna")==0||tipoMov.compareTo("Salida Externa")==0)
                val=true;
        }
        return val; 
    }
    public boolean verificaTipoMov(int idTmov,String cad){
        
        ObjectContext c = DataContext.createDataContext();
        TipoMovimiento tm=this.buscarPorId(c, idTmov);
        return this.verificaTipoMov(tm.getDescripcion(), cad);
    }
}
