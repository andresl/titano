/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Cotizacion;
import dp1.titandevelop.titano.persistent.DetalleCotizacion;
import dp1.titandevelop.titano.persistent.Proveedor;
import dp1.titandevelop.titano.persistent.RequerimientoXProducto;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.Date;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.exp.ExpressionParameter;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author andres
 */
public class CotizacionService {
    public void insertarCotizacion(int idProveedor, Date fecha, float precioTotal, ArrayList<RequerimientoXProducto> listaRxp, ArrayList<Float> listaPrec) {
        ObjectContext context = listaRxp.get(0).getObjectContext();
        ProveedorService ps = new ProveedorService();
        Proveedor p = ps.buscarPorId(context, idProveedor);
        Cotizacion cot = context.newObject(Cotizacion.class);
        cot.setEstado(1);
        cot.setPreciototal(precioTotal);
        cot.setFecharecepcion(fecha);
        cot.setFecharegistro(new Date());
        cot.setToProveedor(p);
        OrdenCompraService ocs = new OrdenCompraService();
        cot.setToOrdenCompra(ocs.buscarPorId(context, 0));
        for (int i=0; i<listaRxp.size(); i++) {
            if ( listaRxp.get(i).isSeleccionado() ) {
                // listaRxp.get(i).setObjectContext(context);
                DetalleCotizacion dc = context.newObject(DetalleCotizacion.class);
                dc.setCantidad(Math.round(listaRxp.get(i).getCantidad()));
                dc.setToRequerimientoXProducto(listaRxp.get(i));
                dc.setEstado(1);
                dc.setPrecio(listaPrec.get(i));
                cot.addToDetalleCotizacionArray(dc);
            }
            
            
        }
       
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Cotizacion");
    }
    
    public Cotizacion buscarPorId(ObjectContext context, int idCotizacion) {
        if ( context == null) {
            context = DataContext.createDataContext();
        }
        Expression q = ExpressionFactory.matchExp(Cotizacion.IDCOTIZACION_PK_COLUMN, idCotizacion);
        SelectQuery s = new SelectQuery(Cotizacion.class, q);
        return (Cotizacion) DataObjectUtils.objectForQuery(context, s);
    }

    public void actualizarCotizacion(Cotizacion cotizacion) {
        ObjectContext context = cotizacion.getObjectContext();
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Cotizacion");

    }
    
    public ArrayList<Cotizacion> buscarTodos(ObjectContext context) {
        if ( context == null) {
            context = DataContext.createDataContext();
        }
        ArrayList<Cotizacion> l = new ArrayList<Cotizacion>();
        Expression q = ExpressionFactory.greaterExp(Cotizacion.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Cotizacion.class, q);
        l.addAll(context.performQuery(s));
        return l;
    }
    
    public ArrayList<Cotizacion> buscarPor(int idProveedor, Date fechaReg, Date fechaRec) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado ();
        Expression exp = ExpressionFactory.matchExp("toProveedor.idproveedor", idProveedor);
        exp.orExp(ExpressionFactory.matchExp(Cotizacion.FECHAREGISTRO_PROPERTY, fechaReg));
        exp.orExp(ExpressionFactory.matchExp(Cotizacion.FECHARECEPCION_PROPERTY, fechaRec));
        exp.orExp(ExpressionFactory.greaterDbExp(Cotizacion.ESTADO_PROPERTY, est.getEliminado()));

        SelectQuery s = new SelectQuery(Cotizacion.class, exp);
        ArrayList<Cotizacion> l = new ArrayList<Cotizacion>();
        l.addAll(context.performQuery(s));

        return l;

    }
    public ArrayList<Cotizacion> buscarPor(int idProveedor) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado ();
        Expression exp = ExpressionFactory.matchExp("toProveedor.idproveedor", idProveedor);
        exp.orExp(ExpressionFactory.greaterDbExp(Cotizacion.ESTADO_PROPERTY, est.getEliminado()));

        SelectQuery s = new SelectQuery(Cotizacion.class, exp);
        ArrayList<Cotizacion> l = new ArrayList<Cotizacion>();
        l.addAll(context.performQuery(s));

        return l;

    }
    
    public ArrayList<Cotizacion> buscarPor(Date fechaReg, Date fechaRec) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado ();
        Expression exp = ExpressionFactory.matchExp(Cotizacion.FECHAREGISTRO_PROPERTY, fechaReg);
        exp.orExp(ExpressionFactory.matchExp(Cotizacion.FECHARECEPCION_PROPERTY, fechaRec));
        exp.orExp(ExpressionFactory.greaterDbExp(Cotizacion.ESTADO_PROPERTY, est.getEliminado()));

        SelectQuery s = new SelectQuery(Cotizacion.class, exp);
        ArrayList<Cotizacion> l = new ArrayList<Cotizacion>();
        l.addAll(context.performQuery(s));

        return l;

    }

   
}
