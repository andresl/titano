/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Usuario;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 *
 * @author cHreS
 */
public class CorreoService {
    
    public void enviarCorreo(String correo, String usuario){
     Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        String passwordreset="password";
           
    
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("soportetitano@gmail.com", "qwertY12345");
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("soportetitano@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(correo));
            message.setSubject("Resetear password");
            message.setText("Hola, te estamos enviando este password temporal, por favor ingresa un nuevo password al iniciar sesion, gracias "
                    + "\n Password:"+passwordreset+"");
            Transport.send(message);
             JOptionPane.showMessageDialog(null, "Se ha enviado un password temporal al correo del usuario ingresado",
                    "Resetear Password",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error al enviar el correo",
                    "Error Resetear Password",
                    JOptionPane.ERROR_MESSAGE);
        }
}
}
