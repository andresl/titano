/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Cotizacion;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.ProductoProveedor;
import dp1.titandevelop.titano.persistent.Proveedor;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Usuario1
 */
public class ProveedorService {
    public int insertar(String razon, String ruc,String direcc,int calif, String marca, String contacto, String dni, String celcont, String mailemp, String mailcont, String telf) {
        ObjectContext context = DataContext.createDataContext();
        Proveedor p = context.newObject(Proveedor.class);
        Estado est = new Estado();
        p.setRazonsocial(razon);
        p.setRuc(ruc);
        p.setDireccionempresa(direcc);
        p.setMarca(marca);
//        p.setCalificaciontiempoentrega(Short.parseShort(calif));
        p.setCalificaciontiempoentrega(calif);
        p.setContacto(contacto);
        p.setDnicontacto(dni);
        p.setCelularcontacto(celcont);
        p.setEmailempresa(mailemp);
        p.setEmailcontacto(mailcont);
        p.setTelefonocontacto(telf);
        p.setEstado(est.getActivo());
        
        
        context.commitChanges(); 
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Proveedor");
        return p.getIdproveedor();
            
    }
    public int insertar_masivo(int idproveedor,String razon, String ruc,String direcc,int calif, String marca, String contacto, String dni, String celcont, String mailemp, String mailcont, String telf) {
        ObjectContext context = DataContext.createDataContext();
        Proveedor p = context.newObject(Proveedor.class);
        Estado est = new Estado();
        p.setIdproveedor(idproveedor);
        p.setRazonsocial(razon);
        p.setRuc(ruc);
        p.setDireccionempresa(direcc);
        p.setMarca(marca);
//        p.setCalificaciontiempoentrega(Short.parseShort(calif));
        p.setCalificaciontiempoentrega(calif);
        p.setContacto(contacto);
        p.setDnicontacto(dni);
        p.setCelularcontacto(celcont);
        p.setEmailempresa(mailemp);
        p.setEmailcontacto(mailcont);
        p.setTelefonocontacto(telf);
        p.setEstado(est.getActivo());
        
        
        context.commitChanges(); 
        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Proveedor Masivo");
        return p.getIdproveedor();
            
    }
    
    public void insertarProdAProveed(int idprov, List<Integer> idproductos, List<Float> precios) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        Expression q = ExpressionFactory.matchExp(Proveedor.IDPROVEEDOR_PK_COLUMN, idprov);
        SelectQuery s = new SelectQuery(Proveedor.class, q);
        Proveedor p =  (Proveedor) DataObjectUtils.objectForQuery(context, s);
        
        //adjuntando datos de los productos del proveedor
          int cont=0;
            while (cont<precios.size())
            {   ProductoProveedor prod = context.newObject(ProductoProveedor.class);

            Expression qualifiers = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PROPERTY,String.valueOf(idproductos.get(cont)));
            SelectQuery selects = new SelectQuery(Producto.class, qualifiers);
            Producto producto = (Producto)DataObjectUtils.objectForQuery(context, selects);
            
            prod.setToProducto(producto);
            prod.setToProveedor(p);
            prod.setPrecio(precios.get(cont));
            prod.setEstado(est.getActivo());
            prod.setCalificacioncalidad(Short.parseShort("5"));
            context.commitChanges(); 
            cont++;
        } 
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "ProductoProveedor");
            
    }    
    public ArrayList<Proveedor> buscarPorRazonSocial (ObjectContext context, String razonSocial) {
        if ( context == null ) 
            context = DataContext.createDataContext();
        
         
        Expression exp = ExpressionFactory.likeIgnoreCaseExp(Proveedor.RAZONSOCIAL_PROPERTY, "%"+razonSocial+"%");
        exp = exp.andExp(ExpressionFactory.greaterExp(Proveedor.ESTADO_PROPERTY, 0));
        SelectQuery s = new SelectQuery(Proveedor.class, exp);
        return (ArrayList<Proveedor>) context.performQuery(s);
        
    }

    public List<Cotizacion> buscarProveedorxCotizacion (int idprov) {
           ObjectContext context = DataContext.createDataContext(); 
           Estado est = new Estado();
           String cadena = "select * from Cotizacion "
                    + " where idProveedor = "+idprov+""
                    + " and estado <> "+est.getEliminado()+"";
            SQLTemplate query = new SQLTemplate(Proveedor.class,cadena);         
            List<Cotizacion> tipos = context.performQuery(query);
            return tipos;
        
    }    
    public ArrayList<Proveedor> buscarTodos (ObjectContext context) {
        if ( context == null ) 
            context = DataContext.createDataContext();
        
        Expression exp = ExpressionFactory.greaterExp(Proveedor.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Proveedor.class, exp);
        return (ArrayList<Proveedor>) context.performQuery(s);
        
    }    
    public ArrayList<Proveedor> buscarTodos () {
       ObjectContext context = DataContext.createDataContext();
        if ( context == null ) 
            context = DataContext.createDataContext();
        
        Expression exp = ExpressionFactory.greaterExp(Proveedor.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Proveedor.class, exp);
        return (ArrayList<Proveedor>) context.performQuery(s);
        
    }    
    public List<Proveedor> buscarProveedor(String razon, String ruc) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        String cadena = "select * from Proveedor "
                + " where razonsocial like '%"+razon+"%' "
                + " and ruc like '%"+ruc+"%'"
                + " and estado = "+est.getActivo()+"";
        SQLTemplate query = new SQLTemplate(Proveedor.class,cadena);         
        List<Proveedor> tipos = context.performQuery(query);
        
        return tipos;
    }
    public List<Proveedor> buscarRUC(String ruc) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        String cadena = "select * from Proveedor "
                + " where ruc = '"+ruc+"'"
                + " and estado = "+est.getActivo()+"";
        SQLTemplate query = new SQLTemplate(Proveedor.class,cadena);         
        List<Proveedor> tipos = context.performQuery(query);      
        return tipos;
    }
    public List<Proveedor> buscarProveedor(String razon, String ruc,  int idProd) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        String cadena = "select * from Proveedor where idProveedor in "
                + "(select p.idProveedor"
                + " from Proveedor a, Producto_Proveedor p"
                + " where a.idProveedor = p.idProveedor and a.razonsocial like '%"+razon+"%' "
                + " and a.ruc like '%"+ruc+"%'"
                + " and p.idProducto = "+idProd+" and p.estado <> "+est.getEliminado()+" and a.estado  <> "+est.getEliminado()+")";
        SQLTemplate query = new SQLTemplate(Proveedor.class,cadena);         
        List<Proveedor> tipos = context.performQuery(query);
        
        return tipos;
    }
     public Proveedor buscarPorId(ObjectContext c, int id) {
        if ( c == null) {
            c = DataContext.createDataContext();
        }
        Expression q = ExpressionFactory.matchExp(Proveedor.IDPROVEEDOR_PK_COLUMN, id);
        SelectQuery s = new SelectQuery(Proveedor.class, q);
        return (Proveedor) DataObjectUtils.objectForQuery(c, s);
    }     
    public void eliminarProveedor(int idProv){
        ObjectContext context = DataContext.createDataContext();
        
        ProductoProveedorService serv = new ProductoProveedorService();
        serv.eliminarPorIdProv(context,idProv);//Elimina todos los elementos de la tabla intermedia productoxproveedor 
        
        Proveedor e = this.buscarPorId(context,idProv);
        e.setEstado(0);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Proveedor");
    }    
     public boolean seEncuentra(int id,List<ProductoProveedor> lista ){
        boolean aux=false;
        for(int i=0;i<lista.size();i++){
            if (id == lista.get(i).getToProducto().getIdproducto()){
                aux = true;break;
            }
        }
        return aux;
    }    
     public void verificaYelimina(ObjectContext context,List<ProductoProveedor> pasados ,List<Integer> actuales){
        boolean esta=false;
        ProductoProveedorService serv = new ProductoProveedorService();
        int id; 
        for(int i=0;i<pasados.size();i++){
            for(int j=0;j<actuales.size();j++){
                id=pasados.get(i).getToProducto().getIdproducto();
                if (id==actuales.get(j)){
                    esta=true;break;
                }
            }
            if(!esta){//Si no esta en la lista actual elimino
                serv.eliminarPorIdPxP(context,pasados.get(i).getIdproductoproveedor());
            }          
            esta=false;
        }
    }    
    public void editar(int idProv, String razon, String ruc,String direcc,int calif, String marca,  String contacto, String dni, String celcont, String mailemp, String mailcont, String telf, List<Integer> idproductos, List<Float> precios) {
        
        ObjectContext context = DataContext.createDataContext();
        Proveedor p = this.buscarPorId(context, idProv);     
        
        p.setRazonsocial(razon);
        p.setRuc(ruc);
        p.setDireccionempresa(direcc);
        p.setMarca(marca);
        p.setCalificaciontiempoentrega(calif);
        p.setContacto(contacto);
        p.setDnicontacto(dni);
        p.setCelularcontacto(celcont);
        p.setEmailempresa(mailemp);
        p.setEmailcontacto(mailcont);
        p.setTelefonocontacto(telf);
        
        //adjuntando datos de los productos del proveedor
        ProductoProveedorService serv = new ProductoProveedorService();
        List<ProductoProveedor> lista = serv.buscarPorIdProdProv(context, idProv);//Lista en la base de datos
        Estado est = new Estado();
        //serv.eliminarPorIdAlm(context, idAlm);
        int cont=0;
        //Verificar la lista nueva
        while (cont < idproductos.size()){
            ProductoProveedor pr;
            
            Expression qualifiers = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PROPERTY,String.valueOf(idproductos.get(cont)));
            SelectQuery selects = new SelectQuery(Producto.class, qualifiers);
            Producto prod = (Producto)DataObjectUtils.objectForQuery(context, selects);
            
            if(seEncuentra(idproductos.get(cont),lista)){//Se edita                
                pr = lista.get(cont);
            }
            else{//se crea                
                pr = context.newObject(ProductoProveedor.class);    
            }
            
            pr.setToProducto(prod);
            pr.setToProveedor(p);
            pr.setPrecio(precios.get(cont));
            pr.setEstado(est.getActivo());
            
            context.commitChanges(); 
            cont++;
        }
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Proveedor");
        this.verificaYelimina(context,lista, idproductos);   
    }
    
}
