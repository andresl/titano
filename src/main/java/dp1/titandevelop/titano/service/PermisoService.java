/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Perfil;
import dp1.titandevelop.titano.persistent.Permiso;
import dp1.titandevelop.titano.persistent.PermisoXPerfil;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author christianjuarez
 */
public class PermisoService {
    
     public List<Permiso> buscar() {
           ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM PERMISO ORDER BY MODULO";
        SQLTemplate query = new SQLTemplate(Permiso.class,cadena);
        
        List<Permiso> tipos = context.performQuery(query);       
        return tipos;
         
      
    }
     
      public ArrayList<Permiso> buscarPermisosActivos(Integer idPerfil) {
          
          PerfilService perfilService = new PerfilService();
         Perfil perfilEncontrado=perfilService.buscarPerfilporID(idPerfil);
          ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT *FROM PERMISO WHERE IDPERMISO IN (SELECT IDPERMISO FROM PERMISO_X_PERFIL WHERE IDPERFIL="+perfilEncontrado.getIdperfil()+")";
        SQLTemplate query = new SQLTemplate(Permiso.class,cadena);        
        ArrayList<Permiso> tipos = (ArrayList<Permiso>)context.performQuery(query);       
        return tipos;
         
      
    }
      
       public ArrayList<Permiso> buscarPermisosDistintos(Integer idPerfil) {
    
        
        PerfilService perfilService = new PerfilService();
         Perfil perfilEncontrado=perfilService.buscarPerfilporID(idPerfil);
          ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT *FROM PERMISO WHERE IDPERMISO NOT IN (SELECT IDPERMISO FROM PERMISO_X_PERFIL WHERE IDPERFIL="+perfilEncontrado.getIdperfil()+")";
        SQLTemplate query = new SQLTemplate(Permiso.class,cadena);        
        ArrayList<Permiso> tipos = (ArrayList<Permiso>)context.performQuery(query);       
        return tipos;
         
      
    }
    
}
