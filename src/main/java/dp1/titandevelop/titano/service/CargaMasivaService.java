/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.DetalleSolicitudVenta;
import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.Receta;
import dp1.titandevelop.titano.persistent.SolicitudVenta;
import dp1.titandevelop.titano.persistent.TipoDocumento;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author cHreS
 */
public class CargaMasivaService {

    public void carga_masiva_empleado(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Worksheet 0 is Empelado
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            String nombres = null, apepat = null, apemat = null, direcc = null, dni = null, sexo = null, tlf = null, cel = null, mail = null;
            Date fecha = null;
            Integer idtipo = null;
            EmpleadoService emp = new EmpleadoService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 11: //Tipo empleado
                                    {
                                        idtipo = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }

                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 1: //Nombre
                                    {
                                        nombres = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //Apellido Paterno
                                    {
                                        apepat = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 3: //Apellido Materno
                                    {
                                        apemat = cell.getStringCellValue();

                                        columna++;
                                        break;
                                    }
                                    case 4: //Fecha Nacimiento
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fecha = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }
                                    case 5: //Sexo
                                    {
                                        sexo = cell.getStringCellValue();
                                        // System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 6: //Direccion
                                    {
                                        direcc = cell.getStringCellValue();
                                        //   System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 10: //Correo
                                    {
                                        mail = cell.getStringCellValue();
                                        //  System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 7: //DNI
                                    {
                                        dni = cell.getStringCellValue();;//System.out.print(cell.getNumericCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 8: //Telefono
                                    // System.out.print(cell.getNumericCellValue() + "\t");
                                    {
                                        tlf = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 9: //Celular
                                    {
                                        cel = cell.getStringCellValue();
                                        // System.out.print(cell.getNumericCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }
                    }
                }
                try {
                    emp.insertarEmpleado(idtipo, nombres, apepat, apemat, direcc, fecha, dni, sexo, tlf, cel, mail);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_maquina(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 1 is Maquina
            XSSFSheet sheet = workbook.getSheetAt(1);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            String kw = null;
            String descripcion = null;
            Integer idproceso = null;
            MaquinaService maqserv = new MaquinaService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //IDProceso
                                    {
                                        idproceso = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }


                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 2: //desripcion
                                    {
                                        descripcion = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    // System.out.print(cell.getStringCellValue() + "\t");
                                    case 3: //KW
                                    {
                                        kw = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }


                                }
                                break;
                            }
                    }
                }
                try {
                    maqserv.insertarMaquina(idproceso, descripcion, kw);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_producto(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 2 is "Producto"
            XSSFSheet sheet = workbook.getSheetAt(2);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            Integer idProducto = null;
            String tipoProducto = null;
            String descripcion = null;
            Integer stockMin = null;
            Integer stockMax = null;
            Integer loteMin = null;
            Integer loteMax = null;
            Integer diasVenc = null;
            float beneficio = 0;
            float costo = 0;
            String UM = null;
            Integer idProc = null;
            float peso = 0;
            ProductoService proserv = new ProductoService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {
                                    case 1: //IDProducto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 4: //Stock minimo
                                    {
                                        stockMin = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 5: //Stock Maximo
                                    {
                                        stockMax = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 6: //Lote Minimo
                                    {
                                        loteMin = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 7: //Lote Maximo
                                    {
                                        loteMax = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 8: //Dias Vencimiento
                                    {
                                        diasVenc = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 9: //Beneficio
                                    {
                                        beneficio = (float) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 10: //Costo
                                    {
                                        costo = (float) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 12: // Pesoo
                                    {
                                        peso = (float) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 13: //idProceso
                                    {
                                        idProc = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 2: //Insumo
                                    {
                                        tipoProducto = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }

                                    case 3: //Descripcion de Producto
                                    {
                                        descripcion = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 11: //Unidad de Medida
                                    {
                                        UM = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }

                    }
                }
                try {
                    proserv.insertarmasivo(idProducto, tipoProducto, descripcion, stockMin, stockMax, loteMin, loteMax, diasVenc, beneficio, costo, UM, idProc, peso);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_receta(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 3 is Receta
            XSSFSheet sheet = workbook.getSheetAt(3);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            String kw = null;


            int idProd = 0;
            int idMat = 0;
            float cantidad = 0;

            ProductoService ps = new ProductoService();

            RecetaService rs = new RecetaService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                Producto pro;
                List<Receta> lista = new ArrayList<Receta>();
                Receta r = new Receta();
                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();



                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //IDProducto
                                    {
                                        idProd = (int) cell.getNumericCellValue();
//                                        Producto producto = ps.buscarPorId(idProd);
//                                         r.setToProducto(producto);
//                                        r.setUm(producto.getUm());
                                        columna++;
                                        break;
                                    }
                                    case 2: //idMaterial
                                    {
                                        idMat = (int) cell.getNumericCellValue();
                                        Producto p = ps.buscarPorId(idMat);
                                        r.setToProducto1(p);
                                        r.setUm(p.getUm());
                                        Estado e = new Estado();
                                        r.setEstado(e.getActivo());
                                        columna++;
                                        break;
                                    }
                                    case 3: //cantidad
                                    {
                                        cantidad = (float) cell.getNumericCellValue();
                                        r.setCantidad(cantidad);
                                        lista.add(r);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                    }
                }
                try {
                    rs.Insertar(idProd, lista);

                } catch (Exception e) {
                    System.out.println("No insertaste nada");
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_productoxproceso(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 4 ProductoXProceso
            XSSFSheet sheet = workbook.getSheetAt(4);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;

            Integer idproceso = 0;
            int orden = 0;
            int tipoMov = 0;
            Integer idproducto = 0;
            Integer idmaterial = 0;

            ProductoXProcesoService spp = new ProductoXProcesoService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    //   cell.getStringCellValue();
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //IDProceso
                                    {
                                        idproceso = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //Orden
                                    {
                                        orden = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 3: //TipoMovimiento
                                    {
                                        tipoMov = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 4: //IdProducto
                                    {
                                        idproducto = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 5: //idMaterial
                                    {
                                        idmaterial = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }



                    }
                }
                try {

                    spp.Inserta(idproceso, orden, tipoMov, idproducto, idmaterial);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
//             JOptionPane.showMessageDialog(this, "Se realizó la carga masiva correctamente ",
//                    "Carga Correcta",
//                    JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_operario(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Worksheet 5 is Operarop
            XSSFSheet sheet = workbook.getSheetAt(5);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            String nombres = null, apepat = null, apemat = null, direcc = null, dni = null, sexo = null, tlf = null, cel = null, mail = null;
            Date fecha = null;
            Integer idtipo = null;
            int idOperario = 0;
            EmpleadoService emp = new EmpleadoService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idOperario
                                    {
                                        idOperario = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 12: //Tipo empleado
                                    {
                                        idtipo = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }

                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 2: //Nombre
                                    {
                                        nombres = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 3: //Apellido Paterno
                                    {
                                        apepat = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 4: //Apellido Materno
                                    {
                                        apemat = cell.getStringCellValue();

                                        columna++;
                                        break;
                                    }
                                    case 5: //Fecha Nacimiento
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fecha = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }
                                    case 6: //Sexo
                                    {
                                        sexo = cell.getStringCellValue();
                                        // System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 7: //Direccion
                                    {
                                        direcc = cell.getStringCellValue();
                                        //   System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 11: //Correo
                                    {
                                        mail = cell.getStringCellValue();
                                        //  System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 8: //DNI
                                    {
                                        dni = cell.getStringCellValue();;//System.out.print(cell.getNumericCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 9: //Telefono
                                    // System.out.print(cell.getNumericCellValue() + "\t");
                                    {
                                        tlf = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 10: //Celular
                                    {
                                        cel = cell.getStringCellValue();
                                        // System.out.print(cell.getNumericCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }
                    }
                }
                try {
                    emp.insertarOperarioMasivo(idOperario, idtipo, nombres, apepat, apemat, direcc, fecha, dni, sexo, tlf, cel, mail);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_operarioxprocesoturno(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Worksheet 6 Empleado x proceso Turno
            XSSFSheet sheet = workbook.getSheetAt(6);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            String nombres = null, apepat = null, apemat = null, direcc = null, dni = null, sexo = null, tlf = null, cel = null, mail = null;
            Date fecha = null;
            Integer idtipo = null;
            int idOperario = 0;
            int idEmpleado = 0;
            int idTurno = 0;
            int idProceso = 0;
            float productividad = 0;
            float rotura = 0;


            EmpleadoService emp = new EmpleadoService();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns

                ArrayList<Integer> procesos = new ArrayList<Integer>();
                ArrayList<Float> prdcts = new ArrayList<Float>();;
                ArrayList<Float> merms = new ArrayList<Float>();;
                ArrayList<Integer> idturnos = new ArrayList<Integer>();;
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //iD eMPLEADO
                                    {
                                        idEmpleado = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //idTurno
                                    {
                                        idTurno = (int) cell.getNumericCellValue();
                                        idturnos.add(idTurno);
                                        columna++;
                                        break;
                                    }
                                    case 3: //idProceso
                                    {
                                        idProceso = (int) cell.getNumericCellValue();
                                        procesos.add(idProceso);
                                        columna++;
                                        break;
                                    }

                                    case 4: //pRODUCTIVIDAD
                                    {
                                        productividad = (float) cell.getNumericCellValue();
                                        prdcts.add(productividad);
                                        columna++;
                                        break;
                                    }

                                    case 5: //merma
                                    {
                                        rotura = (float) cell.getNumericCellValue();
                                        merms.add(rotura);
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
                    // emp.insertarEmpleado(idtipo, nombres, apepat, apemat, direcc, fecha, dni, sexo, tlf, cel, mail);
                    emp.insertarEmpleadoxprocesoturno(idEmpleado, procesos, prdcts, merms, idturnos);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }    
    public void carga_masiva_almacen(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 7 is Almacen
            XSSFSheet sheet = workbook.getSheetAt(7);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            int idAlmacen = 0;
            int idZona = 0;
            String descripcion = null;
            String direccion = null;

            AlmacenService as = new AlmacenService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //IDAlmacen
                                    {
                                        idAlmacen = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //IDZona
                                    {
                                        idZona = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 3: //desripcion
                                    {
                                        descripcion = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }
                                    // System.out.print(cell.getStringCellValue() + "\t");

                                    case 4: //direccion
                                    {
                                        direccion = cell.getStringCellValue();
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
                    as.insertar_masivo(idAlmacen, idZona, descripcion, direccion);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_almacenxproducto(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 8 is AlmacenXProducto
            XSSFSheet sheet = workbook.getSheetAt(8);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;

            int idProducto = 0;
            int idAlmacen = 0;
            int capacidad = 0;
            List<Integer> idproductos = new ArrayList<Integer>();//=new ArrayList<Integer>();
            List<Integer> capacidades = new ArrayList<Integer>();//=new ArrayList<Integer>();
            AlmacenService as = new AlmacenService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly

                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idProducto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                        idproductos.add(idProducto);
                                        columna++;
                                        break;
                                    }
                                    case 2: //idAlmacen
                                    {
                                        idAlmacen = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 3: //capacidad
                                    {
                                        capacidad = (int) cell.getNumericCellValue();
                                        capacidades.add(capacidad);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                    }
                }
                try {
                    as.insertar_masivo_productos_x_almacen(idAlmacen, idproductos, capacidades);
                    idproductos = new ArrayList<Integer>();//=new ArrayList<Integer>();
                    capacidades = new ArrayList<Integer>();

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_distribuidor(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Worksheet 9 is Distribuidor
            XSSFSheet sheet = workbook.getSheetAt(9);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            DistribuidorService ds = new DistribuidorService();

            int idDistribuidor = 0;
            String ruc = null;
            String razonsocial = null;
            String direccion = null;
            String contacto = null;
            String dni = null;
            String correo = null;
            String telefono = null;
            String celular = null;

            EmpleadoService emp = new EmpleadoService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Distribuidor distribuidor = new Distribuidor();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idDistribuidor
                                    {
                                        idDistribuidor = (int) cell.getNumericCellValue();
                                        distribuidor.setIddistribuidor(idDistribuidor);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }

                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 2: //razonsocial
                                    {
                                        razonsocial = cell.getStringCellValue();
                                        distribuidor.setRazonsocial(razonsocial);
                                        columna++;
                                        break;
                                    }
                                    case 3: //RUC
                                    {
                                        ruc = cell.getStringCellValue();
                                        distribuidor.setRuc(ruc);
                                        columna++;
                                        break;
                                    }
                                    case 4: //direccion
                                    {
                                        direccion = cell.getStringCellValue();
                                        distribuidor.setDireccion(direccion);

                                        columna++;
                                        break;
                                    }
                                    case 5: // Contacto
                                    {  //  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        contacto = cell.getStringCellValue();
                                        distribuidor.setContacto(contacto);
                                        columna++;
                                        break;
                                    }
                                    case 6: //dni
                                    {
                                        dni = cell.getStringCellValue();
                                        // System.out.print(cell.getStringCellValue() + "\t");
                                        distribuidor.setDni(dni);
                                        columna++;
                                        break;
                                    }
                                    case 7: //Email
                                    {
                                        correo = cell.getStringCellValue();
                                        distribuidor.setEmail(correo);
                                        //   System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 8: // Telefono Contacto
                                    {
                                        telefono = cell.getStringCellValue();
                                        distribuidor.setTelefonocontacto(telefono);
                                        //  System.out.print(cell.getStringCellValue() + "\t");
                                        columna++;
                                        break;
                                    }
                                    case 9: // Celular 
                                    {
                                        celular = cell.getStringCellValue();;//System.out.print(cell.getNumericCellValue() + "\t");
                                        distribuidor.setCelular(celular);
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
                    //emp.insertarEmpleado(idtipo, nombres, apepat, apemat, direcc, fecha, dni, sexo, tlf, cel, mail);
                    ds.insertarMasiva(idDistribuidor, razonsocial, ruc, direccion, contacto, dni, correo, telefono, celular);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }    
    public void carga_masiva_solicitudventa(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 10 is  solicitud venta
            XSSFSheet sheet = workbook.getSheetAt(10);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;

            int idSolicitudVenta = 0;
            int idDistribuidor = 0;
            Date fecha = new Date();
            float total = 0;
            int estado = 0;
            SolicitudVentaService svs = new SolicitudVentaService();

            DocumentoVentaService dvs = new DocumentoVentaService();
            AlmacenService as = new AlmacenService();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                SolicitudVenta soliv = new SolicitudVenta();
                DistribuidorService dis = new DistribuidorService();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idSolicitudVenta
                                    {
                                        idSolicitudVenta = (int) cell.getNumericCellValue();
                                        columna++;
                                        soliv.setIdsolicitudventa(idSolicitudVenta);
                                        break;
                                    }
                                    case 2: //idDistribuidor
                                    {
                                        idDistribuidor = (int) cell.getNumericCellValue();
                                        Distribuidor distribuidor = new Distribuidor();
                                        distribuidor = dis.buscarDistribuidor(idDistribuidor);
                                        soliv.setToDistribuidor(distribuidor);
                                        columna++;
                                        break;
                                    }
                                    case 4: //total
                                    {
                                        total = (float) cell.getNumericCellValue();
                                        soliv.setTotal(total);
                                        columna++;
                                        break;
                                    }
                                    case 5://estado
                                    {
                                        estado = (int) cell.getNumericCellValue();
                                        soliv.setEstado(estado);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 3: //Fecha 
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fecha = formatter.parse(cell.getStringCellValue());
                                        soliv.setFecha(fecha);
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
                    //  as.insertar_masivo(idAlmacen, idZona, descripcion, direccion);
                    svs.insertarSolicitudVentaMasiva(soliv);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_detallesolicitudventa(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 11 is  solicitud venta
            XSSFSheet sheet = workbook.getSheetAt(11);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;

            int idProveedor = 0;
            //  int idProducto=0;
            float precio = 0;

            int idDetalleSolicitudVenta;
            int idSolicitudVenta;
            int idProducto;
            int cantidad;
            float subtotal;
            int estado;
            ObjectContext c = DataContext.createDataContext();
            SolicitudVentaService svs = new SolicitudVentaService();
            ProductoService ps = new ProductoService();
            //     ProveedorService ps = new ProveedorService();
            DetalleSolicitudVenta dSv = new DetalleSolicitudVenta();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                List<Integer> idproductos = new ArrayList<Integer>();
                List<Float> precios = new ArrayList<Float>();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                SolicitudVenta soliv = new SolicitudVenta();
                DistribuidorService dis = new DistribuidorService();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idDetalleSolicitudVenta
                                    {
                                        idDetalleSolicitudVenta = (int) cell.getNumericCellValue();
                                        dSv.setIddetallesolicitudventa(idDetalleSolicitudVenta);
                                        columna++;
                                        break;
                                    }
                                    case 2: //idSolicitudVenta
                                    {
                                        idSolicitudVenta = (int) cell.getNumericCellValue();
                                        dSv.setToSolicitudVenta(svs.buscarPorId(c, idSolicitudVenta));
                                        // dSv.setto
                                        columna++;
                                        break;
                                    }
                                    case 3: //idProducto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                        Producto p = new Producto();
                                        p = ps.buscarPorId(c, idProducto);
                                        dSv.setToProducto(p);
                                        //    precios.add(precio);                                       
                                        columna++;
                                        break;
                                    }
                                    case 4: //cantidad
                                    {
                                        cantidad = (int) cell.getNumericCellValue();
                                        dSv.setCantidad(cantidad);
                                        columna++;
                                        break;
                                    }

                                    case 5: //subtotal
                                    {
                                        subtotal = (float) cell.getNumericCellValue();
                                        dSv.setSubtotal(subtotal);
                                        columna++;
                                        break;
                                    }

                                    case 6: //estado
                                    {
                                        estado = (int) cell.getNumericCellValue();
                                        dSv.setEstado(estado);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }

                    }
                }
                try {
                    //    ps.insertarProdAProveed(idProveedor, idproductos, precios);
                    svs.insertarDetalleSolicitudVentaMasiva(dSv);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_documentoventa(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Worksheet 12 is Documento Venta
            XSSFSheet sheet = workbook.getSheetAt(12);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            String contacto = null;
            String direccion = null;
            String dni = null;
            Date fechaEmision = new Date();
            Date fechaFactura = new Date();
            float igv = 0;
            float retencion = 0;
            float percepcion = 0;
            float detraccion = 0;
            float descuento = 0;
            float montoTotal=0;
            float total = 0;
            int estado = 0;
            int idSolicitudVenta = 0;
            int idDocumento=0; int idTipoDocumento=0;
            ObjectContext context = DataContext.createDataContext();
            DocumentoVenta dv=(DocumentoVenta)context.newObject(DocumentoVenta.class);
            SolicitudVenta sv=(SolicitudVenta)context.newObject(SolicitudVenta.class);
            TipoDocumento td=(TipoDocumento)context.newObject(TipoDocumento.class);
            DocumentoVentaService dvs=new DocumentoVentaService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {
                                   case 1: //idDocumento
                                    {
                                        idDocumento = (int) cell.getNumericCellValue();
                                     
                                        columna++;
                                        break;
                                    }
                                       case 2: //idTipoDocumento
                                    {
                                        idTipoDocumento = (int) cell.getNumericCellValue();
                                      
                                        columna++;
                                        break;
                                    }
                                    case 7: //Monto Total
                                    {
                                        montoTotal = (float) cell.getNumericCellValue();
                                      //  docVenta.setMontototal(montoTotal);
                                        columna++;
                                        break;
                                    }
                                    case 9: //igv
                                    {
                                        igv = (float) cell.getNumericCellValue();
                                       // docVenta.setIgv(igv);
                                        columna++;
                                        break;
                                    }
                                    case 10: //retencion
                                    {
                                        retencion = (float) cell.getNumericCellValue();
                                      //  docVenta.setRetencion(retencion);
                                        columna++;
                                        break;
                                    }
                                    case 11: //percepcion
                                    {
                                        percepcion = (float) cell.getNumericCellValue();
                                      // docVenta.setPercepcion(percepcion);
                                        columna++;
                                        break;
                                    }
                                    case 12: //detraccion
                                    {
                                        detraccion = (float) cell.getNumericCellValue();
                                       // docVenta.setDetraccion(detraccion);
                                        columna++;
                                        break;
                                    }
                                    case 13: //desciemtp
                                    {
                                        descuento = (float) cell.getNumericCellValue();
                                     //   docVenta.setDescuento(descuento);
                                        columna++;
                                        break;
                                    }

                                    case 14: //total
                                    {
                                        total = (float) cell.getNumericCellValue();
                                     //   docVenta.setTotal(total);
                                        columna++;
                                        break;
                                    }

                                    case 15: //estado
                                    {
                                        estado = (int) cell.getNumericCellValue();
                                    //    docVenta.setEstado(estado);
                                        columna++;
                                        break;
                                    }

                                    case 16: //idSolicitudVenta
                                    {
                                        idSolicitudVenta = (int) cell.getNumericCellValue();
                                       sv.setIdsolicitudventa(idSolicitudVenta);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }

                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 3: //contacto
                                    {
                                        contacto = cell.getStringCellValue();
                                     //   docVenta.setContacto(contacto);
                                        columna++;
                                        break;
                                    }
                                    case 4: //Direccion
                                    {
                                        direccion = cell.getStringCellValue();
                                     //   docVenta.setDireccion(direccion);
                                        columna++;
                                        break;
                                    }
                                    case 5: //dni
                                    {
                                        dni = cell.getStringCellValue();
                                     //   docVenta.setDni(dni);
                                        columna++;
                                        break;
                                    }
                                    case 6: //Fecha Emision
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaEmision = formatter.parse(cell.getStringCellValue());
                                  //      docVenta.setFechaemision(fechaEmision);
                                        columna++;
                                        break;
                                    }
                                    case 8: //Fecha Factura
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaFactura = formatter.parse(cell.getStringCellValue());
                                    //    docVenta.setFechafacturacion(fechaFactura);
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
               
                dvs.insertarDocumentoVentaMasiva(idDocumento, idTipoDocumento, contacto, direccion, dni, fechaEmision, montoTotal, fechaFactura, igv, retencion, percepcion, detraccion, descuento, total, estado, idSolicitudVenta);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_detalledocumentoventa(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 13 is  solicitud venta
            XSSFSheet sheet = workbook.getSheetAt(13);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;

          int idDetalleDocumentoVenta=0;
          int idProducto=0;
          int idDocumento=0;
          int cantidad=0;
          float monto=0;
          int estado=0;
          DocumentoVentaService dvs = new DocumentoVentaService();
          
            ObjectContext c = DataContext.createDataContext();
            SolicitudVentaService svs = new SolicitudVentaService();
            ProductoService ps = new ProductoService();
            //     ProveedorService ps = new ProveedorService();
            DetalleSolicitudVenta dSv = new DetalleSolicitudVenta();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                List<Integer> idproductos = new ArrayList<Integer>();
                List<Float> precios = new ArrayList<Float>();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                SolicitudVenta soliv = new SolicitudVenta();
                DistribuidorService dis = new DistribuidorService();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idDetalleDocumentoVenta
                                    {
                                        idDetalleDocumentoVenta = (int) cell.getNumericCellValue();
                                        //dSv.setIddetallesolicitudventa(idDetalleSolicitudVenta);
                                        columna++;
                                        break;
                                    }
                                    case 2: //idProducto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                       // dSv.setToSolicitudVenta(svs.buscarPorId(c, idSolicitudVenta));
                                        // dSv.setto
                                        columna++;
                                        break;
                                    }
                                    case 3: //idDocumento
                                    {
                                        idDocumento = (int) cell.getNumericCellValue();
                                                                       
                                        columna++;
                                        break;
                                    }
                                    case 4: //cantidad
                                    {
                                        cantidad = (int) cell.getNumericCellValue();
                                      
                                        columna++;
                                        break;
                                    }

                                    case 5: //monto
                                    {
                                        monto = (float) cell.getNumericCellValue();
                                       // dSv.setSubtotal(subtotal);
                                        columna++;
                                        break;
                                    }

                                    case 6: //estado
                                    {
                                        estado = (int) cell.getNumericCellValue();
                                      //  dSv.setEstado(estado);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }

                    }
                }
                try {
                    dvs.insertarDetalleDocumentoVentaMasiva(idDetalleDocumentoVenta, idProducto, idDocumento, cantidad, monto, estado);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_proveedor(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Worksheet 14 Proveedor
            XSSFSheet sheet = workbook.getSheetAt(14);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            DistribuidorService ds = new DistribuidorService();
            ProveedorService ps = new ProveedorService();

            int idProveedor = 0;
            int calificacion = 0;
            String ruc = null;
            String razonsocial = null;
            String marca = null;
            String direccion = null;
            String contacto = null;
            String dni = null;
            String correoEmpresa = null;
            String telefono = null;
            String celular = null;
            String correoContacto = null;
            EmpleadoService emp = new EmpleadoService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Distribuidor distribuidor = new Distribuidor();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idProveedor
                                    {
                                        idProveedor = (int) cell.getNumericCellValue();
                                        // distribuidor.setIddistribuidor(idDistribuidor);
                                        columna++;
                                        break;
                                    }
                                    case 5: //calificacion
                                    {
                                        calificacion = (int) cell.getNumericCellValue();
                                        // distribuidor.setIddistribuidor(idDistribuidor);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }

                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 2: //razonsocial
                                    {
                                        razonsocial = cell.getStringCellValue();
                                        // distribuidor.setRazonsocial(razonsocial);
                                        columna++;
                                        break;
                                    }
                                    case 3: //RUC
                                    {
                                        ruc = cell.getStringCellValue();
                                        //  distribuidor.setRuc(ruc);
                                        columna++;
                                        break;
                                    }
                                    case 4: //direccion
                                    {
                                        direccion = cell.getStringCellValue();
                                        //  distribuidor.setDireccion(direccion);

                                        columna++;
                                        break;
                                    }
                                    case 6: // Marca
                                    {  //  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        marca = cell.getStringCellValue();
                                        //  distribuidor.setContacto(contacto);
                                        columna++;
                                        break;
                                    }
                                    case 7: //contacto
                                    {
                                        contacto = cell.getStringCellValue();
                                        // System.out.print(cell.getStringCellValue() + "\t");
                                        //  distribuidor.setDni(dni);
                                        columna++;
                                        break;
                                    }
                                    case 8: //dni
                                    {
                                        dni = cell.getStringCellValue();

                                        columna++;
                                        break;
                                    }
                                    case 9: // EmailEmpresa
                                    {
                                        correoEmpresa = cell.getStringCellValue();

                                        columna++;
                                        break;
                                    }
                                    case 10: // EmailContacto 
                                    {
                                        correoContacto = cell.getStringCellValue();;//System.out.print(cell.getNumericCellValue() + "\t");
                                        //  distribuidor.setCelular(celular);
                                        columna++;
                                        break;
                                    }
                                    case 11: // Telefono 
                                    {
                                        telefono = cell.getStringCellValue();;//System.out.print(cell.getNumericCellValue() + "\t");
                                        //  distribuidor.setCelular(celular);
                                        columna++;
                                        break;
                                    }
                                    case 12: // Celular 
                                    {
                                        celular = cell.getStringCellValue();;//System.out.print(cell.getNumericCellValue() + "\t");
                                        //  distribuidor.setCelular(celular);
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }
                    }
                }
                try {
                    ps.insertar_masivo(idProveedor, razonsocial, ruc, direccion, calificacion, marca, contacto, dni, celular, correoEmpresa, correoContacto, telefono);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void carga_masiva_productoproveedor(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 15 is  solicitud venta
            XSSFSheet sheet = workbook.getSheetAt(15);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;

            int idProveedor = 0;
            int idProducto = 0;
            float precio = 0;
            ProveedorService ps = new ProveedorService();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                List<Integer> idproductos = new ArrayList<Integer>();
                List<Float> precios = new ArrayList<Float>();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                SolicitudVenta soliv = new SolicitudVenta();
                DistribuidorService dis = new DistribuidorService();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //idSolicitudVenta
                                    {
                                        idProveedor = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //producto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                        idproductos.add(idProducto);
                                        columna++;
                                        break;
                                    }
                                    case 3: //precio
                                    {
                                        precio = (float) cell.getNumericCellValue();
                                        precios.add(precio);
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }

                    }
                }
                try {
                    ps.insertarProdAProveed(idProveedor, idproductos, precios);
                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void carga_masiva_kardex_ventas(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 7 is Almacen
            XSSFSheet sheet = workbook.getSheetAt(16);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            
            int idKardex = 0;
            int idAlmacen = 0;
            int idProducto = 0;
            int idTipoMovimiento = 0;
            Date fechaIn = new Date();
            Date fechaFin = new Date();
            int cantidad = 0;

            KardexService as = new KardexService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //IdKardex
                                    {
                                        idKardex = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //IdAlmacen
                                    {
                                        idAlmacen = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 3://IdProducto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 4://IdTipoMovimiento
                                    {
                                        idTipoMovimiento = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 7://Cantidad
                                    {
                                        cantidad = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 5: //Fecha Ingreso
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaIn = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }
                                    case 6: //Fecha Fin
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaFin = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
                    as.insertar_masivo(idKardex,idAlmacen,idProducto,idTipoMovimiento,fechaIn,fechaFin,cantidad);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
        public void carga_masiva_kardex_entrada_previa(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 7 is Almacen
            XSSFSheet sheet = workbook.getSheetAt(17);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            
            int idKardex = 0;
            int idAlmacen = 0;
            int idProducto = 0;
            int idTipoMovimiento = 0;
            Date fechaIn = new Date();
            Date fechaFin = new Date();
            int cantidad = 0;

            KardexService as = new KardexService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //IdKardex
                                    {
                                        idKardex = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //IdAlmacen
                                    {
                                        idAlmacen = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 3://IdProducto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 4://IdTipoMovimiento
                                    {
                                        idTipoMovimiento = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 7://Cantidad
                                    {
                                        cantidad = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 5: //Fecha Ingreso
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaIn = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }
                                    case 6: //Fecha Fin
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaFin = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
                    as.insertar_masivo(idKardex,idAlmacen,idProducto,idTipoMovimiento,fechaIn,fechaFin,cantidad);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
        public void carga_masiva_kardex_entrada_actual(String ruta) {
        try {
            FileInputStream file = new FileInputStream(new File(ruta));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            //worksheet 7 is Almacen
            XSSFSheet sheet = workbook.getSheetAt(17);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int fila = 0;
            int columna = 1;
            
            int idKardex = 0;
            int idAlmacen = 0;
            int idProducto = 0;
            int idTipoMovimiento = 0;
            Date fechaIn = new Date();
            Date fechaFin = new Date();
            int cantidad = 0;

            KardexService as = new KardexService();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();

                if (fila != 0) {
                }
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) {

                        case Cell.CELL_TYPE_NUMERIC:
                            if (fila != 0) { //esto indica que no coja la primera fila en donde están los ID's
                                switch (columna) {

                                    case 1: //IdKardex
                                    {
                                        idKardex = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 2: //IdAlmacen
                                    {
                                        idAlmacen = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 3://IdProducto
                                    {
                                        idProducto = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 4://IdTipoMovimiento
                                    {
                                        idTipoMovimiento = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                    case 7://Cantidad
                                    {
                                        cantidad = (int) cell.getNumericCellValue();
                                        columna++;
                                        break;
                                    }
                                }
                                break;
                            }


                        case Cell.CELL_TYPE_STRING:
                            if (fila != 0) {
                                switch (columna) {
                                    case 5: //Fecha Ingreso
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaIn = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }
                                    case 6: //Fecha Fin
                                    {
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        fechaFin = formatter.parse(cell.getStringCellValue());
                                        columna++;
                                        break;
                                    }

                                }
                                break;
                            }
                    }
                }
                try {
                    as.insertar_masivo(idKardex,idAlmacen,idProducto,idTipoMovimiento,fechaIn,fechaFin,cantidad);

                } catch (Exception e) {
                }
                fila++;
                columna = 1;
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
