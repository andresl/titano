    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroDemanda;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.DetalleDamanda;


import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.view.LoginT;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;

import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author natali
 */
public class DemandaService {
        //Buscar por fecha las demandas
    public static Demanda buscarPorFecha(ObjectContext context, Date fecha){
       Estado estado=new Estado();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM");
        
        //ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM DEMANDA WHERE TO_CHAR(FECHA,'YYYY-MM')= '"+ format1.format(fecha.getTime())
                + "' AND ESTADO = " + estado.getActivo() +"";
        
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        Demanda lp =(Demanda) DataObjectUtils.objectForQuery(context, query);
       return lp;
    }
    
    
      public  List<Demanda> buscarPorOP(){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select demanda.iddemanda, demanda.descripcion, demanda.fecha, demanda.estado"
                + " from demanda inner join orden_produccion on (demanda.iddemanda=orden_produccion.iddemanda)"
                + " where orden_produccion.estado=1";
        
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
          List<Demanda> lp = context.performQuery(query);       
       return lp;
    }
    //Buscar por fecha las demandas
    public static Demanda buscarPorFecha(Date fecha){
       Estado estado=new Estado();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM");
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM DEMANDA WHERE TO_CHAR(FECHA,'YYYY-MM')= '"+ format1.format(fecha.getTime())
                + "' AND ESTADO = " + estado.getActivo() +"";
        
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        Demanda lp =(Demanda) DataObjectUtils.objectForQuery(context, query);
       return lp;
    }
     //para tipo de empleados
    public List<Demanda> buscarDemanda(Date fechaIni, Date fechaFin, String text, int selectedIndex) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DEMANDA";
        String where=" WHERE ";
        String conector=" AND ";
        if(!text.isEmpty()){cadena+=where+"DESCRIPCION ILIKE '%"+text+"%'"; where=conector;}
        if(fechaIni.toString()==null){ cadena+=where+"FECHA>='"+fechaIni+"'";where=conector;}
        if(fechaFin.toString()==null){ cadena+=where+"FECHA<='"+fechaFin+"'";where=conector;}
        if(selectedIndex>=0){ cadena+=where+"ESTADO="+selectedIndex+"";}
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        List<Demanda> demandas = context.performQuery(query);       
        return demandas;
    }
    
    public List<Demanda> buscarDemandaChristian(Date fechaIni, Date fechaFin, String text, int selectedIndex) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DEMANDA";
        String where=" WHERE ";
        String conector=" AND ";
        if(!text.isEmpty()){cadena+=where+"DESCRIPCION ILIKE '%"+text+"%'"; where=conector;}
        if(fechaIni!=null){ cadena+=where+"FECHA>='"+fechaIni+"'";where=conector;}
        if(fechaFin!=null){ cadena+=where+"FECHA<='"+fechaFin+"'";where=conector;}
        if(selectedIndex>=0){ cadena+=where+"ESTADO="+selectedIndex+"";}
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        List<Demanda> demandas = context.performQuery(query);       
        return demandas;
    }
    public List<Demanda> buscarFiltrado(FiltroDemanda f) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DEMANDA";
        String where=" WHERE ";
        String conector=" AND ";
        if(!f.getDescripcion().isEmpty()){cadena+=where+"DESCRIPCION ILIKE '%"+f.getDescripcion()+"%'";where=conector;}
        if(f.getFechaIni()!=null){ cadena+=where+"FECHA>='"+f.getFechaIni()+"'";where=conector;}
        if(f.getFechaFin()!=null){ cadena+=where+"FECHA<='"+f.getFechaFin()+"'";where=conector;}
        if(f.getEstado()>0){ cadena+=where+"ESTADO="+f.getEstado()+"";}
       cadena+=" ORDER BY IDDEMANDA DESC";
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        List<Demanda> demandas = context.performQuery(query);       
        return demandas;
    }
    
    
    public List<DetalleDamanda> buscarFiltrado2(Integer idDemanda) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DETALLE_DAMANDA";
        String where=" WHERE ";
        String conector=" AND ";
        if(idDemanda!=null){cadena+=where+"IDDEMANDA="+idDemanda; where=conector;}
        cadena+=where+"ESTADO>0";
        SQLTemplate query = new SQLTemplate(DetalleDamanda.class,cadena);
        
        List<DetalleDamanda> demandas = context.performQuery(query);       
        return demandas;
    }
    
    public void  insertar (Demanda d,List<DetalleDamanda> dD){
        try{
            ObjectContext context0 = DataContext.createDataContext();       
        String cadena0 = "UPDATE DEMANDA SET ESTADO=2  WHERE ESTADO=1";
        SQLTemplate query0 = new SQLTemplate(Demanda.class,cadena0);
        
        context0.performQuery(query0);
        context0.commitChanges();
            
        ObjectContext context00 = DataContext.createDataContext();       
        String cadena00 = "UPDATE DETALLE_DAMANDA SET ESTADO=2  WHERE ESTADO=1";
        SQLTemplate query00 = new SQLTemplate(DetalleDamanda.class,cadena00);
        
        context00.performQuery(query00);
        context00.commitChanges();
            
            
        ObjectContext context = DataContext.createDataContext();
        Demanda dem = context.newObject(Demanda.class);
        dem.setDescripcion((String)d.getDescripcion());
        dem.setFecha(d.getFecha());
        dem.setEstado(1);
        context.commitChanges();
        
        
        
        
        for(int i=0;i<dD.size();i++){
        ObjectContext context2 = DataContext.createDataContext();
        Expression qualifier2 = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, dD.get(i).getToProducto().getIdproducto());
        SelectQuery select2 = new SelectQuery(Producto.class, qualifier2);
        Producto p = (Producto) DataObjectUtils.objectForQuery(context2, select2);
        
            Expression qualifier3 = ExpressionFactory.matchExp(Demanda.IDDEMANDA_PK_COLUMN, dem.getIddemanda());
        SelectQuery select3 = new SelectQuery(Demanda.class, qualifier3);
        Demanda d2 = (Demanda) DataObjectUtils.objectForQuery(context2, select3); 
        
        ObjectContext context3 = DataContext.createDataContext();    
        String cadena = "INSERT INTO DETALLE_DAMANDA (IDDEMANDA,IDPRODUCTO,CANTIDAD,ESTADO) VALUES ('"
                +d2.getIddemanda()+"','"+p.getIdproducto() +"',"+dD.get(i).getCantidad()+",1)";
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        context3.performQuery(query);
        }
        
        }catch(Exception e){
        System.out.println("Ha ocurrido un error al guardar");
        }
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Demanda");
        
    }
    
    public List<Producto> listProduct(){
     ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM PRODUCTO WHERE IDTIPOPRODUCTO=3";
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);
        
        List<Producto> productos = context.performQuery(query);       
        return productos;
        
    }
    
    public void  editar (Demanda d){
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE DEMANDA SET FECHA= '"+d.getFecha()+"',DESCRIPCION='"+d.getDescripcion()+"' WHERE IDDEMANDA="+d.getIddemanda()+"";
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Demanda");
        
    }
      public void  agregarItem (DetalleDamanda dd){
        //System.out.println(dd.getToSolicitudVenta().getIdsolicitudventa()+" " +ds.getCantidad()+" "+ds.getSubtotal()+" "+ds.getEstado()+" "+ds.getToProducto().getIdproducto());
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "INSERT INTO DETALLE_DAMANDA (IDDEMANDA,IDPRODUCTO,CANTIDAD,ESTADO)"
                + " VALUES("+dd.getToDemanda().getIddemanda()+","+dd.getToProducto().getIdproducto()+","+dd.getCantidad()+",1)";
        SQLTemplate query = new SQLTemplate(DetalleDamanda.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Demanda");
             
        
    }
    
      public void  editarItem (int idDetalleDemanda, int cantidad){
        //System.out.println(dd.getToSolicitudVenta().getIdsolicitudventa()+" " +ds.getCantidad()+" "+ds.getSubtotal()+" "+ds.getEstado()+" "+ds.getToProducto().getIdproducto());
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE DETALLE_DAMANDA SET CANTIDAD="+cantidad+" WHERE IDDETALLEDEMANDA="+idDetalleDemanda;
        SQLTemplate query = new SQLTemplate(DetalleDamanda.class,cadena);
        System.out.println(cadena);
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Demanda");
             
        
    }
      
    
    public void eliminar(int idDemanda){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE DEMANDA SET ESTADO=0 "+ 
                        " WHERE IDDEMANDA="+idDemanda+"";
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        
        ObjectContext context2 = DataContext.createDataContext();
        String cadena2 = "UPDATE DETALLE_DAMANDA SET ESTADO=0 "+ 
                        "WHERE IDDEMANDA="+idDemanda+"";
        SQLTemplate query2 = new SQLTemplate(DetalleDamanda.class,cadena2);
        
        context2.performQuery(query2);
        context2.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Demanda");
        
    }
    
    public void eliminarItemDetalle(int idDetalleDemanda){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE DETALLE_DAMANDA SET ESTADO=0 "+ 
                        "WHERE IDDETALLEDEMANDA="+idDetalleDemanda+"";
        SQLTemplate query = new SQLTemplate(DetalleDamanda.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Demanda");
        
    }
 
    
}
