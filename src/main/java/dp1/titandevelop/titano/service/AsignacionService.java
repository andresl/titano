/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;


import dp1.titandevelop.titano.bean.DetalleAsig;
import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Maquina;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.Turno;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.map.SQLResult;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author natali
 */
public class AsignacionService {
  public static final int PROCESOFINAL=4;

    public void insertar(int idTurno,int idEmpleado,int idMaquina, int idProducto,double beneficio,double costo,int codigoAsignacion,String descripcion) {
        
        ObjectContext context = DataContext.createDataContext();
          
        //Obtiene la turno
        Expression qualifier = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY,String.valueOf(idTurno));
        SelectQuery select = new SelectQuery(Turno.class, qualifier);
        Turno t = (Turno) DataObjectUtils.objectForQuery(context, select);
              
        //Obtiene Empleado
        Expression qualifier1 = ExpressionFactory.matchExp(Empleado.IDEMPLEADO_PROPERTY,String.valueOf(idEmpleado));
        SelectQuery selec1 = new SelectQuery(Empleado.class, qualifier1);
        Empleado e = (Empleado) DataObjectUtils.objectForQuery(context, selec1);
        
        //Obtiene Maquina
        Expression qualifier2 = ExpressionFactory.matchExp(Maquina.IDMAQUINA_PROPERTY,String.valueOf(idMaquina));
        SelectQuery selec2 = new SelectQuery(Maquina.class, qualifier2);
        Maquina m = (Maquina) DataObjectUtils.objectForQuery(context, selec2);
        
        //Obtiene Producto
        Expression qualifier3 = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PROPERTY,String.valueOf(idProducto));
        SelectQuery selec3 = new SelectQuery(Producto.class, qualifier3);
        Producto p = (Producto) DataObjectUtils.objectForQuery(context, selec3);
        
        //Obtener Codigo mayor
        
        Estado estado=new Estado();
        
        Asignacion asignacion = context.newObject(Asignacion.class);
        asignacion.setCodigoasignacion(codigoAsignacion);
        asignacion.setBeneficio((float)beneficio);
        asignacion.setCosto((float)costo);
        asignacion.setDescripcion(descripcion);
        asignacion.setEscogido(Boolean.FALSE);
        asignacion.setPeriodo(null);
    
        
        Date fecha=new Date(); 
        asignacion.setFecha(fecha);
        
        asignacion.setEstado(estado.getActivo());
        asignacion.setToEmpleado(e);
        asignacion.setToMaquina(m);
        asignacion.setToProducto(p);
        asignacion.setToTurno(t);
        
       
        context.commitChanges(); 
    }
    
    //Sacar la lista de asignaciones de la ultima corrida aceptada
    public ArrayList<Asignacion> SacaUltimo()
    {
        ObjectContext c = DataContext.createDataContext();
        Estado e= new Estado();
        Expression q1 = ExpressionFactory.matchExp(Asignacion.ESTADO_PROPERTY, e.getEnUso());
        SelectQuery s = new SelectQuery(Asignacion.class, q1);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>) c.performQuery(s);
        
        if(lp.isEmpty())
            return null;
        else
            return lp;        
    }
    
    public int ultimoId(){
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from asignacion order by codigoasignacion desc limit 1"; // SELECT MAX(CODIGOASIGNACION) FROM ASIGNACION";

        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        Asignacion a = (Asignacion) DataObjectUtils.objectForQuery(context, query);

        int v=0;
        if ( a != null )
          v = a.getCodigoasignacion();
        
        return v + 1;        
           
    }
    
    public int ultimoId(ObjectContext context){
        
        String cadena = "select * from asignacion order by codigoasignacion desc limit 1"; // SELECT MAX(CODIGOASIGNACION) FROM ASIGNACION";

        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        Asignacion a = (Asignacion) DataObjectUtils.objectForQuery(context, query);

        int v=0;
        if ( a != null )
          v = a.getCodigoasignacion();
        
        return v + 1;        
           
    }
     
    public List SacaAsignaciones() {
       ArrayList<Integer> listaAsignaciones = new ArrayList<Integer>();
       ObjectContext context = DataContext.createDataContext();
       SQLTemplate query = new SQLTemplate(Asignacion.class, "select codigoasignacion S  from asignacion group by codigoasignacion");
       SQLResult resultDescriptor = new SQLResult();
       resultDescriptor.addColumnResult("S");
       query.setResult(resultDescriptor);
       List prices = context.performQuery(query);
       return prices;
}
    
    public void asignar(int idAsignacion,int anio,int mes){ 
                      
        ObjectContext context = DataContext.createDataContext();
        Estado estado=new Estado();
       
        GregorianCalendar finDelMundo = new GregorianCalendar(2012, Calendar.DECEMBER, 21);
        Date d = finDelMundo.getTime();
        //DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        

        ArrayList<Asignacion> a=(ArrayList<Asignacion>)buscarPorId(context,idAsignacion);
        
        for(int i=0;i<a.size();i++){
            a.get(i).setEscogido(Boolean.TRUE);
            a.get(i).setEstado(estado.getEnUso());
            a.get(i).setPeriodo(d);
        }
        
           context.commitChanges(); 
       
    }
    
    public void desasignar(){
                ObjectContext context = DataContext.createDataContext();
        Estado estado=new Estado();
       
  
        

        ArrayList<Asignacion> a=(ArrayList<Asignacion>)buscarAll(context);
        
        for(int i=0;i<a.size();i++){
            a.get(i).setEstado(estado.getActivo());
        }
        
           context.commitChanges(); 

   }
   
    public  ArrayList<Asignacion> buscarAsignado() {
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE ESTADO= "+estado.getEnUso()+"";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        return lp;
    }
    
    public  ArrayList<Asignacion> buscarIdAsignacion(int idAsignacion){
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE CODIGOASIGNACION= "+idAsignacion+"";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        return lp;
    }
    
    public  ArrayList<Asignacion> buscarIdAsignacionActivo(int idAsignacion){
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE CODIGOASIGNACION= "+idAsignacion+
                " AND ESTADO="+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        return lp;
    }
    
    public float ObtenerRotura(int idAsignacion){
        ArrayList<Asignacion> listaAsignacion=buscarIdAsignacion(idAsignacion);
        EmpleadoXProcesoTurnoService servicio=new EmpleadoXProcesoTurnoService();
        float rotura=0;
        
        for(int i=0;i<listaAsignacion.size();i++){
            if(listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso()==PROCESOFINAL){
            EmpleadoXProcesoTurno e=servicio.buscarUnEmpleadoXProceso(listaAsignacion.get(i).getToEmpleado().getIdempleado(),listaAsignacion.get(i).getToTurno().getIdturno() , listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso());
            rotura+=e.getRotura();
            }
        }
        
        return rotura;
    }
    
    public float ObtenerProduccion(int idAsignacion){
        ArrayList<Asignacion> listaAsignacion=buscarIdAsignacion(idAsignacion);
        EmpleadoXProcesoTurnoService servicio=new EmpleadoXProcesoTurnoService();
        float produccion=0;
        
        for(int i=0;i<listaAsignacion.size();i++){
            if(listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso()==PROCESOFINAL){
            EmpleadoXProcesoTurno e=servicio.buscarUnEmpleadoXProceso(listaAsignacion.get(i).getToEmpleado().getIdempleado(),listaAsignacion.get(i).getToTurno().getIdturno() , listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso());
            produccion+=e.getProductividad();
            }
        }
        
        return produccion;
    }
   
    public float ObtenerRoturaPorGalletaAsignaicion(int idAsignacion,int idProducto){
    float rotura=0;
    
        ArrayList<Asignacion> listaAsignacion=buscarIdAsignacion(idAsignacion);
        EmpleadoXProcesoTurnoService servicio=new EmpleadoXProcesoTurnoService();
        
        
        for(int i=0;i<listaAsignacion.size();i++){
            if(listaAsignacion.get(i).getToProducto().getIdproducto()==idProducto){
                if(listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso()==PROCESOFINAL){
                    EmpleadoXProcesoTurno e=servicio.buscarUnEmpleadoXProceso(listaAsignacion.get(i).getToEmpleado().getIdempleado(),listaAsignacion.get(i).getToTurno().getIdturno() , listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso());
                    rotura+=e.getRotura();
                }
            }
        }
       
    return rotura;
    }
    
    public float ObtenerRoturaPorProcesoAsignaicion(int idAsignacion,int idProceso){
    float rotura=0;
    
        ArrayList<Asignacion> listaAsignacion=buscarIdAsignacion(idAsignacion);
        EmpleadoXProcesoTurnoService servicio=new EmpleadoXProcesoTurnoService();
        for(int i=0;i<listaAsignacion.size();i++){
            if(listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso()==idProceso){
            EmpleadoXProcesoTurno e=servicio.buscarUnEmpleadoXProceso(listaAsignacion.get(i).getToEmpleado().getIdempleado(),listaAsignacion.get(i).getToTurno().getIdturno() , listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso());
            rotura+=e.getRotura();
            }
        }
       
    return rotura;
    }
    
    public float ObtenerProduccionPorGalletaAsignaicion(int idAsignacion,int idProducto){
    float produccion=0;
    
        ArrayList<Asignacion> lista=buscarIdAsignacion(idAsignacion);
        EmpleadoXProcesoTurnoService servicio=new EmpleadoXProcesoTurnoService();
        
        
//        for(int i=0;i<listaAsignacion.size();i++){
//            if(listaAsignacion.get(i).getToProducto().getIdproducto()==idProducto){
//            EmpleadoXProcesoTurno e=servicio.buscarUnEmpleadoXProceso(listaAsignacion.get(i).getToEmpleado().getIdempleado(),listaAsignacion.get(i).getToTurno().getIdturno() , listaAsignacion.get(i).getToMaquina().getToProceso().getIdproceso());
//            produccion+=e.getProductividad();
//            }
//        }
//       
//    return produccion;
        
       float suma=0;
       EmpleadoXProcesoTurnoService service= new EmpleadoXProcesoTurnoService();
       
       for(int i=0; i<lista.size() ;i++){
           if(lista.get(i).getToProducto().getIdproducto()==idProducto){
               if(lista.get(i).getToMaquina().getToProceso().getIdproceso()==PROCESOFINAL){
                   EmpleadoXProcesoTurno e=servicio.buscarUnEmpleadoXProceso(lista.get(i).getToEmpleado().getIdempleado(),lista.get(i).getToTurno().getIdturno() , lista.get(i).getToMaquina().getToProceso().getIdproceso());
                   suma+= e.getProductividad();
               }
           }
        }
       
       return suma;
    }
           
    public int buscarIdAsignado() {
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE ESTADO= "+estado.getEnUso()+"";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        List<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        if(!lp.isEmpty())
            return lp.get(0).getCodigoasignacion();
        else
            return 0;
    }

    public String ObtenerDescripcion(int idAsig) {
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE ESTADO > "+estado.getEliminado()
                +" AND CODIGOASIGNACION="+idAsig+" ";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        if(!lp.isEmpty()){
        return lp.get(0).getDescripcion();
        }else return "";
        
    }
    public int ObtenerCantEmpleados(int idAsig) {
        ArrayList<Asignacion> a=buscarIdAsignacion(idAsig);
   
        return a.size();
    }

    public Object ObtenerCantMaquinas(int idAsig) {
        ArrayList<Asignacion> a=buscarIdAsignacion(idAsig);
   
        return a.size();
    }

    Asignacion BuscaPorId(ObjectContext c, Integer id) {
        Expression q = ExpressionFactory.matchExp(Asignacion.IDASIGNACION_PK_COLUMN, id);       
        SelectQuery s = new SelectQuery(Asignacion.class, q);
        return (Asignacion) DataObjectUtils.objectForQuery(c, s);
    }
    
        
     public List<Asignacion> buscarPorId(ObjectContext c, int id) {
         
        String cadena = "SELECT * FROM asignacion WHERE ESTADO > 0 and codigoasignacion ="+id+"";
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);        
        List<Asignacion> almacenes = c.performQuery(query);       
        return almacenes;
    }
     
     public List<Asignacion> buscarAll(ObjectContext c) {
         
        String cadena = "SELECT * FROM asignacion WHERE ESTADO > 0";
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);        
        List<Asignacion> almacenes = c.performQuery(query);       
        return almacenes;
    }
     
     public List<Asignacion> buscarTodo() {
         ObjectContext c = DataContext.createDataContext();
        String cadena = "SELECT * FROM asignacion WHERE ESTADO > 0";
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);        
        List<Asignacion> almacenes = c.performQuery(query);       
        return almacenes;
    }
     

    public float ObtenerProduccionTotal(List<Asignacion> lista) {
       float suma=0;
       EmpleadoXProcesoTurnoService service= new EmpleadoXProcesoTurnoService();
       
       for(int i=0; i<lista.size() ;i++){
           if(lista.get(i).getToMaquina().getToProceso().getIdproceso()==PROCESOFINAL){
               EmpleadoXProcesoTurno e= service.buscarUnEmpleadoXProceso(lista.get(i).getToEmpleado().getIdempleado(),lista.get(i).getToTurno().getIdturno(),lista.get(i).getToMaquina().getToProceso().getIdproceso());
               suma+=e.getProductividad();
           }
        }
       
       return suma;
    }

    public float ObtenerRoturaTotal(List<Asignacion> lista) {
       float suma=0;
       EmpleadoXProcesoTurnoService service= new EmpleadoXProcesoTurnoService();
       
       for(int i=0; i<lista.size() ;i++){
           if(lista.get(i).getToMaquina().getToProceso().getIdproceso()==PROCESOFINAL){
               EmpleadoXProcesoTurno e= service.buscarUnEmpleadoXProceso(lista.get(i).getToEmpleado().getIdempleado(),lista.get(i).getToTurno().getIdturno(),lista.get(i).getToMaquina().getToProceso().getIdproceso());
               suma+=e.getRotura();
           }
        }
       
       return suma;
    }

    public int ObtenerEstado(int idAsig) {
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE ESTADO <> "+estado.getInactivo()
                +" AND CODIGOASIGNACION="+idAsig+" ";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        if(!lp.isEmpty()){
        return lp.get(0).getEstado();
        }else return 0;
        
    }

    public float ObtenerBeneficio(int idAsig) {
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE "
                +" CODIGOASIGNACION="+idAsig+" ";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        float beneficio=0;
        for(int i=0;i<lp.size();i++){
            beneficio+=lp.get(i).getBeneficio();
        }
        if(!lp.isEmpty()){
        return beneficio;
        }else return 0;
        
    }

    public float ObtenerCosto(int idAsig) {
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE "
                +" CODIGOASIGNACION="+idAsig+" ";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        float beneficio=0;
        for(int i=0;i<lp.size();i++){
            beneficio+=lp.get(i).getCosto();
        }
        if(!lp.isEmpty()){
        return beneficio;
        }else return 0;
    }

    
    private Boolean esta(int codigoAsignacion,List<Integer>aux){
        for(int i=0;i<aux.size();i++){
            if(aux.get(i).equals(codigoAsignacion)){
                return true;
            }
        }
        return false;
    }



    public List<Integer> ObtenerIdAsignaciones() {
        Estado estado= new Estado();
        List<Integer> aux=new ArrayList<Integer>();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE "
                +" ESTADO<>"+estado.getInactivo()+" ";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        for(int i=0;i<lp.size();i++){
            if(!esta(lp.get(i).getCodigoasignacion(),aux)){
                aux.add(lp.get(i).getCodigoasignacion());
            }
        }
        
        return aux;

    }

    public Date ObtenerFecha(int idAsig) {
        Estado estado= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ASIGNACION WHERE ESTADO > "+estado.getEliminado()
                +" AND CODIGOASIGNACION="+idAsig+" ";
        
        SQLTemplate query = new SQLTemplate(Asignacion.class,cadena);
        ArrayList<Asignacion> lp = (ArrayList<Asignacion>)context.performQuery(query);
        
        if(!lp.isEmpty()){
        return lp.get(0).getFecha();
        }else return new Date();
    }

    public List<DetalleAsig> ObtenerData() {
        List<Integer> ids=this.SacaAsignaciones();
        List<DetalleAsig> aux=new ArrayList();
        
        for(int i=0;i<ids.size();i++){
            DetalleAsig a=new DetalleAsig();
            ArrayList<Asignacion> lista=this.buscarIdAsignacion(ids.get(i));
            float sumaBeneficio=0;
            float sumaCosto=0;
            for(int j=0;j<lista.size();j++){
                sumaBeneficio+=lista.get(j).getBeneficio();
                sumaCosto+=lista.get(j).getCosto();
            }
            a.setIdAsig(ids.get(i));
            a.setCantidadEmpleado(lista.size());
            a.setDescripcion(lista.get(0).getDescripcion());
            a.setEstado(lista.get(0).getEstado());
            a.setFecha(lista.get(0).getFecha());
            a.setBeneficio(sumaBeneficio);
            a.setCosto(sumaCosto);
            aux.add(a);
        }
        
        return aux;
        
    }
}
