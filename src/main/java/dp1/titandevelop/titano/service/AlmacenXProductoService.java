/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Almacen;
import dp1.titandevelop.titano.persistent.AlmacenXProducto;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Yulian
 */
public class AlmacenXProductoService {
        
    AlmacenService s= new AlmacenService();
    ProductoService sp= new ProductoService();
    
        public static ArrayList<AlmacenXProducto> buscarAll() {
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from almacen_x_producto where estado >0";
        SQLTemplate query = new SQLTemplate(AlmacenXProducto.class,cadena);         
        ArrayList<AlmacenXProducto> prods = (ArrayList<AlmacenXProducto>)context.performQuery(query);
        
        return prods;
    }
    
    public List<AlmacenXProducto> buscarPorIdAlm(int idAlmacen) {
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from almacen_x_producto where idAlmacen ="+idAlmacen+"and estado >0";
        SQLTemplate query = new SQLTemplate(AlmacenXProducto.class,cadena);        
        List<AlmacenXProducto> prods = context.performQuery(query);
        
        return prods;
    }
    public List<AlmacenXProducto> buscarPorIdAlm(ObjectContext context,int idAlmacen) {
        
        String cadena = "select * from almacen_x_producto where idAlmacen ="+idAlmacen+"and estado >0";
        SQLTemplate query = new SQLTemplate(AlmacenXProducto.class,cadena);         
        List<AlmacenXProducto> prods = context.performQuery(query);
        
        return prods;
    }
    
    public List<AlmacenXProducto> buscarPorIdProd(ObjectContext context,int idProd) {
        
        String cadena = "select * from almacen_x_producto where idProducto ="+idProd+" and estado >0 and idalmacen in(select idalmacen from almacen where descripcion not like '%Temporal%')";
        SQLTemplate query = new SQLTemplate(AlmacenXProducto.class,cadena);         
        List<AlmacenXProducto> prods = context.performQuery(query);
        
        return prods;
    }
    
    public List<Producto> buscarProductosxIdAlm(int idAlmacen){
        
        ObjectContext c = DataContext.createDataContext();
        List<AlmacenXProducto> aps = this.buscarPorIdAlm(c, idAlmacen);
        
        List<Producto> productos = new ArrayList<Producto>();
        for(int i=0;i<aps.size();i++){
            productos.add(aps.get(i).getToProducto());
        }
        
        return productos;
    }

    public AlmacenXProducto buscarPorIdAxP(ObjectContext c,int id){
        Expression q= ExpressionFactory.matchExp(AlmacenXProducto.IDALMACENXPRODUCTO_PK_COLUMN,id);
        SelectQuery s = new SelectQuery(AlmacenXProducto.class,q);
        return (AlmacenXProducto) DataObjectUtils.objectForQuery(c, s);
    }
    
    public AlmacenXProducto buscarPorIdAlm_IdProd(ObjectContext context,int idAlmacen,int idProducto){
        
        String cadena = "select * from almacen_x_producto where idAlmacen ="+idAlmacen+" and idProducto ="+idProducto+"and estado >0";
        SQLTemplate query = new SQLTemplate(AlmacenXProducto.class,cadena);         
        List<AlmacenXProducto> prods = context.performQuery(query);
        
        if(prods.isEmpty())
            return null;
        else 
            return prods.get(0);
        
    }
    
    public AlmacenXProducto buscarPorIdAlm_IdProd(int idProducto,int idAlmacen){
        ObjectContext c = DataContext.createDataContext();
        Almacen a= s.buscarPorId(c, idAlmacen);
        Producto p= sp.buscarPorId(c, idProducto);
        Expression q0= ExpressionFactory.matchExp(AlmacenXProducto.TO_ALMACEN_PROPERTY,a);
        Expression q1= ExpressionFactory.matchExp(AlmacenXProducto.TO_PRODUCTO_PROPERTY,p);
        Expression q =q0.andExp(q1);
        SelectQuery s = new SelectQuery(AlmacenXProducto.class,q);
        return (AlmacenXProducto) DataObjectUtils.objectForQuery(c, s);
        
    }
    public void eliminarPorIdAlm(ObjectContext context, int idAlm) {
        List<AlmacenXProducto> lista = this.buscarPorIdAlm(context, idAlm);
        for(int i=0;i<lista.size();i++){
            lista.get(i).setEstado(0);
        }
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "AlmacenXProducto");
    }

    void eliminarPorIdAxP(ObjectContext context,int idalmacenxproducto) {
        AlmacenXProducto e = this.buscarPorIdAxP(context,idalmacenxproducto);
        e.setEstado(0);
        context.commitChanges(); String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "AlmacenXProducto");
    }
}
