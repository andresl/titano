/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.ParametroNombres;
import dp1.titandevelop.titano.persistent.Parametro;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;

/**
 *
 * @author Usuario1
 */
public class ParametroService {
    
     public Parametro buscar(){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PARAMETRO";
        SQLTemplate query = new SQLTemplate(Parametro.class,cadena);
        
        //List<Parametro> tipos = context.performQuery(query);       
        Parametro tipos = (Parametro) DataObjectUtils.objectForQuery(context, query);   
        //return (List<Parametro>)tipos;

        return tipos;
    }
    public List<String> todos(){
        Parametro m =  this.buscar();
        ArrayList<String> p = new  ArrayList<String>();
        p.add(m.getRutaauditoria());          
        p.add(m.getNombresoftware());           
        p.add(m.getProduccionadicional()+"");  
        p.add(m.getIgv()+"");        
        p.add(m.getRetencion()+"");          
        p.add(m.getPerseccion()+"");
        p.add(m.getRuc());
        p.add(m.getRetraccion()+"");          
        p.add(m.getEstado()+"");  
        p.add(m.getCostokhw()+"");
        return p;             
                
    }
    
    public String nombres(int i){
        ParametroNombres m =  new ParametroNombres();
        switch (i){
            case 1: return m.getRuta();          
            case 2: return m.getNombreSw();           
            case 3: return m.getAdicional();  
            case 4: return m.getIgv();        
            case 5: return m.getReten();          
            case 6: return m.getPerce();           
            case 7: return m.getRuc();           
            case 8: return m.getRetra();           
            case 9: return m.getEst();  
            case 10: return m.getKhw();
                
        }
        return "";               
                
    }
    public String datos(int i){
        ParametroNombres m =  new ParametroNombres();
        switch (i){
            case 1: return m.getRuta();          
            case 2: return m.getNombreSw();           
            case 3: return m.getAdicional();  
            case 4: return m.getIgv();        
            case 5: return m.getReten();          
            case 6: return m.getPerce();           
            case 7: return m.getRuc();           
            case 8: return m.getRetra();          
            case 9: return m.getEst();  
            case 10: return m.getKhw();
                
        }
        return "";               
                
    }
    public List<String> nombres(){
        ParametroNombres m =  new ParametroNombres();
        ArrayList<String> p = new  ArrayList<String>();
        p.add(m.getRuta());          
        p.add(m.getNombreSw());           
        p.add(m.getAdicional());  
        p.add(m.getIgv());        
        p.add(m.getReten());          
        p.add(m.getPerce());
        p.add(m.getRuc());
        p.add(m.getRetra());          
        p.add(m.getEst());  
        p.add(m.getKhw());
        return p;             
                
    }
   

     public void  editarParam (String dato, int posic){
        ObjectContext context = DataContext.createDataContext(); 
        String cad = "";
        switch (posic){
            //falta 
            case 1: cad = "SET rutaAuditoria = '" + dato + "' "; break;
            case 2: cad = "SET nombreSoftware = '" + dato + "' "; break;          
            case 3: cad = "SET produccionAdicional = '" + dato + "' "; break;  
            case 4: cad = "SET igv = " + dato + ""; break;     
            case 5: cad = "SET retencion = " + dato + " "; break;          
            case 6: cad = "SET perseccion = " + dato + " "; break;           
            case 7: cad = "SET ruc = '" + dato + "' "; break;          
            case 8: cad = "SET retraccion = " + dato + " "; break; 
            case 9: cad = "SET estadp = " + dato + " "; break; 
            case 10: cad = "SET costoKhw = " + dato + " "; break; 

        }
        String cadena = "UPDATE PARAMETRO " +cad + " WHERE IDPARAMETRO = 1";
        SQLTemplate query = new SQLTemplate(Parametro.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
    }
}
