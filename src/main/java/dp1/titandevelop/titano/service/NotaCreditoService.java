/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroNC;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.DetalleDamanda;
import dp1.titandevelop.titano.persistent.DetalleDocumentoVenta;
import dp1.titandevelop.titano.persistent.DetalleNotaCredito;
import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.persistent.DocumentoVenta;
import dp1.titandevelop.titano.persistent.NotaCredito;
import dp1.titandevelop.titano.persistent.Producto;
import java.util.Date;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author heli
 */
public class NotaCreditoService {
    public List<NotaCredito> buscarNotaCredito(FiltroNC fnc) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT distinct A.* FROM NOTA_CREDITO A, DOCUMENTO_VENTA B WHERE B.IDDOCUMENTO=A.IDDOCUMENTO ";
        String where=" AND ";
        String conector=" AND ";
        if(fnc.getRazonSocial()!=null){cadena+=where+"B.CONTACTO ILIKE '%"+fnc.getRazonSocial()+"%'"; where=conector;}
        if(fnc.getFechaIni()!=null){ cadena+=where+"A.FECHA>='"+fnc.getFechaIni()+"'";where=conector;}
        if(fnc.getFechaFin()!=null){ cadena+=where+"A.FECHA<='"+fnc.getFechaFin()+"'";where=conector;}
        if(fnc.getEstado()!=null && fnc.getEstado()>0){ cadena+=where+"A.ESTADO="+fnc.getEstado()+"";}
        else{
        cadena+=where+"A.ESTADO<>0";
        }
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(NotaCredito.class,cadena);
        
        List<NotaCredito> notas = context.performQuery(query);       
        return notas;
    }
    
    public List<DetalleNotaCredito> buscarDetalleNotaCredito(Integer id) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM DETALLE_NOTA_CREDITO A WHERE A.IDNOTACREDITO="+id;
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(DetalleNotaCredito.class,cadena);
        
        List<DetalleNotaCredito> detalle = context.performQuery(query);
        if(detalle!=null){
        System.out.println("Te devuelvo null");
            return detalle;
        }else
            return null;
    }
    
    public DetalleNotaCredito buscarDetalleFactura(Integer id) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM DETALLE_DOCUMENTO_VENTA A WHERE A.IDDETALLEDOCUMENTOVENTA="+id;
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(DetalleDocumentoVenta.class,cadena);
        
        List<DetalleDocumentoVenta> detalle = context.performQuery(query);
        DetalleNotaCredito d=new DetalleNotaCredito();
        if(detalle!=null){
        System.out.println("No Te devuelvo null");
        d.setCantidad(detalle.get(0).getCantidad());
        d.setToProducto(detalle.get(0).getToProducto());
            return d;
        }else
            return null;
    }
    
    public Float buscarCosto(Integer id) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM DETALLE_NOTA_CREDITO A WHERE A.IDDETALLENOTACREDITO="+id;
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(NotaCredito.class,cadena);
        
        DetalleNotaCredito detalle = (DetalleNotaCredito)context.performQuery(query);       
        return detalle.getToProducto().getCosto();
    }
    
    
    public void  insertar (NotaCredito nc, List<DetalleDocumentoVenta> DNC){
        try{      
            Estado state=new Estado();
        ObjectContext context = DataContext.createDataContext();
        
        Expression qualifier2 = ExpressionFactory.matchExp(DocumentoVenta.IDDOCUMENTO_PK_COLUMN, nc.getToDocumentoVenta().getIddocumento());
        SelectQuery select2 = new SelectQuery(DocumentoVenta.class, qualifier2);
        DocumentoVenta d = (DocumentoVenta) DataObjectUtils.objectForQuery(context, select2);
        
        NotaCredito ds = context.newObject(NotaCredito.class);
        ds.setFecha(nc.getFecha());
        System.out.println(nc.getFecha());
        ds.setMonto(nc.getMonto());
        System.out.println(nc.getMonto());
        ds.setDescripcion(nc.getDescripcion());
        System.out.println(nc.getDescripcion());
        ds.setTipo(nc.getTipo());
        System.out.println(nc.getTipo());
        ds.setToDocumentoVenta(d);
        System.out.println(nc.getToDocumentoVenta().getIddocumento());
        ds.setEstado(state.getSinUsar());
        System.out.println("llegue aki");
        
        context.commitChanges();
        
        
        for(int i=0;i<DNC.size();i++){
            System.out.println("Voy a registrar el detalle");
            ObjectContext context4 = DataContext.createDataContext();    
            Expression qualifier4 = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, DNC.get(i).getToProducto().getIdproducto());
            SelectQuery select4 = new SelectQuery(Producto.class, qualifier4);
            Producto p = (Producto) DataObjectUtils.objectForQuery(context4, select4);
            
            Expression qualifier5 = ExpressionFactory.matchExp(NotaCredito.IDNOTACREDITO_PK_COLUMN, ds.getIdnotacredito());
            SelectQuery select5 = new SelectQuery(NotaCredito.class, qualifier5);
            NotaCredito d0 = (NotaCredito) DataObjectUtils.objectForQuery(context4, select5);
            
            DetalleNotaCredito dnc = context4.newObject(DetalleNotaCredito.class);
            dnc.setCantidad(DNC.get(i).getCantidad());
            
            dnc.setToProducto(p);
            dnc.setToNotaCredito(d0);
            //Aca debe ir el metodo de yulian
            
            AlmacenService servA=new AlmacenService();
            servA.movimientoAlmacen(d0.getIdnotacredito(),p.getIdproducto(),5,dnc.getCantidad());
            
            context4.commitChanges();
            
        }
        
        
        }catch(Exception e){
        System.out.println("Ha ocurrido un error al guardar");
        }
        
    }
    
    public void  editar (NotaCredito nc){
        
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE NOTA_CREDITO SET DESCRIPCION = '"+nc.getDescripcion()
                +"',MONTO = "+nc.getMonto()
                +",TIPO = "+nc.getTipo()
                +",FECHA = '"+nc.getFecha()
                +"',ESTADO="+nc.getEstado()+" WHERE IDNOTACREDITO="+nc.getIdnotacredito();
        SQLTemplate query = new SQLTemplate(NotaCredito.class,cadena);
        context.performQuery(query);
        context.commitChanges();
        
    }
    
    
    public void eliminar(int id){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE NOTA_CREDITO SET ESTADO=0 "+ 
                        "WHERE IDNOTACREDITO="+id+"";
        SQLTemplate query = new SQLTemplate(NotaCredito.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        
        
    }
}
