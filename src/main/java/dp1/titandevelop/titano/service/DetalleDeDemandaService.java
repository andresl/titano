/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.DetalleDamanda;
import java.util.ArrayList;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;


/**
 *
 * @author natali
 */
public class DetalleDeDemandaService {
     public static  ArrayList<DetalleDamanda> buscarPorIdDemanda(int idDemanda){
        Estado estado=new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM DETALLE_DAMANDA WHERE IDDEMANDA= "+ idDemanda
                + " AND ESTADO = " + estado.getActivo() +"";
        
        SQLTemplate query = new SQLTemplate(DetalleDamanda.class,cadena);
        ArrayList<DetalleDamanda> demandas = (ArrayList<DetalleDamanda>)context.performQuery(query);       
       
        return demandas;
    
    }
     
}
