/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;
import dp1.titandevelop.titano.bean.EmpleadoEficiencia;
import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Almacen;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Maquina;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.ProductoXProceso;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.persistent.Zona;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;
/**
 *
 * @author andres
 */
public class ProcesoService {
    
    public List<Zona> buscarZona(String desc, String area) {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ZONA WHERE DESCRIPCION LIKE '%"+desc+"%' AND AREA LIKE '%"+area+"%' AND ESTADO = "+estado.getActivo()+"";
        //String cadena = "SELECT * FROM ZONA";
        SQLTemplate query = new SQLTemplate(Zona.class,cadena);
        
        List<Zona> tipos = context.performQuery(query);       
        return tipos;
    }
    public Zona buscarZonaUnica(int idzona) {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ZONA WHERE IDZONA = "+idzona+" AND ESTADO = "+estado.getActivo()+"";
        //String cadena = "SELECT * FROM ZONA";
        SQLTemplate query = new SQLTemplate(Zona.class,cadena);
        
        Zona tipos = (Zona)DataObjectUtils.objectForQuery(context, query);
        return tipos;
    }
    
    public ArrayList<Integer> llenaCapacidades(ArrayList<Proceso> pro) {
       ArrayList<Integer> lista = new ArrayList<Integer>();
        for (Integer i=0;i<pro.size();i++){
           lista.add(pro.get(i).getCantidadmaquina());
        }        
        return lista;
    }
    public List<Zona> buscarZonaAll() {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ZONA WHERE ESTADO = "+estado.getActivo()+"";
        //String cadena = "SELECT * FROM ZONA";
        SQLTemplate query = new SQLTemplate(Zona.class,cadena);
        
        List<Zona> tipos = context.performQuery(query);       
        return tipos;
    }
    
        public List<Zona> buscarZonaAll(ObjectContext context) {
        Estado estado = new Estado();
        String cadena = "SELECT * FROM ZONA WHERE ESTADO = "+estado.getActivo()+"";
        //String cadena = "SELECT * FROM ZONA";
        SQLTemplate query = new SQLTemplate(Zona.class,cadena);
        
        List<Zona> tipos = context.performQuery(query);       
        return tipos;
    }
        
    public void editarZona(String descripcion, String area ,int idTipo){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE ZONA SET DESCRIPCION = '"+descripcion+"', "+
                        " AREA = '"+area+"' "+ "WHERE IDZONA="+idTipo+"";
        SQLTemplate query = new SQLTemplate(Zona.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Zona");
    }
     public void insertarZona(String descripcion, String area) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        Zona tm = context.newObject(Zona.class);
        tm.setDescripcion(descripcion);
        tm.setArea(area);
        tm.setEstado(est.getActivo());
        context.commitChanges();      
        String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Zona");
        
        
    }
     public int eliminarZona(int idTipo){
         
          String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Zona");
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        if(!utilizado(context,idTipo)){
            String cadena = "UPDATE ZONA SET ESTADO = "+estado.getEliminado()+ 
                            "WHERE IDZONA="+idTipo+"";
            SQLTemplate query = new SQLTemplate(Zona.class,cadena);

            context.performQuery(query);
            context.commitChanges();
            
            return 1;
        }
        else 
            return 0;
        
        
    }
    private boolean utilizado(ObjectContext c,int idTipo) {

        AlmacenService servA = new AlmacenService();
        ProcesoService servP = new ProcesoService();
        List<Almacen> alm = servA.buscarPorZona(c,idTipo);
        List<Proceso> proc = servP.buscarPorZona(c,idTipo);
        if (alm.isEmpty()&&proc.isEmpty()) 
            return false;
        else 
            return true;
    }
     
     // para procesoo-----------------------------
     public List<Proceso> buscarProceso(String desc,  int idzona) {        
        Estado est = new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        Expression qualifier = ExpressionFactory.matchExp(Zona.IDZONA_PROPERTY,String.valueOf(idzona));
        SelectQuery select = new SelectQuery(Zona.class, qualifier);
        Zona z = (Zona) DataObjectUtils.objectForQuery(context, select);
        
        
        String cadena = "SELECT * FROM PROCESO WHERE DESCRIPCION LIKE '%"+desc+"%' AND IDZONA = "+ z.getIdzona() +" AND ESTADO <> " +est.getEliminado()+ "";
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);         
        List<Proceso> tipos = context.performQuery(query);
        
        return tipos;
    }
     public List<Proceso> buscarProceso(String descripcion) {
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from Proceso "
                + " where descripcion like '%"+descripcion+"%' "
                + " and estado >= 1";
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);         
        List<Proceso> tipos = context.performQuery(query);
        
        return tipos;
    }
     public void eliminarProceso(int idTipo){
        Estado est = new Estado();
        MaquinaService maq = new MaquinaService();
        ArrayList<Maquina> maquinas = maq.buscarIdProceso(idTipo);
        ObjectContext context = DataContext.createDataContext();
        
        
        for (int i =0; i< maquinas.size();i++){
            String cadena = "UPDATE MAQUINA SET ESTADO = "+est.getEliminado()+ 
                            "WHERE IDPROCESO = "+idTipo+"";
            SQLTemplate query = new SQLTemplate(Maquina.class,cadena);

            context.performQuery(query);
            context.commitChanges(); 
        }
        
        String cadena = "UPDATE PROCESO SET ESTADO = "+est.getEliminado()+ 
                        "WHERE IDPROCESO = "+idTipo+"";
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);
        
        context.performQuery(query);
        context.commitChanges(); 
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Proceso");
    }
     public void insertarProceso(int idzna, String descripcion, String area, ArrayList<EmpleadoEficiencia> listaEmpleadosBD,int productividad, int merma) {
        ObjectContext context = DataContext.createDataContext();
        int maquinas = Integer.parseInt(area);
        Estado est = new Estado ();
        TurnoService tserv = new TurnoService();
        
        Expression qualifier = ExpressionFactory.matchExp(Zona.IDZONA_PROPERTY,idzna);
        SelectQuery select = new SelectQuery(Zona.class, qualifier);
        Zona zonatemp = (Zona) DataObjectUtils.objectForQuery(context, select);
        
        Proceso tm = context.newObject(Proceso.class);
        tm.setDescripcion(descripcion);
        tm.setCantidadmaquina(maquinas);
        tm.setToZona(zonatemp);  
        tm.setEstado(est.getActivo());
        
        //sacamos los turnos
        ArrayList<Turno> idturnos = tserv.buscar();
       
        Expression q3 = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY,idturnos.get(0).getIdturno());
        SelectQuery s3 = new SelectQuery(Turno.class, q3);
        Turno ttemp1 = (Turno) DataObjectUtils.objectForQuery(context, s3);
        
        Expression q4 = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY,idturnos.get(1).getIdturno());
        SelectQuery s4 = new SelectQuery(Turno.class, q4);
        Turno ttemp2 = (Turno) DataObjectUtils.objectForQuery(context, s4);
        
        Expression q5 = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY,idturnos.get(2).getIdturno());
        SelectQuery s5 = new SelectQuery(Turno.class, q5);
        Turno ttemp3 = (Turno) DataObjectUtils.objectForQuery(context, s5);
        
        
        
        //agregando  productividad a los procesos
        for (int i = 0; i < listaEmpleadosBD.size();i++){
            EmpleadoEficiencia emp = listaEmpleadosBD.get(i);
            Empleado e =  emp.getE();
            int idEmpleado = e.getIdempleado();
            EmpleadoXProcesoTurno  ept = context.newObject(EmpleadoXProcesoTurno.class);
            
            Expression q2 = ExpressionFactory.matchExp(Empleado.IDEMPLEADO_PROPERTY,idEmpleado);
            SelectQuery s2 = new SelectQuery(Empleado.class, q2);
            Empleado emptemp = (Empleado) DataObjectUtils.objectForQuery(context, s2);
         
           //para el primer turno

            float prdcts = productividad*emp.getEficienciaM();
            float merm = merma*emp.getEficienciaM();
            ept.setProductividad(prdcts);
            ept.setRotura(merm);
            ept.setToEmpleado(emptemp);
            ept.setToProceso(tm);
            ept.setToTurno(ttemp1);

            ept.setFo(0.0f);
            ept.setEstado(est.getActivo());
            context.commitChanges(); 
           //para el segundo turno  
            EmpleadoXProcesoTurno  ept2 = context.newObject(EmpleadoXProcesoTurno.class);
            float prdcts2 = productividad*emp.getEficienciaM();
            float merm2 = merma*emp.getEficienciaM();
            ept2.setToEmpleado(emptemp);
            ept2.setToProceso(tm);
            ept2.setToTurno(ttemp2);
            ept2.setProductividad(prdcts2);
            ept2.setRotura(merm2);
            ept2.setFo(0.0f);
            ept2.setEstado(est.getActivo());
            context.commitChanges(); 
           //para el tercer turno
            
             EmpleadoXProcesoTurno  ept3 = context.newObject(EmpleadoXProcesoTurno.class);
            float prdcts3 = productividad*emp.getEficienciaM();
            float merm3 = merma*emp.getEficienciaM();
            ept3.setToEmpleado(emptemp);
            ept3.setToProceso(tm);
            ept3.setToTurno(ttemp3);
            ept3.setProductividad(prdcts3);
            ept3.setRotura(merm3);
            ept3.setFo(0.0f);
            ept3.setEstado(est.getActivo());
            context.commitChanges(); 

        }
        
        for (int i = 0; i < maquinas;i++){
             Maquina a = context.newObject(Maquina.class); 
             a.setDescripcion("Maquina automatica");
             a.setAsignado(Boolean.FALSE);
             a.setKwh(0.0f);
             a.setEstado(est.getActivo());
             a.setToProceso(tm);
             context.commitChanges();   
            
        } 
        
        String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Proceso");
    }
          public Proceso buscarPorId(ObjectContext c, int id) {

            Expression q1 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PK_COLUMN, id);
            SelectQuery s = new SelectQuery(Proceso.class, q1);
            return (Proceso) DataObjectUtils.objectForQuery(c, s);
        }
          public Proceso buscarPorId( int id) {
            ObjectContext context = DataContext.createDataContext();

            Expression q1 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PK_COLUMN, id);
            SelectQuery s = new SelectQuery(Proceso.class, q1);
            return (Proceso) DataObjectUtils.objectForQuery(context, s);
        }
        public void editarProceso(int idproceso, int idzona, String descripcion, int total, int nuevos) {
        
           ObjectContext context = DataContext.createDataContext();
           Estado est = new Estado();
            Expression qualifier = ExpressionFactory.matchExp(Zona.IDZONA_PROPERTY,String.valueOf(idzona));
            SelectQuery select = new SelectQuery(Zona.class, qualifier);
            Zona z = (Zona) DataObjectUtils.objectForQuery(context, select);

            Proceso tm = this.buscarPorId(context, idproceso); 
            tm.setDescripcion(descripcion);
            tm.setCantidadmaquina(nuevos);
            tm.setToZona(z);  
            context.commitChanges();  
            
            int cant = nuevos - total;
            for (int i = 0; i < cant;i++){
                 Maquina a = context.newObject(Maquina.class); 
                 a.setDescripcion("Maquina automatica");
                 a.setAsignado(Boolean.FALSE);
                 a.setKwh(0.0f);
                 a.setEstado(est.getActivo());
                 a.setToProceso(tm);
                 context.commitChanges();   

            }
             String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Proceso");
            
        }
    
        
         public void editarProceso(int idproceso, int idzona, String descripcion, int total, ArrayList<Integer> idmaquinas) {
        
            //Estado est = new Estado ();
            
            ObjectContext context = DataContext.createDataContext();
            
            Expression qualifier = ExpressionFactory.matchExp(Zona.IDZONA_PROPERTY,String.valueOf(idzona));
            SelectQuery select = new SelectQuery(Zona.class, qualifier);
            Zona z = (Zona) DataObjectUtils.objectForQuery(context, select);
            
            
            Proceso a = this.buscarPorId(context, idproceso);     
            int cantidad = total - idmaquinas.size();

            a.setDescripcion(descripcion);
            a.setCantidadmaquina(cantidad);
            a.setToZona(z);
            context.commitChanges();   
          
        MaquinaService maq = new MaquinaService();
        for (int i = 0; i < idmaquinas.size();i++){
             maq.eliminarMaquina(idmaquinas.get(i));
            context.commitChanges();   
            
        }
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Proceso");
    }
      public List<Proceso> buscarProcAll() {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PROCESO WHERE ESTADO = "+estado.getActivo()+"";
        //String cadena = "SELECT * FROM ZONA";
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);
        
        List<Proceso> tipos = context.performQuery(query);  
        if(tipos.isEmpty())
            return null;
        else
            return tipos;
    }
     
    
        public List<Proceso> buscarProcAll(ObjectContext context) {
        Estado estado = new Estado();
        String cadena = "SELECT * FROM PROCESO WHERE ESTADO = "+estado.getActivo()+"";
        //String cadena = "SELECT * FROM ZONA";
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);
        
        List<Proceso> tipos = context.performQuery(query);       
        return tipos;
    }
        
     public static ArrayList<Proceso> buscar() {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PROCESO WHERE ESTADO = "+estado.getActivo()+"";
     
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);
        
        List<Proceso> tipos = context.performQuery(query);       
        return (ArrayList<Proceso>)tipos;
    }   

    public void CreaProcesoBase() {
         ObjectContext context = DataContext.createDataContext();
         
        Proceso tm = context.newObject(Proceso.class);
        tm.setCantidadmaquina(0);
        tm.setCapacidadmaquina(0.1F);
        tm.setDescripcion(" ");
        tm.setEstado(0);
        tm.setIdproceso(0);
        tm.setToZona(this.buscarZonaAll(context).get(0));
        context.commitChanges();
    }
     
      public boolean buscarSinProducto(int idproc) {
         Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from producto_x_proceso where idProceso = "+idproc+" and estado <> " + est.getEliminado() + "";
        SQLTemplate query = new SQLTemplate(ProductoXProceso.class,cadena);          
        List<ProductoXProceso> prods = context.performQuery(query);
        if (prods.isEmpty()) {
            return true;
        }else{
            return false;
        }
    }
      public boolean buscarSinEmpleado(int idproc) {
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from empleado_x_proceso_turno where idProceso = "+idproc+" and estado <> " + est.getEliminado() + "";
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);          
        List<EmpleadoXProcesoTurno> prods = context.performQuery(query);
        if (prods.isEmpty()) {
            return true;
        }else{
            return false;
        }
    }

      public boolean buscarsinProdNoIntermedio(int idproc) {
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from producto where idProceso = "+idproc+" and estado <> " + est.getEliminado() + "";
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);          
        List<Producto> prods = context.performQuery(query);
        if (prods.isEmpty()) {
            return true;
        }else{
            return false;
        }
    }

    public Proceso buscarPorDescripcion(String descripcionProceso) {
        ObjectContext context = DataContext.createDataContext();
        Estado e=new Estado();
        String cadena = "select * from Proceso "
                + " where descripcion like '%"+descripcionProceso+"%' "
                + " and estado= "+ e.getActivo();
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);         
        Proceso tipos = (Proceso) DataObjectUtils.objectForQuery(context, query);
        
        return tipos;
    }


    private List<Proceso> buscarPorZona(ObjectContext c, int idTipo) {
        
        String cadena = "SELECT * FROM PROCESO WHERE ESTADO > 0 and idzona ="+idTipo+"";
        SQLTemplate query = new SQLTemplate(Proceso.class,cadena);        
        List<Proceso> procesos = c.performQuery(query);       
        return procesos;
    }

     
}
