/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.view.LoginT;

import java.util.List;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;

/**
 *
 * @author heli
 */
public class DistribuidorService {
    
     //para tipo de empleados
    public List<Distribuidor> buscarFiltrado(Distribuidor d) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DISTRIBUIDOR ";
        String where="WHERE ";
        String conector=" AND ";
        if(!d.getRazonsocial().isEmpty()){cadena+=where+"RAZONSOCIAL ILIKE '%"+d.getRazonsocial()+"%'"; where=conector;}
        if(!d.getRuc().isEmpty()){cadena+=where+"RUC ILIKE '%"+d.getRuc()+"%'"; where=conector;}
        if(!d.getDni().isEmpty()){cadena+=where+"DNI ILIKE '%"+d.getDni()+"%'"; where=conector;}
        if(d.getEstado()>=0){ cadena+=where+"ESTADO="+d.getEstado()+"";}
        System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(Distribuidor.class,cadena);
        
        List<Distribuidor> distribuidores = context.performQuery(query);       
        return distribuidores;
    }
    
    public List<Distribuidor> buscar(String text) {
        
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DISTRIBUIDOR WHERE ESTADO=1";
        String where=" AND ";
        String conector=" AND ";
        if(!text.isEmpty()){cadena+=where+"RAZONSOCIAL ILIKE '%"+text+"%'"; where=conector;}
        SQLTemplate query = new SQLTemplate(Distribuidor.class,cadena);
        
        List<Distribuidor> distribuidores = context.performQuery(query);       
        return distribuidores;
    }
    
    public Distribuidor buscarDistribuidor(Integer idDistribuidor){
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DISTRIBUIDOR WHERE ESTADO=1 ";
        String where=" AND ";
        String conector=" AND ";
        if(idDistribuidor>=0){cadena+=where+"IDDISTRIBUIDOR="+idDistribuidor; where="";}
        SQLTemplate query = new SQLTemplate(Distribuidor.class,cadena);
        
        List<Distribuidor> distribuidores = context.performQuery(query);       
        return distribuidores.get(0);
    
    }
    
    public void  insertarMasiva (int idDistribuidor, String razonsocial, String ruc, String direccion, String contacto, String dni, String correo, String telefono, String celular){
        try{
            
        ObjectContext context = DataContext.createDataContext();
        Distribuidor ds = context.newObject(Distribuidor.class);
        
        ds.setIddistribuidor(idDistribuidor);
        ds.setRazonsocial(razonsocial);
        ds.setRuc(ruc);
        ds.setDireccion(direccion);
        ds.setContacto(contacto);
        ds.setDni(dni);
        ds.setEmail(correo);
        ds.setTelefonocontacto(telefono);
        ds.setCelular(celular);
        ds.setEstado(1);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Distribuidor Masivo");
        
        }catch(Exception e){
           e.printStackTrace();
        System.out.println("Ha ocurrido un error al guardar");
        }
        
    }
    
    public void  insertar (Distribuidor d){
        try{
            
        ObjectContext context = DataContext.createDataContext();
        Distribuidor ds = context.newObject(Distribuidor.class);
        ds.setRazonsocial(d.getRazonsocial());
        ds.setRuc(d.getRuc());
        ds.setDireccion(d.getDireccion());
        ds.setContacto(d.getContacto());
        ds.setDni(d.getDni());
        ds.setEmail(d.getEmail());
        ds.setTelefonocontacto(d.getTelefonocontacto());
        ds.setCelular(d.getCelular());
        ds.setEstado(1);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Distribuidor");
        /*
        ObjectContext context = DataContext.createDataContext();
        String cadena = "INSERT INTO DEMANDA (FECHA,DESCRIPCION,ESTADO) VALUES ('"+d.getFecha()+"','"+d.getDescripcion()+"',"+1+")";
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        */
        
        
        
        }catch(Exception e){
           e.printStackTrace();
        System.out.println("Ha ocurrido un error al guardar");
        }
        
    }
    
    public List<Distribuidor> listDistribuidor(){
     ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DISTRIBUIDOR WHERE ESTADO=1";
        SQLTemplate query = new SQLTemplate(Distribuidor.class,cadena);
        
        List<Distribuidor> distribuidores = context.performQuery(query);       
        return distribuidores;
        
    }
    
    public void  editar (Distribuidor d){
        
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE DISTRIBUIDOR SET RAZONSOCIAL = '"+d.getRazonsocial()
                +"',RUC = '"+d.getRuc()
                +"',DIRECCION = '"+d.getDireccion()
                +"',CONTACTO = '"+d.getContacto()
                +"',DNI = '"+d.getDni()
                +"',EMAIL = '"+d.getEmail()
                +"',TELEFONOCONTACTO = '"+d.getTelefonocontacto()
                +"',CELULAR = '"+d.getCelular()
                +"',ESTADO=1 WHERE IDDISTRIBUIDOR="+d.getIddistribuidor();
        SQLTemplate query = new SQLTemplate(Distribuidor.class,cadena);
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Distribuidor");
    }
    public void eliminar(int idDistribuidor){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE DISTRIBUIDOR SET ESTADO=0"+ 
                        "WHERE IDDISTRIBUIDOR="+idDistribuidor+"";
        SQLTemplate query = new SQLTemplate(Distribuidor.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Distribuidor");
        
    }
    
    
}
