/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.Parametro;
import dp1.titandevelop.titano.persistent.Perfil;
import dp1.titandevelop.titano.persistent.TipoMovimiento;
import dp1.titandevelop.titano.persistent.Usuario;
import dp1.titandevelop.titano.view.LoginT;

import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;

import org.apache.cayenne.map.SQLResult;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author christianjuarez
 */
public class UsuarioService {
    
    
    public Empleado buscarEmpleadobyUsuario(String username){
        ObjectContext context = DataContext.createDataContext();   
        String cadena = "SELECT IDEMPLEADO S FROM USUARIO "
                + "WHERE USUARIO = '"+username+"' AND ESTADO=1";      
         SQLTemplate query = new SQLTemplate(Usuario.class,cadena);        
         SQLResult resultado = new SQLResult();
         resultado.addColumnResult("S");
         query.setResult(resultado);
         Number idEmpleado = (Number) DataObjectUtils.objectForQuery(context, query);
         
          String cadena2 = "SELECT * FROM EMPLEADO WHERE IDEMPLEADO = "+idEmpleado+" AND ESTADO=1";      
         SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena2);        
         List<Empleado> empleadoOut = context.performQuery(query2);    
                 
        return empleadoOut.get(0);
    }
    
    public Perfil buscarPerfilbyUsuario(String username){
         ObjectContext context = DataContext.createDataContext();   
        String cadena = "SELECT IDPERFIL S FROM USUARIO "
                + "WHERE USUARIO = '"+username+"' AND ESTADO=1";      
         SQLTemplate query = new SQLTemplate(Usuario.class,cadena);        
         SQLResult resultado = new SQLResult();
         resultado.addColumnResult("S");
         query.setResult(resultado);
         Number idPerfil = (Number) DataObjectUtils.objectForQuery(context, query);
         
          String cadena2 = "SELECT * FROM PERFIL WHERE IDPERFIL = "+idPerfil+" AND ESTADO=1";      
         SQLTemplate query2 = new SQLTemplate(Perfil.class,cadena2);        
         List<Perfil> perfilOut = context.performQuery(query2);    
                 
        return perfilOut.get(0);
    }
    
    public void cambiarPass (String username, String password) {
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE USUARIO SET PASSWORD = '"+password+"' "+
                        "WHERE ESTADO = 1 AND USUARIO='"+username+"'";
        SQLTemplate query = new SQLTemplate(TipoMovimiento.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
    }
    
    public void cambiarPerfil (String username, Integer idPerfil) {
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE USUARIO SET IDPERFIL ="+idPerfil+""+
                        "WHERE ESTADO = 1 AND USUARIO='"+username+"'";
        SQLTemplate query = new SQLTemplate(TipoMovimiento.class,cadena);        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Usuario");
    }
    
    public List<Usuario> buscarUsuario(String username) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM USUARIO WHERE USUARIO ='"+username+"' AND ESTADO = 1";
        SQLTemplate query = new SQLTemplate(Usuario.class,cadena);
        
        List<Usuario> tipos = context.performQuery(query);       
        return tipos;
    }
            
          
    public List<Usuario> buscarUsuariobyEmpleado(String username, String nombre, String apaterno, String amaterno) {
        
        ObjectContext context = DataContext.createDataContext();
       String cadena1= "select u.idusuario, u.idempleado, u.idperfil, u.usuario, u.password, u.estado "
       +"from usuario u inner join empleado e on (u.idempleado=e.idempleado) "
       +"where u.usuario like '%"+username+"%' "
        +"and e.nombres like '%"+nombre+"%' "
       +"and e.apellidop like '%"+apaterno+"%' "
        +"and e.apellidom like '%"+amaterno+"%'"
               + "and u.estado=1";
       SQLTemplate query1 = new SQLTemplate(Usuario.class,cadena1);        
         List<Usuario> usuariosOut = context.performQuery(query1);    
       return usuariosOut;
    }
        

  public void eliminaUsuario(Integer idusuario) {
       ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE  USUARIO SET ESTADO=0 WHERE IDUSUARIO ="  +idusuario+"";
        SQLTemplate query = new SQLTemplate(Usuario.class,cadena);
        context.performQuery(query);
        context.commitChanges();
        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Usuario");
      }

        
   public String encrypt(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public void crear_usuario(String usuario, Integer idEmpleado, Integer idPerfil, String password) {
        ObjectContext context = DataContext.createDataContext();
        //Obteniendo el objeto empleado
      
        String cadena = "INSERT INTO USUARIO (IDEMPLEADO, IDPERFIL, USUARIO, PASSWORD) VALUES ("+idEmpleado+""+
                        ","+idPerfil+""+ 
                        ",'"+usuario+"'"+
                        ",'"+password+"')";
        SQLTemplate query = new SQLTemplate(TipoMovimiento.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuarioIn=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuarioIn, perfil, "Usuario");
        
    }
  

    
    
}
