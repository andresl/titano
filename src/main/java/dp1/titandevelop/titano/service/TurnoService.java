/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Turno;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Usuario1
 */
public class TurnoService {
    public ArrayList<Turno> buscar(){
        Estado est= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TURNO WHERE  ESTADO ="+ est.getActivo()  +"ORDER BY IDTURNO";
        SQLTemplate query = new SQLTemplate(Turno.class,cadena);
        
        List<Turno> tipos = context.performQuery(query);       
       
        return (ArrayList<Turno>)tipos;
    }
    public Turno buscarPorId( int id) {
            ObjectContext context = DataContext.createDataContext();

            Expression q1 = ExpressionFactory.matchExp(Turno.IDTURNO_PK_COLUMN, id);
            SelectQuery s = new SelectQuery(Turno.class, q1);
            return (Turno) DataObjectUtils.objectForQuery(context, s);
        }
    
     public Turno buscarPorId( int id, ObjectContext context) {
           

            Expression q1 = ExpressionFactory.matchExp(Turno.IDTURNO_PK_COLUMN, id);
            SelectQuery s = new SelectQuery(Turno.class, q1);
            return (Turno) DataObjectUtils.objectForQuery(context, s);
        }
     
    public List<Turno> buscarTurnos(){
        Estado est= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TURNO WHERE  ESTADO ="+ est.getActivo()  +"";
        SQLTemplate query = new SQLTemplate(Turno.class,cadena);
        
        List<Turno> tipos = context.performQuery(query);       
       
        return tipos;
    }
    public int total (String duracion) {
        int dur = Integer.parseInt(duracion);
        int total = 0;
        int resta = 24;
        List <Turno> turnos = this.buscar();
        for (int i=0; i<turnos.size();i++){
            total = total + turnos.get(i).getDuracion();
        }
        resta = resta - total;
        if (dur > resta ){
            return resta;
        }
        else {
            return -1;
        }
           
    }
    
    
    public int buscarIdTurno(String desc) {
        Estado est= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TURNO WHERE DESCRIPCION LIKE '%"+desc+"%' AND ESTADO ="+ est.getActivo()  +"";
        SQLTemplate query = new SQLTemplate(Turno.class,cadena);
       
        Turno lp =(Turno) DataObjectUtils.objectForQuery(context, query);
             
        return lp.getIdturno();
    }
    
    public List<Turno> buscarFiltrado(String desc, String durac) {
        Estado est= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TURNO WHERE DESCRIPCION LIKE '%"+desc+"%' AND DURACION = "+Integer.parseInt(durac)+" AND ESTADO ="+ est.getActivo()  +"";
        SQLTemplate query = new SQLTemplate(Turno.class,cadena);
        
        List<Turno> tipos = context.performQuery(query);       
        return tipos;
    }
    public List<Turno> buscarFiltrado(String desc) {
        Estado est= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TURNO WHERE DESCRIPCION LIKE '%"+desc+"%' AND ESTADO ="+ est.getActivo()  +"";
        SQLTemplate query = new SQLTemplate(Turno.class,cadena);
        
        List<Turno> tipos = context.performQuery(query);       
        return tipos;
    }
    public void eliminarTurno(int idTipo){
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        String cadena = "UPDATE TURNO SET ESTADO = "+ est.getEliminado() + 
                        "WHERE IDTURNO="+idTipo+"";
        SQLTemplate query = new SQLTemplate(Turno.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
    }
    public void insertarTurno(String desc, String durac) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        Turno tm = context.newObject(Turno.class);
        tm.setDescripcion(desc);
        tm.setDuracion(Integer.parseInt(durac));
        tm.setEstado(est.getActivo());
        context.commitChanges();        
    }

    public void editarTurno(String turno, int durac ,int idTipo){
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE TURNO SET DESCRIPCION = '"+turno+"', "+
                        " DURACION = "+durac+" WHERE IDTURNO ="+idTipo+"";
            
               
        SQLTemplate query = new SQLTemplate(Turno.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
    }
    
}
