/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Yulian
 */
public class ConexionService {
    //Instancia estatica de la conexion, sino uno llega a existir
    private static Connection connection = null;
 
    /**
     *
     * @return Devuelve la instancia unica de Conexion
     */
    public  Connection getConexion() {
        //Si la instancia no ha sido creado aun, se crea
        if (connection == null) {
            contruyendoConexion();
        }
        return connection;
    }
 
    //Obtener las instancias de Conexion JDBC
    private  void contruyendoConexion() {
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://quilla.lab.inf.pucp.edu.pe:1044/titandb3";
            String usuario = "postgres";
            String clave = "membership";
            connection = DriverManager.getConnection(url, usuario, clave);
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException(contruyendoConexion)  : " + e.getMessage());
            System.gc();
        } catch (SQLException e) {
            System.out.println("SQLException(contruyendoConexion) : " + e.getMessage());
            System.gc();
            JOptionPane.showMessageDialog(null, e.getMessage(), "No fue Posible Conectar con la Base de Datos", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        } catch (Exception e) {
            System.out.println(" Exception General (contruyendoConexion) : " + e.getMessage());
            System.gc();
        }
    }
 
    public  void liberarConexionS(Connection conex) {
        try {
            conex.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
 
 
    /**
     * Cierra la conexion. Al provenir de BasicDataSource, en realidad no se
     * esta cerrando. La llamada a close() le indica al BasicDataSource que
     * hemos terminado con dicha conexion y que puede asignarsela a otro que la
     * pida.
     *
     * @param conexion
     */
    //esto es un pool de conexiones, pero no se esta usando
    public static void liberaConexion(Connection conexion) {
        try {
            if (null != conexion) {
                // En realidad no cierra, solo libera la conexion.
                conexion.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
 
    
    public static void liberarStatement(PreparedStatement p){
        try{
            if(null != p){
                p.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
