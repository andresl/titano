/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author cHreS
 */
public class AuditoriaService {

    public void leerDatosAuditoria(ArrayList<String> username, ArrayList<String> perfiles, ArrayList<String> tablas, ArrayList<String> acciones, ArrayList<String> fechas) {

//
//        File archiveInput = new File("C:/Temp/Auditoria.txt");
//        if (archiveInput != null) {
//            try {
//                Scanner sc = new Scanner(archiveInput);
//                String linea = "";
//                while (sc.hasNextLine()) {
//                    linea = sc.nextLine();
//                    String parts[] = linea.split("/");
//                    username.add(parts[0]);
//                    perfiles.add(parts[1]);
//                    tablas.add(parts[2]);
//                    acciones.add(parts[3]);
//                    fechas.add(parts[4]);
//                }
//            } catch (FileNotFoundException ex) {
//                //   Logger.getLogger(LeeDatos.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
        
        File archiveInput2 = new File("/tmp/Auditoria.txt");
        if (archiveInput2 != null) {
            try {
                Scanner sc = new Scanner(archiveInput2);
                String linea = "";
                while (sc.hasNextLine()) {
                    linea = sc.nextLine();
                    String parts[] = linea.split("/");
                    username.add(parts[0]);
                    perfiles.add(parts[1]);
                    tablas.add(parts[2]);
                    acciones.add(parts[3]);
                    fechas.add(parts[4]);
                }
            } catch (FileNotFoundException ex) {
                //   Logger.getLogger(LeeDatos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    public void auditoriaInsert(String usuario, String perfil, String tabla) {
//        try {
//            Date fechaActual = new Date();
//            String data = "" + usuario + "/ " + perfil + "/ " + tabla + "/insert/ " + fechaActual + "";
//
//            File file = new File("C:/Temp/Auditoria.txt");
//
//
//            if (!file.exists()) {
//                file.createNewFile();
////                String data2 = "Usuario,    Perfil,     Tabla Utilizada,    Acción, Fecha";
////                FileWriter fileWritter = new FileWriter(file, true);
////                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
////                bufferWritter.write(data2);
////                bufferWritter.newLine();
////                bufferWritter.close();
//            }
//
//            FileWriter fileWritter = new FileWriter(file, true);
//            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
//            bufferWritter.write(data);
//            bufferWritter.newLine();
//            bufferWritter.close();
//
//            System.out.println("Done");
//
//        } catch (IOException e) {
//          System.out.println("Estas en Ubuntu....");
//        }
        
         try {
            Date fechaActual = new Date();
            String data = "" + usuario + "/" + perfil + "/" + tabla + "/insert/" + fechaActual + "";

            File file = new File("/tmp/Auditoria.txt");


            if (!file.exists()) {
                file.createNewFile();
//                String data2 = "Usuario,    Perfil,     Tabla Utilizada,    Acción, Fecha";
//                FileWriter fileWritter = new FileWriter(file, true);
//                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
//                bufferWritter.write(data2);
//                bufferWritter.newLine();
//                bufferWritter.close();
            }

            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.newLine();
            bufferWritter.close();

            System.out.println("Done");

        } catch (IOException e) {
            System.out.println("Estas en Windows....");
        }
    }

    public void auditoriaUpdate(String usuario, String perfil, String tabla) {
//         try {
//            Date fechaActual = new Date();
//            String data = "" + usuario + "|" + perfil + "|" + tabla + "|update|" + fechaActual + "";
//
//            File file = new File("C:/Temp/Auditoria.txt");
//
//
//            if (!file.exists()) {
//                file.createNewFile();
////                String data2 = "Usuario,    Perfil,     Tabla Utilizada,    Acción, Fecha";
////                FileWriter fileWritter = new FileWriter(file, true);
////                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
////                bufferWritter.write(data2);
////                bufferWritter.newLine();
////                bufferWritter.close();
//            }
//
//            FileWriter fileWritter = new FileWriter(file, true);
//            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
//            bufferWritter.write(data);
//            bufferWritter.newLine();
//            bufferWritter.close();
//
//            System.out.println("Done");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        
         try {
            Date fechaActual = new Date();
            String data = "" + usuario + "/" + perfil + "/" + tabla + "/update/" + fechaActual + "";

            File file = new File("/tmp/Auditoria.txt");


            if (!file.exists()) {
                file.createNewFile();
//                String data2 = "Usuario,    Perfil,     Tabla Utilizada,    Acción, Fecha";
//                FileWriter fileWritter = new FileWriter(file, true);
//                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
//                bufferWritter.write(data2);
//                bufferWritter.newLine();
//                bufferWritter.close();
            }

            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.newLine();
            bufferWritter.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void auditoriaDelete(String usuario, String perfil, String tabla) {
//        try {
//            Date fechaActual = new Date();
//            String data = "" + usuario + "|" + perfil + "|" + tabla + "|delete|" + fechaActual + "";
//
//            File file = new File("C:/Temp/Auditoria.txt");
//
//
//            if (!file.exists()) {
//                file.createNewFile();
////                String data2 = "Usuario,    Perfil,     Tabla Utilizada,    Acción, Fecha";
////                FileWriter fileWritter = new FileWriter(file, true);
////                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
////                bufferWritter.write(data2);
////                bufferWritter.newLine();
////                bufferWritter.close();
//            }
//
//            FileWriter fileWritter = new FileWriter(file, true);
//            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
//            bufferWritter.write(data);
//            bufferWritter.newLine();
//            bufferWritter.close();
//
//            System.out.println("Done");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        
         try {
            Date fechaActual = new Date();
            String data = "" + usuario + "/" + perfil + "/" + tabla + "/delete/" + fechaActual + "";

            File file = new File("/tmp/Auditoria.txt");


            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.newLine();
            bufferWritter.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
}
}