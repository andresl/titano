/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.DetalleFacturaCompra;
import dp1.titandevelop.titano.persistent.OrdenProduccion;
import dp1.titandevelop.titano.persistent.Requerimiento;
import dp1.titandevelop.titano.persistent.RequerimientoXProducto;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author andres
 */
public class RequerimientoService {

    public void insertarRequerimiento(int idOrdenProduccion, int estado, ArrayList<RequerimientoXProducto> listaDetalleRequerimiento, Date fecha) {
        ObjectContext context = DataContext.createDataContext();
        Requerimiento requerim = context.newObject(Requerimiento.class);
        requerim.setEstado(estado);
        requerim.setFecha(fecha);
        requerim.setFecharegistro(new Date()); // esto deberia cambiarse

        // buscare la orden de produccion que toca
        Expression qualifier = ExpressionFactory.matchExp(OrdenProduccion.IDORDENPRODUCCION_PROPERTY, idOrdenProduccion);
        SelectQuery select = new SelectQuery(OrdenProduccion.class, qualifier);
        OrdenProduccion op = (OrdenProduccion) DataObjectUtils.objectForQuery(context, select);
        // y la asignamos
        requerim.setToOrdenProduccion(op);

        // ahora iteramos sobre los ingredientes para guardarlos
        for (int i = 0; i < listaDetalleRequerimiento.size(); i++) {
            RequerimientoXProducto rxp = context.newObject(RequerimientoXProducto.class);
            rxp.setToRequerimiento(requerim);
            rxp.setCantidad(listaDetalleRequerimiento.get(i).getCantidad());
            ProductoService ps = new ProductoService();
            rxp.setSolicitado(Boolean.TRUE); // no se que hace esto, preguntar
            rxp.setEstado(estado);
            rxp.setToProducto(ps.buscarPorId(context, listaDetalleRequerimiento.get(i).getToProducto().getIdproducto()));
        }

        // se terminooooooooooooooo

        context.commitChanges();
        String usuario = LoginT.userLogin.getUsuario();
        String perfil = LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "RequerimientoXProducto");
    }

    public void marcarRequerimiento(Requerimiento r) {
        Boolean p = true;
        for (int i = 0; i < r.getRequerimientoXProductoArray().size(); i++) {
            if (!r.getRequerimientoXProductoArray().get(i).getEstado().equals(Estado.getEstadoId("Cumplido"))) {
                p = p && false;
            }
        }
        if (p) {
            r.setEstado(Estado.getEstadoId("Cumplido"));
            r.getObjectContext().commitChanges();
        }

    }

    public ArrayList<Requerimiento> buscarPorRangoDeFechas(ObjectContext context, Date fi, Date ff) {
        if (context == null) {
            context = DataContext.createDataContext();
        }
        Estado est = new Estado();
        ArrayList<Requerimiento> l = new ArrayList<Requerimiento>();
        Expression exp = ExpressionFactory.betweenExp(Requerimiento.FECHA_PROPERTY, fi, ff);
        exp = exp.andExp(ExpressionFactory.greaterDbExp(Requerimiento.ESTADO_PROPERTY, est.getEliminado()));
        SelectQuery s = new SelectQuery(Requerimiento.class, exp);
        l.addAll(context.performQuery(s));
        return l;
    }

    public Requerimiento buscarPorId(ObjectContext context, int idRequerimiento) {
        if (context == null) {
            context = DataContext.createDataContext();
        }

        Expression exp = ExpressionFactory.matchExp(Requerimiento.IDREQUERIMIENTO_PK_COLUMN, idRequerimiento);
        SelectQuery s = new SelectQuery(Requerimiento.class, exp);
        return (Requerimiento) DataObjectUtils.objectForQuery(context, s);
        //Zona tipos = (Zona)DataObjectUtils.objectForQuery(context, query);
    }

    public Requerimiento buscarPorOrdenProduccion(OrdenProduccion op) {
        ObjectContext context = DataContext.createDataContext();
        Expression exp = ExpressionFactory.matchExp("toOrdenProduccion.idordenproduccion", op.getIdordenproduccion());
        exp = exp.andExp(ExpressionFactory.greaterExp(OrdenProduccion.ESTADO_PROPERTY, 0));
        SelectQuery query = new SelectQuery(Requerimiento.class, exp);
        ArrayList<Requerimiento> rl = new ArrayList<Requerimiento>();
        rl.addAll(context.performQuery(query));
        if (rl.isEmpty()) {
            return null;
        }

        return rl.get(0);
        // tambien habia ya uno pero el mio es mas bonito... viva yo
    }

    public Requerimiento buscarPorIdOrdProd(ObjectContext c, int idOrdProd) {
        String cadena = "SELECT * FROM Requerimiento WHERE ESTADO > 0 and idordenproduccion = " + idOrdProd + "";
        SQLTemplate query = new SQLTemplate(Requerimiento.class, cadena);
        List<Requerimiento> requerimientos = c.performQuery(query);
        if (requerimientos.isEmpty()) {
            return null;
        } else {
            return requerimientos.get(0);
        }
    }

    public Requerimiento buscarActivo() {
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM Requerimiento WHERE ESTADO = " + est.getActivo() + "";
        SQLTemplate query = new SQLTemplate(Requerimiento.class, cadena);
        Requerimiento lp = (Requerimiento) DataObjectUtils.objectForQuery(context, query);
        return lp;
    }

    public ArrayList<Requerimiento> buscarPorFecha(ObjectContext context, Date fecha) {
        if (context == null) {
            context = DataContext.createDataContext();
        }

        Expression exp = ExpressionFactory.matchExp(Requerimiento.FECHA_PROPERTY, fecha);
        SelectQuery s = new SelectQuery(Requerimiento.class, exp);
        ArrayList<Requerimiento> rl = new ArrayList<Requerimiento>();
        rl.addAll(context.performQuery(s));
        return rl;
        //Zona tipos = (Zona)DataObjectUtils.objectForQuery(context, query);
    }

    public void borrarPorId(int idReq) {
        Requerimiento r = this.buscarPorId(null, idReq);
        r.setEstado(0);
        r.getObjectContext().commitChanges();

    }

    public ArrayList<Requerimiento> buscarPor(int idReq, int idOP, Date fechaReg, Date fechaLim) {

        ObjectContext context = DataContext.createDataContext();


        Expression exp = ExpressionFactory.matchExp(Requerimiento.IDREQUERIMIENTO_PK_COLUMN, idReq);
        if (idReq != 0) {
            exp = exp.orExp(ExpressionFactory.matchExp(Requerimiento.IDREQUERIMIENTO_PK_COLUMN, idReq));
        }

        if (idOP != 0) {
            exp = exp.orExp(ExpressionFactory.matchExp("toOrdenProduccion.idordenproduccion", idOP));
        }
//        if ( idReq != 0 )
        exp = exp.orExp(ExpressionFactory.matchExp(Requerimiento.FECHAREGISTRO_PROPERTY, fechaReg));
        exp = exp.orExp(ExpressionFactory.matchExp(Requerimiento.FECHA_PROPERTY, fechaLim));

        SelectQuery s = new SelectQuery(Requerimiento.class, exp);
        ArrayList<Requerimiento> rl = new ArrayList<Requerimiento>();
        rl.addAll(context.performQuery(s));
        return rl;

    }

    public ArrayList<Requerimiento> buscarTodo() {
        ObjectContext context = DataContext.createDataContext();

        Expression exp = ExpressionFactory.greaterExp(Requerimiento.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Requerimiento.class, exp);
        return (ArrayList<Requerimiento>) context.performQuery(s);
    }

    public void actualizaRequerimiento(DetalleFacturaCompra fact) {
        // apague mi laptop sin guardar esto y ahora tengo q volverlo a escribir
    }
}
