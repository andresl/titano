/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.AlmacenXProducto;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.ProductoProveedor;
import dp1.titandevelop.titano.persistent.Proveedor;
import dp1.titandevelop.titano.view.LoginT;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Usuario1
 */
public class ProductoProveedorService {
    
    public List<ProductoProveedor> buscarPorIdProdProv(ObjectContext context,int idProv) {
        Estado est = new Estado();
        String cadena = "select * from Producto_Proveedor where idProveedor ="+idProv+"and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(ProductoProveedor.class,cadena);         
        List<ProductoProveedor> prods = context.performQuery(query);
        
        return prods;
    }
    public List<ProductoProveedor> buscarPorIdProdProv(int idProv) {
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from Producto_Proveedor where idProveedor ="+idProv+"and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(ProductoProveedor.class,cadena);         
        List<ProductoProveedor> prods = context.performQuery(query);
        
        return prods;
    }
    
    public List<ProductoProveedor> buscarPorIdAlm(int idPrv) {
        ObjectContext context = DataContext.createDataContext();
        Estado est = new Estado();
        String cadena = "select * from Producto_Proveedor where idProveedor ="+idPrv+"and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(ProductoProveedor.class,cadena);         
        List<ProductoProveedor> prods = context.performQuery(query);
        
        return prods;
    }
     public ProductoProveedor buscarPorIdPxP(ObjectContext c,int id){
        Expression q= ExpressionFactory.matchExp(ProductoProveedor.IDPRODUCTOPROVEEDOR_PK_COLUMN,id);
        SelectQuery s = new SelectQuery(ProductoProveedor.class,q);
        return (ProductoProveedor) DataObjectUtils.objectForQuery(c, s);
    }
     public ProductoProveedor buscarPorProdProv(Producto prod, Proveedor prov){
        ObjectContext c = DataContext.createDataContext();
        Expression q= ExpressionFactory.matchExp(ProductoProveedor.TO_PRODUCTO_PROPERTY,prod);
        Expression q1= ExpressionFactory.matchExp(ProductoProveedor.TO_PROVEEDOR_PROPERTY,prov);
        Expression q2=q.andExp(q1);
        SelectQuery s = new SelectQuery(ProductoProveedor.class,q2);
        return (ProductoProveedor) DataObjectUtils.objectForQuery(c, s);
    } 
     
    public void eliminarPorIdProv(ObjectContext context, int idProv) {
        List<ProductoProveedor> lista = this.buscarPorIdProdProv(context, idProv);
        for(int i=0;i<lista.size();i++){
            lista.get(i).setEstado(0);
        }
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "ProductosProveedor");
    }
    
    void eliminarPorIdPxP(ObjectContext context,int idprovxproducto) {
        Estado est = new Estado();
        ProductoProveedor e = this.buscarPorIdPxP(context,idprovxproducto);
        e.setEstado(est.getEliminado());
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "ProductosProveedor");
    }
    
    public Boolean esProvistoPorProveedor(Proveedor prov, Producto prod) {
        
        for (int i=0; i<prov.getProductoProveedorArray().size(); i++) {
            if ( prov.getProductoProveedorArray().get(i).getToProducto().getIdproducto() == prod.getIdproducto() ) {
                return true;
            }
        }
        return false;
        
        
    }

}
