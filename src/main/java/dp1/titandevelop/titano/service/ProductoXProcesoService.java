/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.ProductoXProceso;
import dp1.titandevelop.titano.persistent.Zona;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author natali
 */
public class ProductoXProcesoService {
    ProductoService spd = new ProductoService();
    ProcesoService spc = new ProcesoService();
   
    public static ArrayList<ProductoXProceso> buscar(){
      Estado estado=new Estado();
      ObjectContext c=DataContext.createDataContext();
      Expression query = ExpressionFactory.matchExp(ProductoXProceso.ESTADO_PROPERTY,estado.getActivo());
    
      SelectQuery s = new SelectQuery(ProductoXProceso.class,query);
      ArrayList<ProductoXProceso> lp = (ArrayList<ProductoXProceso>) c.performQuery(s);
      return lp;
    }

   public static ArrayList<ProductoXProceso> buscarProcesos(int movimiento){
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PRODUCTO_X_PROCESO WHERE "
                + " AND ESTADO = " + estado.getActivo()
                + " AND TIPOMOVIMIENTO= "+movimiento + "";
        
        
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);
        
        ArrayList<Producto> producto =(ArrayList<Producto>) context.performQuery(query);  
        
        return null;
    }

   
    public static ProductoXProceso buscarPorIdProductoProceso(int idProceso, int idProducto,int movimiento){
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PRODUCTO_X_PROCESO WHERE IDPRODUCTO= "+idProducto
                + " AND IDPROCESO= "+ idProceso
                + " AND ESTADO = " + estado.getActivo()
                + " AND TIPOMOVIMIENTO= "+movimiento + "";
        
        SQLTemplate query = new SQLTemplate(ProductoXProceso.class,cadena);
        
        ProductoXProceso produccion =(ProductoXProceso) DataObjectUtils.objectForQuery(context, query);
       
        return produccion ;
    }

    public ProductoXProceso buscaPorID(Integer idpp) {
        ObjectContext c = DataContext.createDataContext();
        
        Expression qualifier = ExpressionFactory.matchExp(ProductoXProceso.IDPRODUCTOXPROCESO_PK_COLUMN,idpp);
        SelectQuery select = new SelectQuery(ProductoXProceso.class, qualifier);
        ProductoXProceso pp= (ProductoXProceso) DataObjectUtils.objectForQuery(c, select);
        
        return pp;
    
    }
    
    public ProductoXProceso buscaPorID( ObjectContext c,Integer idpp) {
        Expression qualifier = ExpressionFactory.matchExp(ProductoXProceso.IDPRODUCTOXPROCESO_PK_COLUMN,idpp);
        SelectQuery select = new SelectQuery(ProductoXProceso.class, qualifier);
        ProductoXProceso pp= (ProductoXProceso) DataObjectUtils.objectForQuery(c, select);
        
        return pp;
    
    }
    
    
    public void Inserta(Integer idproceso, int orden, int i, Integer idproducto, Integer idproducto0) {
        ObjectContext c = DataContext.createDataContext();
        Producto producto= spd.buscarPorId(c,idproducto);
        Producto material = spd.buscarPorId(c,idproducto0);
        Proceso proceso= spc.buscarPorId(c,idproceso);
        Estado e= new Estado();
        ProductoXProceso p= c.newObject(ProductoXProceso.class);
        p.setEstado(e.getActivo());
        p.setTipomovimiento(i);
        p.setOrden(orden);
        p.setToProceso(proceso);
        p.setToProducto(producto);
        p.setToProducto1(material);
        c.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "ProductoXProceso");
    
    }
    
    public void Actualiza(int idpp, int orden, int mov) {
        ObjectContext c = DataContext.createDataContext();
       
        Estado e= new Estado();
        ProductoXProceso p= this.buscaPorID(c, idpp);
        p.setEstado(e.getActivo());
        p.setTipomovimiento(mov);
        p.setOrden(orden);
        c.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "ProductoXProceso");
    
    }
    
    public void Elimina(int idpp) {
        ObjectContext c = DataContext.createDataContext();
       
        Estado e= new Estado();
        ProductoXProceso p= this.buscaPorID(c, idpp);
        p.setEstado(e.getEliminado());       
        c.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "ProductoXProceso");
    
    }
    
     public ArrayList<ProductoXProceso> buscaPorMaterial(Integer idMaterial, int idProceso, int movimiento) {
        ObjectContext c = DataContext.createDataContext();
        Producto material = spd.buscarPorId(c,idMaterial);
        Proceso proceso= spc.buscarPorId(c,idProceso);
        Expression q = ExpressionFactory.matchExp(ProductoXProceso.TO_PROCESO_PROPERTY,proceso);
        Expression q1 = ExpressionFactory.matchExp(ProductoXProceso.TO_PRODUCTO1_PROPERTY,material);
        Expression q2 = ExpressionFactory.matchExp(ProductoXProceso.TIPOMOVIMIENTO_PROPERTY,movimiento);
        Expression q3 = q1.andExp(q2.andExp(q));
        SelectQuery s = new SelectQuery(ProductoXProceso.class, q3);
        ArrayList<ProductoXProceso> pp = (ArrayList<ProductoXProceso>)c.performQuery(s);
        
        return pp;
    
    }
    
    public ArrayList<ProductoXProceso> buscaPorProducto(int idProducto) {
        ObjectContext c = DataContext.createDataContext();
        Producto producto = spd.buscarPorId(c,idProducto);
        Expression q1 = ExpressionFactory.matchExp(ProductoXProceso.TO_PRODUCTO_PROPERTY,producto);
        Expression q2 = ExpressionFactory.greaterExp(ProductoXProceso.ESTADO_PROPERTY,0);
        Expression q3 = q1.andExp(q2.andExp(q1));
        SelectQuery s = new SelectQuery(ProductoXProceso.class, q3);
        ArrayList<ProductoXProceso> pp = (ArrayList<ProductoXProceso>)c.performQuery(s);
        
        return pp;
    
    }
    
    public ArrayList<ProductoXProceso> buscaPorProducto(ObjectContext c,int idProducto) {
        
        Producto producto = spd.buscarPorId(c,idProducto);
        Expression q1 = ExpressionFactory.matchExp(ProductoXProceso.TO_PRODUCTO_PROPERTY,producto);
        Expression q2 = ExpressionFactory.greaterExp(ProductoXProceso.ESTADO_PROPERTY,0);
        Expression q3 = q1.andExp(q2.andExp(q1));
        SelectQuery s = new SelectQuery(ProductoXProceso.class, q3);
        ArrayList<ProductoXProceso> pp = (ArrayList<ProductoXProceso>)c.performQuery(s);
        
        return pp;
    
    }
    public ArrayList<ProductoXProceso> buscaPorProdProc(int idProducto, int idProc) {
        ObjectContext c = DataContext.createDataContext();
        return buscaPorProdProc(c,idProducto,idProc);
    
    }
    public ArrayList<ProductoXProceso> buscaPorProdProc(ObjectContext c,int idProducto, int idProc) {
        
        Producto producto = spd.buscarPorId(c,idProducto);
        Proceso proceso=spc.buscarPorId(c, idProc);
        Expression q1 = ExpressionFactory.matchExp(ProductoXProceso.TO_PRODUCTO_PROPERTY,producto);
        Expression q2 = ExpressionFactory.matchExp(ProductoXProceso.TO_PROCESO_PROPERTY,proceso);
        Expression q3 = ExpressionFactory.greaterExp(ProductoXProceso.ESTADO_PROPERTY,0);
        Expression q4 = q1.andExp(q2.andExp(q1.andExp(q3)));
        SelectQuery s = new SelectQuery(ProductoXProceso.class, q4);
        ArrayList<ProductoXProceso> pp = (ArrayList<ProductoXProceso>)c.performQuery(s);
        
        return pp;
    
    }
    public Producto DevueveSalida(ObjectContext c,int idProducto, int idProc) {
        
        Producto producto = spd.buscarPorId(c,idProducto);
        Proceso proceso=spc.buscarPorId(c, idProc);
        Expression q1 = ExpressionFactory.matchExp(ProductoXProceso.TO_PRODUCTO_PROPERTY,producto);
        Expression q2 = ExpressionFactory.matchExp(ProductoXProceso.TO_PROCESO_PROPERTY,proceso);
        Expression q3 = ExpressionFactory.greaterExp(ProductoXProceso.ESTADO_PROPERTY,0);
        Expression q4 = ExpressionFactory.matchExp(ProductoXProceso.TIPOMOVIMIENTO_PROPERTY,1);
        Expression q5 = q1.andExp(q2.andExp(q1.andExp(q3.andExp(q4))));
     //   SelectQuery s = new SelectQuery(ProductoXProceso.class, q5);
        SelectQuery select = new SelectQuery(ProductoXProceso.class, q5);
        ProductoXProceso pp= (ProductoXProceso) DataObjectUtils.objectForQuery(c, select);
        Producto p=pp.getToProducto1();
        return p;
    
    }
    public Producto DevueveSalida(int idProducto, int idProc) {
        
       ObjectContext c = DataContext.createDataContext();
        return DevueveSalida(c,idProducto,idProc);
    
    }
}