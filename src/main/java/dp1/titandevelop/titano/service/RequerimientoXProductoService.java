/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.RequerimientoXProducto;
import java.util.ArrayList;
import java.util.Date;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.NamedQuery;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author andres
 */
public class RequerimientoXProductoService {
//    public ArrayList<RequerimientoXProducto> buscarPor(int idOrdProd, int idReq, Date fecha, int idProd, int estado) {
//        String[] keys = new String[] {"loginid", "password"};
//        Object[] values = new String[] {"joe", "secret"};
//
//        NamedQuery query = new NamedQuery("Login", keys, values);
//
//        return context.performQuery(query);
//    }
    
    public ArrayList<RequerimientoXProducto> buscarTodo(ObjectContext context) {
        if ( context == null ) {
            context = DataContext.createDataContext();
        }
               
        Expression q = ExpressionFactory.greaterExp(RequerimientoXProducto.ESTADO_PROPERTY, 0);
        SelectQuery s = new SelectQuery(Producto.class,q);
        ArrayList<RequerimientoXProducto> lp = (ArrayList<RequerimientoXProducto>) context.performQuery(s);
        return lp;
    }
    
    public ArrayList<RequerimientoXProducto> buscarPorRequerimiento(int idRequerimiento){
        Estado e= new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM requerimiento_x_producto WHERE idrequerimiento= "+idRequerimiento+"and estado >"+e.getEliminado()+"";
        
        SQLTemplate query = new SQLTemplate(RequerimientoXProducto.class,cadena);
        ArrayList<RequerimientoXProducto> lp = (ArrayList<RequerimientoXProducto>)context.performQuery(query);
        
        return lp;

        
    }
}
