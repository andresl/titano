/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.Perfil;
import dp1.titandevelop.titano.persistent.Permiso;
import dp1.titandevelop.titano.persistent.PermisoXPerfil;
import dp1.titandevelop.titano.persistent.TipoMovimiento;
import dp1.titandevelop.titano.persistent.Usuario;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author christianjuarez
 */
public class PerfilService {
    
   
    public List<Perfil> buscarPerfiles(String descripcion) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PERFIL WHERE DESCRIPCION LIKE '%"+descripcion+"%' AND ESTADO = 1";
        SQLTemplate query = new SQLTemplate(Perfil.class,cadena);
        
        List<Perfil> tipos = context.performQuery(query);       
        return tipos;
    }
    
     public List<Perfil> buscarTodosPerfiles() {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM PERFIL WHERE ESTADO = 1";
        SQLTemplate query = new SQLTemplate(Perfil.class,cadena);
        
        List<Perfil> tipos = context.performQuery(query);       
        return tipos;
    }
    
     public Perfil buscarPerfilporID(Integer idPerfil) {
        ObjectContext context = DataContext.createDataContext();
        Expression qualifier2 = ExpressionFactory.matchExp(Perfil.IDPERFIL_PK_COLUMN, idPerfil);
            SelectQuery select2 = new SelectQuery(Perfil.class, qualifier2);
        Perfil perf = (Perfil) DataObjectUtils.objectForQuery(context, select2);   
        return perf;
    } 
    
     
     public void eliminaPerfil(Integer idPerfil) {
       ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE  PERFIL SET ESTADO=0 WHERE IDPERFIL ="  +idPerfil+"";
        SQLTemplate query = new SQLTemplate(Perfil.class,cadena);
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Perfil");
        
      }
     
     public boolean buscarPerfilUsado(Integer idperfil) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM USUARIO WHERE IDPERFIL ="+idperfil+"";
        SQLTemplate query = new SQLTemplate(Usuario.class,cadena);
        
        List<Usuario> tipos = context.performQuery(query);       
        
        return (tipos.isEmpty());
    }
    
    public void crearPerfil(String nombrePerfil, ArrayList<Permiso> permisos) {
        ObjectContext context = DataContext.createDataContext();
        Perfil p = context.newObject(Perfil.class);
        p.setEstado(1);
        p.setDescripcion(nombrePerfil);
        context.commitChanges();
        
                 ObjectContext context2 = DataContext.createDataContext();
        Expression qualifier = ExpressionFactory.matchExp(Perfil.IDPERFIL_PK_COLUMN, p.getIdperfil());
        SelectQuery select = new SelectQuery(Perfil.class, qualifier);
        Perfil toPerfil = (Perfil) DataObjectUtils.objectForQuery(context2, select);
        

         for (Integer i = 0; i < permisos.size(); i++) {
                      
        ObjectContext context3 = DataContext.createDataContext();
        String cadena = "INSERT INTO PERMISO_X_PERFIL (IDPERFIL,IDPERMISO) VALUES ('"+toPerfil.getIdperfil()+"' "+
                        ",'"+permisos.get(i).getIdpermiso()+"')";
        SQLTemplate query = new SQLTemplate(PermisoXPerfil.class,cadena);
        
        context3.performQuery(query);
        context3.commitChanges();
         }
         
          String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Perfil");

    }
    
    public void actualizarPerfil(Integer idPerfil, ArrayList<Permiso> permisos) {
        ObjectContext context = DataContext.createDataContext();
        //me borro los perfiles registrados
        
        String cadena = "DELETE FROM PERMISO_X_PERFIL WHERE IDPERFIL ="+idPerfil+"";
        SQLTemplate query = new SQLTemplate(PermisoXPerfil.class,cadena);
        context.performQuery(query);
        context.commitChanges();
        // fin de borrar los perfiles registrados
        
        //registrando los nuevos permisos_x_perfil
        
        for (Integer i = 0; i < permisos.size(); i++) {

        String cadena2 = "INSERT INTO PERMISO_X_PERFIL (IDPERFIL,IDPERMISO) VALUES ("+idPerfil+" "+
                        ",'"+permisos.get(i).getIdpermiso()+"')";
        SQLTemplate query2 = new SQLTemplate(PermisoXPerfil.class,cadena2);    
        context.performQuery(query2);
        context.commitChanges();
         }
        
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Perfil");
       }
           

    
}
