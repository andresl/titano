/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.Perfil;
import dp1.titandevelop.titano.persistent.PermisoXPerfil;
import dp1.titandevelop.titano.persistent.Usuario;
import static dp1.titandevelop.titano.view.LoginT.empleadoLogin;
import static dp1.titandevelop.titano.view.LoginT.idspermisosxperfil;
import static dp1.titandevelop.titano.view.LoginT.perfilLogin;
import static dp1.titandevelop.titano.view.LoginT.permisosxperfil;
import static dp1.titandevelop.titano.view.LoginT.userLogin;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.map.SQLResult;
import org.apache.cayenne.query.SQLTemplate;

/**
 *
 * @author cHreS
 */
public class LoginService {
        
      public void logearse (String usuario, String password){
   
             ObjectContext context = DataContext.createDataContext();
         String cadena = "SELECT * FROM USUARIO WHERE USUARIO = '"+usuario+"'AND ESTADO=1 AND PASSWORD='"+password+"'";      
         SQLTemplate query = new SQLTemplate(Usuario.class,cadena);        
         List<Usuario> usuarios = context.performQuery(query);    
         userLogin=usuarios.get(0);
         
         String cadena3 = "SELECT IDPERFIL S FROM USUARIO WHERE USUARIO = '"+usuario+"' AND ESTADO=1 AND PASSWORD='"+password+"'";      
         SQLTemplate query3 = new SQLTemplate(Usuario.class,cadena3);        
         SQLResult resultado = new SQLResult();
         resultado.addColumnResult("S");
         query3.setResult(resultado);
         Number idPerfil = (Number) DataObjectUtils.objectForQuery(context, query3);
         
         String cadena6 = "SELECT IDEMPLEADO S FROM USUARIO WHERE USUARIO = '"+usuario+"' AND ESTADO=1 AND PASSWORD='"+password+"'";      
         SQLTemplate query6 = new SQLTemplate(Usuario.class,cadena6);        
         SQLResult resultado3 = new SQLResult();
         resultado3.addColumnResult("S");
         query6.setResult(resultado3);
         Number idEmpleado = (Number) DataObjectUtils.objectForQuery(context, query6);
       
        String cadena2 = "SELECT * FROM PERMISO_X_PERFIL WHERE IDPERFIL = "+idPerfil+""; 
         SQLTemplate query2  = new SQLTemplate(PermisoXPerfil.class,cadena2);
         permisosxperfil = context.performQuery(query2);
          
         String cadena4 = "SELECT IDPERMISO S FROM PERMISO_X_PERFIL WHERE IDPERFIL = "+idPerfil+"";     
         SQLTemplate query4 = new SQLTemplate(Usuario.class,cadena4);        
         SQLResult resultado2 = new SQLResult();
         resultado2.addColumnResult("S");
         query4.setResult(resultado2);
         idspermisosxperfil = context.performQuery(query4);
         
         String cadena5 = "SELECT * FROM PERFIL WHERE IDPERFIL = "+idPerfil+""; 
         SQLTemplate query5  = new SQLTemplate(Perfil.class,cadena5);
         List<Perfil> perfiles= context.performQuery(query5);
         perfilLogin = perfiles.get(0);
         
          String cadena7 = "SELECT * FROM EMPLEADO WHERE IDEMPLEADO = "+idEmpleado+""; 
         SQLTemplate query7  = new SQLTemplate(Empleado.class,cadena7);
         List<Empleado> empleados= context.performQuery(query7);
         empleadoLogin = empleados.get(0);
        }
      
         
}
