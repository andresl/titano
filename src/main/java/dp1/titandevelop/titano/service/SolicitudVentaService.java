/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.bean.FiltroSV;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.DetalleDamanda;
import dp1.titandevelop.titano.persistent.DetalleSolicitudVenta;
import dp1.titandevelop.titano.persistent.Distribuidor;
import dp1.titandevelop.titano.persistent.Producto;

import dp1.titandevelop.titano.persistent.Producto;
import dp1.titandevelop.titano.persistent.SolicitudVenta;
import dp1.titandevelop.titano.persistent.TipoProducto;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Heli
 */
public class SolicitudVentaService {
    
     public SolicitudVenta buscarPorId ( ObjectContext c,  int id ) {
        //ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(SolicitudVenta.IDSOLICITUDVENTA_PK_COLUMN, id);
        SelectQuery s = new SelectQuery(SolicitudVenta.class, q);
        return (SolicitudVenta) DataObjectUtils.objectForQuery(c, s);
    }
     public SolicitudVenta buscarPorId (int id ) {
        ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(SolicitudVenta.IDSOLICITUDVENTA_PK_COLUMN, id);
        SelectQuery s = new SelectQuery(SolicitudVenta.class, q);
        return (SolicitudVenta) DataObjectUtils.objectForQuery(c, s);
    }
    public List<SolicitudVenta> buscarFiltrado(FiltroSV f) {
        try{
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT distinct  A.*, B.idDistribuidor, B.razonsocial FROM SOLICITUD_VENTA A  INNER JOIN  DISTRIBUIDOR B ON (A.IDDISTRIBUIDOR=B.IDDISTRIBUIDOR) INNER JOIN  DETALLE_SOLICITUD_VENTA C ON  (C.IDSOLICITUDVENTA=A.IDSOLICITUDVENTA) where  B.ESTADO=1 ";
        String where=" AND ";
        String conector=" AND ";
        if(f.getEstado()!=null && f.getEstado()!=0){cadena+=where+"A.ESTADO="+f.getEstado()+" AND C.ESTADO="+f.getEstado(); where=" AND ";}
        else{
        if(f.getEstado()==0)cadena+=where+"A.ESTADO>0 AND C.ESTADO>0"; where=" AND ";
        }
        if(f.getFechaIni()!=null){cadena+=where+"A.FECHA>='"+f.getFechaIni()+"'"; where=" AND ";}
        if(f.getFechaFin()!=null){cadena+=where+"A.FECHA<='"+f.getFechaFin()+"'"; where=" AND ";}
        if(f.getDistribuidor()!=null){cadena+=where+"B.RAZONSOCIAL ilike '%"+f.getDistribuidor()+"%'"; where=" AND ";}
        
System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(SolicitudVenta.class,cadena);
        
        List<SolicitudVenta> solicitudes = context.performQuery(query);       
        return solicitudes;
        }catch(Exception e){
        System.out.println("Ha ocurrido problemas de conexion");
        return null;
        }
    }
    
    public String numeroSolicitudView(Integer id){
        if(id==0) return "";
    ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM SOLICITUD_VENTA WHERE IDSOLICITUDVENTA<="+id+" AND IDSOLICITUDVENTA>0  order by IDSOLICITUDVENTA ASC";
        SQLTemplate query = new SQLTemplate(SolicitudVenta.class,cadena);
        
        List<SolicitudVenta> documentos= context.performQuery(query);       
        String cadena2;
        cadena2="0";
        //cantidad de 0s= 7
                if(documentos.size()>=0 && documentos.size()<10) cadena2="000000"+documentos.size();
                if(documentos.size()>=10 && documentos.size()<100) cadena2="00000"+documentos.size();
                if(documentos.size()>=100 && documentos.size()<1000) cadena2="0000"+documentos.size();
                if(documentos.size()>=1000 && documentos.size()<10000) cadena2="000"+documentos.size();
                if(documentos.size()>=10000 && documentos.size()<100000) cadena2="00"+documentos.size();
                if(documentos.size()>=100000 && documentos.size()<1000000) cadena2="0"+documentos.size();
                if(documentos.size()>=1000000 && documentos.size()<10000000) cadena2=""+documentos.size();
                
        return cadena2;
    }
    
    
    public List<DetalleSolicitudVenta> buscarFiltrado2(Integer idSolicitudVenta) {
        
        Estado state=new Estado();
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM DETALLE_SOLICITUD_VENTA WHERE IDSOLICITUDVENTA="+idSolicitudVenta+" AND ESTADO<>"+state.getEliminado();
        
System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(DetalleSolicitudVenta.class,cadena);
        
        List<DetalleSolicitudVenta> solicitudes = context.performQuery(query);       
        return solicitudes;
    }
    
    
    public List<SolicitudVenta> buscarSolicitudVenta(String distribuidor) {
        
        Estado state=new Estado();
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT A.*, B.* FROM SOLICITUD_VENTA A, DISTRIBUIDOR B WHERE   A.IDDISTRIBUIDOR=B.IDDISTRIBUIDOR";
        String where=" AND ";
        String conector=" AND ";
        if(distribuidor!=null){cadena+=where+"B.RAZONSOCIAL ilike '%"+distribuidor+"%'"; where=" AND ";}
        cadena+=where+"A.ESTADO="+state.getPendiente()+" AND B.ESTADO=1 ORDER BY A.FECHA DESC";
System.out.println(cadena);
        SQLTemplate query = new SQLTemplate(SolicitudVenta.class,cadena);
        
        List<SolicitudVenta> solicitudes = context.performQuery(query);       
        return solicitudes;
    }
    
    public void  insertarSolicitudVentaMasiva (SolicitudVenta s){
    try{
        Estado state=new Estado();             
        ObjectContext context = DataContext.createDataContext();    
        Expression qualifier = ExpressionFactory.matchExp(Distribuidor.IDDISTRIBUIDOR_PK_COLUMN, s.getToDistribuidor().getIddistribuidor());
        SelectQuery select = new SelectQuery(Distribuidor.class, qualifier);
        Distribuidor d = (Distribuidor) DataObjectUtils.objectForQuery(context, select);
        
        SolicitudVenta sv = new SolicitudVenta();//context.newObject(SolicitudVenta.class);
        sv.setIdsolicitudventa(s.getIdsolicitudventa());
        sv.setToDistribuidor(d);
        sv.setFecha(s.getFecha());
        sv.setTotal(s.getTotal());
        sv.setEstado(s.getEstado());
        
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Solicitud Venta");
    }catch(Exception e){
        
        e.printStackTrace();
        System.out.println("me fui a la mierda");
        }
    }
    
    public void  insertarDetalleSolicitudVentaMasiva (DetalleSolicitudVenta dSv){
    try{
        ObjectContext context2 = DataContext.createDataContext();
            
            Expression qualifier2 = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, dSv.getToProducto().getIdproducto());
        SelectQuery select2 = new SelectQuery(Producto.class, qualifier2);
        Producto p = (Producto) DataObjectUtils.objectForQuery(context2, select2);
            
            Expression qualifier3 = ExpressionFactory.matchExp(SolicitudVenta.IDSOLICITUDVENTA_PK_COLUMN, dSv.getToSolicitudVenta().getIdsolicitudventa());
        SelectQuery select3 = new SelectQuery(SolicitudVenta.class, qualifier3);
        SolicitudVenta sv2 = (SolicitudVenta) DataObjectUtils.objectForQuery(context2, select3); 
            
        ObjectContext context3 = DataContext.createDataContext();    
        
        String cadena = "INSERT INTO DETALLE_SOLICITUD_VENTA (IDSOLICITUDVENTA,IDPRODUCTO,CANTIDAD,SUBTOTAL,ESTADO) VALUES ('"
                +sv2.getIdsolicitudventa()+"','"+p.getIdproducto()+"',"+dSv.getCantidad()+","+dSv.getSubtotal()+","+dSv.getEstado()+")";
        SQLTemplate query = new SQLTemplate(DetalleSolicitudVenta.class,cadena);
        
        context3.performQuery(query);
        context3.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Detalle_Solicitud_Venta");
        
    }catch(Exception e){
        
        e.printStackTrace();
        }
    }
    
    public void  insertar (SolicitudVenta s,List<DetalleSolicitudVenta> dSv){
        try{
        Estado state=new Estado();             
        ObjectContext context = DataContext.createDataContext();    
        Expression qualifier = ExpressionFactory.matchExp(Distribuidor.IDDISTRIBUIDOR_PK_COLUMN, s.getToDistribuidor().getIddistribuidor());
        SelectQuery select = new SelectQuery(Distribuidor.class, qualifier);
        Distribuidor d = (Distribuidor) DataObjectUtils.objectForQuery(context, select);
        
        SolicitudVenta sv = context.newObject(SolicitudVenta.class);
        System.out.println("llegue aki 1");
        sv.setToDistribuidor(d);
        System.out.println("llegue aki 2");
        sv.setFecha(s.getFecha());
        System.out.println("llegue aki 3");
        sv.setTotal(s.getTotal());
        System.out.println("llegue aki 4");
        sv.setEstado(state.getPendiente());
        System.out.println("llegue aki 5");
        
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Solicitid Venta");
        /*
        ObjectContext context = DataContext.createDataContext();
        String cadena = "INSERT INTO DEMANDA (FECHA,DESCRIPCION,ESTADO) VALUES ('"+d.getFecha()+"','"+d.getDescripcion()+"',"+1+")";
        SQLTemplate query = new SQLTemplate(Demanda.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        */
        
        System.out.println("yeehhh");
        for(int i=0;i<dSv.size();i++){
        ObjectContext context2 = DataContext.createDataContext();
            
            Expression qualifier2 = ExpressionFactory.matchExp(Producto.IDPRODUCTO_PK_COLUMN, dSv.get(i).getToProducto().getIdproducto());
        SelectQuery select2 = new SelectQuery(Producto.class, qualifier2);
        Producto p = (Producto) DataObjectUtils.objectForQuery(context2, select2);
            
            Expression qualifier3 = ExpressionFactory.matchExp(SolicitudVenta.IDSOLICITUDVENTA_PK_COLUMN, sv.getIdsolicitudventa());
        SelectQuery select3 = new SelectQuery(SolicitudVenta.class, qualifier3);
        SolicitudVenta sv2 = (SolicitudVenta) DataObjectUtils.objectForQuery(context2, select3); 
            
        ObjectContext context3 = DataContext.createDataContext();    
        
        String cadena = "INSERT INTO DETALLE_SOLICITUD_VENTA (IDSOLICITUDVENTA,IDPRODUCTO,CANTIDAD,SUBTOTAL,ESTADO) VALUES ('"
                +sv2.getIdsolicitudventa()+"','"+p.getIdproducto()+"',"+dSv.get(i).getCantidad()+","+dSv.get(i).getSubtotal()+","+sv.getEstado()+")";
        SQLTemplate query = new SQLTemplate(DetalleSolicitudVenta.class,cadena);
        
        context3.performQuery(query);

        as.auditoriaInsert(usuario, perfil, "Detalle_Solicitud_Venta");
        
        
        /*
        DetalleSolicitudVenta dsv = context2.newObject(DetalleSolicitudVenta.class);
            dsv.setCantidad(dSv.get(i).getCantidad());
            dsv.setSubtotal(dSv.get(i).getSubtotal());
            dsv.setEstado(dSv.get(i).getEstado());
            System.out.println("llegue aki 6");
            dsv.setToProducto(p);
            System.out.println("llegue aki 7");
            dsv.setToSolicitudVenta(sv2);
            System.out.println("llegue aki 8 "+sv.getIdsolicitudventa());
            System.out.println(dsv.getToProducto().getIdproducto()+" "+dsv.getToSolicitudVenta().getIdsolicitudventa() );
          */  context3.commitChanges();
            System.out.println("win");
            //this.agregarItem(dSv.get(i));
        }
        }catch(Exception e){
        System.out.println("Ha ocurrido un error al guardar");
        }
        
    }
    
    public List<Producto> listProduct(){
     ObjectContext context = DataContext.createDataContext();
        
        String cadena = "SELECT * FROM PRODUCTO WHERE IDTIPOPRODUCTO=3";
        SQLTemplate query = new SQLTemplate(Producto.class,cadena);
        
        List<Producto> productos = context.performQuery(query);       
        return productos;
        
    }
    
    
    
    public void  editar (SolicitudVenta s,List<DetalleSolicitudVenta> dSv){
        
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE SOLICITUD_VENTA SET FECHA = '"+s.getFecha()+"',IDDISTRIBUIDOR="+s.getToDistribuidor().getIddistribuidor()+",ESTADO="+s.getEstado()+
                        " WHERE IDSOLICITUDVENTA="+s.getIdsolicitudventa()+"";
        SQLTemplate query = new SQLTemplate(SolicitudVenta.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
             
        ObjectContext context2 = DataContext.createDataContext();       
        String cadena2 = "UPDATE DETALLE_SOLICITUD_VENTA SET ESTADO = "+s.getEstado()
                +" WHERE IDSOLICITUDVENTA="+s.getIdsolicitudventa()+"";
        SQLTemplate query2 = new SQLTemplate(SolicitudVenta.class,cadena2);
        
        context2.performQuery(query2);
        context2.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Solicitud Venta");
  
        as.auditoriaUpdate(usuario, perfil, "Detalle_Solicitud_Venta");
        
    }
    
    public void  editarItem (DetalleSolicitudVenta ds){
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE DETALLE_SOLICITUD_VENTA SET IDPRODUCTO ="+ds.getToProducto().getIdproducto()+",CANTIDAD="+ds.getCantidad()+",SUBTOTAL="+ds.getSubtotal()+",ESTADO="+ds.getEstado()+
                        " WHERE IDDETALLESOLICITUDVENTA="+ds.getIddetallesolicitudventa()+"";
        SQLTemplate query = new SQLTemplate(DetalleSolicitudVenta.class,cadena);
        System.out.println(cadena);
        context.performQuery(query);
        context.commitChanges();
          String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Detalle_Solicitud_Venta");    
        
    }
   
    /*
    public void  editarItem2 (int idDetalleSolicitud, int cantidad){
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE DETALLE_SOLICITUD_VENTA SET CANTIDAD ="+cantidad+",CANTIDAD="+ds.getCantidad()+",SUBTOTAL="+ds.getSubtotal()+",ESTADO="+ds.getEstado()+
                        " WHERE IDDETALLESOLICITUDVENTA="+ds.getIddetallesolicitudventa()+"";
        SQLTemplate query = new SQLTemplate(DetalleSolicitudVenta.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
          String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Detalle_Solicitud_Venta");    
        
    }
    
    */
    
    
    public void  agregarItem (DetalleSolicitudVenta ds){
        System.out.println(ds.getToSolicitudVenta().getIdsolicitudventa()+" " +ds.getCantidad()+" "+ds.getSubtotal()+" "+ds.getEstado()+" "+ds.getToProducto().getIdproducto());
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "INSERT INTO DETALLE_SOLICITUD_VENTA (IDSOLICITUDVENTA,IDPRODUCTO,CANTIDAD,SUBTOTAL,ESTADO)"
                + " VALUES("+ds.getToSolicitudVenta().getIdsolicitudventa()+","+ds.getToProducto().getIdproducto()+","+ds.getCantidad()+","+ds.getSubtotal()+","+ds.getEstado()+")";
        SQLTemplate query = new SQLTemplate(DetalleSolicitudVenta.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Detalle_Solicitud_Venta");
             
        
    }
    
    
    public void eliminar(int idSolicitud){
        Estado state=new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE SOLICITUD_VENTA SET ESTADO "+state.getEliminado()+ 
                        " WHERE IDSOLICITUDVENTA="+idSolicitud+"";
        SQLTemplate query = new SQLTemplate(SolicitudVenta.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
        
        ObjectContext context2 = DataContext.createDataContext();
        String cadena2 = "UPDATE DETALLE_SOLICITUD_VENTA SET ESTADO="+state.getEliminado()+ 
                        " WHERE IDSOLICITUDVENTA="+idSolicitud+"";
        SQLTemplate query2 = new SQLTemplate(DetalleSolicitudVenta.class,cadena2);
        
        context2.performQuery(query2);
        context2.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Solicitud Venta");
     
        as.auditoriaInsert(usuario, perfil, "Detalle_Solicitud_Venta");
        
    }
    
    public void eliminarItemDetalle(int idDetalleSolicitud){
        Estado state=new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE DETALLE_SOLICITUD_VENTA SET ESTADO="+state.getEliminado()+ 
                        " WHERE IDDETALLESOLICITUDVENTA="+idDetalleSolicitud+"";
        SQLTemplate query = new SQLTemplate(DetalleSolicitudVenta.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Detalle_Solicitud_Venta");
        
    }
    
}
