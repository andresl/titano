/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.persistent.Zona;
import dp1.titandevelop.titano.view.LoginT;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Yulian
 */
public class ZonaService {

    public List<Zona> buscar() {
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM ZONA WHERE ESTADO = 1";
        SQLTemplate query = new SQLTemplate(Zona.class,cadena);        
        List<Zona> zonas = context.performQuery(query);       
     
        
        return zonas;
    }
    
    public Zona buscar_x_id(int id){
        ObjectContext c = DataContext.createDataContext();
        Expression q = ExpressionFactory.matchExp(Zona.IDZONA_PK_COLUMN, "%"+id+"%");
        SelectQuery s = new SelectQuery(Zona.class, q);
        return (Zona) DataObjectUtils.objectForQuery(c, s);
    }
    
}
