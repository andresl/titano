/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Maquina;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.TipoEmpleado;
import dp1.titandevelop.titano.persistent.Turno;
import dp1.titandevelop.titano.persistent.Usuario;
import dp1.titandevelop.titano.view.LoginT;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;

/**
 *
 * @author Usuario1
 */
public class EmpleadoService {
    
    public List<Empleado> buscarTOperario() {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO WHERE  IDTIPOEMPLEADO= 2 AND ESTADO = "+estado.getActivo()+"";
        SQLTemplate query = new SQLTemplate(Empleado.class,cadena);
        
        List<Empleado> tipos = context.performQuery(query);       
        return tipos;
    }
    
    public  void Asignar(int idEmpleado){
                      
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "UPDATE EMPLEADO SET  ASIGNADO = true  WHERE IDEMPLEADO = "+idEmpleado+"";
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
     }
    
    public  void desasignar(){
                      
        ObjectContext context = DataContext.createDataContext();
        
        String cadena = "UPDATE EMPLEADO SET  ASIGNADO = false";
        SQLTemplate query = new SQLTemplate(Maquina.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
     }
    
     public List<Empleado> buscar() {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO WHERE ESTADO>0";
        SQLTemplate query = new SQLTemplate(Empleado.class,cadena);
        
        List<Empleado> tipos = context.performQuery(query);       
        return tipos;
    }
     //para tipo de empleados
    public List<TipoEmpleado> buscarTEmpleado(String text) {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TIPO_EMPLEADO WHERE DESCRIPCION LIKE '%"+text+"%' AND ESTADO = "+estado.getActivo()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cadena);
        
        List<TipoEmpleado> tipos = context.performQuery(query);       
        return tipos;
    }
    
    public List<TipoEmpleado> buscarTEmp() {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TIPO_EMPLEADO WHERE  ESTADO = "+estado.getActivo()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cadena);
        
        List<TipoEmpleado> tipos = context.performQuery(query);       
        return tipos;
    }
    public List<TipoEmpleado> buscarTEmpSinOperario() {
        Estado estado = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TIPO_EMPLEADO WHERE  DESCRIPCION NOT LIKE '%perar%' AND ESTADO = "+estado.getActivo()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cadena);
        
        List<TipoEmpleado> tipos = context.performQuery(query);       
        return tipos;
    }
    
     public List<Empleado> buscarEmpleadoParametros(String nombre, String apPaterno, String apMaterno) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO WHERE NOMBRES LIKE '%"+nombre+"%' AND ESTADO = 1 AND APELLIDOP LIKE '%"+apPaterno+"%' AND"
                + " APELLIDOM LIKE '%"+apMaterno+"%'";
        SQLTemplate query = new SQLTemplate(Empleado.class,cadena);
        
        List<Empleado> tipos = context.performQuery(query);       
        return tipos;
    }
     
    public List<Empleado> buscarEmpleadoParametrosUsuario(String username, String nombre, String apaterno, String amaterno) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena1= "select e.idempleado, e.idtipoempleado, e.nombres, e.apellidop, e.apellidom, e.direccion, e.fechanacimiento, e.dni, e.telefono, e.celular, e.email, e.estado, e.asignado"
       +" from empleado e inner join usuario u on (e.idempleado=u.idempleado)  "
       +"where u.usuario like '%"+username+"%' "
        +"and e.nombres like '%"+nombre+"%' "
       +"and e.apellidop like '%"+apaterno+"%' "
        +"and e.apellidom like '%"+amaterno+"%'"
                + "and u.estado=1";
        SQLTemplate query1= new SQLTemplate(Empleado.class,cadena1);
        List<Empleado> tipos = context.performQuery(query1);       
        return tipos;
    } 
     
     public List<Empleado> buscarEmpleadoParametrosValidos(String nombre, String apPaterno, String apMaterno) {
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO WHERE NOMBRES LIKE '%"+nombre+"%' AND ESTADO = 1 AND APELLIDOP LIKE '%"+apPaterno+"%' AND"
                + " APELLIDOM LIKE '%"+apMaterno+"%'"
                + "AND IDEMPLEADO NOT IN (SELECT IDEMPLEADO FROM USUARIO)";
        SQLTemplate query = new SQLTemplate(Empleado.class,cadena);
        
        List<Empleado> tipos = context.performQuery(query);       
       
        
        return tipos;
    } 
     
     public List<Empleado> buscarEmpleadoParametrosValidos1(String nombre, String apPaterno, String apMaterno) {
        List<Empleado> aux=new ArrayList();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO WHERE NOMBRES LIKE '%"+nombre+"%' AND ESTADO = 1 AND APELLIDOP LIKE '%"+apPaterno+"%' AND"
                + " APELLIDOM LIKE '%"+apMaterno+"%'"
                + "AND IDEMPLEADO NOT IN (SELECT IDEMPLEADO FROM USUARIO)"
                + " AND IDTIPOEMPLEADO<>2";
        SQLTemplate query = new SQLTemplate(Empleado.class,cadena);
        
        List<Empleado> tipos = context.performQuery(query);
        
//        for(int i=0;i<tipos.size();i++){
//            if(tipos.get(i).getUsuarioArray().isEmpty()){
//                aux.add(tipos.get(i));
//            }
//         
//        }
       
        return tipos;
    } 
    
    
     public Empleado buscarEmpleadoporID(ObjectContext context,Integer idEmpleado) {
      
         Expression qualifier = ExpressionFactory.matchExp(Empleado.IDEMPLEADO_PK_COLUMN,idEmpleado);
            SelectQuery select = new SelectQuery(Empleado.class, qualifier);
            Empleado emp = (Empleado) DataObjectUtils.objectForQuery(context, select);     
        return emp;
    } 
      public Empleado buscarEmpleadoporID(Integer idEmpleado) {
        ObjectContext context = DataContext.createDataContext();
         Expression qualifier = ExpressionFactory.matchExp(Empleado.IDEMPLEADO_PK_COLUMN,idEmpleado);
            SelectQuery select = new SelectQuery(Empleado.class, qualifier);
            Empleado emp = (Empleado) DataObjectUtils.objectForQuery(context, select);     
        return emp;
    } 
    public void  insertarTEmp (String estado){
        ObjectContext context = DataContext.createDataContext();
        TipoEmpleado templeado = context.newObject(TipoEmpleado.class);
        templeado.setDescripcion(estado);
        templeado.setEstado(1);
        context.commitChanges();
    }
    public void  editarTEmp (String estado, int idemp){
        ObjectContext context = DataContext.createDataContext();       
        String cadena = "UPDATE TIPO_EMPLEADO SET DESCRIPCION = '"+estado+ "'  WHERE IDTIPOEMPLEADO="+idemp+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
    }
    public void eliminarTEmp(int idemp){
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "UPDATE TIPO_EMPLEADO SET ESTADO ="+est.getEliminado()+""+ 
                        "WHERE IDTIPOEMPLEADO="+idemp+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cadena);
        
        context.performQuery(query);
        context.commitChanges();
    }
    
    //para empleados
    public void  insertarEmpleado (int temp, String nombre, String apepat, String apemat, String direcc, Date fecha, String dni, String sexo, String tlf, String cel, String mail){
        ObjectContext context = DataContext.createDataContext();
        
        Expression qualifier = ExpressionFactory.matchExp(TipoEmpleado.IDTIPOEMPLEADO_PROPERTY,String.valueOf(temp));
        SelectQuery select = new SelectQuery(TipoEmpleado.class, qualifier);
        TipoEmpleado t = (TipoEmpleado) DataObjectUtils.objectForQuery(context, select);
        
        
        Empleado  emp = context.newObject(Empleado.class);
        Estado est = new Estado();
        emp.setNombres(nombre);
        emp.setApellidom(apemat);
        emp.setApellidop(apepat);
        emp.setDireccion(direcc);
        emp.setFechanacimiento(fecha);
        emp.setDni(dni);
        emp.setSexo(sexo);
        emp.setTelefono(tlf);
        emp.setEmail(mail);
        emp.setCelular(cel);
        emp.setFechanacimiento(fecha);
        emp.setEstado(est.getActivo());
        emp.setToTipoEmpleado(t);
        emp.setAsignado(Boolean.FALSE);
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Empleado");
    }
    
    // ESTO ES PARA CUANDO EL EMPLEADO ES DE TIPO OPERARIO
    
    public void  insertarOperario (int temp, String nombre, String apepat, String apemat, String direcc, Date fecha, String dni, String sexo, String tlf, String cel, String mail ,ArrayList<Integer> procesos,ArrayList<Integer> prdcts, ArrayList<Integer> merms, ArrayList<Integer> idturnos){
        ObjectContext context = DataContext.createDataContext();
  
        Expression qualifier = ExpressionFactory.matchExp(TipoEmpleado.IDTIPOEMPLEADO_PROPERTY,String.valueOf(temp));
        SelectQuery select = new SelectQuery(TipoEmpleado.class, qualifier);
        TipoEmpleado t = (TipoEmpleado) DataObjectUtils.objectForQuery(context, select);
        
        
        Empleado  emp = context.newObject(Empleado.class);
        Estado est = new Estado();
        emp.setNombres(nombre);
        emp.setApellidom(apemat);
        emp.setApellidop(apepat);
        emp.setDireccion(direcc);
        emp.setFechanacimiento(fecha);
        emp.setDni(dni);
        emp.setSexo(sexo);
        emp.setTelefono(tlf);
        emp.setEmail(mail);
        emp.setCelular(cel);
        emp.setFechanacimiento(fecha);
        emp.setEstado(est.getActivo());
        emp.setToTipoEmpleado(t);
        emp.setAsignado(Boolean.FALSE);
        //le insertamos su productividad y su merma
        int cont=0;
        while (cont<idturnos.size())
        {   EmpleadoXProcesoTurno  ept = context.newObject(EmpleadoXProcesoTurno.class);
        
            //esto es para turno
//            Expression qualifiers = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY, idturnos.get(cont).getIdturno());
            Expression qualifiers = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY, idturnos.get(cont));
            SelectQuery selects = new SelectQuery(Turno.class, qualifiers);
            Turno turn = (Turno)DataObjectUtils.objectForQuery(context, selects);
            //esto es para proceso
            Expression qualifiers2 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PROPERTY,String.valueOf(procesos.get(cont)));
            SelectQuery selects2 = new SelectQuery(Proceso.class, qualifiers2);
            Proceso proc = (Proceso)DataObjectUtils.objectForQuery(context, selects2); 
                       
            ept.setToEmpleado(emp);
            ept.setToProceso(proc);
            ept.setToTurno(turn);
            ept.setProductividad(Float.parseFloat(prdcts.get(cont)+""));
            ept.setRotura(Float.parseFloat(merms.get(cont)+""));
            ept.setFo(0.0f);
            ept.setEstado(est.getActivo());
            context.commitChanges(); 
            cont++;
        }   
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Empleado");
    }
    // FIN DE INSERT DE EMPLEADO TIPO OPERARIO
    public void  insertarOperarioMasivo(int idEmpleado, int temp, String nombre, String apepat, String apemat, String direcc, Date fecha, String dni, String sexo, String tlf, String cel, String mail){
        ObjectContext context = DataContext.createDataContext();
  
        Expression qualifier = ExpressionFactory.matchExp(TipoEmpleado.IDTIPOEMPLEADO_PROPERTY,String.valueOf(temp));
        SelectQuery select = new SelectQuery(TipoEmpleado.class, qualifier);
        TipoEmpleado t = (TipoEmpleado) DataObjectUtils.objectForQuery(context, select);
        
        
        Empleado  emp = context.newObject(Empleado.class);
        Estado est = new Estado();
        emp.setIdempleado(idEmpleado);
        emp.setNombres(nombre);
        emp.setApellidom(apemat);
        emp.setApellidop(apepat);
        emp.setDireccion(direcc);
        emp.setFechanacimiento(fecha);
        emp.setDni(dni);
        emp.setSexo(sexo);
        emp.setTelefono(tlf);
        emp.setEmail(mail);
        emp.setCelular(cel);
        emp.setFechanacimiento(fecha);
        emp.setEstado(est.getActivo());
        emp.setToTipoEmpleado(t);
        emp.setAsignado(Boolean.FALSE);
        //le insertamos su productividad y su merma
       context.commitChanges();
        String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "Empleado Masivo");
    }
     public void  insertarEmpleadoxprocesoturno (int idEmpleado, ArrayList<Integer> procesos,ArrayList<Float> prdcts, ArrayList<Float> merms, ArrayList<Integer> idturnos){
        ObjectContext context = DataContext.createDataContext();
  
       
        Empleado  emp =  this.buscarEmpleadoporID(context,idEmpleado);
      //  this.buscaremplead
       
        //le insertamos su productividad y su merma
        int cont=0;
        while (cont<idturnos.size())
        {   EmpleadoXProcesoTurno  ept = context.newObject(EmpleadoXProcesoTurno.class);
        
            //esto es para turno
//            Expression qualifiers = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY, idturnos.get(cont).getIdturno());
            Expression qualifiers = ExpressionFactory.matchExp(Turno.IDTURNO_PROPERTY, idturnos.get(cont));
            SelectQuery selects = new SelectQuery(Turno.class, qualifiers);
            Turno turn = (Turno)DataObjectUtils.objectForQuery(context, selects);
            //esto es para proceso
            Expression qualifiers2 = ExpressionFactory.matchExp(Proceso.IDPROCESO_PROPERTY,String.valueOf(procesos.get(cont)));
            SelectQuery selects2 = new SelectQuery(Proceso.class, qualifiers2);
            Proceso proc = (Proceso)DataObjectUtils.objectForQuery(context, selects2); 
                       
            ept.setToEmpleado(emp);
            ept.setToProceso(proc);
            ept.setToTurno(turn);
            ept.setProductividad(prdcts.get(cont));
            ept.setRotura(merms.get(cont));
            ept.setFo(0.0f);
            ept.setEstado(1);
            context.commitChanges(); 
            cont++;
        }   
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaInsert(usuario, perfil, "EmpleadoXProcesoTurno");
    }
   
    public List <Empleado> buscarEmpleadoT(String dni,String nombre, String apepat, String sexo, int tipoemp){
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from empleado "
                + " where nombres like '%"+nombre+"%' "
                + " and apellidop like '%"+apepat+"%' "
                + " and sexo like '%"+sexo+"%' "
                + " and dni like '%"+dni+"%' "
                + " and idtipoempleado = "+tipoemp+""
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> tipos = context.performQuery(query);
        
        return tipos;
    }
    public List <Empleado> buscarEmpleadoT(String dni,String nombre, String apepat, int tipoemp){
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "select * from empleado "
                + " where nombres like '%"+nombre+"%' "
                + " and apellidop like '%"+apepat+"%' "
                + " and dni like '%"+dni+"%' "
                + " and idtipoempleado = "+tipoemp+""
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> tipos = context.performQuery(query);
        
        return tipos;
    }
   
    public List <Empleado> buscarEmpleado(String dni,String nombre, String apepat, String sexo){
        Estado est = new Estado();
        int tipoemp=0 ;
        ObjectContext context = DataContext.createDataContext();
        
        String cad1 = "select * from TIPO_EMPLEADO "
                + " where descripcion    like '%perar%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cad1); 
        List<TipoEmpleado> tipos = context.performQuery(query);
        for (int i = 0; i<tipos.size();i++){
            tipoemp = tipos.get(i).getIdtipoempleado();
        }
        
        String cadena = "select * from empleado "
                + " where nombres like '%"+nombre+"%' "
                + " and apellidop like '%"+apepat+"%' "
                + " and sexo like '%"+sexo+"%' "
                + " and dni like '%"+dni+"%' "
                + " and idtipoempleado <> "+tipoemp+""
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> tipos2 = context.performQuery(query2);
        
        return tipos2;
    }
    public List <Empleado> buscarEmpleado(String dni,String nombre, String apepat){
        Estado est = new Estado();
        int tipoemp=0 ;
        ObjectContext context = DataContext.createDataContext();
        
        String cad1 = "select * from TIPO_EMPLEADO "
                + " where descripcion    like '%perar%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cad1); 
        List<TipoEmpleado> tipos = context.performQuery(query);
        for (int i = 0; i<tipos.size();i++){
            tipoemp = tipos.get(i).getIdtipoempleado();
        }
        
        String cadena = "select * from empleado "
                + " where nombres like '%"+nombre+"%' "
                + " and apellidop like '%"+apepat+"%' "
                + " and dni like '%"+dni+"%' "
                + " and idtipoempleado <> "+tipoemp+""
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> tipos2 = context.performQuery(query2);
        
        return tipos2;
    }
    
    public List<Empleado> BuscarEmpleadoSinUsuario(){
        Estado est = new Estado();
        int tipoemp=0 ;
        List<Empleado> aux=new ArrayList();
        ObjectContext context = DataContext.createDataContext();
        String cad1 = "select * from TIPO_EMPLEADO "
                + " where descripcion    like '%perar%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cad1); 
        List<TipoEmpleado> tipos = context.performQuery(query);
        for (int i = 0; i<tipos.size();i++){
            tipoemp = tipos.get(i).getIdtipoempleado();
        }
        String cadena = "select * from empleado "
                + " where idtipoempleado <> "+tipoemp+""
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> tipos2 = context.performQuery(query2);
        
        for(int i=0;i<tipos2.size();i++){
            if(tipos2.get(i).getUsuarioArray().isEmpty()){
                aux.add(tipos2.get(i));
            }
        }
        
        return aux;
    }
    
    public List <Empleado> listarEmpleados(){
        Estado est = new Estado();
        int tipoemp=0 ;
        ObjectContext context = DataContext.createDataContext();
        
        String cad1 = "select * from TIPO_EMPLEADO "
                + " where descripcion    like '%perar%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cad1); 
        List<TipoEmpleado> tipos = context.performQuery(query);
        for (int i = 0; i<tipos.size();i++){
            tipoemp = tipos.get(i).getIdtipoempleado();
        }
        
        String cadena = "select * from empleado "
                + " where idtipoempleado <> "+tipoemp+""
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> tipos2 = context.performQuery(query2);
        
        return tipos2;
    }
    
    public List <Empleado> buscarOperario(String dni,String nombre, String apepat, String sexo){
        
        Estado est = new Estado();
        int tipoemp=0 ;
        ObjectContext context = DataContext.createDataContext();
        
        String cad1 = "select * from TIPO_EMPLEADO "
                + " where descripcion    like '%perar%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cad1); 
        List<TipoEmpleado> tipos = context.performQuery(query);
        for (int i = 0; i<tipos.size();i++){
            tipoemp = tipos.get(i).getIdtipoempleado();
        }
        
        String cadena = "select * from empleado "
                + " where nombres like '%"+nombre+"%' "
                + " and apellidop like '%"+apepat+"%' "
                + " and sexo like '%"+sexo+"%' "
                + " and idtipoempleado = "+tipoemp+""
                + " and dni like '%"+dni+"%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> operarios = context.performQuery(query2);
        
        return operarios;
    }
    public List <Empleado> buscarOperario(String dni,String nombre, String apepat){
        
        Estado est = new Estado();
        int tipoemp=0 ;
        ObjectContext context = DataContext.createDataContext();
        
        String cad1 = "select * from TIPO_EMPLEADO "
                + " where descripcion    like '%perar%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cad1); 
        List<TipoEmpleado> tipos = context.performQuery(query);
        for (int i = 0; i<tipos.size();i++){
            tipoemp = tipos.get(i).getIdtipoempleado();
        }
        
        String cadena = "select * from empleado "
                + " where nombres like '%"+nombre+"%' "
                + " and apellidop like '%"+apepat+"%' "
                + " and idtipoempleado = "+tipoemp+""
                + " and dni like '%"+dni+"%' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> operarios = context.performQuery(query2);
        
        return operarios;
    }
    public List <Empleado> buscarDNI(String dni){
        
        Estado est = new Estado();
        int tipoemp=0 ;
        ObjectContext context = DataContext.createDataContext();
     
        String cadena = "select * from empleado "
                + " where dni = '"+dni+"' "
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> operarios = context.performQuery(query2);
        
        return operarios;
    }
    public List <Empleado> buscarEmpleadoxTipo(int tipo){
        
        Estado est = new Estado();
        
        ObjectContext context = DataContext.createDataContext();
     
        String cadena = "select * from empleado "
                + " where idTipoEmpleado = "+tipo+""
                + " and estado <> "+est.getEliminado()+"";
        SQLTemplate query2 = new SQLTemplate(Empleado.class,cadena);         
        List<Empleado> operarios = context.performQuery(query2);
        
        return operarios;
    }
    public void eliminarEmpleado(int idemp){
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        
        //primero se elimina su usuario
        
        Expression q = ExpressionFactory.matchExp(Empleado.IDEMPLEADO_PK_COLUMN,idemp);
        SelectQuery s = new SelectQuery(Empleado.class,q);
        Empleado m = (Empleado)DataObjectUtils.objectForQuery(context, s);
        
        String cadena = "UPDATE USUARIO SET ESTADO ="+est.getEliminado()+""+ 
                        "WHERE IDEMPLEADO="+idemp+"";
        SQLTemplate query = new SQLTemplate(Usuario.class,cadena);
        context.performQuery(query);
        
        m.setEstado(est.getEliminado());
        
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaDelete(usuario, perfil, "Empleado");
    }
    
    public Empleado buscarPorId(ObjectContext c, int id) {
        
        Expression q = ExpressionFactory.matchExp(Empleado.IDEMPLEADO_PK_COLUMN, id);
        SelectQuery s = new SelectQuery(Empleado.class, q);
        return (Empleado) DataObjectUtils.objectForQuery(c, s);
    }
     
    public void  editarEmpleado (int idEmp, int temp, Date fecha, String nombre, String apepat, String apemat,  String tlf, String cel, String mail){
        
         ObjectContext context = DataContext.createDataContext();
        
        
        Empleado emp = this.buscarPorId(context, idEmp);     
        
        Expression qualifier = ExpressionFactory.matchExp(TipoEmpleado.IDTIPOEMPLEADO_PROPERTY,String.valueOf(temp));
        SelectQuery select = new SelectQuery(TipoEmpleado.class, qualifier);
        TipoEmpleado t= (TipoEmpleado) DataObjectUtils.objectForQuery(context, select);

        emp.setNombres(nombre);
        emp.setApellidom(apemat);
        emp.setApellidop(apepat);
        emp.setTelefono(tlf);
        emp.setEmail(mail);
        emp.setCelular(cel);
        emp.setFechanacimiento(fecha);
        emp.setToTipoEmpleado(t);
        
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Empleado");
    }
    // para operario
    public int idOperario() {
        Estado est = new Estado();
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM TIPO_EMPLEADO WHERE DESCRIPCION LIKE '%pera%' AND ESTADO <> "+est.getEliminado()+"";
        
        SQLTemplate query = new SQLTemplate(TipoEmpleado.class,cadena);
        TipoEmpleado lp =(TipoEmpleado) DataObjectUtils.objectForQuery(context, query);
        return lp.getIdtipoempleado();
        
    }
    public void  editarOperario (int idEmp,String estado, Date fecha, String nombre, String apepat, String apemat,  String tlf, String cel, String mail){
        
         ObjectContext context = DataContext.createDataContext();
        
        
        Empleado emp = this.buscarPorId(context, idEmp);     
        
        
        Estado est = new Estado();
        int idestado = est.getEstadoId(estado);
        emp.setNombres(nombre);
        emp.setApellidom(apemat);
        emp.setApellidop(apepat);
        emp.setTelefono(tlf);
        emp.setEmail(mail);
        emp.setCelular(cel);
        emp.setFechanacimiento(fecha);
        emp.setEstado(idestado);
        
        context.commitChanges();
         String usuario=LoginT.userLogin.getUsuario();
        String perfil=LoginT.perfilLogin.getDescripcion();
        AuditoriaService as = new AuditoriaService();
        as.auditoriaUpdate(usuario, perfil, "Empleado");
    }

    
      
}
