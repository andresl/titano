/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dp1.titandevelop.titano.service;

import dp1.titandevelop.titano.bean.Estado;
import dp1.titandevelop.titano.persistent.Demanda;
import dp1.titandevelop.titano.persistent.Empleado;
import dp1.titandevelop.titano.persistent.EmpleadoXProcesoTurno;
import dp1.titandevelop.titano.persistent.Proceso;
import dp1.titandevelop.titano.persistent.Turno;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.cayenne.DataObjectUtils;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.query.SQLTemplate;

/**
 *
 * @author natali
 */
public class EmpleadoXProcesoTurnoService {
      
      
    public static ArrayList<EmpleadoXProcesoTurno> buscarPorIdProceso(int idProceso){
       Estado estado=new Estado();  
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE "
                + " IDPROCESO="+idProceso+" AND ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        
        ArrayList<EmpleadoXProcesoTurno> produccion =(ArrayList<EmpleadoXProcesoTurno>) context.performQuery(query);       
       
        return produccion ;
    
    }  

  public static ArrayList<EmpleadoXProcesoTurno>  buscarPorIdEmpleado(int idEmpleado) {
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE "
                + " IDEMPLEADO="+idEmpleado +" AND ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        
        ArrayList<EmpleadoXProcesoTurno> produccion =(ArrayList<EmpleadoXProcesoTurno>)context.performQuery(query);       
       
        return produccion ;
    }
  public  ArrayList<EmpleadoXProcesoTurno>  buscarPorIdEmp(int idEmpleado) {
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE "
                + " IDEMPLEADO="+idEmpleado +" AND ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        
        ArrayList<EmpleadoXProcesoTurno> produccion =(ArrayList<EmpleadoXProcesoTurno>)context.performQuery(query);       
       
        return produccion ;
    }
    
  public static ArrayList<EmpleadoXProcesoTurno> buscarPorIdEmpleadoTurno(int idEmpleado, int turno) {
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE "
                + " IDEMPLEADO="+idEmpleado 
                + " AND IDTURNO="+turno
                +" AND ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        
        ArrayList<EmpleadoXProcesoTurno> produccion =(ArrayList<EmpleadoXProcesoTurno>) context.performQuery(query);       
       
        return produccion ;
    }
  
  public EmpleadoXProcesoTurno buscarUnEmpleadoXProceso(int idEmpleado,int turno,int idProceso){
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE "
                + " IDEMPLEADO="+idEmpleado 
                + " AND IDTURNO="+turno
                + " AND IDPROCESO="+idProceso
                +" AND ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        EmpleadoXProcesoTurno lp =(EmpleadoXProcesoTurno) DataObjectUtils.objectForQuery(context, query);
        
       
        return lp ;
  
  }
  
  public  ArrayList<EmpleadoXProcesoTurno> buscarAll() {
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE " 
                +" ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        
        ArrayList<EmpleadoXProcesoTurno> produccion =(ArrayList<EmpleadoXProcesoTurno>) context.performQuery(query);       
       
        return produccion ;
    }

  
  public float eficienciaXTurno(int idEmpleado,int idTurno) {
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE "
                + " IDEMPLEADO="+idEmpleado 
                + " AND IDTURNO="+idTurno
                +" AND ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        ArrayList<EmpleadoXProcesoTurno> produccion =(ArrayList<EmpleadoXProcesoTurno>) context.performQuery(query);       
        
        float total=0;
        float eficienciaParcial=0;
        float eficiencia=0;
                
        
        for(int i=0;i<produccion.size();i++){
            total=produccion.get(i).getProductividad()+produccion.get(i).getRotura();
            eficienciaParcial+=produccion.get(i).getProductividad()/total;
        }
        
        eficiencia=eficienciaParcial/produccion.size();
       
      
        return eficiencia; 
    }
  
  public void insertar(int idEmpleado, int idTurno, int idProceso, float productividad, float rotura)
  {
       ObjectContext c = DataContext.createDataContext();
      TurnoService st= new TurnoService();
      ProcesoService sp = new ProcesoService();
      EmpleadoService se= new EmpleadoService();
      Estado estado=new Estado();    
      Turno t= st.buscarPorId(idTurno, c);
      Empleado e= se.buscarPorId(c, idEmpleado);
      Proceso p =sp.buscarPorId(c, idProceso);
      EmpleadoXProcesoTurno ept = c.newObject(EmpleadoXProcesoTurno.class);
      ept.setEstado(estado.getActivo());
      ept.setFo(0.F);
      ept.setProductividad(productividad);
      ept.setRotura(rotura);
      ept.setToEmpleado(e);
      ept.setToProceso(p);
      ept.setToTurno(t);
      c.commitChanges();
}

    public ArrayList<EmpleadoXProcesoTurno> buscarUnEmpleadoXProceso(int idempleado, int idproceso) {
        Estado estado=new Estado();
        
        ObjectContext context = DataContext.createDataContext();
        String cadena = "SELECT * FROM EMPLEADO_X_PROCESO_TURNO  WHERE "
                + " IDEMPLEADO="+idempleado 
                + " AND IDPROCESO="+idproceso
                +" AND ESTADO = "+estado.getActivo()+"";
        
        SQLTemplate query = new SQLTemplate(EmpleadoXProcesoTurno.class,cadena);
        ArrayList<EmpleadoXProcesoTurno> produccion =(ArrayList<EmpleadoXProcesoTurno>) context.performQuery(query);       
       
        return produccion;
    }
}
