package dp1.titandevelop.titano.persistent.auto;

import java.util.List;

import org.apache.cayenne.CayenneDataObject;

import dp1.titandevelop.titano.persistent.AlmacenXProducto;
import dp1.titandevelop.titano.persistent.Zona;

/**
 * Class _Almacen was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _Almacen extends CayenneDataObject {

    public static final String DESCRIPCION_PROPERTY = "descripcion";
    public static final String DIRECCION_PROPERTY = "direccion";
    public static final String ESTADO_PROPERTY = "estado";
    public static final String IDALMACEN_PROPERTY = "idalmacen";
    public static final String ALMACEN_XPRODUCTO_ARRAY_PROPERTY = "almacenXProductoArray";
    public static final String TO_ZONA_PROPERTY = "toZona";

    public static final String IDALMACEN_PK_COLUMN = "idalmacen";

    public void setDescripcion(String descripcion) {
        writeProperty("descripcion", descripcion);
    }
    public String getDescripcion() {
        return (String)readProperty("descripcion");
    }

    public void setDireccion(String direccion) {
        writeProperty("direccion", direccion);
    }
    public String getDireccion() {
        return (String)readProperty("direccion");
    }

    public void setEstado(Integer estado) {
        writeProperty("estado", estado);
    }
    public Integer getEstado() {
        return (Integer)readProperty("estado");
    }

    public void setIdalmacen(Integer idalmacen) {
        writeProperty("idalmacen", idalmacen);
    }
    public Integer getIdalmacen() {
        return (Integer)readProperty("idalmacen");
    }

    public void addToAlmacenXProductoArray(AlmacenXProducto obj) {
        addToManyTarget("almacenXProductoArray", obj, true);
    }
    public void removeFromAlmacenXProductoArray(AlmacenXProducto obj) {
        removeToManyTarget("almacenXProductoArray", obj, true);
    }
    @SuppressWarnings("unchecked")
    public List<AlmacenXProducto> getAlmacenXProductoArray() {
        return (List<AlmacenXProducto>)readProperty("almacenXProductoArray");
    }


    public void setToZona(Zona toZona) {
        setToOneTarget("toZona", toZona, true);
    }

    public Zona getToZona() {
        return (Zona)readProperty("toZona");
    }


}
