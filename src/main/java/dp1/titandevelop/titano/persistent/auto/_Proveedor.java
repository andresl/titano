package dp1.titandevelop.titano.persistent.auto;

import java.util.List;

import org.apache.cayenne.CayenneDataObject;

import dp1.titandevelop.titano.persistent.Cotizacion;
import dp1.titandevelop.titano.persistent.ProductoProveedor;

/**
 * Class _Proveedor was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _Proveedor extends CayenneDataObject {

    public static final String CALIFICACIONTIEMPOENTREGA_PROPERTY = "calificaciontiempoentrega";
    public static final String CELULARCONTACTO_PROPERTY = "celularcontacto";
    public static final String CONTACTO_PROPERTY = "contacto";
    public static final String DIRECCIONEMPRESA_PROPERTY = "direccionempresa";
    public static final String DNICONTACTO_PROPERTY = "dnicontacto";
    public static final String EMAILCONTACTO_PROPERTY = "emailcontacto";
    public static final String EMAILEMPRESA_PROPERTY = "emailempresa";
    public static final String ESTADO_PROPERTY = "estado";
    public static final String IDPROVEEDOR_PROPERTY = "idproveedor";
    public static final String MARCA_PROPERTY = "marca";
    public static final String RAZONSOCIAL_PROPERTY = "razonsocial";
    public static final String RUC_PROPERTY = "ruc";
    public static final String TELEFONOCONTACTO_PROPERTY = "telefonocontacto";
    public static final String COTIZACION_ARRAY_PROPERTY = "cotizacionArray";
    public static final String PRODUCTO_PROVEEDOR_ARRAY_PROPERTY = "productoProveedorArray";

    public static final String IDPROVEEDOR_PK_COLUMN = "idproveedor";

    public void setCalificaciontiempoentrega(Integer calificaciontiempoentrega) {
        writeProperty("calificaciontiempoentrega", calificaciontiempoentrega);
    }
    public Integer getCalificaciontiempoentrega() {
        return (Integer)readProperty("calificaciontiempoentrega");
    }

    public void setCelularcontacto(String celularcontacto) {
        writeProperty("celularcontacto", celularcontacto);
    }
    public String getCelularcontacto() {
        return (String)readProperty("celularcontacto");
    }

    public void setContacto(String contacto) {
        writeProperty("contacto", contacto);
    }
    public String getContacto() {
        return (String)readProperty("contacto");
    }

    public void setDireccionempresa(String direccionempresa) {
        writeProperty("direccionempresa", direccionempresa);
    }
    public String getDireccionempresa() {
        return (String)readProperty("direccionempresa");
    }

    public void setDnicontacto(String dnicontacto) {
        writeProperty("dnicontacto", dnicontacto);
    }
    public String getDnicontacto() {
        return (String)readProperty("dnicontacto");
    }

    public void setEmailcontacto(String emailcontacto) {
        writeProperty("emailcontacto", emailcontacto);
    }
    public String getEmailcontacto() {
        return (String)readProperty("emailcontacto");
    }

    public void setEmailempresa(String emailempresa) {
        writeProperty("emailempresa", emailempresa);
    }
    public String getEmailempresa() {
        return (String)readProperty("emailempresa");
    }

    public void setEstado(Integer estado) {
        writeProperty("estado", estado);
    }
    public Integer getEstado() {
        return (Integer)readProperty("estado");
    }

    public void setIdproveedor(Integer idproveedor) {
        writeProperty("idproveedor", idproveedor);
    }
    public Integer getIdproveedor() {
        return (Integer)readProperty("idproveedor");
    }

    public void setMarca(String marca) {
        writeProperty("marca", marca);
    }
    public String getMarca() {
        return (String)readProperty("marca");
    }

    public void setRazonsocial(String razonsocial) {
        writeProperty("razonsocial", razonsocial);
    }
    public String getRazonsocial() {
        return (String)readProperty("razonsocial");
    }

    public void setRuc(String ruc) {
        writeProperty("ruc", ruc);
    }
    public String getRuc() {
        return (String)readProperty("ruc");
    }

    public void setTelefonocontacto(String telefonocontacto) {
        writeProperty("telefonocontacto", telefonocontacto);
    }
    public String getTelefonocontacto() {
        return (String)readProperty("telefonocontacto");
    }

    public void addToCotizacionArray(Cotizacion obj) {
        addToManyTarget("cotizacionArray", obj, true);
    }
    public void removeFromCotizacionArray(Cotizacion obj) {
        removeToManyTarget("cotizacionArray", obj, true);
    }
    @SuppressWarnings("unchecked")
    public List<Cotizacion> getCotizacionArray() {
        return (List<Cotizacion>)readProperty("cotizacionArray");
    }


    public void addToProductoProveedorArray(ProductoProveedor obj) {
        addToManyTarget("productoProveedorArray", obj, true);
    }
    public void removeFromProductoProveedorArray(ProductoProveedor obj) {
        removeToManyTarget("productoProveedorArray", obj, true);
    }
    @SuppressWarnings("unchecked")
    public List<ProductoProveedor> getProductoProveedorArray() {
        return (List<ProductoProveedor>)readProperty("productoProveedorArray");
    }


}
