package dp1.titandevelop.titano.persistent.auto;

import org.apache.cayenne.CayenneDataObject;

import dp1.titandevelop.titano.persistent.Asignacion;
import dp1.titandevelop.titano.persistent.PadreProduccion;

/**
 * Class _Produccion was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _Produccion extends CayenneDataObject {

    public static final String BENEFICIO_PROPERTY = "beneficio";
    public static final String COSTO_PROPERTY = "costo";
    public static final String ESTADO_PROPERTY = "estado";
    public static final String IDPRODUCCION_PROPERTY = "idproduccion";
    public static final String PRODUCTIVIDAD_PROPERTY = "productividad";
    public static final String ROTURA_PROPERTY = "rotura";
    public static final String TO_ASIGNACION_PROPERTY = "toAsignacion";
    public static final String TO_PADRE_PRODUCCION_PROPERTY = "toPadreProduccion";

    public static final String IDPRODUCCION_PK_COLUMN = "idproduccion";

    public void setBeneficio(Float beneficio) {
        writeProperty("beneficio", beneficio);
    }
    public Float getBeneficio() {
        return (Float)readProperty("beneficio");
    }

    public void setCosto(Float costo) {
        writeProperty("costo", costo);
    }
    public Float getCosto() {
        return (Float)readProperty("costo");
    }

    public void setEstado(Integer estado) {
        writeProperty("estado", estado);
    }
    public Integer getEstado() {
        return (Integer)readProperty("estado");
    }

    public void setIdproduccion(Integer idproduccion) {
        writeProperty("idproduccion", idproduccion);
    }
    public Integer getIdproduccion() {
        return (Integer)readProperty("idproduccion");
    }

    public void setProductividad(Float productividad) {
        writeProperty("productividad", productividad);
    }
    public Float getProductividad() {
        return (Float)readProperty("productividad");
    }

    public void setRotura(Float rotura) {
        writeProperty("rotura", rotura);
    }
    public Float getRotura() {
        return (Float)readProperty("rotura");
    }

    public void setToAsignacion(Asignacion toAsignacion) {
        setToOneTarget("toAsignacion", toAsignacion, true);
    }

    public Asignacion getToAsignacion() {
        return (Asignacion)readProperty("toAsignacion");
    }


    public void setToPadreProduccion(PadreProduccion toPadreProduccion) {
        setToOneTarget("toPadreProduccion", toPadreProduccion, true);
    }

    public PadreProduccion getToPadreProduccion() {
        return (PadreProduccion)readProperty("toPadreProduccion");
    }


}
