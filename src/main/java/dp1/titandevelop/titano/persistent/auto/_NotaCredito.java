package dp1.titandevelop.titano.persistent.auto;

import java.util.Date;
import java.util.List;

import org.apache.cayenne.CayenneDataObject;

import dp1.titandevelop.titano.persistent.DetalleNotaCredito;
import dp1.titandevelop.titano.persistent.DocumentoVenta;

/**
 * Class _NotaCredito was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _NotaCredito extends CayenneDataObject {

    public static final String DESCRIPCION_PROPERTY = "descripcion";
    public static final String ESTADO_PROPERTY = "estado";
    public static final String FECHA_PROPERTY = "fecha";
    public static final String IDNOTACREDITO_PROPERTY = "idnotacredito";
    public static final String MONTO_PROPERTY = "monto";
    public static final String TIPO_PROPERTY = "tipo";
    public static final String DETALLE_NOTA_CREDITO_ARRAY_PROPERTY = "detalleNotaCreditoArray";
    public static final String TO_DOCUMENTO_VENTA_PROPERTY = "toDocumentoVenta";

    public static final String IDNOTACREDITO_PK_COLUMN = "idnotacredito";

    public void setDescripcion(String descripcion) {
        writeProperty("descripcion", descripcion);
    }
    public String getDescripcion() {
        return (String)readProperty("descripcion");
    }

    public void setEstado(Integer estado) {
        writeProperty("estado", estado);
    }
    public Integer getEstado() {
        return (Integer)readProperty("estado");
    }

    public void setFecha(Date fecha) {
        writeProperty("fecha", fecha);
    }
    public Date getFecha() {
        return (Date)readProperty("fecha");
    }

    public void setIdnotacredito(Integer idnotacredito) {
        writeProperty("idnotacredito", idnotacredito);
    }
    public Integer getIdnotacredito() {
        return (Integer)readProperty("idnotacredito");
    }

    public void setMonto(Float monto) {
        writeProperty("monto", monto);
    }
    public Float getMonto() {
        return (Float)readProperty("monto");
    }

    public void setTipo(Integer tipo) {
        writeProperty("tipo", tipo);
    }
    public Integer getTipo() {
        return (Integer)readProperty("tipo");
    }

    public void addToDetalleNotaCreditoArray(DetalleNotaCredito obj) {
        addToManyTarget("detalleNotaCreditoArray", obj, true);
    }
    public void removeFromDetalleNotaCreditoArray(DetalleNotaCredito obj) {
        removeToManyTarget("detalleNotaCreditoArray", obj, true);
    }
    @SuppressWarnings("unchecked")
    public List<DetalleNotaCredito> getDetalleNotaCreditoArray() {
        return (List<DetalleNotaCredito>)readProperty("detalleNotaCreditoArray");
    }


    public void setToDocumentoVenta(DocumentoVenta toDocumentoVenta) {
        setToOneTarget("toDocumentoVenta", toDocumentoVenta, true);
    }

    public DocumentoVenta getToDocumentoVenta() {
        return (DocumentoVenta)readProperty("toDocumentoVenta");
    }


}
