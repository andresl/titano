package dp1.titandevelop.titano.persistent.auto;

import org.apache.cayenne.CayenneDataObject;

import dp1.titandevelop.titano.persistent.Usuario;

/**
 * Class _HistorialClave was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _HistorialClave extends CayenneDataObject {

    public static final String IDHISTORIALCLAVE_PROPERTY = "idhistorialclave";
    public static final String PASSWORD_PROPERTY = "password";
    public static final String TO_USUARIO_PROPERTY = "toUsuario";

    public static final String IDHISTORIALCLAVE_PK_COLUMN = "idhistorialclave";

    public void setIdhistorialclave(Integer idhistorialclave) {
        writeProperty("idhistorialclave", idhistorialclave);
    }
    public Integer getIdhistorialclave() {
        return (Integer)readProperty("idhistorialclave");
    }

    public void setPassword(String password) {
        writeProperty("password", password);
    }
    public String getPassword() {
        return (String)readProperty("password");
    }

    public void setToUsuario(Usuario toUsuario) {
        setToOneTarget("toUsuario", toUsuario, true);
    }

    public Usuario getToUsuario() {
        return (Usuario)readProperty("toUsuario");
    }


}
