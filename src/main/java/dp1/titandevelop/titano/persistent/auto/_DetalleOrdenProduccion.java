package dp1.titandevelop.titano.persistent.auto;

import org.apache.cayenne.CayenneDataObject;

import dp1.titandevelop.titano.persistent.OrdenProduccion;
import dp1.titandevelop.titano.persistent.Producto;

/**
 * Class _DetalleOrdenProduccion was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _DetalleOrdenProduccion extends CayenneDataObject {

    public static final String CANTIDAD_PROPERTY = "cantidad";
    public static final String ESTADO_PROPERTY = "estado";
    public static final String IDDETALLEORDENPRODUCCION_PROPERTY = "iddetalleordenproduccion";
    public static final String TO_ORDEN_PRODUCCION_PROPERTY = "toOrdenProduccion";
    public static final String TO_PRODUCTO_PROPERTY = "toProducto";

    public static final String IDDETALLEORDENPRODUCCION_PK_COLUMN = "iddetalleordenproduccion";

    public void setCantidad(Integer cantidad) {
        writeProperty("cantidad", cantidad);
    }
    public Integer getCantidad() {
        return (Integer)readProperty("cantidad");
    }

    public void setEstado(Integer estado) {
        writeProperty("estado", estado);
    }
    public Integer getEstado() {
        return (Integer)readProperty("estado");
    }

    public void setIddetalleordenproduccion(Integer iddetalleordenproduccion) {
        writeProperty("iddetalleordenproduccion", iddetalleordenproduccion);
    }
    public Integer getIddetalleordenproduccion() {
        return (Integer)readProperty("iddetalleordenproduccion");
    }

    public void setToOrdenProduccion(OrdenProduccion toOrdenProduccion) {
        setToOneTarget("toOrdenProduccion", toOrdenProduccion, true);
    }

    public OrdenProduccion getToOrdenProduccion() {
        return (OrdenProduccion)readProperty("toOrdenProduccion");
    }


    public void setToProducto(Producto toProducto) {
        setToOneTarget("toProducto", toProducto, true);
    }

    public Producto getToProducto() {
        return (Producto)readProperty("toProducto");
    }


}
