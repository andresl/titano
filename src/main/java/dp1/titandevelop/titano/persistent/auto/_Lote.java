package dp1.titandevelop.titano.persistent.auto;

import java.util.Date;
import java.util.List;

import org.apache.cayenne.CayenneDataObject;

import dp1.titandevelop.titano.persistent.Kardex;

/**
 * Class _Lote was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _Lote extends CayenneDataObject {

    public static final String ESTADO_PROPERTY = "estado";
    public static final String FECHAINGRESO_PROPERTY = "fechaingreso";
    public static final String FECHAVENCIMIENTO_PROPERTY = "fechavencimiento";
    public static final String IDLOTE_PROPERTY = "idlote";
    public static final String KARDEX_ARRAY_PROPERTY = "kardexArray";

    public static final String IDLOTE_PK_COLUMN = "idlote";

    public void setEstado(Integer estado) {
        writeProperty("estado", estado);
    }
    public Integer getEstado() {
        return (Integer)readProperty("estado");
    }

    public void setFechaingreso(Date fechaingreso) {
        writeProperty("fechaingreso", fechaingreso);
    }
    public Date getFechaingreso() {
        return (Date)readProperty("fechaingreso");
    }

    public void setFechavencimiento(Date fechavencimiento) {
        writeProperty("fechavencimiento", fechavencimiento);
    }
    public Date getFechavencimiento() {
        return (Date)readProperty("fechavencimiento");
    }

    public void setIdlote(Integer idlote) {
        writeProperty("idlote", idlote);
    }
    public Integer getIdlote() {
        return (Integer)readProperty("idlote");
    }

    public void addToKardexArray(Kardex obj) {
        addToManyTarget("kardexArray", obj, true);
    }
    public void removeFromKardexArray(Kardex obj) {
        removeToManyTarget("kardexArray", obj, true);
    }
    @SuppressWarnings("unchecked")
    public List<Kardex> getKardexArray() {
        return (List<Kardex>)readProperty("kardexArray");
    }


}
