package dp1.titandevelop.titano.persistent;

import dp1.titandevelop.titano.persistent.auto._RequerimientoXProducto;

public class RequerimientoXProducto extends _RequerimientoXProducto {

    private Boolean seleccionado;

    /**
     * @return the seleccionado
     */
    public Boolean isSeleccionado() {
        if ( this.seleccionado == null ) {
            this.seleccionado = false;
        }
        return seleccionado;
    }

    /**
     * @param seleccionado the seleccionado to set
     */
    public void setSeleccionado(Boolean seleccionado) {
        this.seleccionado = seleccionado;
    }
}
