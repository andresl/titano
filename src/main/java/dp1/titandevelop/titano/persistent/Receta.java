package dp1.titandevelop.titano.persistent;

import dp1.titandevelop.titano.persistent.auto._Receta;

public class Receta extends _Receta {
    private Boolean seleccionado;

    /**
     * @return the seleccionado
     */
    public Boolean isSeleccionado() {
        return seleccionado;
    }

    /**
     * @param seleccionado the seleccionado to set
     */
    public void setSeleccionado(Boolean seleccionado) {
        this.seleccionado = seleccionado;
    }
    

}
