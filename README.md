# README #

Este software es el trabajo que realizamos para el curso de Desarrollo de Programas 1 en la PUCP. 

### Resumen ###

El software realiza un algoritmo grasp que es luego mejorado mediante una búsqueda Tabú. Lo acompaña una interfaz gráfica escrita en Java con la librería gráfica Swing. Como ORM estamos utilizando el proyecto Cayenne, parte de Apache Foundation.

### ¿Cómo configurar? ###
El proyecto requiere Java Virtual Machine 7 y una base de datos PostgreSQL. Necesita cargar en la base da datos los contenidos del archivo **schema.sql**. Hecho esto puede proceder a ejecutar la aplicación.